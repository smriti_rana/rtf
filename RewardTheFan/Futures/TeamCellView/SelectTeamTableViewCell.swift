//
//  SelectTeamTableViewCell.swift
//  RewardTheFan
//
//  Created by 42Works-Worksys2 on 13/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class SelectTeamTableViewCell: UITableViewCell {
    @IBOutlet weak var seeAllLbl: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var lineViewBottom: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
