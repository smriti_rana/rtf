//
//  SFSummaryViewController.swift
//  RewardTheFan
//
//  Created by 42Works-Worksys2 on 14/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class SFSummaryViewController: BaseViewController {

    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var labelHeader: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomLabel.text = "Guarantees entry to SuperBowl game, if\nDenver Broncos reach the finals"
    }

    //MARK:- Back Action
    @IBAction func backAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBOutlet weak var teamSubmitBtn: UIButton!
    @IBAction func teamSubmitAction(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SFRewardID") as! SFRewardViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
