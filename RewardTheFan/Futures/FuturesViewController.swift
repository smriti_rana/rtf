//
//  FuturesViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 10/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class FuturesViewController: UIViewController {

    @IBOutlet var futuresLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            futuresLabel.font = UIFont(name: futuresLabel.font.fontName, size: 32)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backButtonTapped(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func infoPageButtonTapped(sender: AnyObject) {
    }
    
    @IBAction func productDescriptionButtonTapped(sender: AnyObject) {
    }
    
    @IBAction func proceedButtonTapped(sender: AnyObject) {
        goForSelection()
    }
    
    func goForSelection() {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LeagueID") as! LeagueViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
