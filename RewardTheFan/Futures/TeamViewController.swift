//
//  TeamViewController.swift
//  RewardTheFan
//
//  Created by 42Works-Worksys2 on 13/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class TeamViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var labelHeader: UILabel!
    
    var cellArray = ["Denver Broncos", "Denver Broncos", "Denver Broncos", "Denver Broncos", "Denver Broncos"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "HeaderTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell2")
        tableView.registerNib(UINib(nibName: "SelectTeamTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell1")
        tableView.hidden = false
        tableView.estimatedRowHeight = 72.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(animated: Bool) {
      
    }
    
    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell1") as! SelectTeamTableViewCell
        cell1.selectionStyle = .None
        cell1.separatorInset = UIEdgeInsetsZero
        cell1.seeAllLbl?.text = cellArray[indexPath.row]
        cell1.seeAllLbl?.backgroundColor = UIColor.clearColor()
        cell1.seeAllLbl?.textColor = ColorConstant.cellFooterCellBlueColor

        cell1.lineViewBottom.hidden = true
        if indexPath.row % 2 != 0 {
            cell1.contentView.backgroundColor = ColorConstant.cellGrayColor
        } else {
            cell1.contentView.backgroundColor = UIColor.whiteColor()
        }
        
        return cell1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 65
        }
        return 50
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "NFL Teams"
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  headerCell =  tableView.dequeueReusableCellWithIdentifier("Cell2") as! HeaderTableViewCell
        headerCell.seeAllLbl.text = "NFL Teams"
        headerCell.seeAllLbl.textColor = ColorConstant.cellFooterCellBlueColor
        headerCell.pointsLabel.text = "2 Tickets"
        headerCell.lineViewBottom.hidden = false
        headerCell.textLabel?.backgroundColor = UIColor.clearColor()
        headerCell.contentView.backgroundColor = UIColor.whiteColor()
        return headerCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 72
        }
        return 55
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    //MARK:- Back Action
    @IBAction func backAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func getLeagueApi(){
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        guard IJReachability.isConnectedToNetwork() else {
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        let getParam = [
            "productType" : "",
            "customerId":"",
            "configId":""
        ]
        print(getParam)
        self.stopIndicator()
        
        WebServiceCall.sharedInstance.GetPastEventsCustomerOrders("", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            self.stopIndicator()
            
            if isTrue == true {
                self.tableView.hidden = false
                print("dict IS:",responseDisctionary)
                if let resCustomerorderArray = responseDisctionary!.valueForKey("customerOrders") as? NSArray  {
                    print(resCustomerorderArray)
                    //self.responseCustomerOrderArray = resCustomerorderArray
                    
                }
                self.tableView.reloadData()
                
            } else {
                
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    @IBOutlet weak var teamSubmitBtn: UIButton!
    @IBAction func teamSubmitAction(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SFSummaryID") as! SFSummaryViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
