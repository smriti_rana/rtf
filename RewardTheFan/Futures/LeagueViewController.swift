//
//  LeagueViewController.swift
//  RewardTheFan
//
//  Created by 42Works-Worksys2 on 13/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class LeagueViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var labelHeader: UILabel!
    var headerArray = ["Sports", "Others"]
    var cellArray = ["Super Bowl", "NFL", "MLB", "NHL", "NBA"]
    var cellArray1 = ["No Selections Available"]
    //var countArray = ["\("cellArray.count)", 1]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: "SeeAllFooterTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell1")
        tableView.hidden = false
        tableView.estimatedRowHeight = 72.0
        tableView.backgroundColor = ColorConstant.cellBKGrayColor
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return headerArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1 {
            return 1
        }
        return cellArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell1") as! SeeAllFooterTableViewCell
        cell1.selectionStyle = .None
        cell1.separatorInset = UIEdgeInsetsZero
        cell1.seeAllBtn.userInteractionEnabled = false
        cell1.lineViewBk.hidden = true
        cell1.seeAllLbl.hidden =  true
        if indexPath.section == 1 {
            cell1.textLabel?.text = cellArray1[indexPath.row]
        } else {
            cell1.textLabel?.text = cellArray[indexPath.row]
        }
        cell1.textLabel?.backgroundColor = UIColor.clearColor()
        cell1.textLabel?.textColor = ColorConstant.cellFooterCellBlueColor
        if indexPath.row % 2 != 0 {
            cell1.contentView.backgroundColor = ColorConstant.cellGrayColor
        } else {
            cell1.contentView.backgroundColor = UIColor.whiteColor()
        }
        
        return cell1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 65
        }
        return 50
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headerArray[section]
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  headerCell =  tableView.dequeueReusableCellWithIdentifier("Cell1") as! SeeAllFooterTableViewCell
        headerCell.lineViewBk.hidden = true
        headerCell.seeAllLbl.text = headerArray[section]
        headerCell.seeAllBtn.hidden =  true
        headerCell.seeAllLbl.textColor = ColorConstant.cellFooterCellBlueColor
        headerCell.textLabel?.backgroundColor = UIColor.clearColor()
        headerCell.contentView.backgroundColor =  ColorConstant.cellBKGrayColor
        return headerCell
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 70
        }
        return 55
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("TeamID") as! TeamViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Back Action
    @IBAction func backAction(sender: AnyObject) {
         self.navigationController?.popViewControllerAnimated(true)
    }
    
    func getLeagueApi(){
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        guard IJReachability.isConnectedToNetwork() else {
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        let getParam = [
            "productType" : "",
            "customerId":"",
            "configId":""
        ]
        print(getParam)
        self.stopIndicator()
        
        WebServiceCall.sharedInstance.GetPastEventsCustomerOrders("", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            self.stopIndicator()
            
            if isTrue == true {
                self.tableView.hidden = false
                print("dict IS:",responseDisctionary)
                if let resCustomerorderArray = responseDisctionary!.valueForKey("customerOrders") as? NSArray  {
                    print(resCustomerorderArray)
                    //self.responseCustomerOrderArray = resCustomerorderArray
                    
                }
                self.tableView.reloadData()
                
            } else {
                
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
