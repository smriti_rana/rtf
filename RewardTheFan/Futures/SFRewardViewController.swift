//
//  SFRewardViewController.swift
//  RewardTheFan
//
//  Created by 42Works-Worksys2 on 14/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class SFRewardViewController: UIViewController {

    @IBOutlet weak var subHeaderLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var labelHeader: UILabel!
    
    @IBOutlet weak var circlePriceHeaderLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        subHeaderLabel.text = "You have just redeemed 2 tickets for Denver\nBroncos seated together."
        bottomLabel.text = "If Denver Broncos enters the finals of SuperBowls we will give you the set of tickets.\n\nIf Denver Broncos do not reach the final these tickets would be voided."
    }

    //MARK:- Back Action
    @IBAction func backAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
