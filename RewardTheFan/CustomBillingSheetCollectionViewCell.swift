//
//  CustomBillingSheetCollectionViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 27/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
@objc protocol billingSheetCollectionViewCellDelegate {
    
    optional func moreUpdateShippingAddress(getIndex:Int)
    optional func addShippingAddress(getIndex:Int)
    
    
}
class CustomBillingSheetCollectionViewCell: UICollectionViewCell {
    weak var delegate: billingSheetCollectionViewCellDelegate?
    @IBOutlet weak var country_zipcodelbl: UILabel!
    @IBOutlet weak var city_stateLbl: UILabel!
    @IBOutlet weak var Add_StreetLbl: UILabel!
    @IBOutlet weak var firstNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    //MARK:-MoreTapped
    @IBOutlet weak var moreBtn: UIButton!
    @IBAction func moreTapped(sender: AnyObject) {
        self.delegate!.moreUpdateShippingAddress!(moreBtn.tag)
        
        
    }
    //MARK:-AddInfoView
    @IBOutlet weak var addInfoBtn: UIButton!
    @IBAction func AddInFotoViewTapped(sender: AnyObject) {
        self.delegate!.addShippingAddress!(addInfoBtn.tag)
        
    }
}
