//
//  ReferfriendViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 09/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import Social
import APAddressBook
 import MessageUI
class ReferfriendViewController: BaseViewController,MFMailComposeViewControllerDelegate {
    var adbk : ABAddressBook!
    let addressBook = APAddressBook()
    var phoneNumberArray:NSMutableArray = []
    var nameArray:NSMutableArray = []
    var getUserName = ""
      let addressBookRef: ABAddressBookRef = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
    override func viewDidLoad() {
        super.viewDidLoad()

        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            titleLbl.font = UIFont(name: titleLbl.font.fontName, size: 32)
            
        }
    }

    @IBOutlet weak var fetchContactBtn: UIButton!
    @IBAction func fetchContactTapped(sender: AnyObject) {
        promptForAddressBookRequestAccess(fetchContactBtn)

    }
    @IBOutlet weak var fetchGmailBtn: UIButton!
    @IBAction func fetchGmailContactTapped(sender: AnyObject) {
    }
    
  
    @IBOutlet weak var referFriendTf: UITextField!
    @IBOutlet weak var referCodeLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBAction func Backtapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-ShareFacebookTapped
    @IBOutlet weak var shareFbBtn: UIButton!
    @IBAction func shareViaFbTapped(sender: AnyObject) {
        
        let linkcontent: FBSDKShareLinkContent = FBSDKShareLinkContent()
        linkcontent.contentDescription = "testing"
        linkcontent.contentTitle = "\(AppName)"
        let dialog = FBSDKShareDialog()
        dialog.fromViewController = self;
        dialog.shareContent = linkcontent
        dialog.mode = FBSDKShareDialogMode.Automatic;
        dialog.show()
        
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

  
    // MARK: - private
    
    func loadContacts() {
        
        self.addressBook.loadContacts({
            (contacts: [APContact]?, error: NSError?) in
         
            if let unwrappedContacts = contacts {
                print(unwrappedContacts)
               
            } else if let unwrappedError = error {
                let alert = UIAlertView(title: "Error", message: unwrappedError.localizedDescription,
                    delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        })
    }
    func promptForAddressBookRequestAccess(petButton: UIButton) {
        let err: Unmanaged<CFError>? = nil
        
        ABAddressBookRequestAccessWithCompletion(addressBookRef) {
            (granted: Bool, error: CFError!) in
            dispatch_async(dispatch_get_main_queue()) {
                if !granted {
                    print("Just denied")
                } else {
                    print("Just authorized")
                    if !self.determineStatus() {
                        print("not authorized")
                        return
                    }
                    let people = ABAddressBookCopyArrayOfAllPeople(self.adbk).takeRetainedValue() as NSArray as [ABRecord]
                    print(people)
                        for person in people {
                        if  let name:String = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String {
                                print(name)
                            self.nameArray.addObject(name)
//                        print(ABRecordCopyCompositeName(person).takeRetainedValue())
                        let numbers:ABMultiValue = ABRecordCopyValue(person, kABPersonPhoneProperty).takeRetainedValue()
                        
                        if (ABMultiValueGetCount(numbers) > 0) {  //This condition is optional because using '?' before calling takeRetainedValue
                        if let number:String = ABMultiValueCopyValueAtIndex(numbers,0)?.takeRetainedValue() as? String {
                        print("number = \(number)");
                        
                        self.phoneNumberArray.addObject(number)
                    }
                    }
                }
            }
            print("Phone dict as",self.nameArray)
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("contactList") as! ContactFriendListViewController
                print(self.nameArray)
            vc.phoneArrayAs = self.phoneNumberArray
            vc.nameArrayAs = self.nameArray
            self.navigationController?.pushViewController(vc, animated: true)
                    
        }
    }
    }
    }
//    func displayCantAddContactAlert() {
//        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
//                                                    message: "You must give the app permission to add the contact first.",
//                                                    preferredStyle: .Alert)
//        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
//            style: .Default,
//            handler: { action in
////                self.openSettings()
//        }))
//        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
//        presentViewController(cantAddContactAlert, animated: true, completion: nil)
//    }
    func createAddressBook() -> Bool {
        if self.adbk != nil {
            return true
        }
        var err : Unmanaged<CFError>? = nil
        let adbk : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &err).takeRetainedValue()
        if adbk == nil {
            print(err)
            self.adbk = nil
            return false
        }
        self.adbk = adbk
        return true
    }
    
    func determineStatus() -> Bool {
        let status = ABAddressBookGetAuthorizationStatus()
        switch status {
        case .Authorized:
            return self.createAddressBook()
        case .NotDetermined:
            var ok = false
            ABAddressBookRequestAccessWithCompletion(nil) {
                (granted:Bool, err:CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if granted {
                        ok = self.createAddressBook()
                    }
                }
            }
            if ok == true {
                return true
            }
            self.adbk = nil
            return false
        case .Restricted:
            self.adbk = nil
            return false
        case .Denied:
            self.adbk = nil
            return false
        }
    }
 
    //MARK:-Send EmailTapped
    @IBOutlet weak var sendBtn: UIButton!
    @IBAction func sendTapped(sender: AnyObject) {
        referFriendTf.resignFirstResponder()
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for validations
        if referFriendTf.text == "" {
            
            alertVc.message = emailAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if isValidEmail(referFriendTf.text!) == false {
            
            alertVc.message = validEmailAlertmsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        else
        {

        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(referFriendTf.text!)"])
            if NSUserDefaults.standardUserDefaults().valueForKey("customerName") != nil {
              getUserName = NSUserDefaults.standardUserDefaults().valueForKey("customerName") as! String
                    mail.setMessageBody("Hey, Use the below code while signing up \(referCodeLbl.text!)", isHTML: true)

            mail.setSubject("Your friend \(getUserName) has sent you the Referral Code for Reward The Fan")
            presentViewController(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
        
    }
        }
}
}
