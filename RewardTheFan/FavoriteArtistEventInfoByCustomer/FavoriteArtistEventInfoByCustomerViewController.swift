//
//  FavoriteArtistEventInfoByCustomerViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 20/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class FavoriteArtistEventInfoByCustomerViewController: BaseViewController {
    
    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var baseTableView: UITableView!
    
    var managedFavourites : NSMutableArray = []
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // *** register table view cell ***
        baseTableView.registerNib(UINib(nibName: "NavigationItemsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")
        
        // *** api call *** //
        self.favoriteArtistEventInfoByCustomerApiCall()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Table view delegate and datasource methods
    //MARK:-
    
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return managedFavourites.count
//    }
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        
//        if let array : NSMutableArray = self.managedFavourites.objectAtIndex(section) as? NSMutableArray {
//            return array.count
//        }
//        return 0
//    }
//    
//    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//        return false
//    }
//    
//    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        
//        return "Hello"
//    }
//    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        
//        let  headerCell =  tableView.dequeueReusableCellWithIdentifier("Cell1") as! SeeAllFooterTableViewCell
//        if section == 0 {
//            headerCell.seeAllLbl.text = "ARTISTS"
//            headerCell.seeAllBtn.hidden =  true
//            
//        } else if section == 1 {
//            headerCell.seeAllLbl.text = "VENUES"
//            headerCell.seeAllBtn.hidden =  true
//        }
//        headerCell.seeAllLbl.textColor = UIColor.whiteColor()
//        headerCell.contentView.backgroundColor = UIColor.init(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
//        return headerCell
//    }
//    
//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        
//        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
//            return 60.0
//        }
//        return 40.0
//    }
//    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        
//        return CGFloat.min
//    }
//    
//    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        // *** changes for iPad ***
//        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
//            return 84
//        } else {
//            return 56
//        }
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        
//        if indexPath.section == 0 {
//            
//            let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! SearchArtistTableViewCell
//            cell.separatorInset = UIEdgeInsetsZero
//            
//            // *** change color of cell *** //
//            
//            if indexPath.row % 2 != 0 {
//                cell.contentView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
//            } else {
//                cell.contentView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
//            }
//            
//            if artistArrayIS.count > 0{
//                
//                cell.favicon_Width.constant = 24
//                
//                if let artistName = artistArrayIS[indexPath.row].valueForKey("artistName") as? String {
//                    cell.artistLbl.text = artistName
//                }
//                
//                if let favorite =  self.artistArrayIS.objectAtIndex(indexPath.row).valueForKey("isFavoriteFlag") as? Bool {
//                    
//                    if favorite == true {
//                        cell.fav_imgView.image = UIImage(named: "heartRed")
//                    } else {
//                        cell.fav_imgView.image = UIImage(named: "heartWhite")
//                    }
//                }
//            }
//            
//            cell.artistBtn.tag = indexPath.row
//            cell.delegate = self
//            return cell
//            
//        } else {
//            
//            let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell2") as! HomeEventsTableViewCell
//            cell1.delegate = self
//            
//            cell1.cardNameHeightConstraint.constant = 0
//            cell1.topViewHeightConstraint.constant = 0
//            cell1.bottomViewHeightConstraint.constant = 0
//            
//            cell1.shareButton.tag = indexPath.row
//            cell1.favouritesButton.tag = indexPath.row
//            mySubview.shareBtn.tag = indexPath.row
//            mySubview.TwitterBtn.tag = indexPath.row
//            mySubview.shareEmailBtn.tag = indexPath.row
//            cell1.cardNameHeightConstraint.constant = 0
//            cell1.selectionStyle = .None
//            
//            if eventArrayIS.count > 0{
//                
//                if let eventName = eventArrayIS[indexPath.row].valueForKey("artistName") as? String{
//                    cell1.eventNameLabel.text = eventName
//                }
//                
//                if let Eventdate =  eventArrayIS[indexPath.row].valueForKey("eventDateStr") as? String{
//                    //print(Eventdate)
//                    //print(indexPath.row)
//                    if Eventdate != "TBD" {
//                        // *** get datey from string *** //
//                        
//                        let dateFormatter = NSDateFormatter()
//                        dateFormatter.dateFormat = "MM/dd/yyyy"
//                        let dateFromString = dateFormatter.dateFromString(Eventdate)
//                        
//                        // *** get day *** //
//                        
//                        let dateFormatteerForDay = NSDateFormatter()
//                        dateFormatteerForDay.dateFormat = "dd"
//                        let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
//                        
//                        cell1.dayLabel.text = dayFromDate
//                        
//                        // *** get year *** //
//                        
//                        
//                        let dateFormatteerForYear = NSDateFormatter()
//                        dateFormatteerForYear.dateFormat = "YYYY"
//                        let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
//                        
//                        cell1.yearLabel.text = yearFromDate
//                        
//                        // *** get month *** //
//                        
//                        let dateFormatteerForMonth = NSDateFormatter()
//                        dateFormatteerForMonth.dateFormat = "MMM"
//                        let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
//                        
//                        cell1.monthLabel.text = monthFromDate
//                        
//                        // *** get week day *** //
//                        
//                        let dateFormatteerForWeekDay = NSDateFormatter()
//                        dateFormatteerForWeekDay.dateFormat = "EEE"
//                        let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
//                        
//                        cell1.weekDayLabel.text = weekDayFromDate
//                        
//                    } else {
//                        cell1.dayLabel.text = "TBD"
//                        cell1.yearLabel.text = "TBD"
//                        cell1.monthLabel.text = "TBD"
//                        cell1.weekDayLabel.text = "TBD"
//                    }
//                }
//                // *** set favorites *** //
//                if let isFavorite = eventArrayIS.objectAtIndex(indexPath.row).valueForKey("isFavoriteEvent") as? Bool {
//                    if isFavorite {
//                        cell1.favouritesButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
//                    } else {
//                        cell1.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
//                    }
//                } else {
//                    cell1.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
//                }
//                //venue Address
//                var eTime = ""
//                var eVenue = ""
//                var eCity = ""
//                var eState = ""
//                if let Eventtime = eventArrayIS[indexPath.row].valueForKey("eventTimeStr") as? String{
//                    //24hr format
//                    let timeSlot = Eventtime
//                    //print(Eventtime)
//                    if Eventtime == "TBD"  {
//                        eTime  = "TBD"
//                    }
//                    else{
//                        let dateFormatterForTime = NSDateFormatter()
//                        
//                        dateFormatterForTime.dateFormat = "hh:mma"
//                        let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
//                        
//                        
//                        
//                        let dateFormatteerForTimeFormat = NSDateFormatter()
//                        
//                        dateFormatteerForTimeFormat.dateFormat = "HH:mm"
//                        
//                        eTime = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
//                    }
//                }
//                    
//                else{
//                    eTime = "12:00AM"
//                }
//                if let Eventvenue = eventArrayIS[indexPath.row].valueForKey("venueName") as? String{
//                    eVenue = Eventvenue
//                }
//                if let Eventcity = eventArrayIS[indexPath.row].valueForKey("state") as? String{
//                    eCity = Eventcity
//                }
//                if let EventState = eventArrayIS[indexPath.row].valueForKey("country") as? String{
//                    eState = EventState
//                }
//                let string1 = eTime
//                let resultStr = "\(string1)"+"\("-")" + " \(eVenue)" + "\(" ")" + "\(eCity)" + "\(" ")" + "\(eState)"
//                
//                cell1.eventTimeAndAddressLabel.text = resultStr
//                
//                
//                if let EventticketPrice = eventArrayIS[indexPath.row].valueForKey("ticketPriceTag") as? String{
//                    cell1.priceLabel.text = EventticketPrice
//                }
//                // *** change color of cell *** //
//                
//                if indexPath.row % 2 != 0 {
//                    cell1.colorView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
//                } else {
//                    cell1.colorView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
//                }
//            }
//            return cell1
//        }
//    }
    
    //MARK:- API CALL
    //MARK:-
    
    func favoriteArtistEventInfoByCustomerApiCall() {
        
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** api call for FavoriteArtistEventInfoByCustomerViewController ***
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            // * stop activityIndicator
            self.stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        self.setLoadingIndicator(self.view)
        
        var customerId : Int = 0
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        
        if customerId != 0 {
            
            // *** send parameters
            let getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "customerId" : customerId
            ]
            
            WebServiceCall.sharedInstance.favoriteArtistEventInfoByCustomer("FavoriteArtistEventInfoByCustomer.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                // * stop activityIndicator
                self.stopIndicator()
                
                if isTrue == true {
                    
                    print("Response",responseDisctionary!)
                    
                    if let favouriteArtists = responseDisctionary?.objectForKey("favouriteArtists") as? NSMutableArray {
                        
                        let artists : NSMutableArray = []
                        
                        for i in 0..<favouriteArtists.count {
                            
                            let perEachArtistDetails : NSMutableArray = []
                            
                            
                            
                        }
                    }
                    
                    if let favouriteEvents = responseDisctionary?.objectForKey("favouriteEvents") as? NSMutableArray {
                        
                    }
                    
                } else {
                    
                    if message == "" {
                        
                        // * if no response
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        
                        // * if error in the response
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        } else {
            
            alertVc.message = "Please Login"
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        
    }
    
    //MARK:- Button Actions
    //MARK:-
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
