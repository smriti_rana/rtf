//
//  AppDelegate.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 13/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import PinterestSDK
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
     
    var window: UIWindow?
    var ticks:CFTimeInterval = 0.5
    var fixedminutes = 06
    var time :String = ""
    
    
    var navController : UINavigationController!

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        PDKClient.configureSharedInstanceWithAppId("4839179254254020635")
        NSThread.sleepForTimeInterval(3.0)
        
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        IQKeyboardManager.sharedManager().enable = true
        Fabric.with([Crashlytics.self])
        
        //        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        //        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        //        application.registerUserNotificationSettings(pushNotificationSettings)
        //        application.registerForRemoteNotifications()
        
        let configId = "RewardTheFan"
        let platForm = "IOS"
        let productType = "REWARDTHEFAN"
        let Identifier: String = UIDevice.currentDevice().identifierForVendor!.UUIDString
        print(Identifier)
        NSUserDefaults.standardUserDefaults().setValue(configId, forKey: "configId")
        NSUserDefaults.standardUserDefaults().setValue(platForm, forKey: "platForm")
        NSUserDefaults.standardUserDefaults().setValue(productType, forKeyPath: "productType")
        NSUserDefaults.standardUserDefaults().setValue(Identifier, forKey: "deviceId")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        PayPalMobile .initializeWithClientIdsForEnvironments([PayPalEnvironmentProduction: "YOUR_CLIENT_ID_FOR_PRODUCTION",
            PayPalEnvironmentSandbox: "AX18BBSZ-XMlXuaeNLzGbsfWUQOp0kWubJZZpiMhn1rRxldUwvntmDKnaPtI4_Kttl2Rjpvkj6xREkq-"])
        
        var skipWithoutLoginFlag : Bool = true
        
        if NSUserDefaults.standardUserDefaults().objectForKey("skipWithoutLogin") != nil {
            
            skipWithoutLoginFlag = NSUserDefaults.standardUserDefaults().objectForKey("skipWithoutLogin") as! Bool
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil || skipWithoutLoginFlag == false {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Home") as! HomeViewController
            self.navController = UINavigationController(rootViewController: vc)
            self.navController.setNavigationBarHidden(true, animated: true)
            self.window!.rootViewController = self.navController
        } else {
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
            self.navController = UINavigationController(rootViewController: vc)
            self.navController.setNavigationBarHidden(true, animated: true)
            self.window!.rootViewController = self.navController
        }
        
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        let characterSet: NSCharacterSet = NSCharacterSet(charactersInString: "<>")
        let deviceTokenString: String = (deviceToken.description as NSString)
            .stringByTrimmingCharactersInSet(characterSet)
            .stringByReplacingOccurrencesOfString( " ", withString: "") as String
        NSUserDefaults.standardUserDefaults().setValue(deviceTokenString, forKey: "deviceToken")
        NSUserDefaults.standardUserDefaults().synchronize()
        print("device token is :\(deviceTokenString)")
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print(userInfo)
    }

    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
        if  GIDSignIn.sharedInstance().handleURL(url, sourceApplication: sourceApplication, annotation: annotation) {
            return GIDSignIn.sharedInstance().handleURL(url,
                                                        sourceApplication: sourceApplication,
                                                        annotation: annotation)
        }
        else {
            return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

  }

