//
//  TableViewCell.swift
//  RewardTheFan
//
//  Created by 42Works-Worksys2 on 10/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

   
    weak var delegate: HomeEventsTableViewCellDelegate?

    @IBOutlet weak var TicketBgView: UIView!
    @IBOutlet var monthLabel: UILabel!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var weekDayLabel: UILabel!
    @IBOutlet var dateView: UIView!
    @IBOutlet var eventNameLabel: UILabel!
    @IBOutlet var eventTimeAndAddressLabel: UILabel!
    
    @IBOutlet weak var ticketView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        eventNameLabel.text = "Katty perry"
        eventTimeAndAddressLabel.text = "New York"
    }
    
    override var bounds : CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.makeItCircle()
    }
    
    func makeItCircle() {
        
        self.dateView.layer.masksToBounds = true
        self.dateView.layer.cornerRadius = self.dateView.frame.height/2;
        self.dateView.clipsToBounds = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
