//
//  ShippingTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 30/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
@objc protocol shippingCellDelegate {
   
    optional func moreUpdateShippingAddress(getIndex:Int)
    
    
}
class ShippingTableViewCell: UITableViewCell {

    weak var delegate: shippingCellDelegate?
    @IBOutlet weak var countryOrZipcode: UILabel!
    @IBOutlet weak var cityOrStateLbl: UILabel!
    @IBOutlet weak var addressOrStreetLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var firstNameOrlastNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK:-MoreTapped
    @IBOutlet weak var morebtn: UIButton!
    @IBAction func moreTapped(sender: AnyObject) {
        self.delegate!.moreUpdateShippingAddress!(morebtn.tag)
    }
  
    
}
