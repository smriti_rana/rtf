//
//  ShippingInfoViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 30/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class ShippingInfoViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,shippingCellDelegate{
    @IBOutlet weak var emptyLbl: UILabel!
    var shippingArray:NSMutableArray = []
    var customerDict:NSDictionary = [:]
   var shippingAddressId:Int = 0
    var emptyTableMsg: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyTableMsg = "Currently there is no Shipping Address added"
        tableView.registerNib(UINib(nibName: "ShippingTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 81
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if NSUserDefaults.standardUserDefaults().objectForKey("productType") != nil {
            productType = NSUserDefaults.standardUserDefaults().objectForKey("productType") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("deviceId") != nil {
            deviceId = NSUserDefaults.standardUserDefaults().objectForKey("deviceId") as! String
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("configId") != nil {
            configId = NSUserDefaults.standardUserDefaults().objectForKey("configId") as! String
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("platForm") != nil {
            platForm = NSUserDefaults.standardUserDefaults().objectForKey("platForm") as! String
        }
        
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            shippingTitleLbl.font = UIFont(name: shippingTitleLbl.font.fontName, size: 32)
            navbar_Height.constant = 84
        }

    }

    override func viewWillAppear(animated: Bool) {
        //GetShippingInfo Api
        setLoadingIndicator(tableView)
        GetCustomerApi()
        
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shippingTitleLbl: UILabel!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var navbar_Height: NSLayoutConstraint!
    
    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shippingArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
            as! ShippingTableViewCell
        cell.selectionStyle = .None
        cell.delegate = self
        cell.morebtn.tag =  indexPath.row
        //firstname
        var shippingnameStrIs = ""
        var shippingcitySateIs = ""
        var shippingcountryZipcodeIs = ""
        if shippingArray.count > 0 {
            if let billingfirstnameStr = self.shippingArray.objectAtIndex(indexPath.row).valueForKey("firstName") as? String{
                shippingnameStrIs = billingfirstnameStr
                if let shippinglastnameStr = self.shippingArray.objectAtIndex(indexPath.row).valueForKey("lastName") as? String{
                    cell.firstNameOrlastNameLbl.text = "\(shippingnameStrIs)" + "\(" ")" + "\(shippinglastnameStr)"
                    
                }
                
            }
            //set Address
            if let shippingaddStreetStr = self.shippingArray.objectAtIndex(indexPath.row).valueForKey("addressLine1") as? String{
                cell.addressOrStreetLbl.text = shippingaddStreetStr
                
            }
            //set PhoneNumber
            if let shippingPhone = self.shippingArray.objectAtIndex(indexPath.row).valueForKey("phone1") as? String{
                cell.phoneNumberLbl.text = shippingPhone
                
            }
            //set city/State
            if let shippingcityStr = self.shippingArray.objectAtIndex(indexPath.row).valueForKey("city") as? String{
                shippingcitySateIs = shippingcityStr
                if let shippingstateStr = self.shippingArray.objectAtIndex(indexPath.row).valueForKey("stateName") as? String{
                    cell.cityOrStateLbl.text = "\(shippingcityStr)" + "\(" ")" + "\(shippingstateStr)"
                    
                }
                
            }
            // country/zipcode
            if let shippingcountryStr = self.shippingArray.objectAtIndex(indexPath.row).valueForKey("countryName") as? String{
                shippingcountryZipcodeIs = shippingcountryStr
                if let billingZipcodeStr = self.shippingArray.objectAtIndex(indexPath.row).valueForKey("zipCode") as? String{
                    cell.countryOrZipcode.text = "\(shippingcountryStr)" + "\(" ")" + "\(billingZipcodeStr)"
                    
                }
                //set AddressId
                if let shippingId = self.shippingArray.objectAtIndex(indexPath.row).valueForKey("id") as? Int{
                    self.shippingAddressId = shippingId
                }

            
        }
        
            
        }

        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    //MARK:-BackTapped
    @IBOutlet weak var backBtn: UIButton!
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:-AddShippingAddressTapped
    @IBOutlet weak var addAddBtn: UIButton!
    @IBAction func addAddressTapped(sender: AnyObject) {
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        if shippingArray.count < 5 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("bilingInfo") as! BillingInfoViewController
            vc.viewTypeForLblTitle = "Shipping"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else{
            alertVc.message = maxShippingAddressAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
       
        }
        
    }
    //MARK:-Billing/Shipiing Add CustomerInfo
    func GetCustomerApi(){
        // *** api call for forgot password ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** send parameters
        let getParam = [
            "productType" : productType,
            "customerId":customerId,
            "platForm":platForm,
            "configId":configId,
            
            ]
        print(getParam)
        
        // **** call webservice
        WebServiceCall.sharedInstance.postCustomerInfoApiCall("GetCustomerInfo.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            if isTrue == true {
                self.stopIndicator()
                print("response for customer",responseDisctionary!)
                if let customerDictIs = responseDisctionary!.valueForKey("customer") as? NSDictionary{
                        self.customerDict = customerDictIs
                    if let shippingArryIs = customerDictIs.valueForKey("shippingAddress") as? NSArray{
                        print("Shipping address",shippingArryIs)
                        self.shippingArray.removeAllObjects()
                        if shippingArryIs.count > 0{
                            for i in 0 ..< shippingArryIs.count {
                                if i <= 4{
                                    self.shippingArray .addObject(shippingArryIs[i])
                                }
                            }
                            
                        }
                        else{
                            self.emptyLbl.hidden = false
                            self.emptyLbl.text = self.emptyTableMsg
                        }
                       
                    }
                    self.tableView.reloadData()
                    print("Shipping address",self.shippingArray)
                    
                }


                
            } else {
                self.stopIndicator()
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
            
        })
    }
    //MARK:-More Tapped
    func moreUpdateShippingAddress(getIndex:Int){
        if self.shippingArray.count > 0  {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("bilingInfo") as! BillingInfoViewController
            vc.viewTypeForLblTitle = "Shipping"
            vc.shippingArrayFromView = customerDict.valueForKey("shippingAddress")?.objectAtIndex(getIndex) as! NSDictionary
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}
