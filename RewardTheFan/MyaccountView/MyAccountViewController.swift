//
//  MyAccountViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 02/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class MyAccountViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,MyaccountTableViewCellDelegate,UIImagePickerControllerDelegate {
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var creditLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    var popover:UIPopoverController?=nil
       @IBOutlet weak var navbar_height: NSLayoutConstraint!
    @IBOutlet weak var TitleLbl: UILabel!
    var tableTitleHeaderArray:NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        //Regestering cell
        tableView.registerNib(UINib(nibName: "myaccountTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 72.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableTitleHeaderArray = ["My Tickets","Rewards","Billing Information","Shipping Information"]
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            TitleLbl.font = UIFont(name: TitleLbl.font.fontName, size: 31)
            navbar_height.constant = 74
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyAccountViewController.pushViewController), name:"alertMessageforExplore", object: nil)
        //set userLoggedIn name
        if NSUserDefaults.standardUserDefaults().valueForKey("customerName") != nil {
            userNameLbl.text = NSUserDefaults.standardUserDefaults().valueForKey("customerName") as? String
           
        }
        //Set Dollar Saved In user Defaults
        if NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") != nil {
            let customerLoyalityRewardsPointArray = NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") as! NSDictionary
            print(customerLoyalityRewardsPointArray)
            if let activeDollars =  customerLoyalityRewardsPointArray.valueForKey("activeRewardDollers") as? Int {
                var activeDollarsIs = ""
                if activeDollars == 0{
                    activeDollarsIs = "$0"
                    
                }else{
                    activeDollarsIs = "$\(activeDollars)"
                }
                
                creditLbl.text = activeDollarsIs
            }
        }
       
        userImgView.layer.masksToBounds = false
        userImgView.layer.cornerRadius = userImgView.frame.size.width/2
        userImgView.clipsToBounds = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return tableTitleHeaderArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! myaccountTableViewCell
        cell.tableTitleLbl.text = tableTitleHeaderArray.objectAtIndex(indexPath.row) as? String
        cell.delegate = self
//        cell.pushBtn.tag = indexPath.row
        cell.selectionStyle = .None
        // *** change color of cell *** //
        if indexPath.row % 2 != 0 {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
        } else {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        }
        
        return cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == 0
        {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("OrderMyTicket") as! OrderViewController
        self.navigationController?.pushViewController(vc, animated: true)
        }
      else  if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("myaccountReward") as! MyAccountRewardPointsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
       else if indexPath.row == 2 {
            if customerId == 0 {
                let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
                alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
                alertVc.message = loggedInAlert
                alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
                self.presentViewController(alertVc, animated: true, completion: nil)
                
                
            }else{
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("bilingInfo") as! BillingInfoViewController
            self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        if indexPath.row == 3 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("shippingInfo") as! ShippingInfoViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 80.0;//Choose your custom row height
        }else{
            return  UITableViewAutomaticDimension
        }
    }
    //MARK:-BackTapped
    @IBOutlet weak var backBtn: UIButton!
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:-navigateOtherViewDelegate On Button Click
    func myaccountPushtoOtherViewsButtonClicked(index : NSInteger){
        if index == 5 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("bilingInfo") as! BillingInfoViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    //Mark:-PopToLogin
    func pushViewController() {
        self.delay(0.2) { () -> () in
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    //MARK:-Edit ImageView
    @IBOutlet weak var editImgbtn: UIButton!
    @IBAction func editImageTapped(sender: AnyObject) {
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openCamera()                              }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
//        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
//            self.presentViewController(alert, animated: true, completion: nil)
//        }
//        else {
//            popover=UIPopoverController(contentViewController: alert)
//            popover!.presentPopoverFromRect(self.view.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
//            
//        }
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    func openGallary() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.allowsEditing = true
            self .presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.allowsEditing = true
            self .presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    //MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    var imageToSend = UIImage(named: "")
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
//        imageselectFlag = true
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
        imageToSend = image
        userImgView.image = image
    }

}
