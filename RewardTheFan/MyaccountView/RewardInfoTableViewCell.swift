//
//  RewardInfoTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 20/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class RewardInfoTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
