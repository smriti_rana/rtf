//
//  myaccountTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 02/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
@objc protocol MyaccountTableViewCellDelegate
{
       optional func myaccountPushtoOtherViewsButtonClicked(index : NSInteger)
}

class myaccountTableViewCell: UITableViewCell {
    weak var delegate: MyaccountTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var forwardBtn: UIButton!
    @IBAction func forwardTapped(sender: AnyObject) {
    }

    @IBOutlet weak var tableTitleLbl: UILabel!
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK:-toNavigateOtherView Button
    @IBOutlet weak var pushBtn: UIButton!
    @IBAction func PushtoOtherViewTapped(sender: AnyObject) {
        
        self.delegate?.myaccountPushtoOtherViewsButtonClicked!(pushBtn.tag)
    }
    
}
