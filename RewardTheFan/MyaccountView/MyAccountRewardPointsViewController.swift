//
//  MyAccountRewardPointsViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 20/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class MyAccountRewardPointsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,MyaccountTableViewCellDelegate{
    var tableTitleHeaderArray:NSMutableArray = []
    var getPointsDict :NSDictionary = [:]
    @IBOutlet weak var navbar_heigth: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        //Regestering cell
//        tableView.registerNib(UINib(nibName: "myaccountTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 72.0
        tableView.rowHeight = UITableViewAutomaticDimension
         tableTitleHeaderArray = ["Active Amount To Be Used:","Total Used Amount:","Total Earned Amount:","Last Used Amount:","Last Earned Amount:"]
        tableView.registerNib(UINib(nibName: "RewardInfoTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cell")
        //Set Dollar Saved In user Defaults
        if NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") != nil {
            let customerLoyalityRewardsPointArray = NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") as! NSDictionary
            print(customerLoyalityRewardsPointArray)
            getPointsDict = customerLoyalityRewardsPointArray
            print(getPointsDict)
     
        }
    }
   
   
    @IBOutlet weak var titlaLbl: UILabel!

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return tableTitleHeaderArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! RewardInfoTableViewCell
        cell.titleLbl.text = tableTitleHeaderArray.objectAtIndex(indexPath.row) as? String
        
        print("tableview",getPointsDict)
        if indexPath.row == 0{
            if let activeDollars =  getPointsDict.valueForKey("activeRewardDollers") as? Int {
                cell.valueLbl.text = "\("$")"+"\(activeDollars)"
            }
           
           
        }
        else if indexPath.row == 1{
             if let totalSpentPoints =  getPointsDict.valueForKey("totalSpentPoints") as? Int {
                cell.valueLbl.text = "\("$")"+"\(totalSpentPoints)"
            }
            
        }
        else if indexPath.row == 2{
            if let totalEarnedPoints =  getPointsDict.valueForKey("totalEarnedPoints") as? Int {
                cell.valueLbl.text = "\("$")"+"\(totalEarnedPoints)"
            }
           
        }
        else if indexPath.row == 3{
            if let latestSpentPoints =  getPointsDict.valueForKey("latestSpentPoints") as? Int {
                cell.valueLbl.text = "\("$")"+"\(latestSpentPoints)"
            }
            
        }
        else if indexPath.row == 4{
            if let latestEarnedPoints =  getPointsDict.valueForKey("latestEarnedPoints") as? Int {
                cell.valueLbl.text = "\("$")"+"\(latestEarnedPoints)"
            }
            
        }

        cell.selectionStyle = .None
        // *** change color of cell *** //
        if indexPath.row % 2 != 0 {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
        } else {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        }
        return cell
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 80.0;//Choose your custom row height
        }else{
            return  UITableViewAutomaticDimension
        }
    }
    //MARK:-Backtapped
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    //Mark:-PopToLogin
    func pushViewController() {
      
    }
}
