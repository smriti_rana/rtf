//
//  Constants.swift
//  FuturEd
//
//  Created by Anmol Rajdev on 19/02/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import Foundation
import UIKit


//MARK:- Error Message
let ErrorInternetConnection  	 =   "Connect to working internet connection."
let ErrorServerConnection   	 =   "Could not connect to the server.  Please check your connection and try again."
//loginModule Msgs
let fbloginAlertMsg  = "Email is mandatory, Please enter the email below for the registration"
let emailAlertMsg  = "Please enter email."
let passwordAlertMsg = "Please enter password."
let validPwAlertMsg = "Password must have atleast one special character or number"
let rangePwAlertmsg =  "Password must be 8 to 20 characters"
let validEmailAlertmsg = "Please enter a valid email."
let nameAlertmsg = "Please enter name."
let allfeildsAlertMsg = "Please fill all the fields."
let firstnameAlertmsg = "Please enter first name."
let lastnameAlertMsg = "Please enter last name."
let phoneNoAlertMsg = "Please enter phonenumber."
let addressAlertMsg = "Please enter address."
let cityAlertmsg = "Please enter city."
let stateAlertMsg = "Please select state."
let countryAlertMsg  = "Please select country."
let zipcodeAlertMsg = "Please enter zipcode."
//Explore Msgs
let loggedInAlertMsg = "Please login to view"
//Pre-order Msgs 
let addAddressAlertMsg = "Please add billing/shipping address first."
let maxShippingAddressAlertMsg  = "You can add max 5 address."
//CardDetails Msgs 
let cardNoAlertMsg = "Please fill the card number."
let cardExpiryMonthAlertMsg = "Please fill the card expiry month."
let cardExpiryYearAlertMsg = "Please fill the card expiry year."
let fullNameAlertMsg = "Please fill the full name."
let cvvAlertMsg = "Please fill the cvv number."

let rangecvvAlertMsg = "Please fill the correct cvv number."
let validCardAlertMsg = "Please fill the correct card number."
//Noromalized Search AlertMsgs
let ExploreAlertmsgs = "Please enter Event, Band, Show"
//Home(Location)AlertMsgs
let locserviceLAlertMsg = "Please turn on your location services"
//ChooseFav AlertMsgs
let favArtistAlertMsg = "Please select atleast one artist."
let artistFoundAlertMsg = "No artists found in music libreary"
let noartistAlertMsg = "No artist Found"
let viewFavAlertMsg = "Please login to select the Favorites"
//Billing AlertMsgs
let updateBillingAddAlertMsg = "Billing address updated successfully."
let addBillingAddAlertMsg = "Billing address added successfully."
let updateShippingAlertMsg = "Shipping address updated successfully."
let addShippingAddAlertMsg = "Shipping address added successfully."
//MyAccount AlertMsg
 let loggedInAlert = "Please login to view"
//OrderSummary 
let timerExpireAlertmsg = "Session has expired."
let loginAlertMessage = "Please login."
var MessageString = ""
let BgBaseColor   			=   UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
var defaultimageLinkurl = "http://thedoonschool.thealumniapp.com/uploads/logo/icon.png"
struct NSUserKeys {
    static var CurrentCity = "cityCurrent"
}

struct ScreenConstant {
	static var width 	= UIScreen.mainScreen().bounds.size.width
	static var height 	= UIScreen.mainScreen().bounds.size.height
	static var originX 	= UIScreen.mainScreen().bounds.origin.x
	static var originY 	= UIScreen.mainScreen().bounds.origin.y
}

struct ColorConstant {
    static var BoldGreenColor =   UIColor(red: 20/255, green: 73/255, blue: 90/255, alpha: 1.0)
    static var LightGreenColor =   UIColor(red: 30/255, green: 73/255, blue: 90/255, alpha: 1.0)
    static var BoldRedColor =   UIColor(red: 167/255, green: 15/255, blue: 6/255, alpha: 1.0)
    static var BlueColor =   UIColor(red: 120/255, green: 190/255, blue: 210/255, alpha: 1.0)
    static var ShadowColor =   UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
    static var BgCellBaseColor  =   UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
    static var BgAnotherCellBaseColor  =   UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
    static var cellFooterCellBlueColor = UIColor(colorLiteralRed: 39/255.0, green: 133/255.0, blue: 250/255.0, alpha: 1.0)
    static var cellBKGrayColor = UIColor(colorLiteralRed: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.2)
    static var cellGrayColor = UIColor(colorLiteralRed: 245/255.0, green: 246/255.0, blue: 247/255.0, alpha: 1.0)
    static var orangeColor =   UIColor(red: 240/255, green: 104/255, blue: 18/255, alpha: 1.0)
    
}

struct ServerUrl {
    static var link   		=   "http://52.22.87.62/sandbox.zoneswebservices/ws/"
}
struct AlertMsgs {
    static var ErrorInternetConnection   		=   "http://52.22.87.62/sandbox.zoneswebservices/ws/"
}
