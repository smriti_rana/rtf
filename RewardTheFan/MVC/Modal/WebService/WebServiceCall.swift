//
//  WebServiceCall.swift
//  FuturEd
//
//  Created by Anmol Rajdev on 19/02/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit
import Alamofire

typealias ServiceResponse = (NSDictionary?, String?) -> Void
typealias ServiceTrue = (Bool?, String?, NSDictionary?) -> Void

var requestHeaders : NSDictionary = ["x-sign":"8vtRQDaSt+h/kc6MxjZqs2Ev8X8bFo7+dENVZnVF4v0kUCcMcBwFz6mXWBn9RCqm","x-token":"RewardTheFan"]

class WebServiceCall: NSObject {
    
    class var sharedInstance: WebServiceCall {
        
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: WebServiceCall? = nil
        }
        
        dispatch_once(&Static.onceToken) {
            Static.instance = WebServiceCall()
        }
        return Static.instance!
    }

    
    func getReponse(fbAccessToken:String, url:String) {
       
        //let url = "https://graph.facebook.com/me/likes?access_token=\(fbAccessToken)"
        let escapedAddress = "\(url)".stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())

        request(Method.GET, escapedAddress!, parameters: [:], encoding: ParameterEncoding.URL, headers: nil)
            .responseJSON { (response) in
                
                print("Fb: \(response.result.value)")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                    }
                }
                
//                let response: AutoreleasingUnsafeMutablePointer<NSURLResponse?>=nil
//                
//                let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: response, error: nil)
//                
//                if data != nil {
//                    let stringResponse = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                    println("**** stringResponse **** \(stringResponse!)")
//                } else {
//                    print("error 2 getting contacts is ")
//                }

        }
    }
    
    
    //MARK: SignUp
    func signUp(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON
            { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil
                {
                    if response.result.isSuccess
                    {
                        if let responseD = response.result.value! as? NSDictionary
                        {
                            print("**********************REGISTRATION RESPONSE*************************")
                            print("Registration user response is :\(responseD)")
                            if let customerDetailDictionary = responseD.valueForKey("customerDetails") as? NSDictionary
                            {
                                if let status = customerDetailDictionary.valueForKey("status") as? Bool
                                {
                                    if status
                                    {
                                        oncompletion(true, "", responseD)
                                    }
                                    else
                                    {
                                        if let errorDictionary = customerDetailDictionary.valueForKey("error") as? NSDictionary
                                        {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    
    //MARK: FaceBook Login Validator
    func facebookLoginValidator(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON
            { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil
                {
                    if response.result.isSuccess
                    {
                        if let responseD = response.result.value! as? NSDictionary
                        {
                            print("**********************FaceBook Login Validator USER  RESPONSE*************************")
                            print("Registration user response is :\(responseD)")
                            if let customerDetailDictionary = responseD.valueForKey("customerDetails") as? NSDictionary
                            {
                                if let status = customerDetailDictionary.valueForKey("status") as? Bool
                                {
                                    if status
                                    {
                                        oncompletion(true, "", responseD)
                                    }
                                    else
                                    {
                                        if let errorDictionary = customerDetailDictionary.valueForKey("error") as? NSDictionary
                                        {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    
    //MARK: SignIn
    func signIn(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************SIGNIN RESPONSE*************************")
                            print("Sign in user response is :\(responseD)")
                            if let customerDetailDictionary = responseD.valueForKey("customerDetails") as? NSDictionary
                            {
                                if let status = customerDetailDictionary.valueForKey("status") as? Bool
                                {
                                    if status
                                    {
                                        oncompletion(true, "", responseD)
                                    }
                                    else
                                    {
                                        if let errorDictionary = customerDetailDictionary.valueForKey("error") as? NSDictionary
                                        {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    //MARK: Reset Password
    func resetPassword(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************RESET PASSWORD RESPONSE*************************")
                            print("Reset password  user response is :\(responseD)")
                            if let customerDetailDictionary = responseD.valueForKey("resetPasswordRequest") as? NSDictionary
                            {
                                if let status = customerDetailDictionary.valueForKey("status") as? Bool
                                {
                                    if status
                                    {
                                        oncompletion(true, "", responseD)
                                    }
                                    else
                                    {
                                        if let errorDictionary = customerDetailDictionary.valueForKey("error") as? NSDictionary
                                        {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    //MARK: Get Popular Artist/Team – Super Fan
    func getPopularArtistSuperFan(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************Get Popular Artist/Team – Super Fan USER RESPONSE*************************")
                            print("Get popular artist/team user response is :\(responseD)")
                            if let artistList = responseD.valueForKey("artistList") as? NSDictionary
                            {
                                if let status = artistList.valueForKey("status") as? Bool
                                {
                                    if status
                                    {
                                        oncompletion(true, "", artistList)
                                    }
                                    else
                                    {
                                        if let errorDictionary = artistList.valueForKey("error") as? NSDictionary
                                        {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    //MARK: Get Popular Artist/Team – Favorites
    func getPopularArtistFavorites(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************Get Popular Artist/Team – Favorites USER RESPONSE*************************")
                            print("Get popular artist/team user response is :\(responseD)")
                            if let artistList = responseD.valueForKey("artistList") as? NSDictionary
                            {
                                if let status = artistList.valueForKey("status") as? Bool
                                {
                                    if status
                                    {
                                        oncompletion(true, "", artistList)
                                    }
                                    else
                                    {
                                        if let errorDictionary = artistList.valueForKey("error") as? NSDictionary
                                        {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    
    //MARK: search artist
    func searchArtist(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************SearchArtist USER RESPONSE*************************")
                            print("Get popular artist/team user response is :\(responseD)")
                            if let artistList = responseD.valueForKey("artistList") as? NSDictionary
                            {
                                if let status = artistList.valueForKey("status") as? Bool
                                {
                                    if status
                                    {
                                        oncompletion(true, "", artistList)
                                    }
                                    else
                                    {
                                        if let errorDictionary = artistList.valueForKey("error") as? NSDictionary
                                        {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    
    //MARK: Get posible artist by social Information
    func getPossibleArtistsBySocialInfo(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************Possible Artists By SocialInfo USER RESPONSE*************************")
                            print("Possible Artists By SocialInfo user response is :\(responseD)")
                            if let artistList = responseD.valueForKey("artistList") as? NSDictionary {
                                if let status = artistList.valueForKey("status") as? Bool {
                                    if status {
                                        oncompletion(true, "", artistList)
                                    } else {
                                        if let errorDictionary = artistList.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }

    //MARK: Add favorites or superArtist
    func addFavoriteOrSuperArtist(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************Possible Artists By SocialInfo USER RESPONSE*************************")
                            print("Possible Artists By SocialInfo user response is :\(responseD)")
                            if let addArtistDetails = responseD.valueForKey("addArtistDetails") as? NSDictionary {
                                if let status = addArtistDetails.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, addArtistDetails.valueForKey("actionResult") as? String, addArtistDetails)
                                    } else {
                                        if let errorDictionary = addArtistDetails.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    //MARK: Home Card
    func getHomeCards(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************get Home Cards USER RESPONSE*************************")
                            print("get Home Cards user response is :\(responseD)")
                            if let eventCards = responseD.valueForKey("eventCards") as? NSDictionary {
                                
                                if let status = eventCards.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", eventCards)
                                    } else {
                                        if let errorDictionary = eventCards.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }

    
    //MARK: Favourite Event
    func favouriteEvent(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************Favourite Event USER RESPONSE*************************")
                            print("Favourite Event user response is :\(responseD)")
                            if let eventCards = responseD.valueForKey("favouriteEventsResponse") as? NSDictionary {
                                
                                if let status = eventCards.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, eventCards.valueForKey("message") as? String , nil)
                                    } else {
                                        if let errorDictionary = eventCards.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }

    //MARK:- Explore cards
    //MARK:-
    
    func exploreCards(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************EXPLORE CARDS USER RESPONSE*************************")
                            print("Explore cards user response is :\(responseD)")
                            if let exploreOption = responseD.valueForKey("exploreOption") as? NSDictionary {
                                
                                if let status = exploreOption.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", exploreOption)
                                    } else {
                                        if let errorDictionary = exploreOption.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }

    
    
    
    func LogIn(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print("**********************REQUEST PARAMTERS*************************")
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************Favourite Event USER RESPONSE*************************")
                            print("Favourite Event user response is :\(responseD)")
                            if let grandChildList = responseD.valueForKey("grandChildList") as? NSDictionary {
                                
                                if let status = grandChildList.valueForKey("status") as? Bool {
                                    if status {
                                    oncompletion(true, "", grandChildList)
                                    } else {
                                        if let errorDictionary = grandChildList.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }

    //MARK:-Post Call
    func post(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************Favourite Event USER RESPONSE*************************")
                            print("Favourite Event user response is :\(responseD)")
                            if let eventResultDict = responseD.valueForKey("eventResult") as? NSDictionary {
                                
                                if let status = eventResultDict.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        
                                        oncompletion(true, "", eventResultDict)
                                    } else {
                                        if let errorDictionary = eventResultDict.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }

    //MARK:-Post Call
    func postArtistfav(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************Favourite ArtistRESPONSE*************************")
                            print("Favourite Artist user response is :\(responseD)")
                            if let eventResultDict = responseD.valueForKey("addArtistDetails") as? NSDictionary {
                                
                                if let status = eventResultDict.valueForKey("status") as? Bool {
                                    if status {
                                    oncompletion(true, "", eventResultDict)
                                    } else {
                                        if let errorDictionary = eventResultDict.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }
        
    
    //MARK:- Event Detail
    func eventDetail(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************EVENT Detail RESPONSE*************************")
                            print("Event detail user response is :\(responseD)")
                            if let ticketList = responseD.valueForKey("ticketList") as? NSDictionary {
                                
                                if let status = ticketList.valueForKey("status") as? Bool {
                                    if status {
                                        oncompletion(true, "", ticketList)
                                    } else {
                                        if let errorDictionary = ticketList.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }

    
    
    //MARK:-Post Explore Event Search
    func postEventSearch(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************ExploreEventRESPONSE*************************")
                            print("ExploreEvent response is :\(responseD)")
                            if let eventResultDict = responseD.valueForKey("eventResult") as? NSDictionary {
                                
                                if let status = eventResultDict.valueForKey("status") as? Bool {
                                    if status {
                                        oncompletion(true, "", eventResultDict)
                                    } else {
                                        if let errorDictionary = eventResultDict.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }
    
    //MARK:-APi order SummaryPage
    func postOrderSummary(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************OrderSummaryRESPONSE*************************")
                            
                            print("response is :\(responseD)")
                            if let eventResultDict = responseD.valueForKey("ticketGroupLockStatus") as? NSDictionary {
                                
                                if let status = eventResultDict.valueForKey("status") as? Bool {
                                    if status {
                                        oncompletion(true, "", eventResultDict)
                                    } else {
                                        if let errorDictionary = eventResultDict.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }
    //MARK:-APi PRE-order SummaryPage
    func postPreOrderApiCall(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************OrderSummaryRESPONSE*************************")
                            
                            print("response is :\(responseD)")
                            if let eventResultDict = responseD.valueForKey("getTicketGroup") as? NSDictionary {
                                
                                if let status = eventResultDict.valueForKey("status") as? Bool {
                                    if status {
                                        oncompletion(true, "", eventResultDict)
                                    } else {
                                        if let errorDictionary = eventResultDict.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }
    //MARK:-ApiCallFor GetCustomerInfo
    func postCustomerInfoApiCall(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************RESPONSE*************************")
                            
                            print("response is :\(responseD)")
                            if let eventResultDict = responseD.valueForKey("customerDetails") as? NSDictionary {
                                
                                if let status = eventResultDict.valueForKey("status") as? Bool {
                                    if status {
                                        oncompletion(true, "", eventResultDict)
                                    } else {
                                        if let errorDictionary = eventResultDict.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }
    //MARK:-AddCustomer
    //MARK:-ApiCallFor GetCustomerInfo
    func postAddCustomerInfoApiCall(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************RESPONSE*************************")
                            
                            print("response is :\(responseD)")
                            if let eventResultDict = responseD.valueForKey("customerAddress") as? NSDictionary {
                                
                                if let status = eventResultDict.valueForKey("status") as? Bool {
                                    if status {
                                        oncompletion(true, "", eventResultDict)
                                    } else {
                                        if let errorDictionary = eventResultDict.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }
    //MARK:Country/State API Call
    func postStateorCounryApiCall(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            print("**********************RESPONSE*************************")
                            
                            print("response is :\(responseD)")
                            if let eventResultDict = responseD.valueForKey("stateCountryDetails") as? NSDictionary {
                                
                                if let status = eventResultDict.valueForKey("status") as? Bool {
                                    if status {
                                        oncompletion(true, "", eventResultDict)
                                    } else {
                                        if let errorDictionary = eventResultDict.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }
    
    
    
    
    //MARK:- create order API Call
    func createOrder(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            print("**********************CREATE ORDER RESPONSE*************************")
                            print("\(responseD)")
                            if let orderConfirmation = responseD.valueForKey("orderConfirmation") as? NSDictionary {
                                
                                if let status = orderConfirmation.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", orderConfirmation)
                                    } else {
                                        if let errorDictionary = orderConfirmation.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
            }
        }
    
    
    
    //MARK:- getCustomerLoyalityRewards API
    func getCustomerLoyalityRewards(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            print("********************** RESPONSE*************************")
                            print("\(responseD)")
                            if let customerLoyaltyDetails = responseD.valueForKey("customerLoyaltyDetails") as? NSDictionary {
                                
                                if let status = customerLoyaltyDetails.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", customerLoyaltyDetails)
                                    } else {
                                        if let errorDictionary = customerLoyaltyDetails.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    
    //MARK:- orderPaymentByLoyalityRewards API
    func orderPaymentByLoyalityRewards(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            print("********************** RESPONSE*************************")
                            print("\(responseD)")
                            if let orderPaymentRewardDetails = responseD.valueForKey("orderPaymentRewardDetails") as? NSDictionary {
                                
                                if let status = orderPaymentRewardDetails.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", orderPaymentRewardDetails)
                                    } else {
                                        if let errorDictionary = orderPaymentRewardDetails.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    
    //MARK:- GetCustomerOrderByOrderId API
    func getCustomerOrderByOrderId(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            print("********************** RESPONSE*************************")
                            print("\(responseD)")
                            if let orderPaymentRewardDetails = responseD.valueForKey("customerOrderResponse") as? NSDictionary {
                                
                                if let status = orderPaymentRewardDetails.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", orderPaymentRewardDetails)
                                    } else {
                                        if let errorDictionary = orderPaymentRewardDetails.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    
    //MARK:- GetActiveEventsCustomerOrders API
    func GetActiveEventsCustomerOrders(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            print("********************** RESPONSE*************************")
                            print("\(responseD)")
                            if let orderPaymentRewardDetails = responseD.valueForKey("customerOrderResponse") as? NSDictionary {
                                
                                if let status = orderPaymentRewardDetails.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", orderPaymentRewardDetails)
                                    } else {
                                        if let errorDictionary = orderPaymentRewardDetails.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
   //MARK:-RewardsPointApi 
    func GetCustomerLoyaltyRewards(methodName:String, params:NSDictionary, oncompletion:ServiceTrue){
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            print("********************** RESPONSE*************************")
                            print("\(responseD)")
                            if let orderPaymentRewardDetails = responseD.valueForKey("customerLoyaltyDetails") as? NSDictionary {
                                
                                if let status = orderPaymentRewardDetails.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", orderPaymentRewardDetails)
                                    } else {
                                        if let errorDictionary = orderPaymentRewardDetails.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }

    }
    //MARK:- GetReffererCodeAPI
    func GetReffererCode(methodName:String, params:NSDictionary, oncompletion:ServiceTrue){
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            print("********************** RESPONSE*************************")
                            print("\(responseD)")
                            if let orderPaymentRewardDetails = responseD.valueForKey("reffererCodeResponse") as? NSDictionary {
                                
                                if let status = orderPaymentRewardDetails.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", orderPaymentRewardDetails)
                                    } else {
                                        if let errorDictionary = orderPaymentRewardDetails.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
        
    }
//MARK:- stripeTransaction
    func stripeTransaction(methodName:String, params:NSDictionary, oncompletion:ServiceTrue){
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            print("********************** RESPONSE*************************")
                            print("\(responseD)")
                            if let orderPaymentRewardDetails = responseD.valueForKey("reffererCodeResponse") as? NSDictionary {
                                
                                if let status = orderPaymentRewardDetails.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", orderPaymentRewardDetails)
                                    } else {
                                        if let errorDictionary = orderPaymentRewardDetails.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }

        
    }
    func GetPastEventsCustomerOrders(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                //print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            //print("********************** RESPONSE*************************")
                            print("\(responseD)")
                            if let orderPaymentRewardDetails = responseD.valueForKey("customerOrderResponse") as? NSDictionary {
                                
                                if let status = orderPaymentRewardDetails.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", orderPaymentRewardDetails)
                                    } else {
                                        if let errorDictionary = orderPaymentRewardDetails.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
    //MARK:- FavoriteArtistEventInfoByCustomer API CALL
    //MARK:-
    
    func favoriteArtistEventInfoByCustomer(methodName:String, params:NSDictionary, oncompletion:ServiceTrue) {
        print(params)
        
        request(Method.POST, ServerUrl.link+"\(methodName)", parameters: params as? [String : AnyObject], encoding: ParameterEncoding.URL, headers: requestHeaders as? [String : String])
            .responseJSON { (response) in
                //print("**********************JSON RESPONSE*************************")
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let responseD = response.result.value! as? NSDictionary {
                            
                            //print("********************** RESPONSE*************************")
                            print("\(responseD)")
                            if let manageFavorites = responseD.valueForKey("manageFavorites") as? NSDictionary {
                                
                                if let status = manageFavorites.valueForKey("status") as? Bool {
                                    if status {
                                        
                                        oncompletion(true, "", manageFavorites)
                                    } else {
                                        if let errorDictionary = manageFavorites.valueForKey("error") as? NSDictionary {
                                            oncompletion(false, errorDictionary.valueForKey("description") as? String, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    oncompletion(false, ErrorServerConnection, nil)
                }
        }
    }
}
