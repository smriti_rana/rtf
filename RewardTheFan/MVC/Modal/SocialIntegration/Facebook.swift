//
//  Facebook.swift
//  Jobing.com
//
//  Created by clicklabs on 6/26/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

//MARk:- Delegate
@objc protocol FacebookDelegate {
	optional func removeAfterGetInfo(isSiguUp:Bool)
    optional func removeAfterGetInfoFromFacebook(email:String, firstName:String, lastName:String, token:String, tokenSecret:String , verifier:String, visitorId:String, image:String, name:String)
}

class Facebook: NSObject {
	
	//MARk:- Delegate
	weak var delegate: FacebookDelegate?
    var tokenString = ""
	
	class var sharedInstance: Facebook {
		struct Static {
			static var onceToken: dispatch_once_t = 0
			static var instance: Facebook? = nil
		}
		dispatch_once(&Static.onceToken) {
			Static.instance = Facebook()
		}
		return Static.instance!
	}
	
	func facebookAuthenticate() -> Bool {
		if (FBSDKAccessToken.currentAccessToken() != nil) {
			return true
		}
		return false
	}
	
    func facebookSignUp(view:UIViewController) {
		
        
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
        loginManager.logInWithReadPermissions(["public_profile","email","user_friends","user_likes", "public_profile", "user_birthday", "user_work_history", "user_education_history"], fromViewController: view, handler: {(result, error) -> Void in
            
            if (error != nil) {
                print(error)
                self.delegate?.removeAfterGetInfo!(false)
            } else {
                
                if result.grantedPermissions != nil {
                    print(result.grantedPermissions)
                    
                    let accessToken = FBSDKAccessToken.currentAccessToken()
                    self.tokenString = accessToken.tokenString
                    //						NSUserDefaults.standardUserDefaults().setValue(self.tokenString, forKey: NSDefaultKeys.AccessToken.rawValue)
                    //						NSUserDefaults.standardUserDefaults().synchronize()
                    print("tokenString: \(self.tokenString)")
                    //self.FacebookAccessToken = tokenString
                    self.facebookInfo(self.tokenString)
                    //loginManager.logOut()
                }
                else
                {
                    self.delegate?.removeAfterGetInfo!(false)
                }
            }
        })

	}
	
	
	func facebookInfo(accessToken:String) {
		
		if (FBSDKAccessToken.currentAccessToken() != nil) {
			var email = ""
			let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,first_name,last_name"], tokenString: tokenString, version: nil, HTTPMethod: "GET")
			req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
				if(error == nil)
				{
                    print("result \(result)")

                    
                    if let emailGet = result.valueForKey("email") {
                        email = emailGet as! String
                    }
                    
                    let id = result.valueForKey("id") as! String

                    let avatarLocation = "https://graph.facebook.com/\(id)/picture?width=640&height=640"

                    self.delegate?.removeAfterGetInfoFromFacebook!(email, firstName: result.valueForKey("first_name")  as! String, lastName: result.valueForKey("last_name")  as! String, token: self.tokenString, tokenSecret: "", verifier: "", visitorId: result.valueForKey("id") as! String, image:avatarLocation, name: result.valueForKey("name") as! String)
				}
				else
				{
					print("error \(error)")
				}
			})
			
//			FBSDKGraphRequest(graphPath: "me?fields=picture.width(720).height(720)", parameters: nil).startWithCompletionHandler{
//				connection,result,error in
//				if !(error != nil) {
//					
//					if let extractedResult = result.valueForKey("picture") as? NSDictionary {
//						print("extractedResult: \(extractedResult)")
//						
//						if let imageData = extractedResult["data"]! as? NSDictionary {
//							//self.FacebookImageUrl = imageData["url"]! as! String
//							//self.FacebookDataCheck++
//							//self.facebookDataVerify()
//						}
//					}
//				}
//			}
		}
	}
	
}
