//
//  Reuseable.swift
//  Jobing.com
//
//  Created by clicklabs on 6/15/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
import AVFoundation
//import RealmSwift

class Reuseable: NSObject {
	
	class var sharedInstance: Reuseable {
		struct Static {
			static var onceToken: dispatch_once_t = 0
			static var instance: Reuseable? = nil
		}
		dispatch_once(&Static.onceToken) {
			Static.instance = Reuseable()
		}
		return Static.instance!
	}
	

    
	func getRadient(view:UIView) -> UIView {
		let gradient: CAGradientLayer = CAGradientLayer()
		gradient.frame = view.frame
		gradient.frame = CGRectMake(0, -20, view.frame.size.width, 15);
		gradient.colors = [UIColor.whiteColor().CGColor, UIColor.blackColor().CGColor]
		view.layer.insertSublayer(gradient, atIndex: 0)
		return view
	}

	func showActionSheet(objView:UIViewController) {
		let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
		
		let deleteAction = UIAlertAction(title: "Resume", style: .Default, handler: {
			(alert: UIAlertAction) -> Void in
		})
		let saveAction = UIAlertAction(title: "Cover Video", style: .Default, handler: {
			(alert: UIAlertAction) -> Void in
		})
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
			(alert: UIAlertAction) -> Void in
		})
	
		optionMenu.addAction(deleteAction)
		optionMenu.addAction(saveAction)
		optionMenu.addAction(cancelAction)
		objView.presentViewController(optionMenu, animated: true, completion: nil)
	}


	func alert(ViewController : UIViewController ,title:String, message : NSString) {
		
		var messageChanged = ""
		if message == ErrorServerConnection || message == ErrorInternetConnection {
			if IJReachability.isConnectedToNetwork() {
				messageChanged = ErrorServerConnection
			} else {
				messageChanged = ErrorInternetConnection
			}
		} else {
			messageChanged = message as String
		}
		
		var titleString = ""
		if title == "" {
			titleString = "OK"
		} else{
			titleString = "OK"
		}
		let alertController = UIAlertController(title: title, message: messageChanged, preferredStyle: .Alert)
		let cancelAction = UIAlertAction(title: titleString, style: UIAlertActionStyle.Cancel) { (action) in
			return
		}
		
		alertController.addAction(cancelAction)
		ViewController.presentViewController(alertController, animated: true) {
		}
	}

    //MARK:- Size of string
    func sizeOfString (string: String, constrainedToWidth width: Double, font:UIFont) -> (CGSize, CGFloat) {
        let sizeOfLable =  NSString(string: string).boundingRectWithSize(CGSize(width: width, height: DBL_MAX),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: font],
            context: nil).size
        
        let noOFLines = (sizeOfLable.height/font.lineHeight)
        print(noOFLines)
        print(sizeOfLable)

        return (sizeOfLable, noOFLines)
    }
    
	func compareDate(fromDate:NSDate) {
		let date1 : NSDate = fromDate
		let date2 : NSDate = NSDate() //initialized by default with the current date
		
		let compareResult = date1.compare(date2)
		if compareResult == NSComparisonResult.OrderedDescending {
			print("\(date1) is later than \(date2)")
		} else if compareResult == NSComparisonResult.OrderedAscending {
			print("\(date2) is later than \(date1)")
		}
		
		let interval = date1.timeIntervalSinceDate(date2)
		print(interval)
	}
	
	func setUserDefaults(areaId:String, jobProfile:String, location:String, isCurrentLocation:Bool) {		
//		NSUserDefaults.standardUserDefaults().setBool(isCurrentLocation, forKey: NSDefaultKeys.isCurrentLocation.rawValue)
//		NSUserDefaults.standardUserDefaults().setValue(areaId, forKey: NSDefaultKeys.AreaId.rawValue)
//		NSUserDefaults.standardUserDefaults().setValue(jobProfile, forKey: NSDefaultKeys.JobProfileToBeSearch.rawValue)
//		NSUserDefaults.standardUserDefaults().setValue(location, forKey: NSDefaultKeys.LocationToBeSearch.rawValue)
//		NSUserDefaults.standardUserDefaults().synchronize()
	}

	func getDocumentDirectoryPath() -> String {
		let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] 
		let getImagePath = paths.stringByAppendingString("/PickedImage.jpeg")
		return getImagePath
	}
	
    func setUserDefaultsForAlertNil() {
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "stringlocationIdForAlerts")
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "stringAreaIdForAlerts")
        NSUserDefaults.standardUserDefaults().synchronize()
    }

	
    // MARK:- GetValue 
    
    func getValueForLocationArray(array:NSArray, index:Int) -> (String, Double, Double) {
        var stringLoc = ""
        var latLoc:Double = 0.0
        var longLoc:Double = 0.0
        
        if let location = array[index]["locations"] as? NSArray {
            //for var i = 0; i < location.count; i++ {
            if location.count > 0 {
                if let loc = location[0].valueForKey("city") as? String {
                    stringLoc = loc
                }else {
                    stringLoc = "location"
                }
                
                if let lat = location[0]["latitude"] as? Double {
                    latLoc = lat
                }
                
                if let lng = location[0]["longitude"] as? Double {
                    longLoc = lng
                }
            }
        }else {
            stringLoc = "location"
            latLoc = 0.0
            longLoc = 0.0
        }
        return (stringLoc, latLoc, longLoc)
    }
    
    func getValueForLogo(array:NSArray, index:Int) -> String {
        var stringLoc = ""
        
        if let logo = array[index]["company_logo"] as? NSDictionary {
            if let image = logo["company_photo_versions"] as? NSDictionary {
                if let version = image["version_125X60"] as? String {
                    stringLoc = version
                }else {
                    stringLoc = ""
                }
            }
        }else {
            stringLoc = ""
        }
        return stringLoc
    }
    
    func getValueForTime(array:NSArray, index:Int) -> (String, Int) {
        var stringLoc = ""
        var stringNew = 0
        
        if let date = array[index]["utc_start_date"] as? String {
            if date != "0001-01-01T00:00:00" {
                let someDate:NSDate = Reuseable.sharedInstance.getNSDate(date)
                stringLoc  = Reuseable.sharedInstance.getTime(Reuseable.sharedInstance.getNSDate(date))
                let calendar = NSCalendar.currentCalendar()
                let flags = NSCalendarUnit.Day
                let components = calendar.components(flags, fromDate: someDate, toDate: NSDate(), options: [])
                
                if components.day < 1 {
                    stringNew = 1
                } else {
                    stringNew = 0
                }
                
            } else {
                stringNew = 0
                stringLoc  = "invalid Time"
            }
        }else {
            stringNew = 0
            stringLoc  = "invalid Time"
        }
        return (stringLoc, stringNew)
    }
    
    
    func getContentValue(array:NSArray, index:Int) ->  (String, String, String) {
        var stringDesc = ""
        var stringStatus = ""
        var stringAmount = ""
        
        if let name = array[index].valueForKey("content_short") as? NSString {
            stringDesc = name as String
        }
        
        if let name = array[index].valueForKey("statuses") as? NSArray {
            stringStatus = (name[0] as? String)!
            if stringStatus == "" {
                stringStatus = "Full Time"
            }
        } else {
            stringStatus = "Full Time"
        }
        
        if let name = array[index].valueForKey("salary_amount") as? NSString {
            stringAmount = name as String
            stringAmount = "0.0"
        }else {
            stringAmount = "0.0"
        }
        return (stringDesc, stringStatus, stringAmount)
    }
    
	func getDate(tempStr:String) -> String {
		
		let formatter = NSDateFormatter()
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
		formatter.timeZone = NSTimeZone(name: "UTC")
		var dateString = formatter.dateFromString(tempStr)
		
		if dateString == nil {
			formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
			dateString = formatter.dateFromString(tempStr)!
		}
		
		let timeZone = NSTimeZone.defaultTimeZone()
		let timeDifference = timeZone.secondsFromGMT / 60
		let timeComponent = NSDateComponents()
		timeComponent.minute = timeDifference
		let newdate = NSCalendar.currentCalendar().dateByAddingComponents(timeComponent, toDate: dateString!, options: NSCalendarOptions())
		
		formatter.dateFormat = "MMMM dd, yyyy"
		formatter.timeZone = NSTimeZone.defaultTimeZone()
		//formatter.timeStyle = NSDateFormatterStyle.ShortStyle

		return formatter.stringFromDate(newdate!)
	}
	
	func getNSDate(tempStr:String) -> NSDate {
		
		let formatter = NSDateFormatter()
		formatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSS"
		formatter.timeZone = NSTimeZone(name: "UTC")

		var dateString = formatter.dateFromString(tempStr)
		
		if dateString == nil {
			formatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
			dateString = formatter.dateFromString(tempStr)!
		}
		
		return dateString!
	}
	
	func getTime(date:NSDate) -> String {
		//print(date)
		
		
		let timeZone = NSTimeZone.defaultTimeZone()
		let timeDifference = timeZone.secondsFromGMT / 60
		let timeComponent = NSDateComponents()
		timeComponent.minute = timeDifference
		let newdate = NSCalendar.currentCalendar().dateByAddingComponents(timeComponent, toDate: date, options: NSCalendarOptions())

		let calendar = NSCalendar.currentCalendar()
		let components = calendar.components([.Year, .Month, .Day, .Hour, .Minute], fromDate: newdate!, toDate: NSDate(), options: [])
		let year = components.year
		let month = components.month
		let days = components.day
		let hour = components.hour
		let minutes = components.minute
		var returnString = ""
		if year > 0 {
			if year == 1 {
				returnString = "\(year) year ago"
			} else {
				returnString = "\(year) years ago"
			}
		} else if month > 0 {
			if month == 1 {
				returnString = "\(month) month ago"
			} else {
				returnString = "\(month) months ago"
			}
		} else if days > 0 {
			if days == 1 {
				returnString = "\(days) day ago"
			} else {
				returnString = "\(days) days ago"
			}
		} else if hour > 0 {
			if hour == 1 {
				returnString = "\(hour) hour ago"
			} else {
				returnString = "\(hour) hours ago"
			}
		} else if minutes > 0 {
			if minutes > 10 || minutes == 0 {
				returnString = "\(minutes) minutes ago"
			} else {
				returnString = "\(minutes) minute ago"
			}
		}
		return returnString
	}

	func convertTime(UTCDate: String) -> String {
		
		let UTCDate = getNSDate(UTCDate)
		let dateFormatter = NSDateFormatter()
		dateFormatter.timeZone = NSTimeZone.systemTimeZone()
		dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
		let localDate = dateFormatter.stringFromDate(UTCDate)
		
		let currentDate = NSDate()
		dateFormatter.timeZone = NSTimeZone.systemTimeZone()
//		let currentDate_Time = dateFormatter.stringFromDate(currentDate)
		
		let seconds = Int(currentDate.timeIntervalSinceDate(dateFormatter.dateFromString(localDate)!))
		
		let time = Double((Double(seconds) / 60.0) / 60.0) / 24.0
		
		if time > 1 {
			
			dateFormatter.dateFormat = "dd"
			
			return  dateFormatter.stringFromDate(UTCDate)
			
		} else if time <= 1 {
			
			if (seconds / 60) / 60 > 0 {
				
				return "\((seconds / 60) / 60) hrs ago"
				
			} else {
				
				if seconds < 60 {
					if seconds <= 0 {
						return "Just now."
					} else if(seconds > 0) {
						return "\(seconds) sec ago"
					}
				}
				
				return "\(seconds / 60) min ago"
			}
		}
		
		return ""
	}
	
	func addCustomeViewTouch(obj:UIViewController, isAdding:Bool) -> UIView {
		let customView = UIView()

		if isAdding == true {
			customView.frame = CGRectMake(0, 0, ScreenConstant.width, ScreenConstant.height)
			obj.view.addSubview(customView)
		} else {
			customView.removeFromSuperview()
		}
		return customView
	}

    func openGallery(viewController : UIViewController , picker : UIImagePickerController) {
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker.mediaTypes = [kUTTypeImage as String , kUTTypeGIF as String]
        viewController.presentViewController(picker, animated: true, completion: nil)
    }
    
    func openGalleryForProfilePic(viewController : UIViewController , picker : UIImagePickerController) {
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker.mediaTypes = [kUTTypeImage as String ,kUTTypeMovie as String, kUTTypeGIF as String]
        viewController.presentViewController(picker, animated: true, completion: nil)

    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func checkPermissionVideo() -> AVAuthorizationStatus {
        
        let status = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        if(status == AVAuthorizationStatus.Authorized) {
            return AVAuthorizationStatus.Authorized
        } else if(status == AVAuthorizationStatus.Denied){
            return AVAuthorizationStatus.Denied
        } else if(status == AVAuthorizationStatus.Restricted){
            return AVAuthorizationStatus.Restricted
        } else {
            return AVAuthorizationStatus.NotDetermined
        }
    }
    
    func checkPermissionAudio() -> AVAuthorizationStatus {
        
        let status = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeAudio)
        if(status == AVAuthorizationStatus.Authorized) {
            return AVAuthorizationStatus.Authorized
            
        } else if(status == AVAuthorizationStatus.Denied){
            return AVAuthorizationStatus.Denied
            
        } else if(status == AVAuthorizationStatus.Restricted){
            return AVAuthorizationStatus.Restricted
            
        } else {
            return AVAuthorizationStatus.NotDetermined
            
        }
    }
    
    
    func openCameraForPhoto(viewController : UIViewController , picker : UIImagePickerController) {
        
        // if the device has camera provision open it otherwise open gallery
        
        let persmission = checkPermissionVideo()
        
        if persmission == AVAuthorizationStatus.Authorized {
            
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
                picker.sourceType = UIImagePickerControllerSourceType.Camera
                //picker.mediaTypes = [kUTTypeMovie as String]
                picker.showsCameraControls = true
                //picker.cameraDevice = UIImagePickerControllerCameraDevice.Front
                //picker.videoMaximumDuration = 30
                //picker.videoQuality = UIImagePickerControllerQualityType.TypeMedium
                viewController.presentViewController(picker, animated: true, completion: nil)
            } else {
                openGallery(viewController, picker: picker)
            }
            
        } else if  persmission == AVAuthorizationStatus.NotDetermined {
            
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: {
                
                bool in
                
                if bool {
                    self.openCameraForPhoto(viewController, picker: picker)
                    
                } else {
                    self.showAlert(viewController, message: "This app does not have access to camera. You can enable access in Privacy Settings.")
                }
                
            })
            
        }else {
            showAlert(viewController, message: "This app does not have access to camera. You can enable access in Privacy Settings.")
        }
    }

    
    func openCamera(viewController : UIViewController , picker : UIImagePickerController) {
        
        // if the device has camera provision open it otherwise open gallery
        
        let persmission = checkPermissionVideo()
        
        if persmission == AVAuthorizationStatus.Authorized {
            
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
                picker.sourceType = UIImagePickerControllerSourceType.Camera
                picker.mediaTypes = [kUTTypeMovie as String]
                picker.showsCameraControls = true
                picker.cameraDevice = UIImagePickerControllerCameraDevice.Front
                picker.videoMaximumDuration = 30
                picker.videoQuality = UIImagePickerControllerQualityType.TypeMedium
                viewController.presentViewController(picker, animated: true, completion: nil)
            } else {
                openGallery(viewController, picker: picker)
            }
            
        } else if  persmission == AVAuthorizationStatus.NotDetermined {
            
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: {
                
                bool in
                
                if bool {
                    self.openCamera(viewController, picker: picker)
                    
                } else {
                    self.showAlert(viewController, message: "This app does not have access to camera. You can enable access in Privacy Settings.")
                }
                
            })
            
        }else {
            showAlert(viewController, message: "This app does not have access to camera. You can enable access in Privacy Settings.")
        }
    }
    
    func openVideo(viewController : UIViewController , picker : UIImagePickerController) {
        
        let persmission = checkPermissionVideo()
        
        let permissionAudio = checkPermissionAudio()
        
        if permissionAudio == AVAuthorizationStatus.Authorized && persmission == AVAuthorizationStatus.Authorized {
            
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
                
                picker.sourceType = UIImagePickerControllerSourceType.Camera
                
                picker.mediaTypes = [kUTTypeMovie as String]
                
                picker.modalPresentationCapturesStatusBarAppearance = false
                
                picker.videoMaximumDuration = 300
                
                picker.videoQuality = UIImagePickerControllerQualityType.TypeMedium
                
                viewController.presentViewController(picker, animated: true, completion: nil)
                
            }  else {
                
                openGallery(viewController, picker: picker)
                
            }
            
        } else if  persmission == AVAuthorizationStatus.NotDetermined {
            
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: {
                
                bool in
                
                if bool {
                    
                    if  permissionAudio == AVAuthorizationStatus.NotDetermined {
                        
                        AVCaptureDevice.requestAccessForMediaType(AVMediaTypeAudio, completionHandler: {
                            
                            bool in
                            
                            if bool {
                                
                                self.openVideo(viewController, picker: picker)
                                
                            } else {
                                
                                self.showAlert(viewController, message: "This app does not have access to Microphone. You can enable access in Privacy Settings.")
                                
                            }
                            
                        })
                        
                    }
                    
                } else {
                    
                    self.showAlert(viewController, message: "This app does not have access to camera. You can enable access in Privacy Settings.")
                    
                }
                
            })
            
        } else if  permissionAudio == AVAuthorizationStatus.NotDetermined {
            
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeAudio, completionHandler: {
                bool in
                
                if bool {
                    self.openVideo(viewController, picker: picker)
                } else {
                    
                    self.showAlert(viewController, message: "This app does not have access to Microphone. You can enable access in Privacy Settings.")
                }
            })
        } else {
            showAlert(viewController, message: "This app does not have access to either camera or microphone or both. You can enable access in Privacy Settings.")
        }
    }
    
    func showAlert(viewController:UIViewController, message:String) {
        
//        if versionIOS.oSVersion == 8.0 {
//            let alert = UIAlertController(title: "",
//                message: message, preferredStyle: .Alert)
//            let dismissAction = UIAlertAction(title: "Dismiss", style: .Destructive, handler: nil)
//            alert.addAction(dismissAction)
//            viewController.presentViewController(alert, animated: true, completion: nil)
//        } else {
//            // Fallback on earlier versions
//            let alert = UIAlertView.init(title: "", message: message, delegate: nil,
//                cancelButtonTitle: "Dismiss")
//            alert.show()
//        }
    }
    
    func openALink(stringUrl:String, controller:UIViewController) {
        
        if IJReachability.isConnectedToNetwork() {
			
		} else {
            Reuseable().alert(controller, title:"Error", message: ErrorInternetConnection)
        }
    }
    
    func getDataFromUrl(urL:NSURL, completion: ((data: NSData?) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(urL) { (data, response, error) in
            completion(data: data)
            }.resume()
    }
    
    typealias returnImage = (UIImage?) -> Void
    
    func downloadImage(url:NSURL, onCompletion:returnImage){
        //print("url: \(url)")
        
        guard url != "" else {
            return onCompletion(UIImage(named: "company_default"))
        }
        
        getDataFromUrl(url) { data in
            dispatch_async(dispatch_get_main_queue()) {
                return onCompletion(UIImage(data: data!))
            }
        }
    }
    
}




extension String {
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersInString: matchCharacters).invertedSet
        return self.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
    }
}
