//
//  Extensions.swift
//  Jobing.com
//
//  Created by clicklabs on 7/8/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class Extensions: NSObject {
	
	
}

extension UIButton {
    
    @IBInspectable var Shadow: Bool {
        
        get {
            return true
        } set {
            layer.cornerRadius = 0
            layer.shadowOffset = CGSize(width: 0, height:2.5)
            layer.shadowColor =  ColorConstant.ShadowColor.CGColor //UIColor.redColor().CGColor //
            layer.shadowOpacity = 0.5
            layer.shadowRadius = 3.0
        }
    }
}

extension UITextField {
    
    @IBInspectable var ShadowTF: Bool {
        
        get {
            return true
        } set {
            layer.borderColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1).CGColor
            layer.borderWidth = 1.0
            borderStyle = .None
            layer.cornerRadius = 0
        }
    }
    @IBInspectable var leftViewSet: Bool {
        
        get {
            return true
        } set {
            let leftViewM = UILabel()
            leftViewM.frame = CGRectMake(10, 0, 7, 26)
            leftView = leftViewM
            leftViewMode = .Always
            contentVerticalAlignment = .Center
        }
    }
}

//extension UIViewController {
//	
//	func registerForKeyboardNotifications() {
//		let notificationCenter = NSNotificationCenter.defaultCenter()
//		notificationCenter.addObserver(self,
//			selector: Selector("keyboardWillBeShown:"),
//			name: UIKeyboardWillShowNotification,
//			object: nil)
//		notificationCenter.addObserver(self,
//			selector: Selector("keyboardWillBeHidden:"),
//			name: UIKeyboardWillHideNotification,
//			object: nil)
//	}
//	
//	
//}
//
//extension UIView {
//	
//	func registerForKeyboardNotifications() {
//		let notificationCenter = NSNotificationCenter.defaultCenter()
//		notificationCenter.addObserver(self,
//			selector: Selector("keyboardWillBeShown:"),
//			name: UIKeyboardWillShowNotification,
//			object: nil)
//		notificationCenter.addObserver(self,
//			selector: Selector("keyboardWillBeHidden:"),
//			name: UIKeyboardWillHideNotification,
//			object: nil)
//	}
//}

extension UIScrollView {
	func keyboardVisible(sender: NSNotification,activeTextField: UITextField) {
		let info: NSDictionary = sender.userInfo!
		let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as! NSValue
		let keyboardSize: CGSize = value.CGRectValue().size
		//keyboardSize.height -= 127
		let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
		self.contentInset = contentInsets
		self.scrollIndicatorInsets = contentInsets
		
		// If active text field is hidden by keyboard, scroll it so it's visible
		var aRect: CGRect = self.frame
		aRect.size.height -= keyboardSize.height
		let activeTextFieldRect: CGRect? = activeTextField.frame
		let activeTextFieldOrigin: CGPoint? = activeTextFieldRect?.origin
		if (!CGRectContainsPoint(aRect, activeTextFieldOrigin!)) {
			self.scrollRectToVisible(activeTextFieldRect!, animated:true)
		}
	}
	
	func keyboardHidden(sender: NSNotification) {
		let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
		self.contentInset = contentInsets
		self.scrollIndicatorInsets = contentInsets
	}
}