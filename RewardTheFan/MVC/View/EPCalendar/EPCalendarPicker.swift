//
//  EPCalendarPicker.swift
//  EPCalendar
//
//  Created by Prabaharan Elangovan on 02/11/15.
//  Copyright © 2015 Prabaharan Elangovan. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

@objc public protocol EPCalendarPickerDelegate{
    optional    func epCalendarPicker(_: EPCalendarPicker, didCancel error : NSError)
    optional    func epCalendarPicker(_: EPCalendarPicker, didSelectDate date : NSDate)
    optional    func epCalendarPicker(_: EPCalendarPicker, didSelectMultipleDate dates : [NSDate])
}

public class EPCalendarPicker: UICollectionViewController {

    public var calendarDelegate : EPCalendarPickerDelegate?
    public var multiSelectEnabled: Bool
    public var showsTodaysButton: Bool = true
    private var arrSelectedDates = [NSDate]()
    
    // new options
    public var startDate: NSDate?
    public var hightlightsToday: Bool = true
    public var hideDaysFromOtherMonth: Bool = false
    public var barTintColor: UIColor
    
    public var backgroundImage: UIImage?
    public var backgroundColor: UIColor?
    
    private(set) public var startYear: Int
    private(set) public var endYear: Int
    
    var startingRow : Int = 0
    var startingSection : Int = 0
    var endingRow : Int = 0
    var endingSection : Int = 0
    var selectionFlag : Bool = true
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // setup collectionview
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
        self.collectionView?.backgroundColor = UIColor.clearColor()
        self.collectionView?.showsHorizontalScrollIndicator = false
        self.collectionView?.showsVerticalScrollIndicator = false

        // Register cell classes
        self.collectionView!.registerNib(UINib(nibName: "EPCalendarCell1", bundle: NSBundle(forClass: EPCalendarPicker.self )), forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView!.registerNib(UINib(nibName: "EPCalendarHeaderView", bundle: NSBundle(forClass: EPCalendarPicker.self )), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.scrollToToday()
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("selectedDates") != nil {
            arrSelectedDates = NSUserDefaults.standardUserDefaults().objectForKey("selectedDates") as! [NSDate]
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("startingRow") != nil {
            startingRow = NSUserDefaults.standardUserDefaults().objectForKey("startingRow") as! Int
            NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(self.reloadInputViews), userInfo: nil, repeats: false)
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("endingRow") != nil {
            endingRow = NSUserDefaults.standardUserDefaults().objectForKey("endingRow") as! Int
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("startingSection") != nil {
            startingSection = NSUserDefaults.standardUserDefaults().objectForKey("startingSection") as! Int
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("endingSection") != nil {
            endingSection = NSUserDefaults.standardUserDefaults().objectForKey("endingSection") as! Int
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("selectionFlag") != nil {
            selectionFlag = NSUserDefaults.standardUserDefaults().objectForKey("selectionFlag") as! Bool
        }
        
        if backgroundImage != nil {
            self.collectionView!.backgroundView =  UIImageView(image: backgroundImage)
        } else if backgroundColor != nil {
            self.collectionView?.backgroundColor = backgroundColor
        } else {
            self.collectionView?.backgroundColor = UIColor.whiteColor()
        }
        
//        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
//        layout.itemSize = CGSize(width: UIScreen.mainScreen().bounds.width/7, height: UIScreen.mainScreen().bounds.width/7)
//        layout.minimumInteritemSpacing = 0
//        layout.minimumLineSpacing = 1
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public convenience init(){
        self.init(startYear: EPDefaults.startYear, endYear: EPDefaults.endYear, multiSelection: EPDefaults.multiSelection, selectedDates: nil);
    }
    
    public convenience init(startYear: Int, endYear: Int) {
        self.init(startYear:startYear, endYear:endYear, multiSelection: EPDefaults.multiSelection, selectedDates: nil)
    }
    
    public convenience init(multiSelection: Bool) {
        self.init(startYear: EPDefaults.startYear, endYear: EPDefaults.endYear, multiSelection: multiSelection, selectedDates: nil)
    }
    
    public convenience init(startYear: Int, endYear: Int, multiSelection: Bool) {
        self.init(startYear: EPDefaults.startYear, endYear: EPDefaults.endYear, multiSelection: multiSelection, selectedDates: nil)
    }
 
    public init(startYear: Int, endYear: Int, multiSelection: Bool, selectedDates: [NSDate]?) {
        
        self.startYear = startYear
        self.endYear = endYear
        
        self.multiSelectEnabled = multiSelection
        
        //Text color initializations
        self.barTintColor = EPDefaults.barTintColor

        //Layout creation
        let layout = UICollectionViewFlowLayout()
        //layout.sectionHeadersPinToVisibleBounds = true  // If you want make a floating header enable this property(Avaialble after iOS9)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        var headerSize = CGSizeMake(100,70)
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            headerSize = CGSizeMake(200,140)
        } else {
            headerSize = CGSizeMake(100,70)
        }
        
        layout.headerReferenceSize = headerSize
        if let _ = selectedDates  {
            self.arrSelectedDates.appendContentsOf(selectedDates!)
        }
        super.init(collectionViewLayout: layout)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: UICollectionViewDataSource

    override public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if startYear > endYear {
            return 0
        }
        let numberOfMonths = 12 * (endYear - startYear) + 12
        return numberOfMonths
    }


    override public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let startDate = NSDate(year: startYear, month: 1, day: 1)
        let firstDayOfMonth = startDate.dateByAddingMonths(section)
        let addingPrefixDaysWithMonthDyas = ( firstDayOfMonth.numberOfDaysInMonth() + firstDayOfMonth.weekday() - NSCalendar.currentCalendar().firstWeekday )
        let addingSuffixDays = addingPrefixDaysWithMonthDyas%7
        var totalNumber  = addingPrefixDaysWithMonthDyas
        if addingSuffixDays != 0 {
            totalNumber = totalNumber + (7 - addingSuffixDays)
        }
        
        return totalNumber
    }

    override public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! EPCalendarCell1
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.clearColor().CGColor
        
        let calendarStartDate = NSDate(year:startYear, month: 1, day: 1)
        let firstDayOfThisMonth = calendarStartDate.dateByAddingMonths(indexPath.section)
        let prefixDays = ( firstDayOfThisMonth.weekday() - NSCalendar.currentCalendar().firstWeekday)
        
        if indexPath.row >= prefixDays {
            cell.isCellSelectable = true
            let currentDate = firstDayOfThisMonth.dateByAddingDays(indexPath.row-prefixDays)
            let nextMonthFirstDay = firstDayOfThisMonth.dateByAddingDays(firstDayOfThisMonth.numberOfDaysInMonth()-1)
            
            cell.currentDate = currentDate
            cell.lblDay.text = "\(currentDate.day())"
            
            if arrSelectedDates.filter({ $0.isDateSameDay(currentDate)
            }).count > 0 && (firstDayOfThisMonth.month() == currentDate.month()) {
                cell.selectedForLabelColor(UIColor(red:39.0/255, green:133.0/255, blue:250.0/255, alpha:1.0))
            } else {
                
                cell.deSelectedForLabelColor(UIColor.blackColor())
               
                if (currentDate > nextMonthFirstDay) {
                    cell.isCellSelectable = false
                    if hideDaysFromOtherMonth {
                        cell.lblDay.textColor = UIColor.clearColor()
                    } else {
                        cell.lblDay.textColor = UIColor.blackColor()
                    }
                }
                if currentDate.isToday() && hightlightsToday {
                    cell.lblDay.textColor = UIColor.blackColor()

                }
               
                if startDate != nil {
                    if NSCalendar.currentCalendar().startOfDayForDate(cell.currentDate) < NSCalendar.currentCalendar().startOfDayForDate(startDate!) {
                        cell.isCellSelectable = false
                        cell.lblDay.textColor = UIColor(red:0.0/255, green:0.0/255, blue:0.0/255, alpha:0.5)
                    }
                }
            }
        } else {
            cell.deSelectedForLabelColor(UIColor.blackColor())
            cell.isCellSelectable = false
            let previousDay = firstDayOfThisMonth.dateByAddingDays(-( prefixDays - indexPath.row))
            cell.currentDate = previousDay
            cell.lblDay.text = "\(previousDay.day())"
            if hideDaysFromOtherMonth {
                cell.lblDay.textColor = UIColor.clearColor()
            } else {
                cell.lblDay.textColor = UIColor(red:0.0/255, green:0.0/255, blue:0.0/255, alpha:0.5)
            }
        }

        if indexPath.section >= startingSection && indexPath.section <= endingSection {
            if !(startingSection == endingSection) {
                if !(indexPath.section == endingSection) {
                    if indexPath.section == startingSection {
                        if indexPath.row >= startingRow {
                            cell.backgroundColor = UIColor(red:39.0/255, green:133.0/255, blue:250.0/255, alpha:0.5)
                        } else {
                            cell.backgroundColor = UIColor.clearColor()
                        }
                    } else {
                        cell.backgroundColor = UIColor(red:39.0/255, green:133.0/255, blue:250.0/255, alpha:0.5)
                    }
                } else {
                    if indexPath.row <= endingRow {
                        cell.backgroundColor = UIColor(red:39.0/255, green:133.0/255, blue:250.0/255, alpha:0.5)
                    } else {
                        cell.backgroundColor = UIColor.clearColor()
                    }
                }
            } else {
                if indexPath.row >= startingRow && indexPath.row <= endingRow {
                    cell.backgroundColor = UIColor(red:39.0/255, green:133.0/255, blue:250.0/255, alpha:0.5)
                }
            }
        } else {
            cell.backgroundColor = UIColor.clearColor()
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
//        let rect = UIScreen.mainScreen().bounds
        let screenWidth = self.view.frame.width
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return CGSizeMake((screenWidth/7)-1.2, (screenWidth/7)-1.2);
        } else {
            return CGSizeMake((screenWidth/7)-1.1, (screenWidth/7)-1.1);
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(0, 0, 0, 0); //top,left,bottom,right
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1
    }
    
    override public func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {

        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "Header", forIndexPath: indexPath) as! EPCalendarHeaderView
            
            let startDate = NSDate(year: startYear, month: 1, day: 1)
            let firstDayOfMonth = startDate.dateByAddingMonths(indexPath.section)
            
            header.lblTitle.text = firstDayOfMonth.monthNameFull()
         //   header.lblTitle.textColor = monthTitleColor
          //  header.updateWeekdaysLabelColor(weekdayTintColor)
         //   header.updateWeekendLabelColor(weekendTintColor)
            header.backgroundColor = UIColor.clearColor()
            
            return header;
        }
        return UICollectionReusableView()
    }
    
    override public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if selectionFlag
        {
            arrSelectedDates.removeAll()
            self.startingRow = 0
            self.startingSection = 0
            self.endingRow = 0
            self.endingSection = 0
        }
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! EPCalendarCell1
        if !multiSelectEnabled {
            calendarDelegate?.epCalendarPicker!(self, didSelectDate: cell.currentDate)
            cell.selectedForLabelColor(UIColor(red:39.0/255, green:133.0/255, blue:250.0/255, alpha:1.0))
            dismissViewControllerAnimated(true, completion: nil)
            return
        }
        
        if cell.isCellSelectable! {
            
            if arrSelectedDates.contains(cell.currentDate) {
                
            } else {
                
                arrSelectedDates.append(cell.currentDate)
                
                NSUserDefaults.standardUserDefaults().setObject(arrSelectedDates, forKey: "selectedDates")
                
                cell.selectedForLabelColor(UIColor(red:39.0/255, green:133.0/255, blue:250.0/255, alpha:1.0))
                
                if cell.currentDate.isToday() {
                    cell.setTodayCellColor(UIColor(red:39.0/255, green:133.0/255, blue:250.0/255, alpha:1.0))
                }
                
                if selectionFlag {
                    
                    startingRow = indexPath.row
                    startingSection = indexPath.section
                    selectionFlag = false
                } else {
                    
                    if indexPath.section < startingSection {
                        
                        arrSelectedDates.removeFirst()
                        startingRow = indexPath.row
                        startingSection = indexPath.section
                        
                    } else {
                        
                        if indexPath.section != startingSection {
                            
                            endingRow = indexPath.row
                            endingSection = indexPath.section
                            selectionFlag = true
                        } else {
                            
                            if indexPath.row < startingRow {
                                
                                arrSelectedDates.removeFirst()
                                startingRow = indexPath.row
                                startingSection = indexPath.section
                                
                            } else {
                                
                                endingRow = indexPath.row
                                endingSection = indexPath.section
                                selectionFlag = true
                            }
                        }
                    }
                    
                }
                NSNotificationCenter.defaultCenter().postNotificationName("dates", object: arrSelectedDates)
                NSUserDefaults.standardUserDefaults().setInteger(startingRow, forKey: "startingRow")
                NSUserDefaults.standardUserDefaults().setInteger(endingRow, forKey: "endingRow")
                NSUserDefaults.standardUserDefaults().setInteger(startingSection, forKey: "startingSection")
                NSUserDefaults.standardUserDefaults().setInteger(endingSection, forKey: "endingSection")
                NSUserDefaults.standardUserDefaults().setBool(selectionFlag, forKey: "selectionFlag")
                
                self.collectionView?.reloadData()
            }
        }
    }
    
    //MARK: Button Actions
    
    internal func onTouchCancelButton() {
       //TODO: Create a cancel delegate
        calendarDelegate?.epCalendarPicker!(self, didCancel: NSError(domain: "EPCalendarPickerErrorDomain", code: 2, userInfo: [ NSLocalizedDescriptionKey: "User Canceled Selection"]))
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func doneButton() ->  NSArray{
        return arrSelectedDates
    }
    
    internal func onTouchDoneButton() {
        //gathers all the selected dates and pass it to the delegate
        calendarDelegate?.epCalendarPicker!(self, didSelectMultipleDate: arrSelectedDates)
        dismissViewControllerAnimated(true, completion: nil)
    }

    internal func onTouchTodayButton() {
        scrollToToday()
    }
    
    
    public func scrollToToday () {
        let today = NSDate()
        scrollToMonthForDate(today)
    }
    
    public func scrollToMonthForDate (date: NSDate) {

        let month = date.month()
        let year = date.year()
        let section = ((year - startYear) * 12) + month
        let indexPath = NSIndexPath(forRow:1, inSection: section-1)
        
        self.collectionView?.scrollToIndexpathByShowingHeader(indexPath)
    }
    
    
}
