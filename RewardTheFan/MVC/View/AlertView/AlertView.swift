//
//  AlertView.swift
//  RewardtheFan
//
//  Created by 42Works-Worksys2 on 07/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class AlertView: UIView {

    @IBOutlet weak var alertBk: UIView!
    @IBOutlet weak var midView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var subHeaderLabel: UILabel!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noBtnwidth: NSLayoutConstraint!
    
    @IBAction func noAction(sender: AnyObject) {
    }
    
    @IBAction func yesAction(sender: AnyObject) {
    }
}
