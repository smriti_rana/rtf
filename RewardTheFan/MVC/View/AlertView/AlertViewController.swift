
//
//  AlertViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 22/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {

    @IBOutlet var titleLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var ButtonLeading: NSLayoutConstraint!
    
    @IBOutlet var messageLabel: UILabel!
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var yesOrOkButton: UIButton!
    var message : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(red:0.0/255, green:0.0/255, blue:0.0/255, alpha:0.3)
        ButtonLeading.constant = 8
        yesOrOkButton.setTitle("Ok", forState: .Normal)
        messageLabel.textColor = UIColor.blackColor()
        messageLabel.text = message
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func yesOrOkButtonTapped(sender: AnyObject) {
        if message == "Please login to select the Favorites" {
            dismissViewControllerAnimated(true, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("alertMessage", object: nil)
        } else if message == "Please login to be a Super Fan" {
            dismissViewControllerAnimated(true, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("alertMessageSuperFan", object: nil)
        }
        else if message == "Please add billing/shipping address first."{
            dismissViewControllerAnimated(true, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("alertMessagePreOrder", object: nil)
        }
        else if message == "Billing address updated successfully." ||  message ==  "Billing address added successfully. " ||  message ==  "Shipping address updated successfully." ||  message ==  "Shipping address added successfully." {
            dismissViewControllerAnimated(true, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("alertMessageBilling", object: nil)
        }
        else if message == "Please login to view" {
            dismissViewControllerAnimated(true, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("alertMessageforExplore", object: nil)
        }
        else if message == "Session has expired." {
            dismissViewControllerAnimated(true, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("TimerExpires", object: nil)
        }
        else if message == "Please login." {
            
            dismissViewControllerAnimated(true, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("loginAlertMessage", object: nil)
        }else {
            dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
}
