//
//  LoadingIndicatorView.swift
//  C4C
//
//  Created by Smriti Thakur on 13/12/15.
//  Copyright © 2015 Smriti. All rights reserved.
//

import UIKit
import Gifu

class LoadingIndicatorView: UIView {
	
//	@IBOutlet var imageView: AnimatableImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
	
    @IBOutlet var imageView: AnimatableImageView!

    
	func StartLoad() {
        imageView.animateWithImage(named:"loader.gif")
        imageView.startAnimatingGIF()
	}
	
	func StopLoad() {
        imageView.stopAnimatingGIF()
	}
}
