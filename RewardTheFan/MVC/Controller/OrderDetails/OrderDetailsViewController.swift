//
//  OrderDetailsViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 07/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class OrderDetailsViewController: BaseViewController {

    //MARK:-Properties
    //MARK:-
    
    @IBOutlet var monthLabel: UILabel!
    
    @IBOutlet var daylabel: UILabel!
    
    @IBOutlet var yearLabel: UILabel!
    
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    @IBOutlet var weekDayLabel: UILabel!
    
    @IBOutlet var datesView: UIView!
    
    @IBOutlet var eventNameLabel: UILabel!
    
    @IBOutlet var eventTimeAndVenueLabel: UILabel!
    
    @IBOutlet var deliveryMethodLabel: UILabel!
    
    @IBOutlet var quantityLabel: UILabel!
    
    @IBOutlet var priceLabel: UILabel!
    
    @IBOutlet var loyalityRewardPointsLabel: UILabel!
    
    @IBOutlet var zoneLabel: UILabel!
    
    @IBOutlet var zoneDescriptionLabel: UILabel!
    
    @IBOutlet var billingAddressFirstNameLastNameLabel: UILabel!
    
    @IBOutlet var billingAddressStreetAddressLabel: UILabel!
    
    @IBOutlet var billingAddressCityStateLabel: UILabel!
    
    @IBOutlet var billingAddressCountryLabel: UILabel!
    
    @IBOutlet var shippingAddressFirstNameLastNameLabel: UILabel!
    
    @IBOutlet var shippingAddressStreetAddressLabel: UILabel!
    
    @IBOutlet var shippingAddressCityStateLabel: UILabel!
    
    @IBOutlet var shippingAddressCountryLabel: UILabel!
    
    @IBOutlet var cardTypeImageButton: UIButton!
    
    @IBOutlet var cardNumberLastDigitsLabel: UILabel!
    
    @IBOutlet var loyalityPointsUsedLabel: UILabel!
    
    @IBOutlet var submitButtonHeightConstraint: NSLayoutConstraint!
    
    var orderId : Int = 0
    
    var apiCallFlag : Bool = true
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        self.submitButtonHeightConstraint.constant = 0
    }

    override func viewDidAppear(animated: Bool) {
        
        if apiCallFlag {
            
            // *** set Indicator loading *** //
            self.setLoadingIndicator(self.view)
            
            // *** API Call *** //
            getCustomerOrderByOrderIdApiCall()
            
            // *** set the flag *** //
            apiCallFlag = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- GetCustomerOrderByOrderId API Call
    //MARK:-
    
    func getCustomerOrderByOrderIdApiCall() {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        guard IJReachability.isConnectedToNetwork() else {
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        let getParam = [
            
            "productType" : productType,
            "configId" : configId,
            "orderId" : (orderId)
        ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.getCustomerOrderByOrderId("GetCustomerOrderByOrderId.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            // *** stop Indicator *** //
            self.stopIndicator()
            
            if isTrue == true {
                
                print(responseDisctionary!)
                
                if let customerOrders = responseDisctionary!.objectForKey("customerOrders") as? NSMutableArray {
                    
                    if let details = customerOrders.objectAtIndex(0) as? NSDictionary {
                        
                        // *** set event name *** //
                        if let eventName = details.objectForKey("eventName") as? String {
                            
                            self.eventNameLabel.text = eventName
                        }
                        
                        // *** set event Time and venue name *** //
                        if let venueName = details.objectForKey("venueName") as? String {
                            
                            self.eventTimeAndVenueLabel.text = venueName
                        }
                        
                        // *** set delivery method *** //
                        if let shippingMethod = details.objectForKey("shippingMethod") as? String {
                            
                            self.deliveryMethodLabel.text = shippingMethod
                        }
                        
                        // *** set quantity *** //
                        if let qty = details.objectForKey("qty") as? Int {
                            
                            if qty == 1 {
                                
                                self.quantityLabel.text = "\(qty)"
                            } else {
                                
                                self.quantityLabel.text = "\(qty) tickets seated together"
                            }
                        }
                        
                        // *** set price label *** //
                        if let ticketPrice = details.objectForKey("ticketPrice") as? Int {
                            
                            self.priceLabel.text = "US $\(ticketPrice)"
                        }
                        
                        // *** set zone *** //
                        if let section = details.objectForKey("section") as? String {
                            
                            self.zoneLabel.text = section
                        }
                        
                        // *** set zone description *** //
                        self.zoneDescriptionLabel.text = "No description available"
                        
                        // *** set reward points *** //
                        if let customerLoyaltyHistory = details.objectForKey("customerLoyaltyHistory") as? NSDictionary {
                            
                            // ** set points ** //
                            if let pointsEarned = customerLoyaltyHistory.objectForKey("pointsEarned") as? Int {
                                
                                self.loyalityRewardPointsLabel.text = "\(pointsEarned)"
                            }
                        }
                        
                        // *** set billing address and shipping address *** //
                        if let customerOrderDetail = details.objectForKey("customerOrderDetail") as? NSDictionary {
                            
                            // *** set first name and last name *** //
                            var firstName : String = ""
                            var lastName : String = ""
                            
                            // *** get first name *** //
                            if let billingFirstName = customerOrderDetail.objectForKey("billingFirstName") as? String {
                                firstName = billingFirstName
                            }
                            
                            // *** set last Name *** //
                            if let billingLastName = customerOrderDetail.objectForKey("billingLastName") as? String {
                                lastName = billingLastName
                            }
                            
                            // *** set billing address first name and last name *** // 
                            if firstName == "" && lastName == "" {
                                self.billingAddressFirstNameLastNameLabel.text = ""
                            } else if firstName == "" {
                                self.billingAddressFirstNameLastNameLabel.text = lastName
                            } else if lastName == "" {
                                self.billingAddressFirstNameLastNameLabel.text = firstName
                            } else {
                                self.billingAddressFirstNameLastNameLabel.text = "\(firstName) \(lastName)"
                            }
                            
                            // *** set address street name *** //
                            var address1 : String = ""
                            var address2 : String = ""
                            
                            // *** set address 1 *** //
                            if let billingAddress1 = customerOrderDetail.objectForKey("billingAddress1") as? String {
                                address1 = billingAddress1
                            }
                            
                            // *** set address 2 *** //
                            if let billingAddress2 = customerOrderDetail.objectForKey("billingAddress2") as? String {
                                address2 = billingAddress2
                            }
                            
                            // *** set billing address and street name *** //
                            if address1 == "" && address2 == "" {
                                self.billingAddressStreetAddressLabel.text = ""
                            } else if address1 == "" {
                                self.billingAddressStreetAddressLabel.text = address2
                            } else if address2 == "" {
                                self.billingAddressStreetAddressLabel.text = address1
                            } else {
                                self.billingAddressStreetAddressLabel.text = "\(address1), \(address2)"
                            }
                            
                            
                            // *** set city state name *** //
                            var city : String = ""
                            var state : String = ""
                            
                            // *** set city *** //
                            if let billingCity = customerOrderDetail.objectForKey("billingCity") as? String {
                                city = billingCity
                            }
                            
                            // *** set sate *** //
                            if let billingState = customerOrderDetail.objectForKey("billingState") as? NSDictionary {
                                
                                if let shortDesc = billingState.objectForKey("shortDesc") as? String {
                                    
                                    state = shortDesc
                                }
                            }
                            
                            // *** set city and state name *** //
                            if city == "" && state == "" {
                                self.billingAddressCityStateLabel.text = ""
                            } else if city == "" {
                                self.billingAddressCityStateLabel.text = state
                            } else if state == "" {
                                self.billingAddressCityStateLabel.text = city
                            } else {
                                self.billingAddressCityStateLabel.text = "\(city), \(state)"
                            }
                            
                            // *** set country zipcode *** //
                            var country : String = ""
                            var zipCode : String = ""
                            
                            // *** set country *** //
                            if let billingCountry = customerOrderDetail.objectForKey("billingCountry") as? NSDictionary {
                                
                                if let shortDesc = billingCountry.objectForKey("shortDesc") as? String {
                                    
                                    country = shortDesc
                                }
                            }
                            
                            // *** set zipCode *** //
                            if let billingZipCode = customerOrderDetail.objectForKey("billingZipCode") as? String {
                                zipCode = billingZipCode
                            }
                            
                            // *** set city and state name *** //
                            if country == "" && zipCode == "" {
                                self.billingAddressCountryLabel.text = ""
                            } else if country == "" {
                                self.billingAddressCountryLabel.text = zipCode
                            } else if zipCode == "" {
                                self.billingAddressCountryLabel.text = country
                            } else {
                                self.billingAddressCountryLabel.text = "\(country), \(zipCode)"
                            }
                            
                            
                            // *** set shipping address *** //
                            
                            // *** set first name and last name *** //
                            var firstName1 : String = ""
                            var lastName1 : String = ""
                            
                            // *** get first name *** //
                            if let shippingFirstName = customerOrderDetail.objectForKey("shippingFirstName") as? String {
                                firstName1 = shippingFirstName
                            }
                            
                            // *** set last Name *** //
                            if let shippingLastName = customerOrderDetail.objectForKey("shippingLastName") as? String {
                                lastName1 = shippingLastName
                            }
                            
                            // *** set billing address first name and last name *** //
                            if firstName1 == "" && lastName1 == "" {
                                self.shippingAddressFirstNameLastNameLabel.text = ""
                            } else if firstName1 == "" {
                                self.shippingAddressFirstNameLastNameLabel.text = lastName1
                            } else if lastName1 == "" {
                                self.shippingAddressFirstNameLastNameLabel.text = firstName1
                            } else {
                                self.shippingAddressFirstNameLastNameLabel.text = "\(firstName1) \(lastName1)"
                            }
                            
                            // *** set address street name *** //
                            var address3 : String = ""
                            var address4 : String = ""
                            
                            // *** set address 1 *** //
                            if let shippingAddress1 = customerOrderDetail.objectForKey("shippingAddress1") as? String {
                                address3 = shippingAddress1
                            }
                            
                            // *** set address 2 *** //
                            if let shippingAddress2 = customerOrderDetail.objectForKey("shippingAddress2") as? String {
                                address4 = shippingAddress2
                            }
                            
                            // *** set billing address and street name *** //
                            if address3 == "" && address4 == "" {
                                self.shippingAddressStreetAddressLabel.text = ""
                            } else if address3 == "" {
                                self.shippingAddressStreetAddressLabel.text = address4
                            } else if address4 == "" {
                                self.shippingAddressStreetAddressLabel.text = address3
                            } else {
                                self.shippingAddressStreetAddressLabel.text = "\(address3), \(address4)"
                            }
                            
                            
                            // *** set city state name *** //
                            var city1 : String = ""
                            var state1 : String = ""
                            
                            // *** set city *** //
                            if let shippingCity = customerOrderDetail.objectForKey("shippingCity") as? String {
                                city1 = shippingCity
                            }
                            
                            // *** set sate *** //
                            if let shippingState = customerOrderDetail.objectForKey("shippingState") as? NSDictionary {
                                
                                if let shortDesc = shippingState.objectForKey("shortDesc") as? String {
                                    
                                    state1 = shortDesc
                                }
                            }
                            
                            // *** set city and state name *** //
                            if city1 == "" && state1 == "" {
                                self.shippingAddressCityStateLabel.text = ""
                            } else if city1 == "" {
                                self.shippingAddressCityStateLabel.text = state1
                            } else if state1 == "" {
                                self.shippingAddressCityStateLabel.text = city1
                            } else {
                                self.shippingAddressCityStateLabel.text = "\(city1), \(state1)"
                            }
                            
                            // *** set country zipcode *** //
                            var country1 : String = ""
                            var zipCode1 : String = ""
                            
                            // *** set country *** //
                            if let shippingCountry = customerOrderDetail.objectForKey("shippingCountry") as? NSDictionary {
                                
                                if let shortDesc = shippingCountry.objectForKey("shortDesc") as? String {
                                    
                                    country1 = shortDesc
                                }
                            }
                            
                            // *** set zipCode *** //
                            if let shippingZipCode = customerOrderDetail.objectForKey("shippingZipCode") as? String {
                                zipCode1 = shippingZipCode
                            }
                            
                            // *** set city and state name *** //
                            if country1 == "" && zipCode1 == "" {
                                self.billingAddressStreetAddressLabel.text = ""
                            } else if country1 == "" {
                                self.billingAddressStreetAddressLabel.text = zipCode1
                            } else if zipCode1 == "" {
                                self.billingAddressStreetAddressLabel.text = country1
                            } else {
                                self.billingAddressStreetAddressLabel.text = "\(country1), \(zipCode1)"
                            }
                            
                        }
                        
                        if let customerCardInfo = details.objectForKey("customerCardInfo") as? NSDictionary {
                            
                            if let cardType = customerCardInfo.objectForKey("cardType") as? String {
                                
                                if cardType == "visa" {
                                    self.cardTypeImageButton.setImage(UIImage(named: "visa"), forState: .Normal)
                                } else if cardType == "master card" {
                                    self.cardTypeImageButton.setImage(UIImage(named: "masterCard"), forState: .Normal)
                                } else if cardType == "american express" {
                                    self.cardTypeImageButton.setImage(UIImage(named: "americanExpress"), forState: .Normal)
                                } else if cardType == "discovers network" {
                                    self.cardTypeImageButton.setImage(UIImage(named: "discover"), forState: .Normal)
                                } else {
                                    self.cardTypeImageButton.setImage(nil, forState: .Normal)
                                }
                            }
                            
                            if var cardNo = customerCardInfo.objectForKey("cardNo") as? String {
                                
                                cardNo = cardNo.substringWithRange((cardNo.startIndex.advancedBy(12)..<cardNo.endIndex))
                                self.cardNumberLastDigitsLabel.text = cardNo
                            }
                            
                            self.loyalityPointsUsedLabel.text = ""
                        }
                    }
                }
                
            }
            else {
                self.stopIndicator()
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    //MARK:- ButtonActions
    //MARK:-
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as? HomeViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func editButtonTapped(sender: AnyObject) {
        
    }
    
    @IBAction func venueMapButtonTapped(sender: AnyObject) {
        
    }
    
    
    
}
