//
//  EventTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 13/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
@objc protocol EventsTableViewCellDelegate
{
    optional func EventsFavouritesButtonClicked(button: UIButton, index : NSInteger)
    
    optional func EventsShareButtonClicked(button: UIButton, index : NSInteger)
}
class EventTableViewCell: UITableViewCell {

    @IBOutlet var topViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var favouritesButton: UIButton!
    
    @IBOutlet var shareButton: UIButton!
    
    @IBOutlet var cardNameLabel: UILabel!
    
    @IBOutlet var cardNameHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var monthLabel: UILabel!
    
    @IBOutlet var dayLabel: UILabel!
    
    @IBOutlet var yearLabel: UILabel!
    
    @IBOutlet var weekDayLabel: UILabel!
    
    @IBOutlet var priceLabel: UILabel!
    
    @IBOutlet var dateViewLeading: NSLayoutConstraint!
    
    weak var delegate: EventsTableViewCellDelegate?
    
    @IBOutlet var colorView: UIView!
    
    @IBOutlet var dateView: UIView!
    
    @IBOutlet var eventNameLabel: UILabel!
    
    @IBOutlet var eventTimeAndAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        eventNameLabel.text = "Katty perry"
        eventTimeAndAddressLabel.text = "New York"
        
    }
    
    override var bounds : CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.makeItCircle()
    }
    
    func makeItCircle() {
        self.dateView.layer.masksToBounds = true
        self.dateView.layer.cornerRadius = self.dateView.frame.height/2;
        self.dateView.clipsToBounds = true
    }

    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func shareButtonTapped(sender: AnyObject) {
        self.delegate?.EventsShareButtonClicked!(sender as! UIButton, index: sender.tag)
    }
    
    @IBAction func favouritesButtonTapped(sender: AnyObject) {
        self.delegate?.EventsFavouritesButtonClicked!(sender as! UIButton, index: sender.tag)
    }

    
}
