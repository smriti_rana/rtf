//
//  SeeAllArtistViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 17/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
import CoreLocation
class SeeAllArtistViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,SearchArtistCellDelegate {
    @IBOutlet weak var navBar_height: NSLayoutConstraint!
   
    @IBOutlet weak var emptyLbl: UILabel!
    var  titleTypeIs = ""
    var getAllToPassApi = ""
    var artistDictFromSearchEvent = [:]
    var dicttopassApi = [:]
    @IBOutlet weak var seeAll_Lbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var artistArrayIS:NSMutableArray = []
    var venueArrayIS = []
    var eventArrayIS = []
    
    var latStr : String = ""
    var longStr : String = ""
    var locationOnOff = ""
    var  userDefaultLocDict:NSMutableArray = []
      var locationManager : CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.registerNib(UINib(nibName: "SearchArtistTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 72.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
      
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            navBar_height.constant = 94
            seeAll_Lbl.font = UIFont(name: seeAll_Lbl.font.fontName, size: 32)
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("locationDict") != nil {
            userDefaultLocDict = (NSUserDefaults.standardUserDefaults().objectForKey("locationDict")  as? NSMutableArray)!
        }
        //getCustomerId
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        //Location check for APi Params
        // *** check for gps is on or off ***
        if (CLLocationManager.locationServicesEnabled())  {
            
            locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            
            if  (NSUserDefaults.standardUserDefaults().objectForKey("locationDict") != nil){
                locationOnOff = "Enabled"
                let latStrIs = "\(userDefaultLocDict.objectAtIndex(2))"
                let  longStrIS = "\(userDefaultLocDict.objectAtIndex(3))"
                latStr = latStrIs
                longStr = longStrIS
            }
            else{
                locationOnOff = "AllLocation"
                latStr = "0"
                longStr = "0"
            }
            
        }
        else{
            locationOnOff = "Disabled"
            latStr = ""
            longStr = ""
        }

       self.emptyLbl.hidden = true
        
    }
    override func viewWillAppear(animated: Bool) {
        
        print("Dict Is:",artistDictFromSearchEvent)
        let searcedText = artistDictFromSearchEvent.valueForKey("searchKey") as! String
        let searcedType = artistDictFromSearchEvent.valueForKey("searchType") as! String
        self.setLoadingIndicator(self.view)
        self.ApiCallForAllArtist(artistDictFromSearchEvent, getSearchkey: searcedText, getSearchType: searcedType,getAllType: getAllToPassApi)
        seeAll_Lbl.text = titleTypeIs
            
        }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
         if artistArrayIS.count > 0{
            count = artistArrayIS.count
        }
        else if venueArrayIS.count > 0{
            count = venueArrayIS.count
        }
        return count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! SearchArtistTableViewCell
        
        cell.selectionStyle = .None
        cell.separatorInset = UIEdgeInsetsZero
        cell.artistBtn.tag = indexPath.row
        cell.delegate = self
        if (indexPath.row % 2 == 0)
        {
            cell.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            cell.backgroundColor = BgBaseColor
        }
        if artistArrayIS.count > 0{
             cell.favicon_Width.constant = 24
            if let artistName = artistArrayIS[indexPath.row].valueForKey("artistName") as? String{
            cell.artistLbl.text = artistName
                
            }
            if venueArrayIS.count > 0{
                 cell.favicon_Width.constant = 0
                if let artistName = venueArrayIS[indexPath.row].valueForKey("venueName") as? String{
                    cell.artistLbl.text = artistName
                    
            }
            
        }
            if let favorite =  self.artistArrayIS.objectAtIndex(indexPath.row).valueForKey("isFavoriteFlag") as? Bool {
                
                if favorite == true {
                    
                    cell.fav_imgView.image = UIImage(named: "heartRed")
                }
                else {
                    cell.fav_imgView.image = UIImage(named: "heartWhite")
                }
                
            }
        
    }
    return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if artistArrayIS.count > 0 {
            if let artistId = artistArrayIS[indexPath.row].valueForKey("artistId") as? Int {
                if let artistName = artistArrayIS[indexPath.row].valueForKey("artistName") as? String {
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("seeAllEvents") as!SeeAllEventsViewController
                    vc.titleTypeIs = "Artist"
                    vc.getAllToPassApi = "artist"
                    vc.artistIdIs = "\(artistId)"
                    vc.venueIdIs = ""
                    vc.artistNameIs = artistName
                    vc.eventsDictFromSearchEvent = dicttopassApi
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }
        else if venueArrayIS.count > 0{
            if let venueId = venueArrayIS[indexPath.row].valueForKey("venueId") as? Int {
                if  let venueName = venueArrayIS[indexPath.row].valueForKey("venueName") as? String {
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("seeAllEvents") as!SeeAllEventsViewController
                    vc.titleTypeIs = "Venues"
                    vc.getAllToPassApi = "venues"
                    vc.venueIdIs = "\(venueId)"
                    vc.artistIdIs = " "
                    vc.artistNameIs = venueName
                    vc.eventsDictFromSearchEvent = dicttopassApi
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }

    }
    //MARK:Back tapped
    @IBOutlet weak var backBtn: UIButton!
    @IBAction func backTapped(sender: AnyObject) {
      self.navigationController!.popViewControllerAnimated(true)  
    }
        //MARK:-Api CallForAllArtist
    func ApiCallForAllArtist(getDict:NSDictionary,getSearchkey:String,getSearchType:String,getAllType:String){
    
            // *** api call for AllArtist ***
            let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
            alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
    
            // * check for validations
            
                // * check for internet connection
                guard IJReachability.isConnectedToNetwork() else {
    
                    alertVc.message = ErrorInternetConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                    return
                }
    
                // ** start activityIndicator
                //        self.setLoadingIndicator()
    
                // *** send parameters
                let getParam = [
                    "deviceId" : deviceId,
                    "productType" : productType,
                    "searchType" : "AUTOSEARCH",
                    "searchKey": getSearchkey,
                    "locationOption":locationOnOff,
                    "latitude":latStr,
                    "longitude":longStr,
                    "configId":configId,
                    "pageNumber":"1",
                    "getAll":getAllType,
                    "customerId":customerId
                ]
                print(getParam)
        dicttopassApi = getParam
                // **** call webservice
                WebServiceCall.sharedInstance.post("GeneralizedSearch.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
                    //  * stop activityIndicator
                    self.stopIndicator()
    
                    // ** check for valid result
                    if isTrue == true {
                        self.tableView.hidden = false
                        print("dict IS:",responseDisctionary)
                        if let autosearchDict = responseDisctionary!.valueForKey("autoSearchResult") as? NSDictionary  {
                            if let artistArray = autosearchDict.objectForKey("artistResults")  as? NSArray {
                                print(artistArray.count)
                             
                               self.artistArrayIS.removeAllObjects()
                                for i in 0 ..< artistArray.count {
                                    self.artistArrayIS .addObject(artistArray[i])
                                }
                                }
                            }
                       
                        self.tableView.reloadData()
    
                    } else {
    
                        if message == "" {
                            // * if no response
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        } else {
                            // * if error in the response
                            
                            alertVc.message = message!
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    }
                })
            }
    // MARK:-FavArtist TappedDelegateCall
    func ArtistFavouritesButtonClicked(button: UIButton, index : NSInteger){
        
        if let artistId = artistArrayIS[index].valueForKey("artistId") as? Int{
            favArtsitApiCall(artistId,getIndex: index)
        }
        
    }
    //MARK:AddFavArtistcall
    func favArtsitApiCall(getArtistid:Int,getIndex:Int){
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** api call for fav Artist ***
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        //            self.setLoadingIndicator()
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            "artistActionType" :"FAVORITE",
            "artistIds":getArtistid,
            "customerId":customerId
        ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.postArtistfav("AddFavoriteOrSuperArtist.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            
            // ** check for valid result
            if isTrue == true {
                print("response Dict For Fav Artist",responseDisctionary)
                var isFavorite : Bool = false
                if let favorite =  self.artistArrayIS.objectAtIndex(getIndex).valueForKey("isFavoriteFlag") as? Bool {
                    isFavorite = favorite
                    
                    if isFavorite == true {
                        isFavorite = false
                    } else {
                        isFavorite = true
                    }
                    
                }
                let dict = self.artistArrayIS.objectAtIndex(getIndex) as! NSMutableDictionary
                let foundationDictionary = NSMutableDictionary(dictionary: dict)
                foundationDictionary.setValue(isFavorite, forKey: "isFavoriteFlag")
                self.artistArrayIS.replaceObjectAtIndex(getIndex, withObject: foundationDictionary)
                print("artist Array is",self.artistArrayIS[0])
                let msgIs = responseDisctionary?.valueForKey("actionResult") as! String
                self.view.makeToast(message: msgIs)
                self.tableView.reloadData()
                
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }

        }
