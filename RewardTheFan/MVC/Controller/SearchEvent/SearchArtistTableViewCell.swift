//
//  SearchArtistTableViewCell.swift
//  RewardTheFanFinal
//
//  Created by Anmol Rajdev on 12/05/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit
@objc protocol SearchArtistCellDelegate
{
   optional func ArtistFavouritesButtonClicked(button: UIButton, index : NSInteger)
}
class SearchArtistTableViewCell: UITableViewCell {
    @IBOutlet weak var fav_imgView: UIImageView!
    weak var delegate: SearchArtistCellDelegate?
   
    @IBOutlet weak var favicon_Width: NSLayoutConstraint!
    @IBOutlet weak var artistLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK:-favrouiteArtist Tapped
    @IBOutlet weak var artistBtn: UIButton!
    @IBAction func favArtistTapped(sender: AnyObject) {
    self.delegate?.ArtistFavouritesButtonClicked!(sender as! UIButton, index: sender.tag)
    }
    
}
