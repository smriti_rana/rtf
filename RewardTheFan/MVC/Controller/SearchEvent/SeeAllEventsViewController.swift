//
//  SeeAllEventsViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 17/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
import Social
import MessageUI
import CoreLocation
class SeeAllEventsViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,customActionsheetDelegate,HomeEventsTableViewCellDelegate,MFMailComposeViewControllerDelegate,CLLocationManagerDelegate{
    var  titleTypeIs = ""
    var getAllToPassApi = ""
    var eventsDictFromSearchEvent = [:]
    var locationManager : CLLocationManager!
    var artistIdIs = ""
    var venueIdIs = ""
    var artistNameIs = ""
    @IBOutlet weak var navBar_Height: NSLayoutConstraint!
    var artistArrayIS = []
    var venueArrayIS = []
    var eventArrayIS :NSMutableArray = []
    var mySubview:CustomSheetView!
    var pageCount = 1
    var searcedText = ""
    var searcedType = ""
    var startDate : String = ""
    
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    var endDate : String = ""
    
    @IBOutlet weak var emptyLbl: UILabel!
    var latStr : String = ""
    var longStr : String = ""
    var locationOnOff = ""
    var  userDefaultLocDict:NSMutableArray = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBAction func searchTapped(sender: AnyObject) {
    }
    @IBOutlet weak var searchTf: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: "HomeEventsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell2")
         tableView.registerNib(UINib(nibName: "SeeAllFooterTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell1")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 72.0
        tableView.rowHeight = UITableViewAutomaticDimension
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            navBar_Height.constant = 94
            
        }
        
        
        //CustomSheet For Sharing
        mySubview = NSBundle.mainBundle().loadNibNamed("CustomSheetView", owner: self, options: nil).first as? CustomSheetView
        mySubview.delegate = self
        if NSUserDefaults.standardUserDefaults().objectForKey("locationDict") != nil {
            userDefaultLocDict = (NSUserDefaults.standardUserDefaults().objectForKey("locationDict")  as? NSMutableArray)!
        }
        //Location check for APi Params
        // *** check for gps is on or off ***
        if (CLLocationManager.locationServicesEnabled())  {
            
            locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            
            if  (NSUserDefaults.standardUserDefaults().objectForKey("locationDict") != nil){
                locationOnOff = "Enabled"
                let latStrIs = "\(userDefaultLocDict.objectAtIndex(2))"
                let  longStrIS = "\(userDefaultLocDict.objectAtIndex(3))"
                latStr = latStrIs
                longStr = longStrIS
            }
            else{
                locationOnOff = "AllLocation"
                latStr = "0"
                longStr = "0"
            }
            
        }
        else{
            locationOnOff = "Disabled"
            latStr = ""
            longStr = ""
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        //Differentiate api
        if titleTypeIs == "Artist" {
            print("Artist id Is:",artistIdIs)
            searcedText = artistNameIs
            venueIdIs = ""
            searcedType = "NORMALSEARCH"
            getAllToPassApi = ""
            searchTf.text = searcedText
         
        }
        else if titleTypeIs == "Venues"{
            print("Artist id Is:",artistIdIs)
            searcedText = artistNameIs
            artistIdIs = ""
            searcedType = "NORMALSEARCH"
            getAllToPassApi = ""
            searchTf.text = searcedText
            
        }
        else{
            print("Dict Is:",eventsDictFromSearchEvent)
            searcedText = eventsDictFromSearchEvent.valueForKey("searchKey") as! String
            searcedType = eventsDictFromSearchEvent.valueForKey("searchType") as! String
            searchTf.text = searcedText
         
        }
         self.setLoadingIndicator(self.view)
        self.ApiCallForAllEvents(eventsDictFromSearchEvent, getSearchkey: searcedText, getSearchType: searcedType,getAllType: getAllToPassApi,pageNo: pageCount)
    }
    
    
    //MARK:-Api CallForAllEvents
    func ApiCallForAllEvents(getDict:NSDictionary,getSearchkey:String,getSearchType:String,getAllType:String,pageNo:Int){
        
        // *** api call for AllArtist ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for validations
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        //        self.setLoadingIndicator()
        
        // *** send parameters
        let getParam = [
            "deviceId" : deviceId,
            "productType" : productType,
            "searchType" : searcedType,
            "searchKey": getSearchkey,
            "locationOption":locationOnOff,
            "latitude":latStr,
            "longitude":longStr,
            "configId":configId,
            "customerId":customerId,
            "pageNumber":pageNo,
            "startDate":startDate,
            "endDate":endDate,
            "artistId":(artistIdIs),
            "venueId":(venueIdIs),
            "getAll":getAllType
        ]
        print(getParam)
        // **** call webservice
        WebServiceCall.sharedInstance.post("GeneralizedSearch.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            //  * stop activityIndicator
            self.stopIndicator()
            
            // ** check for valid result
            if isTrue == true {
                self.tableView.hidden = false
                print("dict IS:",responseDisctionary)
                if let autosearchDict = responseDisctionary!.valueForKey("autoSearchResult") as? NSDictionary  {
                    if autosearchDict.count > 0{
                        if let eventArray = autosearchDict.objectForKey("events")  as? NSArray {
                            print(eventArray)
                            if eventArray.count > 0{
                                if self.pageCount == 1 {
                                    self.eventArrayIS.removeAllObjects()
                                }
                                
                                for i in 0 ..< eventArray.count {
                                    self.eventArrayIS .addObject(eventArray[i])
                                }
                            }
                            else{
                                if let normalsearchDict = responseDisctionary!.valueForKey("normalSearchResult") as? NSDictionary  {
                                    print(normalsearchDict)
                                    if let eventArray = autosearchDict.objectForKey("events")  as? NSArray {
                                        print("event array",eventArray)
                                    }
                                    self.pagingSpinner.stopAnimating()
                                }
                            }
                            if self.eventArrayIS.count == 0{
                                self.tableView.hidden = true
                                self.emptyLbl.hidden = false
                                self.emptyLbl.text = self.emptyTableSearchMsg
                            }
                        }
                        else{
                            if let normalsearchDict = responseDisctionary!.valueForKey("normalSearchResult") as? NSDictionary  {
                                print(normalsearchDict)
                                if let eventArray = normalsearchDict.objectForKey("events")  as? NSArray {
                                    print("event array",eventArray)
                                    if self.pageCount == 1 {
                                        self.eventArrayIS.removeAllObjects()
                                    }
                                    
                                    for i in 0 ..< eventArray.count {
                                        self.eventArrayIS .addObject(eventArray[i])
                                    }
                                }
                            }
                        }
                        if self.eventArrayIS.count == 0{
                            self.tableView.hidden = true
                            self.emptyLbl.hidden = false
                            self.emptyLbl.text = self.emptyTableSearchMsg
                        }
                    }
                    self.tableView.reloadData()
                }
            } else {
                
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    //MARK:Back Tapped
    weak var backBtn: UIButton!
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController!.popViewControllerAnimated(true)
        
    }
    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventArrayIS.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell2") as! HomeEventsTableViewCell
        cell.selectionStyle = .None
        if (indexPath.row % 2 == 0)
        {
            cell.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            cell.backgroundColor = BgBaseColor
        }
        cell.topViewHeightConstraint.constant = 0
        cell.bottomViewHeightConstraint.constant = 0
        cell.delegate = self
        cell.shareButton.tag = indexPath.row
        cell.favouritesButton.tag = indexPath.row
        
        mySubview.shareBtn.tag = indexPath.row
        mySubview.TwitterBtn.tag = indexPath.row
        mySubview.shareEmailBtn.tag = indexPath.row
        
        cell.selectionStyle = .None
        cell.cardNameHeightConstraint.constant = 0
        if eventArrayIS.count > 0{
            print(eventArrayIS)
            if let eventName = eventArrayIS[indexPath.row].valueForKey("artistName") as? String{
                
                cell.eventNameLabel.text = eventName
                
            }
            
            if let Eventdate =  eventArrayIS[indexPath.row].valueForKey("eventDateStr") as? String{
                if Eventdate != "TBD" {
                    
                    // *** get datey from string *** //
                    let dateFormatter = NSDateFormatter()
                    
                    dateFormatter.dateFormat = "MM/dd/yyyy"
                    
                    let dateFromString = dateFormatter.dateFromString(Eventdate)
                    
                    // *** get day *** //
                    
                    let dateFormatteerForDay = NSDateFormatter()
                    
                    dateFormatteerForDay.dateFormat = "dd"
                    
                    let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                    
                    cell.dayLabel.text = dayFromDate
                    
                    // *** get year *** //
                    
                    let dateFormatteerForYear = NSDateFormatter()
                    
                    dateFormatteerForYear.dateFormat = "YYYY"
                    
                    let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                    
                    cell.yearLabel.text = yearFromDate
                    
                    
                    
                    // *** get month *** //
                    
                    
                    let dateFormatteerForMonth = NSDateFormatter()
                    
                    dateFormatteerForMonth.dateFormat = "MMM"
                    
                    let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                    
                    
                    
                    cell.monthLabel.text = monthFromDate
                    
                    // *** get week day *** //
                    
                    
                    let dateFormatteerForWeekDay = NSDateFormatter()
                    
                    dateFormatteerForWeekDay.dateFormat = "EEE"
                    
                    let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                    
                    cell.weekDayLabel.text = weekDayFromDate
                    
                }
                
            }
            
            // *** set favorites *** //
            
            if let isFavorite = eventArrayIS.objectAtIndex(indexPath.row).valueForKey("isFavoriteEvent") as? Bool {
                if isFavorite {
                    cell.favouritesButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
                } else {
                    cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                }
            } else {
                cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
            }
            
            //venue Address
            var eTime = ""
            var eVenue = ""
            var eCity = ""
            var eState = ""
            //Time
            if let Eventtime = eventArrayIS[indexPath.row].valueForKey("eventTimeStr") as? String{
                if Eventtime == "TBD"{
                    eTime = "TBD"
                }else{
                    //24hr format
                    let timeSlot = Eventtime
                    let dateFormatterForTime = NSDateFormatter()
                    
                    dateFormatterForTime.dateFormat = "hh:mma"
                    let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                    
                    
                    
                    let dateFormatteerForTimeFormat = NSDateFormatter()
                    
                    dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                    
                    eTime = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                }
            }
            else{
                eTime = "12:00AM"
            }
            if let Eventvenue = eventArrayIS[indexPath.row].valueForKey("venueName") as? String{
                eVenue = Eventvenue
            }
            if let Eventcity = eventArrayIS[indexPath.row].valueForKey("state") as? String{
                eCity = Eventcity
            }
            if let EventState = eventArrayIS[indexPath.row].valueForKey("country") as? String{
                eState = EventState
            }
            let string1 = eTime
            let resultStr = "\(string1)"+"\("-")" + " \(eVenue)" + "\(" ")" + "\(eCity)" + "\(" ")" + "\(eState)"
            
            cell.eventTimeAndAddressLabel.text = resultStr
            
            if let ticketPriceStr = eventArrayIS[indexPath.row].valueForKey("ticketPriceTag") as? String{
                cell.priceLabel.text = "\(ticketPriceStr)"
            }
            
        }
        
        return cell
        
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Hello"
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell =  tableView.dequeueReusableCellWithIdentifier("Cell1") as! SeeAllFooterTableViewCell
        var titletoSet = ""
        print(titleTypeIs)
        if section == 0 {
            if titleTypeIs == "Artist" {
            titletoSet = "Artist's Events"
            
            }
            else if titleTypeIs == "Venues"{
               titletoSet = "Venue's Events"
            }
            else{
                titletoSet = "Events"
            }
            headerCell.seeAllLbl.text = titletoSet
            headerCell.seeAllBtn.hidden =  true
            
        }
        headerCell.seeAllLbl.textColor = UIColor.whiteColor()
        headerCell.contentView.backgroundColor = UIColor.init(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        return headerCell
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         return UITableViewAutomaticDimension
        
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EventDetail") as! EventDetailViewController
        if let id = eventArrayIS[indexPath.row].valueForKey("eventId") as? Int{
            vc.eventId = String(id)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    //MARK:-Share delegate
    func homeEventsShareButtonClicked(button: UIButton, index : NSInteger){
        
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.3)
        mySubview.delegate = self
        mySubview.customSheetBgView.backgroundColor = greyColor
        
        self.mySubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(SearchEventArtistViewController.handleTap(_:)))
        //        mySubview.customSheetBgView.addGestureRecognizer(tap)
        //        tap.cancelsTouchesInView = false
        mySubview.customSheetBgView.hidden = false
        mySubview.sortBgView.hidden = true
        mySubview.priceBgView.hidden = true
        mySubview.sharingBgView.hidden = false
        mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, mySubview.customSheetBgView.frame.size.height)
        self.view.addSubview(mySubview)
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 1.0
            self.mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height - 123, self.view.frame.size.width, self.mySubview.customSheetBgView.frame.size.height)
            }, completion: { finished in
        })
        
        
    }
    // *** touch events *** //
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        handleTap()
        
    }
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        searchTf.resignFirstResponder()
        removeCustomView()
    }
    func removeCustomView() {
        searchTf.resignFirstResponder()
        if mySubview == nil {
            return
        }
        
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 0.0
            }, completion: { finished in
                self.mySubview.removeFromSuperview()
        })
    }
    
    //Mark:ShareDialogMethods
    func shareOnFacebook(index:Int) {
        print(index-1)
        let shareLinkIs = eventArrayIS[index-1].valueForKey("shareLinkUrl") as? String
        let facebookPost = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        facebookPost.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                break
                
            case SLComposeViewControllerResult.Done:
                //Code here to deal with it being completed
                self.removeCustomView()
                break
            }
        }
        
        facebookPost.setInitialText(shareLinkIs!) //The default text in the tweet
        presentViewController(facebookPost, animated: false, completion: {
            //Optional completion statement
        })
        
        
    }
    //Mark:TwitterCicked
    func shareOnTwitter(index:Int){
        let shareLinkIs = eventArrayIS[index-1].valueForKey("shareLinkUrl") as? String
        let twitterSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        twitterSheet.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                //print("cancel clicked")
                
                break
                
            case SLComposeViewControllerResult.Done:
                self.removeCustomView()
                //Code here to deal with it being completed
                break
            }
        }
        twitterSheet.setInitialText("\(shareLinkIs!)")
        self.presentViewController(twitterSheet, animated: false, completion: {
            
        })
        
    }
    //MARK:-Email sharing
    func shareViaEmail(index:Int){
        let shareLinkIs = eventArrayIS[index-1].valueForKey("shareLinkUrl") as? String
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setMessageBody(shareLinkIs!, isHTML: true)
            
            presentViewController(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
        
        
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        removeCustomView()
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    //Mark:-PaginationTableView
    var pagingSpinner = UIActivityIndicatorView()
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if scrollView == tableView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
                pagingSpinner.startAnimating()
                pagingSpinner.color = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
                pagingSpinner.hidesWhenStopped = true
                
                tableView.tableFooterView = pagingSpinner
                pageCount += 1
                
                self.ApiCallForAllEvents(eventsDictFromSearchEvent, getSearchkey: searcedText, getSearchType: searcedType,getAllType: getAllToPassApi,pageNo: pageCount)
                
            }
        }
    }
    //MARK:-favouriteClicked
    func homeEventsFavouritesButtonClicked(button: UIButton, index: NSInteger) {
        self.searchTf.resignFirstResponder()
        var eventId : Int = 0
        if let id = eventArrayIS.objectAtIndex(index).valueForKey("eventId") as? Int {
            eventId = id
        }
        
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            "customerId" : customerId,
            "eventId" : eventId
        ]
        print(getParam)
        // **** call webservice
        
        WebServiceCall.sharedInstance.favouriteEvent("FavouriteEvents.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            // ** check for valid result
            if isTrue == true {
                
                var isFavorite : Bool = false
                self.view.makeToast(message: message!)
                if let favorite =  self.eventArrayIS.objectAtIndex(index).valueForKey("isFavoriteEvent") as? Bool {
                    isFavorite = favorite
                    
                    if isFavorite == true {
                        isFavorite = false
                    } else {
                        isFavorite = true
                    }
                    
                }
                let dict = self.eventArrayIS.objectAtIndex(index) as! NSMutableDictionary
                let foundationDictionary = NSMutableDictionary(dictionary: dict)
                foundationDictionary.setValue(isFavorite, forKey: "isFavoriteEvent")
                self.eventArrayIS.replaceObjectAtIndex(index, withObject: foundationDictionary)
                self.tableView.reloadData()
                
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
        
    }
    
}