//
//  SeeAllFooterTableViewCell.swift
//  RewardTheFanFinal
//
//  Created by Anmol Rajdev on 13/05/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit
@objc protocol searchEventCellDelegate
{
    optional  func seeAllArtistClicked(cell:SeeAllFooterTableViewCell)
   
    
}
class SeeAllFooterTableViewCell: UITableViewCell {
    @IBOutlet weak var lineViewBk: UIView!
    weak var delegate: searchEventCellDelegate?
    @IBOutlet weak var seeAllLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var seeAllBtn: UIButton!
    @IBAction func seeAllTapped(sender: AnyObject) {
        self.delegate?.seeAllArtistClicked!(self)
        
    }
    
}
