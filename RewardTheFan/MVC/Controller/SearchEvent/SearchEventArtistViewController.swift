//
//  SearchEventArtistViewController.swift
//  RewardTheFanFinal
//
//  Created by Anmol Rajdev on 13/05/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit
import Social
import MessageUI
import CoreLocation

class SearchEventArtistViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,customActionsheetDelegate,EventsTableViewCellDelegate,MFMailComposeViewControllerDelegate,HomeEventsTableViewCellDelegate,CLLocationManagerDelegate,searchEventCellDelegate,SearchArtistCellDelegate {
    
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    @IBOutlet weak var emptyLbl: UILabel!
    //MARK:- Properties
    //MARK:-
    
    var locationManager : CLLocationManager!
    
    var dictToPass = [:]
    
    var mySubview:CustomSheetView!
    
    let placeholderStringIs = "Artist, Team, Event or Venue"
    
    var searchTypeIs = ""
    
    var pageCount = 1
    
    var  userDefaultLocDict:NSMutableArray = []
    
    var artistArrayIS:NSMutableArray = []
    
    var venueArrayIS:NSMutableArray = []
    
    var eventArrayIS :NSMutableArray = []
    
    var totalArtistCount : Int = 0
    
    var totalVenueCount : Int = 0
    
    var totalEventCount : Int = 0
    
    var latStr : String = ""
    
    var longStr : String = ""
    
    var locationOnOff = ""
    
    var locationTypeParam :String = ""
    
    var SearcbarTextfromexploreCell = ""
    
    var exploreTypeToSearch = ""
    
    var viewTypeIs = ""
    
    var startDate : String = ""
    
    var endDate : String = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchTF: UITextField!
    
    @IBOutlet weak var searchBtn: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // *** register table view cell *** //
        tableView.registerNib(UINib(nibName: "SearchArtistTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")
        tableView.registerNib(UINib(nibName: "SeeAllFooterTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell1")
        tableView.registerNib(UINib(nibName: "HomeEventsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell2")
        
        // *** table view height
        tableView.estimatedRowHeight = 72.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        // *** get values from defaults *** //
        if NSUserDefaults.standardUserDefaults().objectForKey("locationDict") != nil {
            userDefaultLocDict = (NSUserDefaults.standardUserDefaults().objectForKey("locationDict")  as? NSMutableArray)!
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("startDate") != nil {
            startDate = NSUserDefaults.standardUserDefaults().objectForKey("startDate") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("endDate") != nil {
            endDate = NSUserDefaults.standardUserDefaults().objectForKey("endDate") as! String
        }
        
        print("Saved location Dict As",userDefaultLocDict)
        
        mySubview = NSBundle.mainBundle().loadNibNamed("CustomSheetView", owner: self, options: nil).first as? CustomSheetView
        mySubview.delegate = self
        tableView.hidden = true
        
        //set searchKey As Search
        self.searchTF.returnKeyType = .Search
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(SearchEventArtistViewController.handleTap(_:)))
        tableView.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        
        //Location check for APi Params
        // *** check for gps is on or off ***
        if (CLLocationManager.locationServicesEnabled())  {
            
            locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            
            if  (NSUserDefaults.standardUserDefaults().objectForKey("locationDict") != nil) {
                
                locationOnOff = "Enabled"
                let latStrIs = "\(userDefaultLocDict.objectAtIndex(2))"
                let  longStrIS = "\(userDefaultLocDict.objectAtIndex(3))"
                latStr = latStrIs
                longStr = longStrIS
            } else {
                
                locationOnOff = "AllLocation"
                latStr = "0"
                longStr = "0"
            }
        } else {
            
            locationOnOff = "Disabled"
            latStr = ""
            longStr = ""
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if viewTypeIs == "ExploreSearchAll" {
            searchTF.placeholder = "Search your Event, Band, Show"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var countIs = 0
        if section == 0 {
            countIs = artistArrayIS.count
        } else if section ==  1 {
            countIs = venueArrayIS.count
        } else if section == 2 {
            countIs = eventArrayIS.count
        }
        return countIs
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 2 {
            
            let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell2") as! HomeEventsTableViewCell
            
            // *** set cell selection style *** //
            cell1.selectionStyle = .None
            cell1.delegate = self
            cell1.separatorInset = UIEdgeInsetsZero
            cell1.leadingBgView.constant = 0
            cell1.trailingBgView.constant = 0
            cell1.cardNameHeightConstraint.constant = 0
            cell1.topViewHeightConstraint.constant = 0
            cell1.bottomViewHeightConstraint.constant = 0
            
            // *** change color of cell *** //
            if indexPath.row % 2 != 0 {
                cell1.colorView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
            } else {
                cell1.colorView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
            }
            
            
            mySubview.shareBtn.tag = indexPath.row
            mySubview.TwitterBtn.tag = indexPath.row
            mySubview.shareEmailBtn.tag = indexPath.row
            cell1.favouritesButton.tag = indexPath.row
            cell1.shareButton.tag = indexPath.row
            cell1.delegate = self
            
            
            if eventArrayIS.count > 0 {
                
                if let eventName = eventArrayIS[indexPath.row].valueForKey("artistName") as? String {
                    cell1.eventNameLabel.text = eventName
                }
                
                // *** set favorites *** //
                if let isFavorite = eventArrayIS.objectAtIndex(indexPath.row).valueForKey("isFavoriteEvent") as? Bool {
                    if isFavorite {
                        cell1.favouritesButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
                    } else {
                        cell1.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                    }
                } else {
                    cell1.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                }
                
                if let Eventdate =  eventArrayIS[indexPath.row].valueForKey("eventDateStr") as? String{
                    print(Eventdate)
                    print(indexPath.row)
                    if Eventdate != "TBD" {
                        
                        // *** get datey from string *** //
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "MM/dd/yyyy"
                        let dateFromString = dateFormatter.dateFromString(Eventdate)
                        
                        // *** get day *** //
                        let dateFormatteerForDay = NSDateFormatter()
                        dateFormatteerForDay.dateFormat = "dd"
                        let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                        cell1.dayLabel.text = dayFromDate
                        
                        // *** get year *** //
                        let dateFormatteerForYear = NSDateFormatter()
                        dateFormatteerForYear.dateFormat = "YYYY"
                        let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                        cell1.yearLabel.text = yearFromDate
                        
                        // *** get month *** //
                        let dateFormatteerForMonth = NSDateFormatter()
                        dateFormatteerForMonth.dateFormat = "MMM"
                        let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                        cell1.monthLabel.text = monthFromDate
                        
                        // *** get week day *** //
                        let dateFormatteerForWeekDay = NSDateFormatter()
                        dateFormatteerForWeekDay.dateFormat = "EEE"
                        let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                        cell1.weekDayLabel.text = weekDayFromDate
                    }
                    
                    //venue Address
                    var eTime = ""
                    var eVenue = ""
                    var eCity = ""
                    var eState = ""
                    
                    if let Eventtime = eventArrayIS[indexPath.row].valueForKey("eventTimeStr") as? String {
                        
                        if Eventtime == "TBD" {
                            eTime = "TBD"
                        } else {
                            
                            //24hr format
                            let timeSlot = Eventtime
                            let dateFormatterForTime = NSDateFormatter()
                            dateFormatterForTime.dateFormat = "hh:mma"
                            let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                            
                            let dateFormatteerForTimeFormat = NSDateFormatter()
                            dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                            eTime = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                        }
                    }
                    
                    if let Eventvenue = eventArrayIS[indexPath.row].valueForKey("venueName") as? String {
                        eVenue = Eventvenue
                    }
                    
                    if let Eventcity = eventArrayIS[indexPath.row].valueForKey("state") as? String {
                        eCity = Eventcity
                    }
                    
                    if let EventState = eventArrayIS[indexPath.row].valueForKey("country") as? String {
                        eState = EventState
                    }
                    
                    let string1 = eTime
                    let resultStr = "\(string1) - \(eVenue) \(eCity) \(eState)"
                    cell1.eventTimeAndAddressLabel.text = resultStr
                    
                    if let EventticketPrice = eventArrayIS[indexPath.row].valueForKey("ticketPriceTag") as? String {
                        cell1.priceLabel.text = "\(EventticketPrice)"
                    }
                    
                } else {
                    
                    cell1.dayLabel.text = "31"
                    cell1.yearLabel.text = "2015"
                    cell1.monthLabel.text = "06"
                }
            }
            return cell1
            
        } else {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! SearchArtistTableViewCell
            
            // *** set cell selection style *** //
            cell.selectionStyle = .None
            cell.separatorInset = UIEdgeInsetsZero
            
            // *** change the background color ***& //
            if indexPath.row % 2 != 0 {
                cell.contentView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
            } else {
                cell.contentView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
            }
            
            if indexPath.section == 0 {
                
                if artistArrayIS.count > 0 {
                    
                    cell.favicon_Width.constant = 24
                    cell.delegate = self
                    cell.artistBtn.tag = indexPath.row
                    
                    if let artistName = artistArrayIS[indexPath.row].valueForKey("artistName") as? String {
                        cell.artistLbl.text = artistName
                        
                    }
                    
                    if let favorite =  self.artistArrayIS.objectAtIndex(indexPath.row).valueForKey("isFavoriteFlag") as? Bool {
                        
                        if favorite == true {
                            cell.fav_imgView.image = UIImage(named: "heartRed")
                        } else {
                            cell.fav_imgView.image = UIImage(named: "heartWhite")
                        }
                    }
                }
                
            } else if indexPath.section == 1 {
                
                if venueArrayIS.count > 0 {
                    cell.favicon_Width.constant = 0
                    if let venueName = venueArrayIS[indexPath.row].valueForKey("venueName") as? String {
                        
                        cell.artistLbl.text = venueName
                    }
                }
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Hello"
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  headerCell =  tableView.dequeueReusableCellWithIdentifier("Cell1") as! SeeAllFooterTableViewCell
        
        if section == 0 {
            
            headerCell.seeAllLbl.text = "ARTISTS"
            headerCell.seeAllBtn.hidden =  true
       
        } else if section == 1 {
            
            headerCell.seeAllBtn.hidden =  true
            headerCell.seeAllLbl.text = "VENUES"
        } else if section == 2 {
            
            headerCell.seeAllBtn.hidden =  true
            headerCell.seeAllLbl.text = "EVENTS"
        }
        
        headerCell.seeAllLbl.textColor = UIColor.whiteColor()
        headerCell.contentView.backgroundColor = UIColor.init(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        
        return headerCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 60.0
        }
        return 40.0
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == 0 {
            print(totalArtistCount)
            if totalArtistCount <= 5 {
                
                return  CGFloat.min
            } else {
                
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                    return 60.0
                }
                return 40.0
            }
            
        } else if section == 1 {
            print(totalVenueCount)
            if totalVenueCount <= 5 {
                return  CGFloat.min
            } else {
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                    return 60.0
                }
                return 40.0
            }
            
        } else if section == 2 {
            
            if totalEventCount <= 5 {
                
                return  CGFloat.min
            } else {
                
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                    return 60.0
                }
                return 40.0
            }
        }
        return 0
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let  headerCell =  tableView.dequeueReusableCellWithIdentifier("Cell1") as! SeeAllFooterTableViewCell
        
        headerCell.delegate = self
        if section == 0 {
            
            headerCell.seeAllBtn.hidden =  false
            headerCell.seeAllLbl.text = "See all artist"
            
        } else if section == 1 {
            
            headerCell.seeAllBtn.hidden =  false
            headerCell.seeAllLbl.text = "See all venues"
            
        } else if section == 2{
            
            headerCell.seeAllBtn.hidden =  false
            headerCell.seeAllLbl.text = "See all events"
        }
        return headerCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            if let artistId = artistArrayIS[indexPath.row].valueForKey("artistId") as? Int {
                if let artistName = artistArrayIS[indexPath.row].valueForKey("artistName") as? String {
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("seeAllEvents") as!SeeAllEventsViewController
                    vc.titleTypeIs = "Artist"
                    vc.getAllToPassApi = "artist"
                    vc.artistIdIs = "\(artistId)"
                    vc.venueIdIs = ""
                    vc.artistNameIs = artistName
                    vc.eventsDictFromSearchEvent = dictToPass
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }
        else if indexPath.section == 1 {
            if let venueId = venueArrayIS[indexPath.row].valueForKey("venueId") as? Int {
                if  let venueName = venueArrayIS[indexPath.row].valueForKey("venueName") as? String {
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("seeAllEvents") as!SeeAllEventsViewController
                    vc.titleTypeIs = "Venues"
                    vc.getAllToPassApi = "venues"
                    vc.venueIdIs = "\(venueId)"
                    vc.artistIdIs = " "
                    vc.artistNameIs = venueName
                    vc.eventsDictFromSearchEvent = dictToPass
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
            
        else if indexPath.section == 2 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EventDetail") as! EventDetailViewController
            if let id = eventArrayIS[indexPath.row].valueForKey("eventId") as? Int{
                vc.eventId = String(id)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    //MARK:-Api CallForSearched data
    func ApiCallForsearchedText(getText:String,searchType:String) {
        
        // *** api call for forgot password ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for validations
        if searchTF.text == "" {
            stopIndicator()
        } else {
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                stopIndicator()
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            // *** send parameters
            let getParam = [
                
                "deviceId" : deviceId,
                "productType" : productType,
                "searchType" : searchType,
                "searchKey":getText,
                "artistId":"",
                "startDate":startDate,
                "endDate":endDate,
                "customerId":customerId,
                "locationOption":locationOnOff,
                "latitude":latStr,
                "longitude":longStr,
                "configId":configId,
                "pageNumber":"1"
                
            ]
            
            dictToPass = getParam
            
            // **** call webservice
            WebServiceCall.sharedInstance.post("GeneralizedSearch.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
                
                if isTrue == true {
                    
                    self.tableView.hidden = false
                    
                    if let autosearchDict = responseDisctionary!.valueForKey("autoSearchResult") as? NSDictionary  {
                        self.searchTF.resignFirstResponder()
                        if let artistArray = autosearchDict.objectForKey("artistResults")  as? NSArray {
                            print(artistArray.count)
                            self.artistArrayIS.removeAllObjects()
                            for i in 0 ..< artistArray.count {
                                self.artistArrayIS .addObject(artistArray[i])
                            }
                        }
                        if let venArray = autosearchDict.objectForKey("venueResults")  as? NSArray {
                            print(venArray)
                            self.venueArrayIS.removeAllObjects()
                            for i in 0 ..< venArray.count {
                                self.venueArrayIS .addObject(venArray[i])
                            }
                        }
                        if let eventArray = autosearchDict.objectForKey("events")  as? NSArray {
                            print(eventArray)
                            //                            self.eventArrayIS = eventArray
                            self.eventArrayIS.removeAllObjects()
                            if eventArray.count > 0{
                            for i in 0 ..< eventArray.count {
                                    self.eventArrayIS .addObject(eventArray[i])
                                }
                            }
                            
                        }
                        if self.artistArrayIS.count == 0 && self.venueArrayIS.count == 0 && self.eventArrayIS.count == 0{
                            self.tableView.hidden = true
                            self.emptyLbl.hidden = false
                             self.emptyLbl.text = self.emptyTableSearchMsg
                        }
                        if let artistArrayCount = autosearchDict.objectForKey("totalArtistCount")  as? Int {
                            self.totalArtistCount = artistArrayCount
                        }
                        if let venueArrayCount = autosearchDict.objectForKey("totalVenueCount")  as? Int {
                            self.totalVenueCount = venueArrayCount
                            print("venue Array count",self.totalVenueCount )
                        }
                        if let eventArrayCount = autosearchDict.objectForKey("totalEventCount")  as? Int {
                            self.totalEventCount = eventArrayCount
                            
                        }
                    }
                    if self.searchTypeIs == "NORMALSEARCH"{
                        print("in search View",responseDisctionary!.valueForKey("normalSearchResult") as? NSDictionary)
                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ResultSearchArtist") as! ResultSearchEventViewController
                        vc.resultDictIS = (responseDisctionary!.valueForKey("normalSearchResult") as? NSDictionary)!
                        vc.searchedtxtIS = self.searchTF.text!
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    self.stopIndicator()
                    self.tableView.reloadData()
                    NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(SearchEventArtistViewController.update), userInfo: nil, repeats: false)
                    
                } else {
                    self.stopIndicator()
                    if message == "" {
                        // * if no response
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    //MARK:-ApiCall for ExploreSearch
    func ApiCallForExploresearchedText(searchOption:String,pageCountIs:Int) {
        
        // *** api call for forgot password ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for validations
        if searchTF.text == "" {
            stopIndicator()
            alertVc.message = ExploreAlertmsgs
            self.presentViewController(alertVc, animated: true, completion: nil)
            
        } else {
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                stopIndicator()
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            // *** send parameters
            let getParam = [
                
                "productType" : productType,
                "exploreSearchOption" : searchOption,
                "startDate":startDate,
                "endDate":endDate,
                "customerId":customerId,
                "locationOption":locationOnOff,
                "latitude":latStr,
                "longitude":longStr,
                "configId":configId,
                "pageNumber":pageCountIs
                
            ]
            
            dictToPass = getParam
            
            // **** call webservice
            WebServiceCall.sharedInstance.postEventSearch("EventSearchByExplore.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
                
                if isTrue == true {
                    self.tableView.hidden = false
                    self.stopIndicator()
                    if let autosearchDict = responseDisctionary!.valueForKey("normalSearchResult") as? NSDictionary  {
                        if let artistArray = autosearchDict.objectForKey("artistResults")  as? NSArray {
                            print(artistArray.count)
                            if self.pageCount == 1 {
                                self.artistArrayIS.removeAllObjects()
                            }
                            for i in 0 ..< artistArray.count {
                                self.artistArrayIS .addObject(artistArray[i])
                            }
                        }
                        
                        if let venArray = autosearchDict.objectForKey("venueResults")  as? NSArray {
                            print(venArray)
                            if self.pageCount == 1 {
                                self.venueArrayIS.removeAllObjects()
                            }
                            for i in 0 ..< venArray.count {
                                self.venueArrayIS .addObject(venArray[i])
                            }
                            
                        }
                        
                        
                        if let artistArrayCount = autosearchDict.objectForKey("totalArtistCount")  as? Int {
                            self.totalArtistCount = artistArrayCount
                        }
                        if let venueArrayCount = autosearchDict.objectForKey("totalVenueCount")  as? Int {
                            self.totalVenueCount = venueArrayCount
                            print("venue Array count",self.totalVenueCount )
                        }
                        if let eventArrayCount = autosearchDict.objectForKey("totalEventCount")  as? Int {
                            self.totalEventCount = eventArrayCount
                            
                        }
                    }
                    self.tableView.reloadData()
                    NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(SearchEventArtistViewController.update), userInfo: nil, repeats: false)
                    
                } else {
                    
                    if message == "" {
                        // * if no response
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    func update() {
        tableView.reloadData()
    }
    //MARK:-Searched Type text
    @IBAction func searchBarValueChanges(sender: AnyObject) {
        print(self.searchTF.text)
     if self.searchTF.text != ""{
        setLoadingIndicator(self.view)
        searchTypeIs = "AUTOSEARCH"
        delay(0.1) { () -> () in
            self.ApiCallForsearchedText(self.searchTF.text!,searchType: self.searchTypeIs)
        }
    }
     else{
        self.artistArrayIS.removeAllObjects()
        self.venueArrayIS.removeAllObjects()
        self.eventArrayIS.removeAllObjects()
        tableView.reloadData()
        tableView.hidden =  true
        self.emptyLbl.hidden = false
        emptyLbl.text = emptyTableSearchMsg
        }
        
    }
    //MARK:-Share delegate
    func homeEventsShareButtonClicked(button: UIButton, index : NSInteger){
        
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.3)
        mySubview.delegate = self
        mySubview.customSheetBgView.backgroundColor = greyColor
        
        self.mySubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        
        
        mySubview.customSheetBgView.hidden = false
        mySubview.sortBgView.hidden = true
        mySubview.priceBgView.hidden = true
        mySubview.sharingBgView.hidden = false
        mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, mySubview.customSheetBgView.frame.size.height)
        self.view.addSubview(mySubview)
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 1.0
            self.mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height - 123, self.view.frame.size.width, self.mySubview.customSheetBgView.frame.size.height)
            }, completion: { finished in
        })
        
        
    }
    // *** touch events *** //
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        handleTap()
        
    }
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        searchTF.resignFirstResponder()
        removeCustomView()
    }
    func removeCustomView() {
        searchTF.resignFirstResponder()
        if mySubview == nil {
            return
        }
        
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 0.0
            }, completion: { finished in
                self.mySubview.removeFromSuperview()
        })
    }
    
    //Mark:ShareDialogMethods
    func shareOnFacebook(index:Int) {
        print(index-1)
        let shareLinkIs = eventArrayIS[index-1].valueForKey("shareLinkUrl") as? String
        let facebookPost = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        facebookPost.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                break
                
            case SLComposeViewControllerResult.Done:
                //Code here to deal with it being completed
                self.removeCustomView()
                break
            }
        }
        
        facebookPost.setInitialText(shareLinkIs!) //The default text in the tweet
        presentViewController(facebookPost, animated: false, completion: {
            //Optional completion statement
        })
        
        
    }
    //Mark:TwitterCicked
    func shareOnTwitter(index:Int){
        let shareLinkIs = eventArrayIS[index-1].valueForKey("shareLinkUrl") as? String
        let twitterSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        twitterSheet.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                //print("cancel clicked")
                
                break
                
            case SLComposeViewControllerResult.Done:
                self.removeCustomView()
                //Code here to deal with it being completed
                break
            }
        }
        twitterSheet.setInitialText("\(shareLinkIs!)")
        self.presentViewController(twitterSheet, animated: false, completion: {
            
        })
        
    }
    //MARK:-Email sharing
    func shareViaEmail(index:Int){
        let shareLinkIs = eventArrayIS[index-1].valueForKey("shareLinkUrl") as? String
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setMessageBody(shareLinkIs!, isHTML: true)
            
            presentViewController(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
        
        
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        removeCustomView()
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBOutlet weak var backBtn: UIButton!
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    //MARK:-OnSearch Key tapped From Keyboard
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
         print(textField.text)
        searchTF.resignFirstResponder()
        if textField.text! == "" {
             performAction()
        }
       
    return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print(textField.text)
        
         return true
    }
    func performAction() {
        searchTypeIs = "NORMALSEARCH"
        setLoadingIndicator(self.view)
        delay(0.3) { () -> () in
            self.ApiCallForsearchedText(self.searchTF.text!,searchType: self.searchTypeIs)
        }
    }
    //MARK:-Location Delegates
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
        case .Authorized, .AuthorizedWhenInUse:
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "locationFlag")
        case .Denied:
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "locationFlag")
            break
        default:
            break
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last! as CLLocation
        
        let geoCoder = CLGeocoder()
        
        let latitude : String = ("\(location.coordinate.latitude)")
        latStr = latitude
        
        let longitude : String = ("\(location.coordinate.longitude)")
        
        longStr = longitude
        
        let locationForZip = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(locationForZip, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            
            placeMark = placemarks?[0]
            // Address dictionary
            
            if !(placeMark == nil) {
                
                if !(placeMark.addressDictionary == nil) {
                    
                    print(placeMark.addressDictionary)
                    
                    // Zip code
                    if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                        print(zip)
                        NSUserDefaults.standardUserDefaults().setObject(zip, forKey: "zipCode")
                    } else {
                        
                    }
                } else {
                    
                }
            } else {
                
                
                
            }
        })
        locationManager.stopUpdatingLocation()
    }
    //MARK:-SeeAll Artist
    func seeAllArtistClicked(cell:SeeAllFooterTableViewCell){
        
        if dictToPass.count > 0 {
            if cell.seeAllLbl.text == "See all artist" {
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("seeAllArtist") as!SeeAllArtistViewController
                vc.titleTypeIs = "Artist"
                vc.getAllToPassApi = "artist"
                vc.artistDictFromSearchEvent = dictToPass
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if cell.seeAllLbl.text  == "See all venues"{
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("seeAllArtist") as!SeeAllArtistViewController
                vc.titleTypeIs = "Venues"
                vc.getAllToPassApi = "venues"
                vc.artistDictFromSearchEvent = dictToPass
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else {
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("seeAllEvents") as!SeeAllEventsViewController
                vc.titleTypeIs = "Events"
                vc.getAllToPassApi = "events"
                vc.eventsDictFromSearchEvent = dictToPass
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    //MARK:-favouriteClicked
    func homeEventsFavouritesButtonClicked(button: UIButton, index: NSInteger) {
        
        self.searchTF.resignFirstResponder()
        var eventId : Int = 0
        if let id = eventArrayIS.objectAtIndex(index).valueForKey("eventId") as? Int {
            eventId = id
        }
        
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            "customerId" : customerId,
            "eventId" : eventId
        ]
        print(getParam)
        // **** call webservice
        
        WebServiceCall.sharedInstance.favouriteEvent("FavouriteEvents.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            // ** check for valid result
            if isTrue == true {
                
                var isFavorite : Bool = false
                self.view.makeToast(message: message!)
                if let favorite =  self.eventArrayIS.objectAtIndex(index).valueForKey("isFavoriteEvent") as? Bool {
                    isFavorite = favorite
                    
                    if isFavorite == true {
                        isFavorite = false
                    } else {
                        isFavorite = true
                    }
                    
                }
                let dict = self.eventArrayIS.objectAtIndex(index) as! NSMutableDictionary
                let foundationDictionary = NSMutableDictionary(dictionary: dict)
                foundationDictionary.setValue(isFavorite, forKey: "isFavoriteEvent")
                self.eventArrayIS.replaceObjectAtIndex(index, withObject: foundationDictionary)
                self.tableView.reloadData()
                
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
        
    }
    // MARK:-FavArtist TappedDelegateCall
    func ArtistFavouritesButtonClicked(button: UIButton, index : NSInteger){
        
        if let artistId = artistArrayIS[index].valueForKey("artistId") as? Int{
            favArtsitApiCall(artistId,getIndex: index)
        }
        
    }
    //MARK:AddFavArtistcall
    func favArtsitApiCall(getArtistid:Int,getIndex:Int){
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** api call for fav Artist ***
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        //            self.setLoadingIndicator()
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            "artistActionType" :"FAVORITE",
            "artistIds":getArtistid,
            "customerId":customerId
        ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.postArtistfav("AddFavoriteOrSuperArtist.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            
            // ** check for valid result
            if isTrue == true {
                print("response Dict For Fav Artist",responseDisctionary)
                var isFavorite : Bool = false
                if let favorite =  self.artistArrayIS.objectAtIndex(getIndex).valueForKey("isFavoriteFlag") as? Bool {
                    isFavorite = favorite
                    
                    if isFavorite == true {
                        isFavorite = false
                    } else {
                        isFavorite = true
                    }
                    
                }
                let dict = self.artistArrayIS.objectAtIndex(getIndex) as! NSMutableDictionary
                let foundationDictionary = NSMutableDictionary(dictionary: dict)
                foundationDictionary.setValue(isFavorite, forKey: "isFavoriteFlag")
                self.artistArrayIS.replaceObjectAtIndex(getIndex, withObject: foundationDictionary)
                print("artist Array is",self.artistArrayIS[0])
                let msgIs = responseDisctionary?.valueForKey("actionResult") as! String
                self.view.makeToast(message: msgIs)
                self.tableView.reloadData()
                
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    @IBAction func searchTapped(sender: AnyObject) {
    }
    
}