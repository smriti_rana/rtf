//
//  ResultSearchEventViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 16/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
import Social
import MessageUI
import CoreLocation
class ResultSearchEventViewController: BaseViewController,UITextFieldDelegate,MFMailComposeViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,customActionsheetDelegate,HomeEventsTableViewCellDelegate,CLLocationManagerDelegate,SearchArtistCellDelegate{
    var dictToPass = [:]
    @IBOutlet weak var emptyLbl: UILabel!
    @IBOutlet weak var topbar: UIView!
    var mySubview:CustomSheetView!
    var resultDictIS = [:]
    var searchedtxtIS = ""
    var searchTypeIs = ""
    var  userDefaultLocDict:NSMutableArray = []
    
    var totalArtistCount : Int = 0
    var totalVenueCount : Int = 0
    var totalEventCount : Int = 0
    var grandChildIs :String = ""
     var pageCount = 1
    var artistArrayIS:NSMutableArray = []
    var venueArrayIS :NSMutableArray = []
    var eventArrayIS :NSMutableArray = []
     var locationManager : CLLocationManager!
    
    @IBOutlet weak var explore_titleLbl: UILabel!
    var latStr : String = ""
    var longStr : String = ""
    var locationOnOff = ""
    var artistIdToPassApi = ""
    var SearcbarTextfromexploreCell = ""
    var exploreTypeToSearch = ""
    var viewTypeIs = ""
    var startDate : String = ""
    var grandChildIdtoPassApi = 0
    var getartistIdtoPass = ""
    var endDate : String = ""
   
    @IBOutlet weak var exploreBGView: UIView!
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    @IBAction func searchTapped(sender: AnyObject) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: "SearchArtistTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")
        tableView.registerNib(UINib(nibName: "SeeAllFooterTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell1")
        tableView.registerNib(UINib(nibName: "HomeEventsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell2")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 72.0
        tableView.rowHeight = UITableViewAutomaticDimension
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            navbar_height.constant = 94
             explore_titleLbl.font = UIFont(name: explore_titleLbl.font.fontName, size: 32)
            explore_titleLbl.text = SearcbarTextfromexploreCell
            
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("productType") != nil {
            productType = NSUserDefaults.standardUserDefaults().objectForKey("productType") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("deviceId") != nil {
            deviceId = NSUserDefaults.standardUserDefaults().objectForKey("deviceId") as! String
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("configId") != nil {
            configId = NSUserDefaults.standardUserDefaults().objectForKey("configId") as! String
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("locationDict") != nil {
            userDefaultLocDict = (NSUserDefaults.standardUserDefaults().objectForKey("locationDict")  as? NSMutableArray)!
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("startDate") != nil {
            startDate = NSUserDefaults.standardUserDefaults().objectForKey("startDate") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("endDate") != nil {
            endDate = NSUserDefaults.standardUserDefaults().objectForKey("endDate") as! String
        }
        mySubview = NSBundle.mainBundle().loadNibNamed("CustomSheetView", owner: self, options: nil).first as? CustomSheetView
        mySubview.delegate = self
        tableView.hidden = true
        
        //set searchKey As Search
        self.searchTF.returnKeyType = .Search
        self.searchTF.delegate = self
        self.searchTF.text = searchedtxtIS
        
      
        tableView.reloadData()
//        let tap = UITapGestureRecognizer(target: self, action: #selector(SearchEventArtistViewController.handleTap(_:)))
//        tableView.addGestureRecognizer(tap)
//        tap.cancelsTouchesInView = false
        
        //Location check for APi Params
        // *** check for gps is on or off ***
        if (CLLocationManager.locationServicesEnabled())  {
            
            locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            
            if  (NSUserDefaults.standardUserDefaults().objectForKey("locationDict") != nil){
                locationOnOff = "Enabled"
                let latStrIs = "\(userDefaultLocDict.objectAtIndex(2))"
                let  longStrIS = "\(userDefaultLocDict.objectAtIndex(3))"
                latStr = latStrIs
                longStr = longStrIS
            }
            else{
                locationOnOff = "AllLocation"
                latStr = "0"
                longStr = "0"
            }
            
        }
        else{
            locationOnOff = "Disabled"
            latStr = ""
            longStr = ""
        }

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 3
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var countIs = 0
        if section == 0{
            countIs = artistArrayIS.count
        }
        else if section ==  1{
            countIs = venueArrayIS.count
        }
        else if section == 2{
            countIs = eventArrayIS.count
        }
        return countIs
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        if indexPath.section == 2 {
        let cell1 = tableView.dequeueReusableCellWithIdentifier("Cell2") as! HomeEventsTableViewCell
            cell1.delegate = self
            
            cell1.cardNameHeightConstraint.constant = 0
            cell1.topViewHeightConstraint.constant = 0
            cell1.bottomViewHeightConstraint.constant = 0
            
            cell1.shareButton.tag = indexPath.row
            cell1.favouritesButton.tag = indexPath.row
            mySubview.shareBtn.tag = indexPath.row
            mySubview.TwitterBtn.tag = indexPath.row
            mySubview.shareEmailBtn.tag = indexPath.row
            cell1.cardNameHeightConstraint.constant = 0
            cell1.selectionStyle = .None
            
            if eventArrayIS.count > 0{
                
                if let eventName = eventArrayIS[indexPath.row].valueForKey("artistName") as? String{
                    cell1.eventNameLabel.text = eventName
                }
    
                if let Eventdate =  eventArrayIS[indexPath.row].valueForKey("eventDateStr") as? String{
                    //print(Eventdate)
                    //print(indexPath.row)
                    if Eventdate != "TBD" {
                        // *** get datey from string *** //
                        
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "MM/dd/yyyy"
                        let dateFromString = dateFormatter.dateFromString(Eventdate)
                        
                        // *** get day *** //
                        
                        let dateFormatteerForDay = NSDateFormatter()
                        dateFormatteerForDay.dateFormat = "dd"
                        let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                        
                        cell1.dayLabel.text = dayFromDate
                        
                        // *** get year *** //
                        
                        
                        let dateFormatteerForYear = NSDateFormatter()
                        dateFormatteerForYear.dateFormat = "YYYY"
                        let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                        
                        cell1.yearLabel.text = yearFromDate
                        
                        // *** get month *** //
                        
                        let dateFormatteerForMonth = NSDateFormatter()
                        dateFormatteerForMonth.dateFormat = "MMM"
                        let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                        
                        cell1.monthLabel.text = monthFromDate
                        
                        // *** get week day *** //
                        
                        let dateFormatteerForWeekDay = NSDateFormatter()
                        dateFormatteerForWeekDay.dateFormat = "EEE"
                        let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                        
                        cell1.weekDayLabel.text = weekDayFromDate

                    } else {
                        cell1.dayLabel.text = "TBD"
                        cell1.yearLabel.text = "TBD"
                        cell1.monthLabel.text = "TBD"
                        cell1.weekDayLabel.text = "TBD"
                    }
                }
                // *** set favorites *** //
                if let isFavorite = eventArrayIS.objectAtIndex(indexPath.row).valueForKey("isFavoriteEvent") as? Bool {
                    if isFavorite {
                        cell1.favouritesButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
                    } else {
                        cell1.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                    }
                } else {
                    cell1.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                }
                //venue Address
                var eTime = ""
                var eVenue = ""
                var eCity = ""
                var eState = ""
                if let Eventtime = eventArrayIS[indexPath.row].valueForKey("eventTimeStr") as? String{
                    //24hr format
                    let timeSlot = Eventtime
                    //print(Eventtime)
                    if Eventtime == "TBD"  {
                        eTime  = "TBD"
                    }
                    else{
                    let dateFormatterForTime = NSDateFormatter()
                    
                    dateFormatterForTime.dateFormat = "hh:mma"
                    let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                    
                    
                    
                    let dateFormatteerForTimeFormat = NSDateFormatter()
                    
                    dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                    
                    eTime = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                    }
                }
                    
                else{
                    eTime = "12:00AM"
                }
                if let Eventvenue = eventArrayIS[indexPath.row].valueForKey("venueName") as? String{
                    eVenue = Eventvenue
                }
                if let Eventcity = eventArrayIS[indexPath.row].valueForKey("state") as? String{
                    eCity = Eventcity
                }
                if let EventState = eventArrayIS[indexPath.row].valueForKey("country") as? String{
                    eState = EventState
                }
                let string1 = eTime
                let resultStr = "\(string1)"+"\("-")" + " \(eVenue)" + "\(" ")" + "\(eCity)" + "\(" ")" + "\(eState)"
                
                cell1.eventTimeAndAddressLabel.text = resultStr
                
              
                if let EventticketPrice = eventArrayIS[indexPath.row].valueForKey("ticketPriceTag") as? String{
                     cell1.priceLabel.text = EventticketPrice
                }
                // *** change color of cell *** //
                
                if indexPath.row % 2 != 0 {
                    cell1.colorView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
                } else {
                    cell1.colorView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
                }
                }
            return cell1
        }
            
        else{
         
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! SearchArtistTableViewCell
            cell.separatorInset = UIEdgeInsetsZero
            cell.artistBtn.tag = indexPath.row
            cell.delegate = self
            if indexPath.section == 0 {
                
                if artistArrayIS.count > 0{
                    
                    cell.favicon_Width.constant = 24
                    
                    if let artistName = artistArrayIS[indexPath.row].valueForKey("artistName") as? String{
                        
                        cell.artistLbl.text = artistName
                        
                    }
                    
                    //print("tableView Artist Array",artistArrayIS.objectAtIndex(indexPath.row))
                    if let favorite =  self.artistArrayIS.objectAtIndex(indexPath.row).valueForKey("isFavoriteFlag") as? Bool {
                       
                            if favorite == true {
                                
                                cell.fav_imgView.image = UIImage(named: "heartRed")
                            }
                            else {
                                cell.fav_imgView.image = UIImage(named: "heartWhite")
                            }
                            
                        }
                   
                }
                
            }
                
            else if indexPath.section == 1{
                
                if venueArrayIS.count > 0{
                    
                    cell.favicon_Width.constant = 0
                    
                    if let artistName = venueArrayIS[indexPath.row].valueForKey("venueName") as? String{
                        cell.artistLbl.text = artistName
                        
                    }
                    
                }
                
            }
            // *** change color of cell *** //
            
            if indexPath.row % 2 != 0 {
                cell.contentView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
            } else {
                cell.contentView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
            }

            return cell
            
        }
    
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      
        return "Hello"
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell =  tableView.dequeueReusableCellWithIdentifier("Cell1") as! SeeAllFooterTableViewCell
        if section == 0 {
            headerCell.seeAllLbl.text = "ARTISTS"
            headerCell.seeAllBtn.hidden =  true
            
        }
        else if section == 1{
            headerCell.seeAllLbl.text = "VENUES"
            headerCell.seeAllBtn.hidden =  true
        }
        else if section == 2{
            headerCell.seeAllLbl.text = "EVENTS"
            headerCell.seeAllBtn.hidden =  true
        }
        headerCell.seeAllLbl.textColor = UIColor.whiteColor()
        headerCell.contentView.backgroundColor = UIColor.init(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        return headerCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 60.0
        }
        return 40.0
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       
        return CGFloat.min
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            if let artistId = artistArrayIS[indexPath.row].valueForKey("artistId") as? Int {
                if let artistName = artistArrayIS[indexPath.row].valueForKey("artistName") as? String {
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("seeAllEvents") as!SeeAllEventsViewController
                    vc.titleTypeIs = "Artist"
                    vc.getAllToPassApi = "artist"
                    vc.artistIdIs = "\(artistId)"
                    vc.venueIdIs = ""
                    vc.artistNameIs = artistName
                    vc.eventsDictFromSearchEvent = dictToPass
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }
        else if indexPath.section == 1 {
            if let venueId = venueArrayIS[indexPath.row].valueForKey("venueId") as? Int {
                if  let venueName = venueArrayIS[indexPath.row].valueForKey("venueName") as? String {
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("seeAllEvents") as!SeeAllEventsViewController
                    vc.titleTypeIs = "Venues"
                    vc.getAllToPassApi = "venues"
                    vc.venueIdIs = "\(venueId)"
                    vc.artistIdIs = " "
                    vc.artistNameIs = venueName
                    vc.eventsDictFromSearchEvent = dictToPass
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
            
        else if indexPath.section == 2 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EventDetail") as! EventDetailViewController
            if let id = eventArrayIS[indexPath.row].valueForKey("eventId") as? Int{
                vc.eventId = String(id)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }

        
    }

    //MARK:-Searched Type text
    @IBAction func searchBarValueChanges(sender: AnyObject) {
         self.navigationController!.popViewControllerAnimated(true)
    }
    //MARK:-Share delegate
    func EventsShareButtonClicked(button: UIButton, index : NSInteger){
        
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.3)
        mySubview.delegate = self
        mySubview.customSheetBgView.backgroundColor = greyColor
        
        self.mySubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(SearchEventArtistViewController.handleTap(_:)))
        mySubview.customSheetBgView.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        mySubview.customSheetBgView.hidden = false
        mySubview.sortBgView.hidden = true
        mySubview.priceBgView.hidden = true
        mySubview.sharingBgView.hidden = false
        mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, mySubview.customSheetBgView.frame.size.height)
        self.view.addSubview(mySubview)
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 1.0
            self.mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height - 123, self.view.frame.size.width, self.mySubview.customSheetBgView.frame.size.height)
            }, completion: { finished in
        })
        
        
    }
    // *** touch events *** //
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        handleTap()
        
    }
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        searchTF.resignFirstResponder()
        removeCustomView()
    }
    func removeCustomView() {
        searchTF.resignFirstResponder()
        if mySubview == nil {
            return
        }
        
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 0.0
            }, completion: { finished in
                self.mySubview.removeFromSuperview()
        })
    }
    
    //MARK-:ShareDialogMethods
    func shareOnFacebook() {
        
        let facebookPost = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        facebookPost.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                break
                
            case SLComposeViewControllerResult.Done:
                //Code here to deal with it being completed
                self.removeCustomView()
                break
            }
        }
        
        //        facebookPost.setInitialText(desc) //The default text in the tweet
        //        facebookPost.addImage(UIImage(data: data!)) //Add an image
        presentViewController(facebookPost, animated: false, completion: {
            //Optional completion statement
        })
        
        
    }
    //MARK:-TwitterCicked
    func shareOnTwitter(){
        let twitterSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        twitterSheet.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                ////print("cancel clicked")
                
                break
                
            case SLComposeViewControllerResult.Done:
                self.removeCustomView()
                //Code here to deal with it being completed
                break
            }
        }
        //        twitterSheet.setInitialText("\(desc), \(urlImage)")
        self.presentViewController(twitterSheet, animated: false, completion: {
            
        })
        
    }
    //MARK:-Email sharing
    func shareViaEmail(){
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["ghakher.preeti03@gmail.com"])
            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            presentViewController(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
        
        
    }
    @IBOutlet weak var backBtn: UIButton!
    @IBAction func backTapped(sender: AnyObject) {
         self.navigationController!.popViewControllerAnimated(true)
    }
    //MARK:-Share delegate
    func homeEventsShareButtonClicked(button: UIButton, index : NSInteger){
        
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.3)
        mySubview.delegate = self
        mySubview.customSheetBgView.backgroundColor = greyColor
        
        self.mySubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ResultSearchEventViewController.handleTap(_:)))
        mySubview.customSheetBgView.addGestureRecognizer(tap)
        
        mySubview.customSheetBgView.hidden = false
        mySubview.sortBgView.hidden = true
        mySubview.priceBgView.hidden = true
        mySubview.sharingBgView.hidden = false
        mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, mySubview.customSheetBgView.frame.size.height)
        self.view.addSubview(mySubview)
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 1.0
            self.mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height - 123, self.view.frame.size.width, self.mySubview.customSheetBgView.frame.size.height)
            }, completion: { finished in
        })
        
        
    }
    
   
    //Mark:ShareDialogMethods
    func shareOnFacebook(index:Int) {
        let shareLinkIs = eventArrayIS[index-1].valueForKey("eventName") as? String
        let facebookPost = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        facebookPost.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                break
                
            case SLComposeViewControllerResult.Done:
                //Code here to deal with it being completed
                self.removeCustomView()
                break
            }
        }
        
        facebookPost.setInitialText(shareLinkIs!) //The default text in the tweet
        presentViewController(facebookPost, animated: false, completion: {
            //Optional completion statement
        })
        
        
    }
    //Mark:TwitterCicked
    func shareOnTwitter(index:Int){
        let shareLinkIs = eventArrayIS[index-1].valueForKey("eventName") as? String
        let twitterSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        twitterSheet.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                ////print("cancel clicked")
                
                break
                
            case SLComposeViewControllerResult.Done:
                self.removeCustomView()
                //Code here to deal with it being completed
                break
            }
        }
        twitterSheet.setInitialText("\(shareLinkIs!)")
        self.presentViewController(twitterSheet, animated: false, completion: {
            
        })
        
    }
    //MARK:-Email sharing
    func shareViaEmail(index:Int){
        let shareLinkIs = eventArrayIS[index-1].valueForKey("eventName") as? String
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setMessageBody(shareLinkIs!, isHTML: true)
            
            presentViewController(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
        
        
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        removeCustomView()
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    //MARK:-Api CallForSearched data
    func ApiCallForsearchedText(getText:String,searchType:String,pageNo:Int){
        
        // *** api call for forgot password ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for validations
        if searchTF.text == "" {
            stopIndicator()
            
        }
        else
        {
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                stopIndicator()
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            // *** send parameters
            let getParam = [
                "deviceId" : deviceId,
                "productType" : productType,
                "searchType" : "NORMALSEARCH",
                "searchKey":getText,
                "grandChildId":grandChildIs,
                "startDate":startDate,
                "endDate":endDate,
                "customerId":customerId,
                "locationOption":locationOnOff,
                "latitude":latStr,
                "longitude":longStr,
                "configId":configId,
                "artistId":artistIdToPassApi,
                "pageNumber":pageNo
                
            ]
            //print(getParam)
            dictToPass = getParam
            // **** call webservice
            WebServiceCall.sharedInstance.post("GeneralizedSearch.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
                
                if isTrue == true {
                    self.tableView.hidden = false
                        if let autosearchDict = responseDisctionary!.valueForKey("normalSearchResult") as? NSDictionary  {
                        
                        if let artistArray = autosearchDict.objectForKey("artistResults")  as? NSArray {
                            //print("artist Array",artistArray)
                                if self.pageCount == 1 {
                                    self.artistArrayIS.removeAllObjects()
                                }
                                
                                for i in 0 ..< artistArray.count {
                                    self.artistArrayIS .addObject(artistArray[i])
                                }
                            }
                           
                        if let venArray = autosearchDict.objectForKey("venueResults")  as? NSArray {
                            //print(venArray)
                            if self.pageCount == 1 {
                                    self.venueArrayIS.removeAllObjects()
                                }
                                
                                for i in 0 ..< venArray.count {
                                    self.venueArrayIS .addObject(venArray[i])
                                }
                            }
                           
                        if let eventArray = autosearchDict.objectForKey("events")  as? NSArray {
                            //print("event array",eventArray)
                            if eventArray.count  > 0 {
                                if self.pageCount == 1 {
                                    self.eventArrayIS.removeAllObjects()
                                }
                                
                                for i in 0 ..< eventArray.count {
                                    self.eventArrayIS .addObject(eventArray[i])
                                }
                            }else{
                                self.pagingSpinner.stopAnimating()
                            }
                            }
                     
                        
                        if let artistArrayCount = autosearchDict.objectForKey("totalArtistCount")  as? Int {
                            self.totalArtistCount = artistArrayCount
                        }
                        if let venueArrayCount = autosearchDict.objectForKey("totalVenueCount")  as? Int {
                            self.totalVenueCount = venueArrayCount
                          
                        }
                        if let eventArrayCount = autosearchDict.objectForKey("totalEventCount")  as? Int {
                            self.totalEventCount = eventArrayCount
                            
                        }
                            //print(self.artistArrayIS.count,self.venueArrayIS.count,self.eventArrayIS.count)
                            if self.artistArrayIS.count == 0 && self.venueArrayIS.count == 0 && self.eventArrayIS.count == 0{
                                //print("empty tableview")
                                self.tableView.hidden = true
                                self.emptyLbl.hidden = false
                                self.emptyLbl.text = self.emptyTableSearchMsg
                            }
                      
                    }
                    self.stopIndicator()
                    self.tableView.reloadData()
                   NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(ResultSearchEventViewController.update), userInfo: nil, repeats: false)
                   
                    
                } else {
                    self.stopIndicator()
                    if message == "" {
                        // * if no response
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    func update() {
        tableView.reloadData()
    }
    //MARK:-PaginationTableView
    var pagingSpinner = UIActivityIndicatorView()
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if scrollView == tableView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
                pagingSpinner.startAnimating()
                pagingSpinner.color = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
                pagingSpinner.hidesWhenStopped = true
                
                tableView.tableFooterView = pagingSpinner
                pageCount += 1
                if viewTypeIs == "Explore" {
                delay(0.1) { () -> () in
                self.ApiCallForExploresearchedText(self.exploreTypeToSearch,pageCountIs: self.pageCount)
                    }
                }else{
                    self.ApiCallForsearchedText(self.searchTF.text!,searchType: self.searchTypeIs,pageNo: pageCount)
                }
              
                
            }
        }
    }
    override func viewWillAppear(animated: Bool) {
       
        if viewTypeIs == "Explore" || viewTypeIs == "ExploreSearchAll"{
            setLoadingIndicator(self.tableView)
            searchTF.placeholder = "Search your Event, Band, Show"
            if viewTypeIs == "Explore" {
                self.searchTF.text = SearcbarTextfromexploreCell
                explore_titleLbl.text = SearcbarTextfromexploreCell
//                searchBtn.hidden = true
                topbar.hidden = true
                exploreBGView.hidden = false
                delay(0.1) { () -> () in
                    self.ApiCallForExploresearchedText(self.exploreTypeToSearch,pageCountIs: self.pageCount)
                }
            }
        }
        else if viewTypeIs == "HomeViewWithSeeMore"{
            artistIdToPassApi = getartistIdtoPass
          if grandChildIdtoPassApi == 0{
                grandChildIs = ""
            }
          else{
            grandChildIs = "\(grandChildIdtoPassApi)"
            }
            self.ApiCallForsearchedText(self.searchTF.text!,searchType: self.searchTypeIs,pageNo: pageCount)
        }
        else{
        
        if viewTypeIs == "HomeView" {
            grandChildIs = "\(grandChildIdtoPassApi)"
        }else{
            grandChildIs = ""
        }
        let frameIs :CGRect = topbar.frame
        setLoadingIndicator(self.view)
            //print(frameIs)
        topbar.hidden = false
        exploreBGView.hidden = true
        self.ApiCallForsearchedText(self.searchTF.text!,searchType: self.searchTypeIs,pageNo: pageCount)
        }
      
        
    }
    //MARK:-ApiCall for ExploreSearch
    func ApiCallForExploresearchedText(searchOption:String,pageCountIs:Int){
        
        // *** api call for forgot password ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for validations
        if searchTF.text == "" {
            stopIndicator()
            alertVc.message = ExploreAlertmsgs
            self.presentViewController(alertVc, animated: true, completion: nil)
            
        }
        else
        {
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                stopIndicator()
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            // *** send parameters
            let getParam = [
                "productType" : productType,
                "exploreSearchOption" : searchOption,
                "startDate":startDate,
                "endDate":endDate,
                "customerId":customerId,
                "locationOption":locationOnOff,
                "latitude":latStr,
                "longitude":longStr,
                "configId":configId,
                "pageNumber":pageCountIs
                
            ]
            //print(getParam)
           
            // **** call webservice
            WebServiceCall.sharedInstance.postEventSearch("EventSearchByExplore.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
                
                if isTrue == true {
                    self.tableView.hidden = false
                    self.stopIndicator()
                    //print("explore result as",responseDisctionary)
                    if let autosearchDict = responseDisctionary!.valueForKey("normalSearchResult") as? NSDictionary  {
                        if let artistArray = autosearchDict.objectForKey("artistResults")  as? NSArray {
                            //print(artistArray.count)
                            if self.pageCount == 1 {
                                self.artistArrayIS.removeAllObjects()
                            }
                            for i in 0 ..< artistArray.count {
                                self.artistArrayIS .addObject(artistArray[i])
                            }
                        }
                        else{
                            self.pagingSpinner.stopAnimating()
                            
                        }
                        if let venArray = autosearchDict.objectForKey("venueResults")  as? NSArray {
                            //print(venArray)
                            if self.pageCount == 1 {
                                self.venueArrayIS.removeAllObjects()
                            }
                            for i in 0 ..< venArray.count {
                                self.venueArrayIS .addObject(venArray[i])
                            }
                            
                        }
                        else{
                            self.pagingSpinner.stopAnimating()
                            
                        }
                        if let eventArray = autosearchDict.objectForKey("events")  as? NSArray {
                            //print("event array",eventArray)
                            if self.pageCount == 1 {
                                self.eventArrayIS.removeAllObjects()
                            }
                            
                            for i in 0 ..< eventArray.count {
                                self.eventArrayIS .addObject(eventArray[i])
                            }
                        }
                        if self.artistArrayIS.count == 0 && self.venueArrayIS.count == 0 && self.eventArrayIS.count == 0{
                            self.tableView.hidden = true
                            self.emptyLbl.hidden = false
                            self.emptyLbl.text = self.emptyTableSearchMsg
                        }
                        if let artistArrayCount = autosearchDict.objectForKey("totalArtistCount")  as? Int {
                            self.totalArtistCount = artistArrayCount
                        }
                        if let venueArrayCount = autosearchDict.objectForKey("totalVenueCount")  as? Int {
                            self.totalVenueCount = venueArrayCount
                            //print("venue Array count",self.totalVenueCount )
                        }
                        if let eventArrayCount = autosearchDict.objectForKey("totalEventCount")  as? Int {
                            self.totalEventCount = eventArrayCount
                     
                    }
                    //print("\(self.artistArrayIS.count)" + "\(self.venueArrayIS.count)" + "\(self.eventArrayIS.count)")
                        if ((self.artistArrayIS.count == 0) && (self.venueArrayIS.count  == 0) && (self.eventArrayIS.count == 0)){
                            self.tableView.hidden = true
                        }
                    self.tableView.reloadData()
                    NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(SearchEventArtistViewController.update), userInfo: nil, repeats: false)
                    }
                    
                } else {
                    self.stopIndicator()
                    if message == "" {
                        // * if no response
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        //print(message)
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        }
    }

    
    //MARK:-favouriteClicked
    func homeEventsFavouritesButtonClicked(button: UIButton, index: NSInteger) {
        
        self.searchTF.resignFirstResponder()
        var eventId : Int = 0
        if let id = eventArrayIS.objectAtIndex(index).valueForKey("eventId") as? Int {
            eventId = id
        }
        
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            "customerId" : customerId,
            "eventId" : eventId
        ]
        //print(getParam)
        // **** call webservice
        
        WebServiceCall.sharedInstance.favouriteEvent("FavouriteEvents.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            // ** check for valid result
            if isTrue == true {
                
                var isFavorite : Bool = false
                self.view.makeToast(message: message!)
                if let favorite =  self.eventArrayIS.objectAtIndex(index).valueForKey("isFavoriteEvent") as? Bool {
                    isFavorite = favorite
                    
                    if isFavorite == true {
                        isFavorite = false
                    } else {
                        isFavorite = true
                    }
                    
                }
                let dict = self.eventArrayIS.objectAtIndex(index) as! NSMutableDictionary
                let foundationDictionary = NSMutableDictionary(dictionary: dict)
                foundationDictionary.setValue(isFavorite, forKey: "isFavoriteEvent")
                self.eventArrayIS.replaceObjectAtIndex(index, withObject: foundationDictionary)
                self.tableView.reloadData()
                
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    // MARK:-FavArtist TappedDelegateCall
    func ArtistFavouritesButtonClicked(button: UIButton, index : NSInteger){
        
        if let artistId = artistArrayIS[index].valueForKey("artistId") as? Int{
            favArtsitApiCall(artistId,getIndex: index)
        }
        
    }
    //MARK:AddFavArtistcall
    func favArtsitApiCall(getArtistid:Int,getIndex:Int){
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** api call for fav Artist ***
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        //            self.setLoadingIndicator()
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            "artistActionType" :"FAVORITE",
            "artistIds":getArtistid,
            "customerId":customerId
        ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.postArtistfav("AddFavoriteOrSuperArtist.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            
            // ** check for valid result
            if isTrue == true {
                //print("response Dict For Fav Artist",responseDisctionary)
                var isFavorite : Bool = false
                if let favorite =  self.artistArrayIS.objectAtIndex(getIndex).valueForKey("isFavoriteFlag") as? Bool {
                    isFavorite = favorite
                    
                    if isFavorite == true {
                        isFavorite = false
                    } else {
                        isFavorite = true
                    }
                    
                }
                let dict = self.artistArrayIS.objectAtIndex(getIndex) as! NSMutableDictionary
                let foundationDictionary = NSMutableDictionary(dictionary: dict)
                foundationDictionary.setValue(isFavorite, forKey: "isFavoriteFlag")
                self.artistArrayIS.replaceObjectAtIndex(getIndex, withObject: foundationDictionary)
                //print("artist Array is",self.artistArrayIS[0])
                let msgIs = responseDisctionary?.valueForKey("actionResult") as! String
                self.view.makeToast(message: msgIs)
                self.tableView.reloadData()
                
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
}
