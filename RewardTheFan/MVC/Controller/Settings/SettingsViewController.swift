//
//  SettingsViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 26/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import FBSDKShareKit
import Social
import FBSDKLoginKit
import CoreLocation
import PinterestSDK
class SettingsViewController: BaseViewController, UIDocumentInteractionControllerDelegate , CLLocationManagerDelegate {
    var refferCodeToShare = ""
    var setLocation_pushflag = 1
    
    var yourImage = UIImage(named : "nav")
    @IBOutlet weak var locationToggleBgColor: UIView!
    @IBOutlet var currentLocationImageImageView: UIImageView!
    
    @IBOutlet weak var currentLocationView_height: NSLayoutConstraint!
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    var getUserName = ""
    var documentController: UIDocumentInteractionController!
    @IBOutlet var signInButton: UIButton!
    
    @IBOutlet weak var socialIconsBgViewWidth: NSLayoutConstraint!
    @IBOutlet weak var linkedinWidth: NSLayoutConstraint!
    @IBOutlet weak var twitterWidth: NSLayoutConstraint!
    @IBOutlet weak var facebook_width: NSLayoutConstraint!
    @IBOutlet var loggedInAsLabel: UILabel!
    @IBOutlet weak var pinterestWidth: NSLayoutConstraint!
    
    @IBOutlet weak var instagram_Width: NSLayoutConstraint!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet var logoutButton: UIButton!
    @IBOutlet weak var TitleLbl: UILabel!
    
    var locationManager : CLLocationManager!
    
    var viewType : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            signInButton.hidden = true
            logoutButton.hidden = false
            loggedInAsLabel.hidden = false
            
            if NSUserDefaults.standardUserDefaults().valueForKey("customerName") != nil {
                getUserName = NSUserDefaults.standardUserDefaults().valueForKey("customerName") as! String
                
                loggedInAsLabel.text = "You are logged in as: \(getUserName)"
                
                let attributedString = NSMutableAttributedString(attributedString: loggedInAsLabel.attributedText!)
                let range:NSRange = (loggedInAsLabel.text! as NSString).rangeOfString(getUserName)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: blueColorComment , range: range)
                loggedInAsLabel.attributedText = attributedString
            }
            
        } else {
            signInButton.hidden = false
            logoutButton.hidden = true
            loggedInAsLabel.hidden = true
        }
        
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            TitleLbl.font = UIFont(name: TitleLbl.font.fontName, size: 31)
            navbar_height.constant = 74
        }
        
//        if  UIApplication.sharedApplication().canOpenURL(NSURL(string: "fb://")!) {
//            NSLog("No Facebook")
//            facebook_width.constant = 0
//        }
//        else if UIApplication.sharedApplication().canOpenURL(NSURL(string: "instagram://app")!) {
//            NSLog("No Insta")
//            instagram_Width.constant = 0
//        }
//        else if UIApplication.sharedApplication().canOpenURL(NSURL(string: "linkedin://app")!)  {
//            NSLog("No Insta")
//            linkedinWidth.constant = 0
//        }
//        socialIconsBgViewWidth.constant = facebook_width.constant + pinterestWidth.constant + twitterWidth.constant  + instagram_Width.constant + linkedinWidth.constant + 50
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SettingsViewController.appWillEnterForeground(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            locationManager.startUpdatingLocation()
            if (CLLocationManager.authorizationStatus() == .AuthorizedAlways || CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse) {
                self.locationBtn.setOn(true, animated: true)
                self.locationBtn.thumbTintColor = ColorConstant.orangeColor
                self.locationBtn.onImage = UIImage(named: "settingsOrange")
                currentLocationView_height.constant = 52
                if let loc = NSUserDefaults.standardUserDefaults().valueForKey(NSUserKeys.CurrentCity) as? String {
                    locationLabel.text = loc
                    self.locationLabel.textColor = ColorConstant.orangeColor
                    currentLocationImageImageView.hidden = false
                     locationToggleBgColor.backgroundColor = UIColor.clearColor()
                }
                
            } else {
                self.locationBtn.setOn(false, animated: true)
                self.locationBtn.thumbTintColor = UIColor.lightGrayColor()
                
                self.locationBtn.offImage = UIImage(named: "settingsGrey")
                currentLocationView_height.constant = 0
                self.locationLabel.hidden = true
              locationToggleBgColor.backgroundColor = ColorConstant.BgCellBaseColor
                    currentLocationImageImageView.hidden = true
            }
        } else {
            self.locationBtn.setOn(false, animated: true)
            self.locationBtn.thumbTintColor = UIColor.lightGrayColor()
            locationToggleBgColor.backgroundColor = ColorConstant.BgCellBaseColor
            currentLocationView_height.constant = 0
            self.locationBtn.offImage = UIImage(named: "settingsGrey")
             self.locationLabel.hidden = true
            self.locationLabel.text = "Turn on location"
            self.locationLabel.textColor = UIColor.lightGrayColor()
            currentLocationImageImageView.hidden = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
        case .Authorized, .AuthorizedWhenInUse: break
        case .Denied:
            self.locationBtn.setOn(false, animated: true)
            self.locationBtn.thumbTintColor = UIColor.lightGrayColor()
            self.locationBtn.offImage = UIImage(named: "settingsGrey")
            currentLocationView_height.constant = 0
            locationToggleBgColor.backgroundColor = ColorConstant.BgCellBaseColor
            self.locationLabel.hidden = true
            self.locationLabel.text = "Turn on location"
            self.locationLabel.textColor = UIColor.lightGrayColor()
            currentLocationImageImageView.hidden = true
            break
            
        default:
            self.locationBtn.setOn(false, animated: true)
            self.locationBtn.thumbTintColor = UIColor.lightGrayColor()
            self.locationBtn.offImage = UIImage(named: "settingsGrey")
            locationToggleBgColor.backgroundColor = ColorConstant.BgCellBaseColor
            currentLocationView_height.constant = 0
             self.locationLabel.hidden = true
            self.locationLabel.text = "Turn on location"
            self.locationLabel.textColor = UIColor.lightGrayColor()
            currentLocationImageImageView.hidden = true
            break
        }
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last! as CLLocation
        
        let geoCoder = CLGeocoder()
        
        let latitude : String = ("\(location.coordinate.latitude)")
        NSUserDefaults.standardUserDefaults().setObject(latitude, forKey: "latitude")
        
        let longitude : String = ("\(location.coordinate.longitude)")
        NSUserDefaults.standardUserDefaults().setObject(longitude, forKey: "longitude")
        
        let locationForZip = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(locationForZip, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            
            placeMark = placemarks?[0]
            // Address dictionary
            
            if !(placeMark == nil) {
                
                if !(placeMark.addressDictionary == nil) {
                    
                    print(placeMark.addressDictionary)
                    
                    if let city = placeMark.addressDictionary!["City"] as? String {
                        print(city)
                        NSUserDefaults.standardUserDefaults().setObject(city, forKey: NSUserKeys.CurrentCity)
                        
                        if let loc = NSUserDefaults.standardUserDefaults().valueForKey(NSUserKeys.CurrentCity) as? String {
                            self.currentLocationView_height.constant = 52
                            self.locationLabel.text = loc
                            self.locationLabel.textColor = ColorConstant.orangeColor
                            self.currentLocationImageImageView.hidden = false
                        }
                    } else {
                        
                    }
                    
                } else {
                    
                }
            } else {
                print("*********************** response nil ***********************")
                
            }
        })
        locationManager.stopUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func signInButtonTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func logoutButtonTapped(sender: AnyObject) {
        
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "customerId")
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "skipWithoutLogin")
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:-Facebook Tapped
    @IBOutlet weak var facebookBtn: UIButton!
    @IBAction func facebookTapped(sender: AnyObject) {
        if self.refferCodeToShare == ""{
            let alert = UIAlertController(title: "Message", message: "There is no content to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let linkcontent: FBSDKShareLinkContent = FBSDKShareLinkContent()
            linkcontent.contentDescription = refferCodeToShare
            linkcontent.contentTitle = "\(AppName)"
            let dialog = FBSDKShareDialog()
            dialog.fromViewController = self;
            dialog.shareContent = linkcontent
            dialog.mode = FBSDKShareDialogMode.Automatic;
            dialog.show()
        }
        
    }
    //MARK:-Twitter Tapped
    @IBOutlet weak var twitterBtn: UIButton!
    @IBAction func twitterTapped(sender: AnyObject) {
        
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
            let twitterSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twitterSheet.completionHandler = {
                result in
                switch result {
                case SLComposeViewControllerResult.Cancelled:
                    
                    break
                    
                case SLComposeViewControllerResult.Done:
                    
                    break
                }
            }
            if self.refferCodeToShare == ""{
                let alert = UIAlertController(title: "Message", message: "There is no content to share.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }else{
                twitterSheet.setInitialText(refferCodeToShare)
                self.presentViewController(twitterSheet, animated: true, completion: nil)
            }
        }
        else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    //MARK:-RefferalCode Api
    func getRefferalcodeApi(){
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        guard IJReachability.isConnectedToNetwork() else {
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        self.setLoadingIndicator(self.view)
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            "customerId" : "50"
            
        ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.GetReffererCode("GetReffererCode.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            // * stop activityIndicator
            self.stopIndicator()
            
            // ** check for valid result
            if isTrue == true {
                print(responseDisctionary)
                if let responseRefferCodeIs = responseDisctionary?.valueForKey("referrerCode") as? String{
                    print(responseRefferCodeIs)
                    self.refferCodeToShare = responseRefferCodeIs
                    
                }
                
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    override func viewWillAppear(animated: Bool) {
        getRefferalcodeApi()
    }
    //MARK:-Linkedin Tapped
    @IBOutlet weak var linkedinBtn: UIButton!
    @IBAction func linkedinTapped(sender: AnyObject) {
        
        
        
        guard IJReachability.isConnectedToNetwork() else {
            let alertView = UIAlertView(title: "", message: ErrorInternetConnection, delegate: nil, cancelButtonTitle: "OK")
            alertView.show()
            return
        }
        
        let url:String = "https://api.linkedin.com/v1/people/~/shares"
        let payload:String = "{\"comment\":\"\(refferCodeToShare)\",\"visibility\":{\"code\":\"anyone\"}}"
        if LISDKSessionManager.hasValidSession(){
            LISDKAPIHelper.sharedInstance().postRequest(url, stringBody: payload, success: { (response) -> Void in
                ////print("\(response.data)")
                self.delay(0.2) { () -> () in
                    let alertView = UIAlertView(title: "", message: "Content has been shared successfully.", delegate: nil, cancelButtonTitle: "OK")
                    alertView.show()
                }
                
                }, error: { (apiError) -> Void in
                    //////print("\(apiError.description)")
                    self.delay(0.2) { () -> () in
                        let alertView = UIAlertView(title: "", message: "Error", delegate: nil, cancelButtonTitle: "OK")
                        alertView.show()
                    }
            })
        } else{
            //////print("seccision expired")
            
            LISDKSessionManager .createSessionWithAuth([LISDK_BASIC_PROFILE_PERMISSION,LISDK_EMAILADDRESS_PERMISSION,LISDK_W_SHARE_PERMISSION], state: nil, showGoToAppStoreDialog: true, successBlock: {
                response in
                //Do something with the response
                
                if  let session = LISDKSessionManager.sharedInstance().session
                {
                    if let _ = session.accessToken.accessTokenValue
                    {
                        //////print(sessionkey)
                        
                    }
                }
                
                let url = NSString(string:"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address,formatted-name,phonetic-last-name,location:(country:(code)),industry,distance,current-status,current-share,network,skills,phone-numbers,date-of-birth,main-address,positions:(title),educations:(school-name,field-of-study,start-date,end-date,degree,activities),siteStandardProfileRequest)?format=json")
                
                if LISDKSessionManager.hasValidSession(){
                    //self.stopIndicator()
                    LISDKAPIHelper.sharedInstance().getRequest(url as String, success: {
                        response in
                        let data = response.data.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
                        do {
                            let json = try NSJSONSerialization.JSONObjectWithData(data, options: []) as! [String: AnyObject]
                            //print((json)
                            
                        } catch _ as NSError {
                            ////////print("Failed to load: \(error.localizedDescription)")
                        }
                        
                        
                        _ = response.data.componentsSeparatedByString(",")
                        ////////print("Srting is--",str)
                        }, error: {
                            error in
                            ////print(error);
                    })
                }
                },errorBlock:{
                    error in
                    ////print(error);
            });
        }
        
        
    }
    //MARK:-Pinterest Sharing
    @IBOutlet weak var pinterestBtn: UIButton!
    @IBAction func pinterestTapped(sender: AnyObject) {
        PDKPin.pinWithImageURL(NSURL(string: defaultimageLinkurl), link:NSURL(string:"https://itunesconnect.apple.com/itc/static/login?view=1&path=%2FWebObjects%2FiTunesConnect.woa%3F") , suggestedBoardName:AppName , note: "", withSuccess: {
            
        }) { (error) in
            print(error)
        }
    }
    
    //MARK:Instagram Sharing
    @IBOutlet weak var intagramBtn: UIButton!
    @IBAction func InstagramTapped(sender: AnyObject) {
        shareToInstagram()
    }
    
    
    func shareToInstagram() {
        
        let instagramURL = NSURL(string: "instagram://app")
        if (UIApplication.sharedApplication().canOpenURL(instagramURL!)) {
            
            let imageData = UIImageJPEGRepresentation(yourImage!, 100)
            let captionString = "caption"
            
            let writePath = (NSTemporaryDirectory() as NSString).stringByAppendingPathComponent("instagram.igo")
            if imageData?.writeToFile(writePath, atomically: true) == false {
                return
                
            } else {
                let fileURL = NSURL(fileURLWithPath: writePath)
                
                self.documentController = UIDocumentInteractionController(URL: fileURL)
                
                self.documentController.delegate = self
                
                self.documentController.UTI = "com.instagram.exlusivegram"
                
                self.documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption")
                self.documentController.presentOpenInMenuFromRect(self.view.frame, inView: self.view, animated: true)
            }
            
        } else {
            print(" Instagram isn't installed ")
        }
    }
    func appWillEnterForeground(notification: NSNotification) {
        
        if (CLLocationManager.authorizationStatus() == .AuthorizedAlways || CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse) {
            
            if let loc = NSUserDefaults.standardUserDefaults().valueForKey(NSUserKeys.CurrentCity) as? String {
                 currentLocationView_height.constant = 52
                locationLabel.text = loc
                self.locationLabel.textColor = UIColor.grayColor()
                locationToggleBgColor.backgroundColor = UIColor.clearColor()
                currentLocationImageImageView.hidden = false
            }
             currentLocationView_height.constant = 52
            self.locationBtn.thumbTintColor = ColorConstant.orangeColor
            locationToggleBgColor.backgroundColor = UIColor.clearColor()
            self.locationBtn.setOn(true, animated: true)
        } else {
            locationToggleBgColor.backgroundColor = ColorConstant.BgCellBaseColor
            currentLocationView_height.constant = 0
            locationLabel.text = "Turn on location"
            self.locationLabel.textColor = UIColor.lightGrayColor()
            currentLocationImageImageView.hidden = true
            self.locationBtn.thumbTintColor = UIColor.lightGrayColor()
            self.locationBtn.setOn(false, animated: true)
        }
    }
    
    //MARK:-Location Toggle
    @IBOutlet weak var locationBtn: UISwitch!
    @IBAction func locationTapped(sender: AnyObject) {
        
        if locationBtn.on {
            
            if (CLLocationManager.authorizationStatus() == .Denied || CLLocationManager.authorizationStatus() == .NotDetermined) {
                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                locationToggleBgColor.backgroundColor = UIColor.clearColor()
                self.locationBtn.thumbTintColor = ColorConstant.orangeColor
                locationBtn.setOn(true, animated: true)
                currentLocationView_height.constant = 52
                currentLocationImageImageView.hidden = false
                }
        } else {
            
            if (CLLocationManager.authorizationStatus() == .AuthorizedAlways || CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse) {
                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                currentLocationView_height.constant = 0
               locationToggleBgColor.backgroundColor = ColorConstant.BgCellBaseColor
                self.locationBtn.thumbTintColor = UIColor.lightGrayColor()
                self.locationBtn.setOn(false, animated: true)
                locationLabel.hidden = true
                locationLabel.text = "Turn on location"
                self.locationLabel.textColor = UIColor.lightGrayColor()
                currentLocationImageImageView.hidden = true
            }
        }
    }
    
    @IBOutlet weak var termsofuseBtn: UIButton!
    @IBAction func termsofUseTaped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("other") as! OtherViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    @IBOutlet weak var faqBtn: UIButton!
    @IBAction func faqTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("other") as! OtherViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBOutlet weak var aboutUsBtn: UIButton!
    @IBAction func aboutUsTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("other") as! OtherViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBOutlet weak var feedbackbtn: UIButton!
    @IBAction func feedbackTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("other") as! OtherViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}