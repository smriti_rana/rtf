//
//  RewardPointsViewController.swift
//  RewardTheFanFinal
//
//  Created by Anmol Rajdev on 03/05/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit

class RewardPointsViewController: BaseViewController {
    
    //MARK:- Properties
    //MARK:-
    
    @IBOutlet weak var timerLbl: UILabel!
    var getMinutes : Int = 0
    var getSeconds : Int = 0
    var getSec : Int = 0
    let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
    @IBOutlet weak var otherCreditBgView_height: NSLayoutConstraint!
    
    @IBOutlet weak var otherMethodBgView_height: NSLayoutConstraint!
    
    @IBOutlet weak var additionPointsBtn: UIButton!
    
    @IBOutlet weak var onlyRewardsBtn: UIButton!
    
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    
    @IBOutlet weak var rewardTextFeild: UITextField!
    
    @IBOutlet weak var rewardLbl: UILabel!
    
    @IBOutlet weak var backbtn: UIButton!
    
    @IBOutlet weak var pointDetailLbl: UILabel!
    
    @IBOutlet var payUsingLabel: UILabel!
    
    @IBOutlet var onlyRewardPointsLabel: UILabel!
    
    @IBOutlet var rewardPointsAndOthersLabel: UILabel!
    
    @IBOutlet var rewardPointsWarningLabel: UILabel!
    
    @IBOutlet var creditCardButton: UIButton!
    
    @IBOutlet var paypalButton: UIButton!
    
    @IBOutlet var applePayButton: UIButton!
    
    var total : Int = 0
    
    var othersViewToHide = false
    
    var loadingFlag : Bool = true
    
    //MARK:-
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            rewardLbl.font = UIFont(name: rewardLbl.font.fontName, size: 32)
            navbar_height.constant = 74
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RewardPointsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // *** setting total amount *** //
        self.payUsingLabel.text = "Pay $\(total) using:"
        
        self.creditCardButton.selected = true
        
//       NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RewardPointsViewController.AlertController), name:"TimerExpires", object: nil)

        
    }
//    // MARK:-Alert Ok
//    override func AlertController(){
//        poptoEventDetail()
//    }
//    
//    //MARK:-PoptoSpecificView
//    override func poptoEventDetail(){
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKindOfClass(EventDetailViewController) {
//                print("controller in Checkout:",controller)
//                self.navigationController?.popToViewController((controller as? EventDetailViewController)!, animated: false)
//                break
//            }
//        }
//    }
    //MARK:SetTimeOn Label
    func getTimerRewardPoints(minutes1:Int,seconds1:Int,totalSec1:Int){
        
        print("reward:",totalSec1)
        print("rewardminutes:",minutes1)
        print("rewardseconds:",seconds1)
        let seconds1Str = String(seconds1)
        if seconds1Str.characters.count == 1{
            timerLbl.text = "\(0)\(minutes1)\(":")\(0)\(seconds1)"
            
        }
        else{
            self.timerLbl.text = "\(0)\(minutes1)\(":")\(seconds1)"
        }
//        if checkoutFlag == true{
//            self.base.getTimerRewardPoints(minutes1, seconds1: seconds1, totalSec1: totalSec1)
//            
//        }
    }
    override func viewDidAppear(animated: Bool) {
        
        if loadingFlag {
            
            // *** set inidcator loading *** //
            self.setLoadingIndicator(self.view)
            
            // *** get loyalityRewardPoints API call *** //
            getCustomerLoyalityRewardPoints()
            
            self.loadingFlag = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    
    func getCustomerLoyalityRewardPoints() {
        
        // *** alert controller ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            // *** stop indicator *** //
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** get customer ID
        var customerId : String = ""
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = ("\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)")
        }
        
        // *** send parameters
        let getParam = [
            
            "productType" : productType,
            "customerId" : customerId,
            "platForm" : platForm,
            "configId" : configId,
            ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.getCustomerLoyalityRewards("GetCustomerLoyaltyRewards.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            // *** stop inidcator *** //
            self.stopIndicator()
            
            if isTrue == true {
                
                print(responseDisctionary!)
                
                if let customerLoyalty = responseDisctionary?.objectForKey("customerLoyalty")  as? NSDictionary {
                    
                    var activeRewardPoints : Int = 0
                    var rewardPoints : String = ""
                    if let activeRewardDollers = customerLoyalty.objectForKey("activeRewardDollers") as? NSInteger {
                        rewardPoints = "\(activeRewardDollers)"
                        activeRewardPoints = activeRewardDollers
                    }
                    self.pointDetailLbl.text = rewardPoints
                    
                    if  activeRewardPoints < self.total {
                        self.onlyRewardsBtn.enabled = false
                        self.onlyRewardsBtn.alpha = 0.5
                        self.onlyRewardPointsLabel.alpha = 0.5
                        self.additionPointsBtn.selected = true
                        self.rewardPointsWarningLabel.text = "you don't have enough reward points"
                    }
                    
                    if rewardPoints == "0" {
                        self.onlyRewardsBtn.enabled = false
                        self.onlyRewardsBtn.alpha = 0.5
                        self.onlyRewardPointsLabel.alpha = 0.5
                        self.rewardTextFeild.alpha = 0.5
                        self.rewardTextFeild.userInteractionEnabled =  false
                        self.additionPointsBtn.selected = true
                        self.rewardPointsWarningLabel.text = "No reward points available"
                    }
                }
                
            } else {
                
                // *** stop inidicator *** //
                self.stopIndicator()
                
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    self.stopIndicator()
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    func orderPaymentByLoyalityRewards() {
        
        // *** alert controller ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** get customer ID
        var customerId : String = ""
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = ("\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)")
        }
        
        // *** get payment Type *** //
        var paymentType : String = ""
        
        if additionPointsBtn.selected {
            paymentType = "PartialMode"
        }
        
        if onlyRewardsBtn.selected {
            paymentType = "FullMode"
        }
        
        let rewardPoints : String = pointDetailLbl.text!
        
        // *** send parameters
        let getParam = [
            
            "productType" : productType,
            "customerId" : customerId,
            "platForm" : platForm,
            "configId" : configId,
            "rewardPoints" : rewardPoints,
            "orderTotal" : (total),
            "paymentType" : paymentType
            
        ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.orderPaymentByLoyalityRewards("OrderPaymentByReward.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            self.stopIndicator()
            
            if isTrue == true {
                
                print(responseDisctionary!)
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PreOrderConfirmation") as? PreOrderConfirmationViewController {
                    
                    if self.additionPointsBtn.selected {
                        if self.creditCardButton.selected {
                            vc.paymentMethod = "Credit Card"
                        } else if self.paypalButton.selected {
                            vc.paymentMethod = "Paypal"
                        } else if self.applePayButton.selected {
                            vc.paymentMethod = "Applepay"
                        }
                    } else {
                        vc.paymentMethod = "Loyality"
                    }
//                    vc.getMinutes = self.minutes1
//                    vc.getSeconds = self.seconds1
//                    vc.getSec = self.seconds
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                
                // *** stop indicator *** //
                self.stopIndicator()
                if message == "" {
                    
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    
                    // * if error in the response
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        rewardTextFeild.resignFirstResponder()
    }
    
    //MARK:OnlyRewardTapped
    @IBAction func onlyRewardsTapped(sender: AnyObject) {
        
        onlyRewardsBtn.selected = true
        additionPointsBtn.selected = false
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseIn, animations: {
            self.othersViewToHide = true
            self.onlyRewardsBtn.selected = false
            self.onlyRewardsBtn.setImage(UIImage(named: "check"), forState: .Normal)
            self.additionPointsBtn.setImage(UIImage(named: "uncheck-1"), forState: .Normal)
            self.onlyRewardsBtn.selected = true
            self.otherCreditBgView_height.constant = 0
            self.otherMethodBgView_height.constant = 0
            self.view.layoutIfNeeded()
            }, completion: { finished in
                
        })
    }
    
    @IBAction func additionPointsTapped(sender: AnyObject) {
        onlyRewardsBtn.selected = true
        additionPointsBtn.selected = false
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.additionPointsBtn.setImage(UIImage(named: "check"), forState: .Normal)
            self.onlyRewardsBtn.setImage(UIImage(named: "uncheck-1"), forState: .Normal)
            self.otherCreditBgView_height.constant = 60
            self.otherMethodBgView_height.constant = 50
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                self.otherCreditBgView_height.constant = 90
                self.otherMethodBgView_height.constant = 75
            }
            self.view.layoutIfNeeded()
            }, completion: { finished in
        })
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true
        }
        
        if textField == rewardTextFeild {
            
            let text = textField.text!
            let finalText = "\(text)\(string)"
            
            if Int(finalText) > total {
                self.rewardPointsWarningLabel.text = "you don't have enough reward points"
                return false
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if pointDetailLbl.text == "0" {
            rewardPointsWarningLabel.text = "No reward points available"
        }
        
        //        if Int(textField.text!)! > total {
        //
        //        }
    }
    
    //MARK:AdditionalViewTapped
    @IBAction func doneButtonTapped(sender: AnyObject) {
        orderPaymentByLoyalityRewards()
    }
    
    @IBAction func backTapped(sender: AnyObject) {
      
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func rewardPointsValueChanged(sender: AnyObject) {
        
        let textField = sender as! UITextField
        var amount : Int = 0
        
        if Int(textField.text!) != nil {
            
            amount = Int(textField.text!)!
        }
        
        rewardPointsWarningLabel.text = "\((Int(pointDetailLbl.text!)! - amount)) reward points left"
        
        if amount > Int(pointDetailLbl.text!)! {
            rewardPointsWarningLabel.text = "no reward points left"
            rewardTextFeild.text = pointDetailLbl.text
        } else {
        }
    }
    
    @IBAction func creditCardButtonTapped(sender: AnyObject) {
        
        self.paypalButton.setImage(UIImage(named: "uncheck-1"), forState: .Normal)
        self.creditCardButton.setImage(UIImage(named: "check"), forState: .Normal)
        self.applePayButton.setImage(UIImage(named: "uncheck-1"), forState: .Normal)
        self.creditCardButton.selected = true
        self.paypalButton.selected = false
        self.applePayButton.selected = false
    }
    
    @IBAction func paypalButtonTapped(sender: AnyObject) {
        
        self.paypalButton.setImage(UIImage(named: "check"), forState: .Normal)
        self.creditCardButton.setImage(UIImage(named: "uncheck-1"), forState: .Normal)
        self.applePayButton.setImage(UIImage(named: "uncheck-1"), forState: .Normal)
        self.paypalButton.selected = true
        self.creditCardButton.selected = false
        self.applePayButton.selected = false
    }
    
    @IBAction func applePayButtonTapped(sender: AnyObject) {
        
        self.paypalButton.setImage(UIImage(named: "uncheck-1"), forState: .Normal)
        self.creditCardButton.setImage(UIImage(named: "uncheck-1"), forState: .Normal)
        self.applePayButton.setImage(UIImage(named: "check"), forState: .Normal)
        self.applePayButton.selected = true
        self.creditCardButton.selected = false
        self.paypalButton.selected = false
    }


    override func viewWillAppear(animated: Bool) {
        checkoutFlag = "Reward"
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RewardPointsViewController.poptoEventDetail), name:"TimerExpires", object: nil)
        
    }
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "TimerExpires", object: nil)
    }
    func poptoEventDetail(){
        for controller in self.navigationController!.viewControllers as Array {
            print("controllers in Stack :",controller)
            if controller.isKindOfClass(EventDetailViewController) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
//        if popToViewFlag == true{
//            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EventDetail") as! EventDetailViewController
//            self.navigationController?.popToViewController(vc, animated: false)
//            
//        }
    }
}