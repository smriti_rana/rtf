//
//  CheckoutTypeViewController.swift
//  RewardTheFanFinal
//
//  Created by Anmol Rajdev on 03/05/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit
import PassKit
import Stripe

class CheckoutTypeViewController: BaseViewController,checkTimerDelegate{
    
    var base = RewardPointsViewController()
    var vc = PreOrderConfirmationViewController()
    //MARK:- Properties
    //MARK:-
//    var timer = NSTimer()
   
    var getMinutes : Int = 0
    var getSeconds : Int = 0
    var gettotalSec : Int = 0
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var applepayBtn: UIButton!
    
    @IBOutlet weak var navBar_height: NSLayoutConstraint!
    
    @IBOutlet weak var checkOutLbl: UILabel!
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var rewardBtn: UIButton!
     let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
    var total : Int = 0
    
    var paymentType : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
          
        //        applePayButton.hidden = !PKPaymentAuthorizationViewController.canMakePaymentsUsingNetworks(SupportedPaymentNetworks)
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            checkOutLbl.font = UIFont(name: checkOutLbl.font.fontName, size: 32)
            navBar_height.constant = 74
        }
        
//        handleTimerMethod()
        

       
    }
    func poptoEventDetail(){
        for controller in self.navigationController!.viewControllers as Array {
            print("controllers in Stack :",controller)
            if controller.isKindOfClass(EventDetailViewController) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    override func viewWillAppear(animated: Bool) {
       checkoutFlag = "Checkout"
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CheckoutTypeViewController.poptoEventDetail), name:"TimerExpires", object: nil)
        
    }
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "TimerExpires", object: nil)
    }

    //MARK:SetTimeOn Label
    func getTimerCheckDelegate(minutes1:Int,seconds1:Int,totalSec1:Int){
        print("checkout:",totalSec1)
        print("checkoutminutes:",minutes1)
        print("checkoutseconds:",seconds1)
        let seconds1Str = String(seconds1)
        if seconds1Str.characters.count == 1{
            timerLbl.text = "\(0)\(minutes1)\(":")\(0)\(seconds1)"

        }
        else{
            self.timerLbl.text = "\(0)\(minutes1)\(":")\(seconds1)"
            }
        if checkoutFlag == "Reward"{
            self.base.getTimerRewardPoints(minutes1, seconds1: seconds1, totalSec1: totalSec1)
            
        }
        else if checkoutFlag == "Preorder"{
            self.vc.getTimerPreOrder(minutes1, seconds1: seconds1, totalSec1: totalSec1)
        }

    }
   
    //MARK:- Button actions
    //MARK:-
    //MARK:-BackClicked
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func rewardPointClicked(sender: AnyObject) {
        
         base = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("RewardPoints") as! RewardPointsViewController
        base.total = self.total
        checkoutFlag = "Reward"
        self.navigationController?.pushViewController(base, animated: true)
    }
    
    @IBAction func creditCardButtonTapped(sender: AnyObject) {
        vc = (self.storyboard?.instantiateViewControllerWithIdentifier("PreOrderConfirmation") as? PreOrderConfirmationViewController)!
        vc.paymentMethod = "Credit Card"
        checkoutFlag = "Preorder"
        self.navigationController?.pushViewController(vc, animated: true)
        }
    
  @IBAction func paypalAction(sender: AnyObject) {
        
         vc = (self.storyboard?.instantiateViewControllerWithIdentifier("PreOrderConfirmation") as? PreOrderConfirmationViewController)!
        vc.paymentMethod = "Paypal"
        checkoutFlag = "Preorder"
            self.navigationController?.pushViewController(vc, animated: true)
        }
   
    @IBAction func applePayTapped(sender: AnyObject) {
        vc = (self.storyboard?.instantiateViewControllerWithIdentifier("PreOrderConfirmation") as? PreOrderConfirmationViewController)!
            checkoutFlag = "Preorder"
            vc.paymentMethod = "Applepay"
            self.navigationController?.pushViewController(vc, animated: true)
        }

    
    
//    //MARK:-PoptoSpecificView
//    func poptoEventDetail(){
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKindOfClass(EventDetailViewController) {
//                print("controller in checkout:",controller)
//                self.navigationController?.popToViewController(controller as! EventDetailViewController, animated: true)
//                break
//            }
//        }
//    }

}
