//
//  MusicLibraryViewController.swift
//  RewardtheFan
//
//  Created by Anmol Rajdev on 01/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import MediaPlayer

class MusicLibraryViewController: BaseViewController {
    
    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var progressView: CProgressView!
    
    @IBOutlet var scanningDataLabel: UILabel!
    
    @IBOutlet var chooseAFewButton: MKButton!
    
    @IBOutlet var labelView: UIView!
    
    @IBOutlet var matchesView: UIView!
    
    @IBOutlet var matchesCountLabel: UILabel!
    
    var scanningFlag : Bool = true
    
    var artistsArray : NSMutableArray = []
    
    var selectedArtists : NSMutableArray = []
    
    @IBOutlet var backButton: UIButton!
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // matches found view border
        self.labelView.layer.borderColor = UIColor(colorLiteralRed: 177.0/255.0, green: 177.0/255.0, blue: 177.0/255.0, alpha: 0.5).CGColor
        self.labelView.layer.borderWidth = 1.0
        
        // *** choose few button  changes ***
        self.chooseAFewButton.layer.borderColor = UIColor(colorLiteralRed: 177.0/255.0, green: 177.0/255.0, blue: 177.0/255.0, alpha: 0.5).CGColor
        self.chooseAFewButton.layer.borderWidth = 1.0
        
        // *** border radius to innner view
        self.matchesView.layer.cornerRadius = self.matchesView.frame.height/2;
        self.matchesView.clipsToBounds = true

        // *** changes for iPad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            scanningDataLabel.font = UIFont(name: scanningDataLabel.font.fontName, size: 30)
            backButton.imageEdgeInsets = UIEdgeInsetsMake(18, 18, 18, 18);
        } else {
            backButton.imageEdgeInsets = UIEdgeInsetsMake(13, 13, 13, 13);
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        
        if scanningFlag {
            loadProgressWithAnimation(0)
            scanningFlag = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // *** load progress circle ***
    func loadProgressWithAnimation(index: Int) -> Void {
        
        var i : Int = index
        let artistQuery = MPMediaQuery.artistsQuery()
        self.progressView.lineWidth = 1
        if  i < artistQuery.collections?.count {
            
            let sfd : Float = (Float)((artistQuery.collections?.count)!)
            let j : Float = Float(i)/sfd
            let x : Float = (Float)(j*100)
            
            let artistCollection = artistQuery.collections![i]
            let artistName = artistCollection.representativeItem?.valueForProperty(MPMediaItemPropertyArtist)
            artistsArray .addObject(artistName as! String)
            
            UIView.animateWithDuration(5.0, delay: 1.0, options: .CurveEaseInOut, animations: {
                self.progressView.updateProgressCircle(x)
                self.matchesCountLabel.text = ("\(self.artistsArray.count)")
            }) { finished in
                i += 1
                self.loadProgressWithAnimation(i)
            }
        } else {
            
            self.progressView.updateProgressCircle(100)
        }
    }
    
    //MARK:- Button actions
    //MARK:-
    
    // *** cross button action ***
    @IBAction func menuButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    // *** choose few button action ***
    @IBAction func chooseAFewButtonTapped(sender: AnyObject) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        if self.artistsArray.count != 0 {
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavoritesFrom") as! ChooseFavoritesFromViewController
            vc.artistArray = self.artistsArray.mutableCopy() as! NSMutableArray
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            
            alertVc.message = noartistAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
    }
    
    // *** select all button action ***
    @IBAction func selectAllButtonTapped(sender: AnyObject) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        if self.artistsArray.count != 0 {
            let selectedArtists : String = self.artistsArray.componentsJoinedByString(",")
            
            // *** api call for get popular artists ***
            
            // * check for internet connection
            checkInternetConnection()
            
            // ** start activityIndicator
            setLoadingIndicator(self.view)
            
            var custId : String = ""
            if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
                
                custId = ("\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)")
            } else {
                custId = ""
            }
            
            // *** send parameters
            
            let getParam : NSDictionary
            
            getParam = [
                
                "configId" : NSUserDefaults.standardUserDefaults().objectForKey("configId") as! String,
                "productType" : NSUserDefaults.standardUserDefaults().objectForKey("productType") as! String,
                "artistKeys" : selectedArtists,
                "customerId" : custId
            ]
            
            // **** call webservice
            WebServiceCall.sharedInstance.getPossibleArtistsBySocialInfo("GetPossibleArtistBySocialInfo.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                // * stop activityIndicator
                self.stopIndicator()
                
                // ** check for valid result
                if isTrue == true {
                    
                    // * get the response dictionary
                    let artistList : NSDictionary = responseDisctionary!
                    
                    // ** get the artist from response dictionary
                    if let artists : NSArray = artistList.objectForKey("artists") as? NSArray {
                        // *** get the artists names
                        var i : Int = 0
                        while i < artists.count {
                            let artistDetail = artists.objectAtIndex(i) as! NSDictionary
                            self.selectedArtists .addObject(artistDetail.objectForKey("name")! as! String)
                            i += 1
                        }
                    }
                    
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
                    vc.selectedArtistsFromMusicLibreary = self.selectedArtists.mutableCopy() as! NSMutableArray
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    
                    // * if no response
                    if message == "" {
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        } else {
            
            alertVc.message = "No artist Found"
            self.presentViewController(alertVc, animated: true, completion: nil)
            self.navigationController?.popViewControllerAnimated(true)
        }

    }

}
