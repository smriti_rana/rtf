//
//  ChooseFavoritesFromTableViewCell.swift
//  RewardtheFan
//
//  Created by Anmol Rajdev on 01/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

@objc protocol cellDelegate
{
    optional func checkBoxClicked(button: UIButton, index : NSInteger)
}

class ChooseFavoritesFromTableViewCell: UITableViewCell {

    //MARK:- Properties
    //MARK:-
    
    weak var delegate: cellDelegate?
    
    @IBOutlet var checkBoxImageView: UIImageView!
    
    @IBOutlet var checkBoxButton: UIButton!
    
    @IBOutlet var artistNameLabel: UILabel!
    
    @IBOutlet var backGroundView: UIView!
    
    //MARK:-
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    // *** check box button action ***
    @IBAction func checkBoxButtonTapped(sender: AnyObject) {
        self.delegate?.checkBoxClicked!(sender as! UIButton, index: sender.tag)
    }
}
