//
//  ChooseFavoritesFromViewController.swift
//  RewardtheFan
//
//  Created by Anmol Rajdev on 01/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class ChooseFavoritesFromViewController: BaseViewController, cellDelegate {

    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var chooseFavoritesFromLabel: UILabel!
    
    @IBOutlet var skipThisStepLabel: UILabel!
    
    @IBOutlet var baseTableView: UITableView!
    
    var artistNames : NSMutableArray = []
    
    var artistArray : NSMutableArray = []
    
    var selectedArtistsFromMusicLibreary : NSMutableArray = []

    @IBOutlet var backButton: UIButton!
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // *** Underline Skip this step ***
        let text = NSMutableAttributedString.init(attributedString: skipThisStepLabel.attributedText!)
        text.addAttribute(NSUnderlineStyleAttributeName, value: Int(1), range: NSMakeRange(0, (skipThisStepLabel.text?.characters.count)!))
        skipThisStepLabel.attributedText = text
        
        // *** register table view cell ***
        baseTableView.registerNib(UINib(nibName: "ChooseFavoritesFromTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")
        
        self.baseTableView.estimatedRowHeight = 44
        self.baseTableView.rowHeight = UITableViewAutomaticDimension
        
        // *** setting font size for iPad
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            self.baseTableView.estimatedRowHeight = 54
            chooseFavoritesFromLabel.font = UIFont(name: chooseFavoritesFromLabel.font.fontName, size: 32)
            skipThisStepLabel.font = UIFont(name: skipThisStepLabel.font.fontName, size: 25)
            backButton.imageEdgeInsets = UIEdgeInsetsMake(18, 18, 18, 18);
        } else {
            backButton.imageEdgeInsets = UIEdgeInsetsMake(13, 13, 13, 13);
        }
        
        // *** loading empty array to check values
        var i : Int = 0
        while i < artistArray.count {
            artistNames .addObject("")
            i += 1
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Table view delegate and datasource methods
    //MARK:-
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.artistArray.count
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
            as! ChooseFavoritesFromTableViewCell
        if !(indexPath.row % 2 == 0) {
            cell.backGroundView.backgroundColor = UIColor(colorLiteralRed: 246.0/255.0, green: 246.0/255.0, blue: 246.0/255.0, alpha: 1.0)
        } else {
            cell.backGroundView.backgroundColor = UIColor.whiteColor()
        }
        
        cell.artistNameLabel.text = self.artistArray.objectAtIndex(indexPath.row) as? String
        
        if artistNames.objectAtIndex(indexPath.row) as! String == "" {
            cell.checkBoxImageView.image = UIImage(named: "uncheck")
        } else {
            cell.checkBoxImageView.image = UIImage(named: "checked")
        }
        
        cell.checkBoxButton.tag = indexPath.row
        cell.delegate = self
        return cell
    }
    
    //MARK:- Button actions
    //MARK:-
    
    // *** cross button action ***
    @IBAction func menuButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    // *** finish button action API call ***
    @IBAction func finishButtonTapped(sender: AnyObject) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        if artistArray.count == 0 {
            
            alertVc.message = artistFoundAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else {
            
            let artists : NSMutableArray = []
            var i : Int = 0
            while i < artistNames.count {
                
                if !(artistNames.objectAtIndex(i) as! String == "") {
                    artists .addObject(artistNames.objectAtIndex(i) as! String)
                }
                i += 1
            }
            
            if artists.count != 0 {
                
                let selectedArtists : String = artists.componentsJoinedByString(",")
                
                // *** api call for get popular artists ***
                
                // * check for internet connection
                guard IJReachability.isConnectedToNetwork() else {
                    
                    alertVc.message = ErrorInternetConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                    return
                }
                
                // ** start activityIndicator
                setLoadingIndicator(self.view)
                
                
                var custId : String = ""
                if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
                    
                    custId = ("\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)")
                } else {
                    custId = ""
                }
                
                // *** send parameters
                
                let getParam : NSDictionary
                
                getParam = [
                    
                    "configId" : NSUserDefaults.standardUserDefaults().objectForKey("configId") as! String,
                    "productType" : NSUserDefaults.standardUserDefaults().objectForKey("productType") as! String,
                    "artistKeys" : selectedArtists,
                    "customerId" : custId
                ]
                
                // **** call webservice
                WebServiceCall.sharedInstance.getPossibleArtistsBySocialInfo("GetPossibleArtistBySocialInfo.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                    
                    // * stop activityIndicator
                    self.stopIndicator()
                    
                    // ** check for valid result
                    if isTrue == true {
                        
                        // * get the response dictionary
                        let artistList : NSDictionary = responseDisctionary!
                        
                        // ** get the artist from response dictionary
                        if let artists : NSArray = artistList.objectForKey("artists") as? NSArray {
                            // *** get the artists names
                            var i : Int = 0
                            while i < artists.count {
                                
                                let artistDetail = artists.objectAtIndex(i) as! NSDictionary
                                self.selectedArtistsFromMusicLibreary .addObject(artistDetail.objectForKey("name")! as! String)
                                i += 1
                            }
                        }
                        
                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
                        vc.selectedArtistsFromMusicLibreary = self.selectedArtistsFromMusicLibreary.mutableCopy() as! NSMutableArray
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        
                        // * if no response
                        if message == "" {
                            
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        } else {
                            
                            // * if error in the response
                            
                            alertVc.message = message!
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    }
                })
            } else {
                
                alertVc.message = favArtistAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            }
        }
    }
    
    // *** skip button action ***
    @IBAction func skipButtonTapped(sender: AnyObject) {
        
    }
    
    // *** check box button action ***
    func checkBoxClicked(button: UIButton, index: NSInteger) {
        let cell = button.superview?.superview?.superview as! ChooseFavoritesFromTableViewCell
        
        if artistNames.objectAtIndex(index) as! String == "" {
            cell.checkBoxImageView.image = UIImage(named: "checked")
            artistNames .replaceObjectAtIndex(index, withObject: artistArray.objectAtIndex(index) as! String)
        } else {
            cell.checkBoxImageView.image = UIImage(named: "uncheck")
            artistNames .replaceObjectAtIndex(index, withObject: "")
        }
    }
}
