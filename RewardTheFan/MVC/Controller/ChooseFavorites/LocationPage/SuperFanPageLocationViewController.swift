//
//  SuperFanPageLocationViewController.swift
//  RewardtheFan
//
//  Created by Anmol Rajdev on 02/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import CoreLocation


class SuperFanPageLocationViewController: BaseViewController {

    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var superFanPageLabel: UILabel!
    
    @IBOutlet var secondLineLabel: UILabel!
    
    @IBOutlet var skipThisStepLabel: UILabel!
    
    var message : String = ""
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // *** show toast message for artist added
        if message != "" {
            self.view.makeToast(message: message)
        }
        
        // *** Underline Skip this step ***
        let text1 = NSMutableAttributedString.init(attributedString: skipThisStepLabel.attributedText!)
        text1.addAttribute(NSUnderlineStyleAttributeName, value: Int(1), range: NSMakeRange(0, (skipThisStepLabel.text?.characters.count)!))
        skipThisStepLabel.attributedText = text1
        
        // *** changes for iPad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            superFanPageLabel.font = UIFont(name: superFanPageLabel.font.fontName, size: 32)
            skipThisStepLabel.font = UIFont(name: skipThisStepLabel.font.fontName, size: 25)
        }
        
        // *** change color of text ***
        let text = NSMutableAttributedString.init(attributedString: secondLineLabel.attributedText!)
        text.addAttribute(NSForegroundColorAttributeName, value: UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0), range: NSMakeRange(5, 17))
        secondLineLabel.attributedText = text
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SuperFanPageLocationViewController.appWillEnterForeground(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
    }
    
    func appWillEnterForeground(notification: NSNotification) {
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "gpsFlag")
            if NSUserDefaults.standardUserDefaults().objectForKey("gpsFlag") as! Bool {
                
                // ** navigate lo next screen
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "gpsFlag")
        }
    }
    
    override func viewWillAppear(animated: Bool) {
    }
    
    override func viewDidAppear(animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button actions
    //MARK:-
    
    // *** cross button action ***
    @IBAction func menuButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    // *** turn on location button action ***
    @IBAction func turnOnYourLocationButtonTapped(sender: AnyObject) {
        
        if !(CLLocationManager.locationServicesEnabled()) {
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
        }
    }
    
    // *** Skip this step button action ***
    @IBAction func skipThisStepButtonTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
