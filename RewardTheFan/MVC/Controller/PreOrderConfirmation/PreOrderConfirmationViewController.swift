//
//  PreOrderConfirmationViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 04/05/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import PassKit
import Stripe

extension PreOrderConfirmationViewController: PKPaymentAuthorizationViewControllerDelegate {
    
    
    
    func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: ((PKPaymentAuthorizationStatus) -> Void)) {
        
        // 1
        //        let shippingAddress = self.createShippingAddressFromRef(payment.shippingAddress)
        
        // 2
        Stripe.setDefaultPublishableKey("pk_test_cIRBXCjRs4MS1f7rLZND9VkB")
        
        // 3
        STPAPIClient.sharedClient().createTokenWithPayment(payment) {
            (token, error) -> Void in
            
            if (error != nil) {
                
                print(error)
                completion(PKPaymentAuthorizationStatus.Failure)
                return
            }
            
            self.setLoadingIndicator(self.view)
            
            // *** api call for forgot password ***
            let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
            alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                self.stopIndicator()
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            let getParam = [
                
                "token": token!.tokenId,
                "description": "Title",
                "transactionAmount": (0),
                "productType" : self.productType,
                "platform" : self.platForm
                
            ]
            
            // **** call webservice
            WebServiceCall.sharedInstance.stripeTransaction("StripeTransaction.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
                
                if isTrue == true {
                    self.stopIndicator()
                    print("response for customer",responseDisctionary!)
                    completion(PKPaymentAuthorizationStatus.Success)
                    
                } else {
                    
                    
                    self.stopIndicator()
                    completion(PKPaymentAuthorizationStatus.Failure)
                    if message == "" {
                        // * if no response
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
            
        }
    }
   
    func paymentAuthorizationViewControllerDidFinish(controller: PKPaymentAuthorizationViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Sent to the delegate before the payment is authorized, but after the user has authenticated using
    // passcode or Touch ID. Optional.
    @available(iOS 8.3, *)
    func paymentAuthorizationViewControllerWillAuthorizePayment(controller: PKPaymentAuthorizationViewController) {
        
    }
    
    // Sent when the user has selected a new shipping method.  The delegate should determine
    // shipping costs based on the shipping method and either the shipping address supplied in the original
    // PKPaymentRequest or the address fragment provided by the last call to paymentAuthorizationViewController:
    // didSelectShippingAddress:completion:.
    //
    // The delegate must invoke the completion block with an updated array of PKPaymentSummaryItem objects.
    //
    // The delegate will receive no further callbacks except paymentAuthorizationViewControllerDidFinish:
    // until it has invoked the completion block.
    @available(iOS 8.0, *)
    func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController, didSelectShippingMethod shippingMethod: PKShippingMethod, completion: (PKPaymentAuthorizationStatus, [PKPaymentSummaryItem]) -> Void) {
        
    }
    
    // Sent when the user has selected a new shipping address.  The delegate should inspect the
    // address and must invoke the completion block with an updated array of PKPaymentSummaryItem objects.
    //
    // The delegate will receive no further callbacks except paymentAuthorizationViewControllerDidFinish:
    // until it has invoked the completion block.
    @available(iOS, introduced=8.0, deprecated=9.0, message="Use the CNContact backed delegate method instead")
    func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController, didSelectShippingAddress address: ABRecord, completion: (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {
        
    }
    
    @available(iOS 9.0, *)
    func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController, didSelectShippingContact contact: PKContact, completion: (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {
        
    }
    
    // Sent when the user has selected a new payment card.  Use this delegate callback if you need to
    // update the summary items in response to the card type changing (for example, applying credit card surcharges)
    //
    // The delegate will receive no further callbacks except paymentAuthorizationViewControllerDidFinish:
    // until it has invoked the completion block.
    @available(iOS 9.0, *)
    func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController, didSelectPaymentMethod paymentMethod: PKPaymentMethod, completion: ([PKPaymentSummaryItem]) -> Void) {
        
    }
    
}

class PreOrderConfirmationViewController: BaseViewController,BillingActionsheetDelegate, PayPalPaymentDelegate {
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    @IBOutlet var paymentMethodLabel: UILabel!
    var billingArrayFlag =  false
    var shippingArrayFlag =  false
    var getMinutes : Int = 0
    var getSeconds : Int = 0
    var getSec : Int = 0
    
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var billingCountrtZIpcode: UILabel!
    
    @IBOutlet weak var billingstate_city: UILabel!
    
    @IBOutlet weak var billingAdd_street: UILabel!
    
    @IBOutlet weak var billingFirstnameLbl: UILabel!
    
    @IBOutlet weak var shippingcountry_zipcode: UILabel!
    
    @IBOutlet weak var shippingcity_stateLbl: UILabel!
    
    @IBOutlet weak var shippingFirstnameTf: UILabel!
    
    @IBOutlet weak var ShippingAddStreetLbl: UILabel!
    
    @IBOutlet weak var billingBgView: NSLayoutConstraint!
    
    @IBOutlet var dateView: UIView!
    
    @IBOutlet weak var shippingView_height: NSLayoutConstraint!
    
    @IBOutlet var cardTypeImageButton: UIButton!
    
    @IBOutlet var cardNumberLabel: UILabel!
    
    @IBOutlet weak var zoneDescrLbl: UILabel!
    
    @IBOutlet weak var priceLbl: UILabel!
    
    @IBOutlet weak var sectionLbl: UILabel!
    
    @IBOutlet weak var qtyLbl: UILabel!
    
    @IBOutlet weak var deliveryMethodLbl: UILabel!
    
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet var webViewBackGround: UIView!
    
    @IBOutlet weak var venueAndAddress: UILabel!
    
    @IBOutlet weak var EnameLbl: UILabel!
    
    @IBOutlet weak var dayLbl: UILabel!
    
    @IBOutlet weak var yearLbl: UILabel!
    
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var monthLbl: UILabel!
    
    @IBOutlet var descriptionLabel: UILabel!
    
    @IBOutlet weak var rewardsPointLabel: UILabel!
    
    @IBOutlet weak var billingBtn: UIButton!
    
    @IBOutlet weak var shippingBtn: UIButton!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet var paymentTypeImageView: UIImageView!
    
    var shippingUpdatedict = [:]
    
    var selectedCountryId : Int = 0
    
    var selectedStateId :Int = 0
    
    var billingAddressId :Int = 0
    
    var shippingAddressId:Int = 0
    
    var addressTypeIs = ""
    
    var shippingAddId = ""
    
    var actionIs = ""
    
    var loyalityUsed:String = ""
    
    var collectionIndexShipping  = 0
    
    var setDataFromShippingGrid = []
    
    var billingSubview:CustomBillingSheet!
    
    var ResponsecountryArray : NSMutableArray = []
    
    var ResponsestateArray : NSMutableArray = []
    
    var eventIdToPass = ""
    
    var ticketIdToPass = ""
    
    var ViewTypeIs = ""
    
    var billingAddArray:NSMutableArray = []
    
    var shippingArray:NSMutableArray = []
    
    var responseDictFromEvent = [:]
    
    var price : String = ""
    
    var qty : String = ""
    
    let billingaddresslbl: String = "(Billing Address)"
    
    var shippingaddresslbl: String = "(Shipping Address)"
    
    var paymentMethod : String = ""
    
    
    //MARK:- Paypal actions
    //MARK:-
    
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnectWithEnvironment(newEnvironment)
            }
        }
    }
    
    #if HAS_CARDIO
    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
    // for more details.
    var acceptCreditCards: Bool = true {
    didSet {
    payPalConfig.acceptCreditCards = acceptCreditCards
    }
    }
    #else
    var acceptCreditCards: Bool = false {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    #endif
    
    var payPalConfig = PayPalConfiguration()
    
    var resultText = ""
    
    //MARK:- apple pay properties
    //MARK:-
    
    let SupportedPaymentNetworks = [PKPaymentNetworkVisa, PKPaymentNetworkMasterCard, PKPaymentNetworkAmex]
    let ApplePaySwagMerchantID = "merchant.com.rewardthefan.42works" // Fill in your merchant ID here!
    
    //MARK:- DidLoad
    //MARK:-
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // *** dateView Changes *** //
        self.dateView.layer.cornerRadius = self.dateView.frame.height/2;
        self.dateView.clipsToBounds = true
        
        // *** open custom sheet *** //
        billingSubview = NSBundle.mainBundle().loadNibNamed("CustomBillingSheet", owner: self, options: nil).first as? CustomBillingSheet
        billingSubview.delegate = self
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        
        if NSUserDefaults.standardUserDefaults().valueForKey("eventId") != nil {
            eventIdToPass = (NSUserDefaults.standardUserDefaults().valueForKey("eventId") as? String)!
        }
        
        if NSUserDefaults.standardUserDefaults().valueForKey("id") != nil {
            ticketIdToPass = (NSUserDefaults.standardUserDefaults().valueForKey("id") as? String)!
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PreOrderConfirmationViewController.pushViewController), name:"alertMessagePreOrder", object: nil)
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PreOrderConfirmationViewController.pushView), name:"alertMessagefortimer", object: nil)
        
        
        //Billing/Shipping height
        self.billingBgView.constant = 0
        self.shippingView_height.constant = 0
        self.shippingBtn.hidden = true
        self.billingBtn.hidden = true
        
        //  *** pay pal code *** //
        payPalConfig.acceptCreditCards = acceptCreditCards;
        payPalConfig.merchantName = "Awesome Shirts, Inc."
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0]
        payPalConfig.payPalShippingAddressOption = .PayPal;
        //        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
        
        
        
        if self.paymentMethod == "Paypal" {
            self.paymentTypeImageView.image = UIImage(named: "paypal")
        } else if self.paymentMethod == "Applepay" {
            self.paymentTypeImageView.image = UIImage(named: "pay")
        } else if self.paymentMethod == "Credit Card" {
            self.paymentTypeImageView.image = UIImage(named: "credit")
        } else if self.paymentMethod == "Loyality" {
            self.paymentTypeImageView.image = UIImage(named: "reward")
        }
        
        
        // *** set paymentMethod label *** //
        self.paymentMethodLabel.text = self.paymentMethod
        
        rewardsPointLabel.text = ""
        
        //Api call forPreOrder
        self.setLoadingIndicator(self.view)
        setDataForpreOrderApi()
        
        //Api call Billing/shipping
        getCustomerApi()
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
//            Tit.font = UIFont(name: checkOutLbl.font.fontName, size: 32)
            navbar_height.constant = 74
        }

         
    }
   
    override func viewWillAppear(animated: Bool) {
        
        // *** paypal changes *** //
        //PayPalMobile.preconnectWithEnvironment(environment)
        delay(0.1) { () -> () in
            self.dateView.layer.cornerRadius = self.dateView.frame.height/2;
            self.dateView.clipsToBounds = true
            self.dateView.layoutIfNeeded()
            
        }
        checkoutFlag = "Preorder"
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PreOrderConfirmationViewController.poptoEventDetail), name:"TimerExpires", object: nil)
        
    }
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "TimerExpires", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- AlertOk clicked
    func pushViewController() {
        
        self.delay(0.2) { () -> () in
            self.billingSubview.registerCustomCellForCollectionView(self.shippingArray)
            if self.billingAddArray.count == 0 {
                self.openBillingAlert(self.billingSubview.TitleLbl.text!,getActiontypeIs: self.actionIs)
            }
            else if self.shippingArray.count == 0{
                
                self.openBillingAlert("\(self.shippingaddresslbl)",getActiontypeIs: self.actionIs)
            }
           
        }
    }

    func poptoEventDetail(){
        for controller in self.navigationController!.viewControllers as Array {
            print("controllers in Stack :",controller)
            if controller.isKindOfClass(EventDetailViewController) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
    @IBAction func backButtonTaped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:BillingTapped
    @IBAction func billingEditTapped(sender: AnyObject) {
        
        billingSubview.addorUpdateAddLbl.text = "Update Address"
        billingSubview.TitleLbl.text = "\(billingaddresslbl)"
        addressTypeIs = "BILLING_ADDRESS"
        actionIs = "Update"
        openBillingAlert(billingSubview.TitleLbl.text!,getActiontypeIs: actionIs)
    }
    
    func removeCustomView() {
        
        if billingSubview == nil {
            return
        }
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            
            }, completion: { finished in
                
                if self.billingSubview.keyBoardFlag == true {
                    
                    self.billingSubview.firstNameTf.resignFirstResponder()
                    self.billingSubview.lastNameTf.resignFirstResponder()
                    self.billingSubview.phoneNumberTf.resignFirstResponder()
                    self.billingSubview.addressTF.resignFirstResponder()
                    self.billingSubview.cityTf.resignFirstResponder()
                    self.billingSubview.stateTf.resignFirstResponder()
                    self.billingSubview.countryTf.resignFirstResponder()
                    self.billingSubview.zipcodeTf.resignFirstResponder()
                } else {
                    self.billingSubview.removeFromSuperview()
                }
        })
    }
    
    @IBAction func shippingInfoTapped(sender: AnyObject) {
        
        billingSubview.addorUpdateAddLbl.text = "Update Address"
        billingSubview.TitleLbl.text = "\(self.shippingaddresslbl)"
        addressTypeIs = "SHIPPING_ADDRESS"
        actionIs = "Update"
        print(shippingArray)
        if shippingArray.count > 0 {
            openShippingAlertToUpdateInfo(shippingArray)
        }
    }
    
    //MARK:BillingAlert
    func openBillingAlert(getTitleAs:String,getActiontypeIs:String) {
        
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.3)
        billingSubview.BillingBgView.backgroundColor = greyColor
        billingSubview.delegate = self
        self.billingSubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        self.billingSubview.TitleLbl.text = getTitleAs
        billingSubview.shippingAddView.hidden =  true
        billingSubview.AddAddView.hidden = false
        self.view.addSubview(billingSubview)
        let tap = UITapGestureRecognizer(target: self, action: #selector(PreOrderConfirmationViewController.handleTap(_:)))
        billingSubview.addGestureRecognizer(tap)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(PreOrderConfirmationViewController.self.BillingRemove))
        billingSubview.AddAddView.addGestureRecognizer(tap1)
        if billingSubview.TitleLbl.text == "\(self.shippingaddresslbl)" {
            billingSubview.registerCustomCellForCollectionView(shippingArray)
        }
        
        if actionIs == "Update" {
            setDataOnAlertFeilds()
        } else {
            self.clearTextFeilds()
        }
        
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.billingSubview.alpha = 1.0
            }, completion: { finished in
        })
    }
    
    func BillingRemove() {
        
    }
    
    func handleTapOnShippingView(sender: UITapGestureRecognizer? = nil) {
        
    }
    
    //MARK:-Shipping Alert
    func openShippingAlertToUpdateInfo(addedshippingAddressArry:NSArray) {
        print(addedshippingAddressArry)
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.3)
        billingSubview.BillingBgView.backgroundColor = greyColor
        billingSubview.delegate = self
        self.billingSubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        billingSubview.AddAddView.hidden = true
        billingSubview.shippingAddView.hidden =  false
        self.view.addSubview(billingSubview)
        billingSubview.registerCustomCellForCollectionView(addedshippingAddressArry)
        let tap = UITapGestureRecognizer(target: self, action: #selector(PreOrderConfirmationViewController.handleTap(_:)))
        billingSubview.addGestureRecognizer(tap)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(PreOrderConfirmationViewController.handleTapOnShippingView(_:)))
        billingSubview.shippingAddView.addGestureRecognizer(tap1)
        if actionIs == "Update" {
            setDataOnAlertFeilds()
        }
        
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.billingSubview.alpha = 1.0
            }, completion: { finished in
        })
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        removeCustomView()
    }
    
    //MARK:To Set DataOnAlertTextFeilds
    func setDataOnAlertFeilds() {
        if shippingArray.count > 0 || billingAddArray.count > 0 {
            //to set data
            toSetDataFromBillingorShippingArray()
        }
        
    }
    
    //MARK:-SetDatafromEventDetail As
    func setDataForpreOrderApi() {
        
        // *** api call for forgot password ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** send parameters
        let getParam = [
            
            "productType" : productType,
            "sessionIdOrDeviceId" : deviceId,
            "categoryTicketGroupId":ticketIdToPass,
            "eventId":eventIdToPass,
            "customerId":customerId,
            "platForm":platForm,
            "configId":configId,
            
            ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.postPreOrderApiCall("GetTicketGroup.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            if isTrue == true {
                
                // *** stop loading indicator *** //
                self.stopIndicator()
                
                print("Pre Order Deatail Array",responseDisctionary!)
                if let eventArray = responseDisctionary!.valueForKey("event") as? NSDictionary  {
                    
                    // *** save event name *** //
                    if let eventName = eventArray.valueForKey("eventName") as? String {
                        
                        self.EnameLbl.text = eventName
                    }
                    
                    //venue Address
                    
                    // *** save time *** //
                    var eTime = ""
                    if let eventTime = eventArray.valueForKey("eventTimeStr") as? String {
                        if eventTime == "TBD" {
                            
                            eTime = "TBD"
                        } else {
                            
                            //24hr format
                            let timeSlot = eventTime
                            let dateFormatterForTime = NSDateFormatter()
                            dateFormatterForTime.dateFormat = "hh:mma"
                            let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                            let dateFormatteerForTimeFormat = NSDateFormatter()
                            dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                            eTime = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                        }
                    }
                    
                    var eVenue = ""
                    var eCity = ""
                    var eState = ""
                    var eCountry = ""
                    
                    // *** save venue name *** //
                    if let venueName = eventArray.valueForKey("venueName") as? String {
                        eVenue = venueName
                    }
                    // *** save city *** //
                    if let city = eventArray.valueForKey("city") as? String {
                        eCity = city
                    }
                    // *** save state *** //
                    if let state = eventArray.valueForKey("state") as? String {
                        eState =  state
                    }
                    
                    // *** save country *** //
                    if let country = eventArray.valueForKey("country") as? String {
                        eCountry =  country
                    }
                    
                    // *** event Id *** //
                    if let eventId = eventArray.valueForKey("eventId") as? NSInteger {
                        self.eventIdToPass = "\(eventId)"
                    }
                    
                    let string1 = eTime
                    let resultStr = "\(string1)-\(eVenue),\(eCity),\(eState),\(eCountry)"
                    self.venueAndAddress.text = resultStr
                    
                    // *** save date *** //
                    if let eventDate = eventArray.valueForKey("eventDateStr") as? String {
                        if eventDate != "TBD" {
                            // *** get datey from string *** //
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "MM/dd/yyyy"
                            let dateFromString = dateFormatter.dateFromString(eventDate)
                            
                            // *** get day *** //
                            let dateFormatteerForDay = NSDateFormatter()
                            dateFormatteerForDay.dateFormat = "dd"
                            let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                            self.dateLbl.text = dayFromDate
                            
                            // *** get year *** //
                            let dateFormatteerForYear = NSDateFormatter()
                            dateFormatteerForYear.dateFormat = "YYYY"
                            let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                            self.yearLbl.text = yearFromDate
                            
                            // *** get month *** //
                            let dateFormatteerForMonth = NSDateFormatter()
                            dateFormatteerForMonth.dateFormat = "MMM"
                            let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                            self.monthLbl.text = monthFromDate
                            
                            // *** get week day *** //
                            let dateFormatteerForWeekDay = NSDateFormatter()
                            dateFormatteerForWeekDay.dateFormat = "EEE"
                            let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                            self.dayLbl.text = weekDayFromDate
                        }
                    }
                    
                }
                if let catGroupArry = responseDisctionary!.valueForKey("categoryTicketGroup") as? NSDictionary  {
                    
                    var newSectionAS = ""
                    // *** save ticket Section *** //
                    if let ticketSection = catGroupArry.valueForKey("section") as? String {
                        newSectionAS = ticketSection
                    }
                    
                    if let ticketRow = catGroupArry.valueForKey("row") as? String {
                        
                        var newSectionOrrowAS = ""
                        if ticketRow == "" {
                            
                            newSectionOrrowAS = newSectionAS
                        } else {
                            
                            newSectionOrrowAS = "\(newSectionAS) | Row \(ticketRow)"
                        }
                        self.sectionLbl.text = newSectionOrrowAS
                    }
                    
                    if let ticketPrice = catGroupArry.valueForKey("price") as? Int {
                        
                        self.priceLbl.text = "US $\(ticketPrice)"
                        self.price = "\(ticketPrice)"
                    }
                    
                    if let sectDescr = catGroupArry.valueForKey("sectionDescription") as? String {
                        var descriptionIS = ""
                        if sectDescr == ""{
                            descriptionIS = "No description found"
                        }else{
                            descriptionIS = sectDescr
                        }
                        self.descriptionLabel.text = descriptionIS
                    }
                    
                    var qtyIs = ""
                    
                    if let qty = catGroupArry.valueForKey("quantity") as? NSInteger {
                        
                        if qty == 1 {
                            qtyIs =  "\(qty)"
                        } else{
                            qtyIs =  "\(qty) tickets seated together"
                        }
                        self.qty = "\(qty)"
                        self.qtyLbl.text = "\(qtyIs)"
                    }
                    
                    if let delevieryMethod = catGroupArry.objectForKey("shippingMethod") as? String {
                        self.deliveryMethodLbl.text = delevieryMethod
                    }
                }
            } else {
                
                // *** stop indicator *** //
                self.stopIndicator()
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
            
        })
    }
    
    
    //MARK:-Billing/Shipiing Add CustomerInfo
    func getCustomerApi() {
        
        // *** alert message controller ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** send parameters
        let getParam = [
            
            "productType" : productType,
            "customerId":customerId,
            "platForm":platForm,
            "configId":configId,
            
            ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.postCustomerInfoApiCall("GetCustomerInfo.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            if isTrue == true {
                
                // *** stop indicator *** //
                self.stopIndicator()
                
                print("response for customer",responseDisctionary!)
                if let customerArray = responseDisctionary!.valueForKey("customer") as? NSDictionary {
                    
                    if let billingArryIs = customerArray.valueForKey("billingAddress") as? NSArray {
                        
                        self.billingAddArray.removeAllObjects()
                        if billingArryIs.count > 0 {
                            
                            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                                self.billingBgView.constant = 133
                            }else{
                                self.billingBgView.constant = 127
                            }
                            
                            self.billingBtn.hidden = false
                            for i in 0 ..< billingArryIs.count {
                                self.billingAddArray .addObject(billingArryIs[i])
                            }
                            
                            //firstname
                            var billingnameStrIs = ""
                            var billingaddStreetIs = ""
                            var billingcountryZipcodeIs = ""
                            
                            if let billingfirstnameStr = self.billingAddArray.objectAtIndex(0).valueForKey("firstName") as? String {
                                
                                billingnameStrIs = billingfirstnameStr
                                if let billinglastnameStr = self.billingAddArray.objectAtIndex(0).valueForKey("lastName") as? String {
                                    
                                    self.billingFirstnameLbl.text = "\(billingnameStrIs) \(billinglastnameStr)"
                                }
                            }
                            
                            //set Address
                            if let billingaddStreetStr = self.billingAddArray.objectAtIndex(0).valueForKey("addressLine1") as? String {
                                
                                billingaddStreetIs = billingaddStreetStr
                                self.billingAdd_street.text = billingaddStreetIs
                            }
                            
                            //set city/State
                            if let billingcityStr = self.billingAddArray.objectAtIndex(0).valueForKey("city") as? String {
                                
                                if let billingstateStr = self.billingAddArray.objectAtIndex(0).valueForKey("stateName") as? String {
                                    self.billingstate_city.text = "\(billingcityStr) \(billingstateStr)"
                                }
                            }
                            
                            // country/zipcode
                            if let billingcountryStr = self.billingAddArray.objectAtIndex(0).valueForKey("countryName") as? String {
                                
                                billingcountryZipcodeIs = billingcountryStr
                                if let billingZipcodeStr = self.billingAddArray.objectAtIndex(0).valueForKey("zipCode") as? String {
                                    
                                    self.billingCountrtZIpcode.text = "\(billingcountryZipcodeIs) \(billingZipcodeStr)"
                                }
                            }
                            
                            //set AddressId
                            if let billingId = self.billingAddArray.objectAtIndex(0).valueForKey("id") as? Int{
                                self.billingAddressId = billingId
                            }
                        } else {
                            
                            self.billingBgView.constant = 0
                            self.billingBtn.hidden = true
                        }
                    }
                    
                    if let shippingArryIs = customerArray.valueForKey("shippingAddress") as? NSArray {
                        
                        if shippingArryIs.count > 0 {
                            
                            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                                self.shippingView_height.constant = 135
                            } else {
                                self.shippingView_height.constant = 127
                            }
                            
                            self.shippingBtn.hidden = false
                            self.shippingArray.removeAllObjects()
                            
                            for i in 0 ..< shippingArryIs.count {
                                
                                if i <= 4 {
                                    self.shippingArray .addObject(shippingArryIs[i])
                                }
                            }
                            
                            //firstname
                            var shippingnameStrIs = ""
                            
                            if let billingfirstnameStr = self.shippingArray.objectAtIndex(0).valueForKey("firstName") as? String {
                                
                                shippingnameStrIs = billingfirstnameStr
                                if let shippinglastnameStr = self.shippingArray.objectAtIndex(0).valueForKey("lastName") as? String {
                                    
                                    self.shippingFirstnameTf.text = "\(shippingnameStrIs) \(shippinglastnameStr)"
                                }
                            }
                            
                            //set Address
                            if let shippingaddStreetStr = self.shippingArray.objectAtIndex(0).valueForKey("addressLine1") as? String {
                                
                                self.ShippingAddStreetLbl.text = shippingaddStreetStr
                            }
                            
                            //set city/State
                            if let shippingcityStr = self.shippingArray.objectAtIndex(0).valueForKey("city") as? String {
                                
                                if let shippingstateStr = self.shippingArray.objectAtIndex(0).valueForKey("stateName") as? String {
                                    
                                    self.shippingcity_stateLbl.text = "\(shippingcityStr) \(shippingstateStr)"
                                }
                            }
                            
                            // country/zipcode
                            if let shippingcountryStr = self.shippingArray.objectAtIndex(0).valueForKey("countryName") as? String {
                                
                                if let billingZipcodeStr = self.shippingArray.objectAtIndex(0).valueForKey("zipCode") as? String {
                                    self.shippingcountry_zipcode.text = "\(shippingcountryStr) \(billingZipcodeStr)"
                                }
                                
                                //set AddressId
                                if let shippingId = self.shippingArray.objectAtIndex(0).valueForKey("id") as? Int {
                                    self.shippingAddressId = shippingId
                                }
                            }
                        } else {
                            
                            self.shippingView_height.constant = 0
                            self.shippingBtn.hidden = true
                        }
                    }
                }
            } else {
                
                // *** stop loading indicator *** //
                self.stopIndicator()
                
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    //MARK:AddCustomer DetailApi
    func AddCustomerDetailApi(getCountryId:Int,getStateId: Int,addIdIs :Int) {
        
        // *** alert controller  ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        if ViewTypeIs == "CustomSheet" {
            getDataforAddCustomerApi()
        } else {
            
            // *** stop indicator *** //
            stopIndicator()
            
            // * check for validations
            if billingSubview.firstNameTf.text == "" && billingSubview.lastNameTf.text == ""  && billingSubview.addressTF.text == "" && billingSubview.cityTf.text == "" && billingSubview.stateTf.text == ""
                && billingSubview.countryTf.text == "" && billingSubview.zipcodeTf.text == "" {
                
                alertVc.message = allfeildsAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else  if billingSubview.firstNameTf.text == "" {
                
                alertVc.message = firstnameAlertmsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else  if billingSubview.lastNameTf.text == "" {
                
                alertVc.message = lastnameAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else  if billingSubview.phoneNumberTf == "" {
                
                alertVc.message = phoneNoAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else  if billingSubview.addressTF == "" {
                
                alertVc.message = addressAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else  if billingSubview.cityTf == "" {
                
                alertVc.message = cityAlertmsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            }  else  if billingSubview.stateTf == "" {
                
                alertVc.message = stateAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else  if billingSubview.countryTf == "" {
                
                alertVc.message = countryAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else  if billingSubview.zipcodeTf == "" {
                
                alertVc.message = zipcodeAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else{
                
                // * check for internet connection
                guard IJReachability.isConnectedToNetwork() else {
                    
                    // *** stop indicator *** //
                    stopIndicator()
                    alertVc.message = ErrorInternetConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                    return
                }
                
                // *** get parameters ***
                let getParam = [
                    
                    "productType" : productType,
                    "customerId" : customerId,
                    "platForm" : platForm,
                    "configId" : configId,
                    "addressType" : addressTypeIs,
                    "firstName" : billingSubview.firstNameTf.text!,
                    "lastName" : billingSubview.lastNameTf.text!,
                    "addressLine1" : billingSubview.addressTF.text!,
                    "phone1": billingSubview.phoneNumberTf.text!,
                    "city" : billingSubview.cityTf.text!,
                    "state": getStateId,
                    "country": getCountryId,
                    "postalCode": billingSubview.zipcodeTf.text!,
                    "action": actionIs,
                    "addressId": addIdIs
                    
                ]
                
                // *** call add customer address method in webservice call *** //
                WebServiceCall.sharedInstance.postAddCustomerInfoApiCall("AddCustomerAddress.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
                    
                    if isTrue == true {
                        
                        // *** stop indicator *** //
                        self.stopIndicator()
                        
                        self.removeCustomView()
                      
                        print("response for customer",responseDisctionary!)
                        if let customerArray = responseDisctionary!.valueForKey("customer") as? NSDictionary {
                             if  self.billingSubview.TitleLbl.text == "(Billing Address)"{
                            if let billingArryIs = customerArray.valueForKey("billingAddress") as? NSArray {
                                
                                self.billingAddArray.removeAllObjects()
                                if billingArryIs.count > 0 {
                                    self.billingArrayFlag =  true
                                    for i in 0 ..< billingArryIs.count {
                                        self.billingAddArray .addObject(billingArryIs[i])
                                    }
                                    
                                    self.billingBtn.hidden = false
                                    self.delay(0.3, closure: {
                                        if  self.shippingArray.count ==  0{
                                            print(self.shippingaddresslbl)
                                            self.addressTypeIs = "SHIPPING_ADDRESS"
                                            self.actionIs = "Save"
                                            self.billingSubview.addorUpdateAddLbl.text = "Add Address"
                                            self.openBillingAlert("\(self.shippingaddresslbl)",getActiontypeIs: self.actionIs)
                                            
                                        }
                                    })
                                    if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                                        self.billingBgView.constant = 133
                                        
                                       
                                    } else {
                                        self.billingBgView.constant = 127
                                       
                                    }
                                    
                                    //firstname
                                    var billingnameStrIs = ""
                                    var billingaddStreetIs = ""
                                    var billingcountryZipcodeIs = ""
                                    
                                    if let billingfirstnameStr = billingArryIs.objectAtIndex(0).valueForKey("firstName") as? String {
                                        
                                        billingnameStrIs = billingfirstnameStr
                                        if let billinglastnameStr = billingArryIs.objectAtIndex(0).valueForKey("lastName") as? String {
                                            
                                            self.billingFirstnameLbl.text = "\(billingnameStrIs) \(billinglastnameStr)"
                                        }
                                    }
                                    
                                    //set Address
                                    if let billingaddStreetStr = billingArryIs.objectAtIndex(0).valueForKey("addressLine1") as? String {
                                        billingaddStreetIs = billingaddStreetStr
                                        self.billingAdd_street.text = billingaddStreetIs
                                    }
                                    
                                    //set city/State
                                    if let billingcityStr = billingArryIs.objectAtIndex(0).valueForKey("city") as? String {
                                        if let billingstateStr = billingArryIs.objectAtIndex(0).valueForKey("stateName") as? String {
                                            self.billingstate_city.text = "\(billingcityStr) \(billingstateStr)"
                                        }
                                    }
                                    
                                    // country/zipcode
                                    if let billingcountryStr = billingArryIs.objectAtIndex(0).valueForKey("countryName") as? String {
                                        
                                        billingcountryZipcodeIs = billingcountryStr
                                        if let billingZipcodeStr = billingArryIs.objectAtIndex(0).valueForKey("zipCode") as? String {
                                            
                                            self.billingCountrtZIpcode.text = "\(billingcountryZipcodeIs) \(billingZipcodeStr)"
                                        }
                                    }
                                    
                                    //set AddressId
                                    if let billingId = billingArryIs.objectAtIndex(0).valueForKey("id") as? Int {
                                        self.billingAddressId = billingId
                                    }
                                    
                                }
                                self.removeCustomView()
                            }
                            }
                             else{
                            //Shipping Add
                            if let shippingArryIs = customerArray.valueForKey("shippingAddress") as? NSArray {
                                
                                print("response Customer shipping Array is",shippingArryIs)
                                if shippingArryIs.count > 0 {
                                    
                                    self.shippingArray.removeAllObjects()
                                    for i in 0 ..< shippingArryIs.count {
                                        self.shippingArray .addObject(shippingArryIs[i])
                                    }
                                    
                                    self.shippingBtn.hidden = false
                                    //firstname
                                    if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                                        self.shippingView_height.constant = 135
                                        self.billingSubview.shippingTitleLbl.font = UIFont(name:  self.billingSubview.TitleLbl.font.fontName, size: 30)
                                    } else {
                                        self.shippingView_height.constant = 127
                                        self.billingSubview.shippingTitleLbl.font = UIFont(name:  self.billingSubview.TitleLbl.font.fontName, size: 18)
                                    }
                                    
                                    var shippingnameStrIs = ""
                                    
                                    if let billingfirstnameStr = shippingArryIs.objectAtIndex(0).valueForKey("firstName") as? String {
                                        
                                        shippingnameStrIs = billingfirstnameStr
                                        if let shippinglastnameStr = shippingArryIs.objectAtIndex(0).valueForKey("lastName") as? String {
                                            
                                            self.shippingFirstnameTf.text = "\(shippingnameStrIs) \(shippinglastnameStr)"
                                        }
                                    }
                                    
                                    //set Address
                                    if let shippingaddStreetStr = shippingArryIs.objectAtIndex(0).valueForKey("addressLine1") as? String {
                                        self.ShippingAddStreetLbl.text = shippingaddStreetStr
                                    }
                                    
                                    //set city/State
                                    if let shippingcityStr = shippingArryIs.objectAtIndex(0).valueForKey("city") as? String {
                                        
                                        if let shippingstateStr = shippingArryIs.objectAtIndex(0).valueForKey("stateName") as? String {
                                            
                                            self.shippingcity_stateLbl.text = "\(shippingcityStr) \(shippingstateStr)"
                                        }
                                    }
                                    
                                    // country/zipcode
                                    if let shippingcountryStr = shippingArryIs.objectAtIndex(0).valueForKey("countryName") as? String {
                                        
                                        if let billingZipcodeStr = shippingArryIs.objectAtIndex(0).valueForKey("zipCode") as? String {
                                            
                                            self.shippingcountry_zipcode.text = "\(shippingcountryStr) \(billingZipcodeStr)"
                                        }
                                    }
                                    
                                    //set AddressId
                                    if let shippingId = shippingArryIs.objectAtIndex(0).valueForKey("id") as? Int {
                                        self.shippingAddressId = shippingId
                                    }
                                    
                                    ///## get Country/StateId ## to pass
                                    if let stateArrayIs = shippingArryIs.objectAtIndex(0).valueForKey("state") as? NSArray {
                                        
                                        if let countryId = stateArrayIs.valueForKey("countryId") as? Int {
                                            self.selectedCountryId = countryId
                                        }
                                        
                                        if let stateId = stateArrayIs.valueForKey("id") as? Int {
                                            self.selectedStateId = stateId
                                        }
                                        
                                    }
                                    
                                    if self.ViewTypeIs == "moreTapped" {
                                        
                                        print("Shipping address",self.shippingArray)
                                        if self.ViewTypeIs == "moreTapped" {
                                            
                                            print(self.collectionIndexShipping)
                                            let dictToSetShippingdataOnView = self.shippingArray.objectAtIndex(self.collectionIndexShipping) as? NSDictionary
                                            self.getShippingInfoOnGridTapDelegate(dictToSetShippingdataOnView!)
                                        }
                                    } else {
                                        
                                        if self.ViewTypeIs == "CustomSheet" {
                                            
                                            print(self.self.collectionIndexShipping)
                                            print((self.shippingArray.objectAtIndex(self.collectionIndexShipping) as? NSDictionary)!)
                                            self.getShippingInfoOnGridTapDelegate((self.shippingArray.objectAtIndex(self.collectionIndexShipping) as? NSDictionary)!)
                                        } else {
                                            
                                            let lastElement = self.shippingArray.lastObject as? NSDictionary;                                            print(lastElement)
                                            self.getShippingInfoOnGridTapDelegate(lastElement!)
                                        }
                                    }
                                    self.removeCustomView()
                                }
                            }
                            }
                        }
                        print(self.shippingArray)
                    } else {
                        
                        // *** stop indicator *** //
                        self.stopIndicator()
                        
                        if message == "" {
                            
                            // * if no response
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        } else {
                            
                            // * if error in the response
                            alertVc.message = message!
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    }
                })
            }
        }
    }
    
    //MARK:-Click On BillingAlert Submit
    func countryDelegateM(addId:String,index:Int,countryIdtoPass:Int,stateId : Int,getViewType:String,stateIdToPass:String,getArray:NSDictionary) {
        
        collectionIndexShipping = index
        shippingUpdatedict = getArray
        print("shipping Array ",shippingUpdatedict)
        
        if countryIdtoPass != 0 || stateId != 0 {
            selectedCountryId = countryIdtoPass
            selectedStateId = stateId
        }
        
        ViewTypeIs = getViewType
        var addIdToPassApi = 0
        if actionIs == "Update" {
            
            if  billingSubview.TitleLbl.text == "\(billingaddresslbl)" {
                addIdToPassApi = billingAddressId
            } else if billingSubview.TitleLbl.text == "\(self.shippingaddresslbl)" {
                
                print("shipping Array ",shippingUpdatedict)
                if getViewType == "moreTapped" {
                    addIdToPassApi =  Int(addId)!
                } else {
                    addIdToPassApi = shippingAddressId
                }
                shippingAddId = stateIdToPass
            }
            
            // *** set loading indicator *** //
            setLoadingIndicator(self.view)
            AddCustomerDetailApi(selectedCountryId,getStateId: selectedStateId,addIdIs: addIdToPassApi)
        } else {
            
            //not to send addId For Action = save
            ViewTypeIs = "AddShippingFirst"
            AddCustomerDetailApi(countryIdtoPass,getStateId: stateId,addIdIs: 0)
        }
    }
    
    //MARK:-SetData From Billing/shippingArray
    func toSetDataFromBillingorShippingArray() {
        
        var arrayToSetData = []
        if billingSubview.TitleLbl.text == "\(billingaddresslbl)" {
            if billingAddArray.count > 0 {
                arrayToSetData = billingAddArray
            }
        } else if billingSubview.TitleLbl.text == "\(self.shippingaddresslbl)" {
            
            if shippingArray.count > 0 {
                arrayToSetData = shippingArray
            }
        }
        
        //firstname
        var billingnameStrIs = ""
        var billingaddStreetIs = ""
        var billingcountryZipcodeIs = ""
        
        if let billingfirstnameStr = arrayToSetData.objectAtIndex(0).valueForKey("firstName") as? String {
            billingnameStrIs = billingfirstnameStr
            if let billinglastnameStr = arrayToSetData.objectAtIndex(0).valueForKey("lastName") as? String {
                billingSubview.firstNameTf.text = "\(billingnameStrIs)"
                billingSubview.lastNameTf.text = billinglastnameStr
            }
        }
        
        //set Address
        if let billingaddStreetStr = arrayToSetData.objectAtIndex(0).valueForKey("addressLine1") as? String {
            billingaddStreetIs = billingaddStreetStr
            billingSubview.addressTF.text = billingaddStreetIs
        }
        
        //set Address
        if let billingaddPhone = arrayToSetData.objectAtIndex(0).valueForKey("phone1") as? String {
            billingSubview.phoneNumberTf.text = billingaddPhone
        }
        
        //set city/State
        if let billingcityStr = arrayToSetData.objectAtIndex(0).valueForKey("city") as? String {
            if let billingstateStr = arrayToSetData.objectAtIndex(0).valueForKey("stateName") as? String {
                billingSubview.cityTf.text = "\(billingcityStr)"
                billingSubview.stateTf.text = "\(billingstateStr)"
            }
        }
        
        // country/zipcode
        if let billingcountryStr = arrayToSetData.objectAtIndex(0).valueForKey("countryName") as? String {
            
            billingcountryZipcodeIs = billingcountryStr
            if let billingZipcodeStr = arrayToSetData.objectAtIndex(0).valueForKey("zipCode") as? String {
                billingSubview.countryTf.text = "\(billingcountryZipcodeIs)"
                billingSubview.zipcodeTf.text =  "\(billingZipcodeStr)"
            }
        }
        
        //set AddressId
        if let billingId = arrayToSetData.objectAtIndex(0).valueForKey("id") as? Int {
            self.billingAddressId = billingId
        }
        
        ///## get Country/StateId ## to pass
        if let stateArrayIs = arrayToSetData.objectAtIndex(0).valueForKey("state") as? NSDictionary {
            
            if let countryId = stateArrayIs.valueForKey("countryId") as? Int {
                self.selectedCountryId = countryId
            }
            
            if let stateId = stateArrayIs.valueForKey("id") as? Int {
                self.selectedStateId = stateId
            }
        }
    }
    
    //MARK:-ShippingSheet Delegate
    func ShippingNewAddDelegate(getArrayFromShippingGrid :NSArray,getIndex:Int) {
        
        collectionIndexShipping = getIndex
        print(collectionIndexShipping)
        setDataFromShippingGrid = getArrayFromShippingGrid
        if setDataFromShippingGrid.count >= 5 {
            
            let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
            alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
            alertVc.message = maxShippingAddressAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
            removeCustomView()
            
        } else {
            
            ViewTypeIs = "CustomSheet"
            actionIs = "Save"
            billingSubview.addorUpdateAddLbl.text = "Add Address"
            openBillingAlert("\(self.shippingaddresslbl)", getActiontypeIs: actionIs)
        }
    }
    
    //MARK:-
    //Mark:-AfteraddedCustomer Reload
    func openShippingAlert(getArry:NSArray) {
        openShippingAlertToUpdateInfo(getArry)
    }
    
    //MARK:-
    //Mark:-Clear Textfeilds
    func clearTextFeilds(){
        
        billingSubview.firstNameTf.text = ""
        billingSubview.lastNameTf.text = ""
        billingSubview.addressTF.text = ""
        billingSubview.cityTf.text = ""
        billingSubview.stateTf.text = ""
        billingSubview.countryTf.text = ""
        billingSubview.countryTf.text = ""
        billingSubview.phoneNumberTf.text = ""
        billingSubview.zipcodeTf.text = ""
    }
    
    //MARK:-CustomerDetailApi data
    func getDataforAddCustomerApi() {
        
        // *** alert controller *** //
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** check for internet connectivity *** //
        guard IJReachability.isConnectedToNetwork() else {
            
            // *** stop inidcator *** //
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** get paramteters *** //
        let getParam = [
            
            "productType" : productType,
            "customerId" : customerId,
            "platForm" : platForm,
            "configId" : configId,
            "addressType" : addressTypeIs,
            "firstName" : billingSubview.firstNameTf.text!,
            "lastName" : billingSubview.lastNameTf.text!,
            "addressLine1" : billingSubview.addressTF.text!,
            "phone1": billingSubview.phoneNumberTf.text!,
            "city" : billingSubview.cityTf.text!,
            "state": selectedStateId,
            "country": selectedCountryId,
            "postalCode": billingSubview.zipcodeTf.text!,
            "action": actionIs,
            "addressId" : shippingAddId
            
        ]
        
        // **** call webservice mthod *** //
        WebServiceCall.sharedInstance.postAddCustomerInfoApiCall("AddCustomerAddress.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            if isTrue == true {
                
                // *** stop indicator *** //
                self.stopIndicator()
                
                self.removeCustomView()
                if let customerArray = responseDisctionary!.valueForKey("customer") as? NSDictionary {
                    
                    if let billingArryIs = customerArray.valueForKey("shippingAddress") as? NSArray {
                        
                        self.billingSubview.reloadcollectionView(billingArryIs)
                        self.getCustomerApi()
                    }
                }
            } else {
                
                // *** stop loading *** //
                self.stopIndicator()
                
                if message == "" {
                    
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    
                    // * if error in the response
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    //MARK:- Paypal methods
    //MARK:-
    func openPaypayView() {
        
        resultText = ""
        
        let item1 = PayPalItem(name: "ticket", withQuantity: UInt(qty)!, withPrice: NSDecimalNumber(string: price), withCurrency: "USD", withSku: "Hip-0037")
        
        let items = [item1]
        let subtotal = PayPalItem.totalPriceForItems(items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "5.99")
        let tax = NSDecimalNumber(string: "2.50")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Hipster Clothing", intent: .Sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        self.payPalConfig.payPalShippingAddressOption = .None
        
        
        //        self.payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionProvided
        //        payment.shippingAddress = address;
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            presentViewController(paymentViewController!, animated: false, completion: nil)
        } else {
            print("Payment not processalbe: \(payment)")
        }
    }
    
    // PayPalPaymentDelegate
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        paymentViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController, didCompletePayment completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismissViewControllerAnimated(true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            self.resultText = completedPayment.description
            self.setLoadingIndicator(self.view)
            self.createOrderApiCall()
        })
    }
    
    //MARK:-
    
    @IBAction func submitTapped(sender: AnyObject) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        if  billingAddArray.count == 0 {
            
            alertVc.message = addAddressAlertMsg
            billingSubview.TitleLbl.text = "\(billingaddresslbl)"
            addressTypeIs = "BILLING_ADDRESS"
            actionIs = "Save"
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if shippingArray.count == 0 {
            
            alertVc.message = addAddressAlertMsg
            print(self.shippingaddresslbl)
            billingSubview.TitleLbl.text = "\(self.shippingaddresslbl)"
            addressTypeIs = "SHIPPING_ADDRESS"
            actionIs = "Save"
            self.presentViewController(alertVc, animated: true, completion: nil)
            
        } else {
            
            if self.paymentMethod == "Paypal" {
                
                // Set up payPalConfig
                openPaypayView()
            } else if self.paymentMethod == "Applepay" {
                
                // *** apple pay code *** //
                
                let request = PKPaymentRequest()
                
                request.merchantIdentifier = ApplePaySwagMerchantID
                request.supportedNetworks = SupportedPaymentNetworks
                request.merchantCapabilities = PKMerchantCapability.Capability3DS
                request.countryCode = "US"
                request.currencyCode = "USD"
                
                request.paymentSummaryItems = [
                    PKPaymentSummaryItem(label: "Ticket", amount: NSDecimalNumber(string : "\(15)"))
                ]
                
                //                request.requiredShippingAddressFields = PKAddressField.All
                
                let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
                applePayController.delegate = self
                self.presentViewController(applePayController, animated: true, completion: nil)
                
            } else if self.paymentMethod == "Credit Card" {
                
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("CardDetail") as? CardDetailViewController {
                    
                    vc.eventId = self.eventIdToPass
                    vc.categoryTicketGroupId = self.ticketIdToPass
                    vc.billingAddressId = self.billingAddressId
                    vc.shippingAddressId = self.shippingAddressId
                    vc.quantity = self.qty
                    vc.ticketPrice = self.price
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else if self.paymentMethod == "Loyality" {
                
                self.setLoadingIndicator(self.view)
                createOrderApiCall()
            }
        }
    }
    
    func createOrderApiCall() {
        
        // *** alert controller *** //
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        guard IJReachability.isConnectedToNetwork() else {
            
            // *** stop inidcator *** //
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** get payment Type *** //
        var paymentType : String = ""
        
        if self.paymentMethod == "Paypal" {
            paymentType = "PAYPAL"
        } else if self.paymentMethod == "Applepay" {
            paymentType = "APPLEPAY"
        } else if self.paymentMethod == "Loyality" {
            paymentType = "LOYALTY"
        }
        
        // *** get customer id *** //
        var customerId : Int = 0
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        
        // *** get the paramters *** //
        
        let getParam = [
            
            "productType" : productType,
            "platForm" : platForm,
            "configId" : configId,
            "sessionId" : deviceId,
            "customerId" : (customerId),
            "shippingMethod" : "ETICKET",
            "paymentMethod" : paymentType,
            "isPartialLoyaltyPointUsed" : "",
            "eventId" : eventIdToPass,
            "categoryTicketGroupId" : ticketIdToPass,
            "loyaltySpent" : "",
            "quantity" : (qty),
            "ticketPrice" : (price),
            "shippingSameAsBilling" : "",
            "billingAddressId" : billingAddressId,
            "shippingAddressId" : shippingAddressId,
            "expiryMonth" : "",
            "expiryYear" : "",
            "cardNo" : "",
            "cardType" : "",
            "nameOnCard" : "",
            "securityCode" : ""
            
        ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.createOrder("CreateOrder.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            self.stopIndicator()
            if isTrue == true {
                
                if let orderId = responseDisctionary?.objectForKey("orderId") as? Int {
                    
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetails") as? OrderDetailsViewController
                    vc?.orderId = orderId
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            else {
                self.stopIndicator()
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    @IBAction func editTapped(sender: AnyObject) {
        
    }
    
    //MARK:-ToAddInfo OnViewtappeCollectionViewDelegateis
    func getShippingInfoOnGridTapDelegate(getArray:NSDictionary) {
        
        // *** stop indicator *** //
        stopIndicator()
        print(getArray)
        if getArray.count > 0 {
            //firstname
            removeCustomView()
            var shippingnameStrIs = ""
            
            if let billingfirstnameStr = getArray.valueForKey("firstName") as? String {
                
                shippingnameStrIs = billingfirstnameStr
                if let shippinglastnameStr = getArray.valueForKey("lastName") as? String {
                    self.shippingFirstnameTf.text = "\(shippingnameStrIs) \(shippinglastnameStr)"
                }
            }
            
            //set Address
            if let shippingaddStreetStr = getArray.valueForKey("addressLine1") as? String {
                self.ShippingAddStreetLbl.text = shippingaddStreetStr
            }
            
            //set city/State
            if let shippingcityStr = getArray.valueForKey("city") as? String {
                
                if let shippingstateStr = getArray.valueForKey("stateName") as? String {
                    self.shippingcity_stateLbl.text = "\(shippingcityStr) \(shippingstateStr)"
                }
            }
            
            // country/zipcode
            if let shippingcountryStr = getArray.valueForKey("countryName") as? String {
                
                if let billingZipcodeStr = getArray.valueForKey("zipCode") as? String {
                    self.shippingcountry_zipcode.text = "\(shippingcountryStr) \(billingZipcodeStr)"
                }
                
                //set AddressId
                if let shippingId = getArray.valueForKey("id") as? Int {
                    self.shippingAddressId = shippingId
                }
            }
            
        }
    }
  


        //MARK:SetTimeOn Label
    func getTimerPreOrder(minutes1:Int,seconds1:Int,totalSec1:Int){
        
        print("Pre:",totalSec1)
        print("Pre:",minutes1)
        print("Pre:",seconds1)
        let seconds1Str = String(seconds1)
        if seconds1Str.characters.count == 1{
            timerLbl.text = "\(0)\(minutes1)\(":")\(0)\(seconds1)"
            
        }
        else{
            self.timerLbl.text = "\(0)\(minutes1)\(":")\(seconds1)"
        }
       
    }
 
}