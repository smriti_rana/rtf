//
//  SuperFanViewController.swift
//  RewardtheFan
//
//  Created by Anmol Rajdev on 02/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import CoreLocation

class SuperFanViewController: BaseViewController, CLLocationManagerDelegate {
    
    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var itemsScrollView: UIScrollView!
    
    @IBOutlet var skipThisStepLabel: UILabel!
    
    @IBOutlet var superFanPageLabel: UILabel!
    
    @IBOutlet var searchBarView: UIView!
    
    @IBOutlet var searchBarTextField: UITextField!
    
    @IBOutlet var secondLineLabel: UILabel!
    
    @IBOutlet var artistsTableView: UITableView!
    
    var selectedValues : NSMutableArray = []
    
    var favoritesArray : NSMutableArray = []
    
    var artistNamesFromSearch : NSMutableArray = []
    
    var superFanFlag : NSMutableArray = []
    
    var artistNameFlagValue : Bool = true
    
    var artistId : NSMutableArray = []
    
    var finalArtistId : NSInteger = 0
    
    var artistAdded : String = ""
    
    var finalFlag : Bool = true
    
    var message : String = ""
    
    var locationManager : CLLocationManager!
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if message != "" {
            self.view.makeToast(message: message)
        }
        
        // *** Underline Skip this step ***
        let text = NSMutableAttributedString.init(attributedString: skipThisStepLabel.attributedText!)
        text.addAttribute(NSUnderlineStyleAttributeName, value: Int(1), range: NSMakeRange(0, (skipThisStepLabel.text?.characters.count)!))
        skipThisStepLabel.attributedText = text

        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad)
        {
            skipThisStepLabel.font = UIFont(name: skipThisStepLabel.font.fontName, size: 25)
            superFanPageLabel.font = UIFont(name: superFanPageLabel.font.fontName, size: 32)
        }
        
        // *** search bar changes ***
        self.searchBarView.layer.borderColor = UIColor(colorLiteralRed: 177.0/255.0, green: 177.0/255.0, blue: 177.0/255.0, alpha: 0.5).CGColor
        self.searchBarView.layer.borderWidth = 1.0
        
        // *** artist tableview changes ***
        self.artistsTableView.hidden = true
        self.artistsTableView.layer.borderWidth = 1.0
        self.artistsTableView.layer.borderColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1).CGColor
        
        
        let text1 = NSMutableAttributedString.init(attributedString: secondLineLabel.attributedText!)
        text1.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: NSMakeRange(0, 12))
        secondLineLabel.attributedText = text1
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            locationManager.startUpdatingLocation()
            // ** start activityIndicator
            setLoadingIndicator(self.view)
        } else {
            getPosibleArtistApiCall(false, zipCode: "", latitude: "", longitude: "")
            setLoadingIndicator(self.view)
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SuperFanViewController.pushViewController), name:"alertMessageSuperFan", object: nil)
        
    }
    
    func pushViewController() {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
        case .Authorized, .AuthorizedWhenInUse:
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "locationFlag")
        case .Denied:
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "locationFlag")
            getPosibleArtistApiCall(false, zipCode: "", latitude: "", longitude: "")
            break
        default:
            break
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last! as CLLocation
        
        let geoCoder = CLGeocoder()
        
        let latitude : String = ("\(location.coordinate.latitude)")
        NSUserDefaults.standardUserDefaults().setObject(latitude, forKey: "latitude")
        
        let longitude : String = ("\(location.coordinate.longitude)")
        NSUserDefaults.standardUserDefaults().setObject(longitude, forKey: "longitude")
        
        let locationForZip = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(locationForZip, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            
            placeMark = placemarks?[0]
            // Address dictionary
            
            if !(placeMark == nil) {
                
                if !(placeMark.addressDictionary == nil) {
                    
                    print(placeMark.addressDictionary)
                    
                    // Zip code
                    if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                        print(zip)
                        NSUserDefaults.standardUserDefaults().setObject(zip, forKey: "zipCode")
                        self.getPosibleArtistApiCall(true, zipCode: zip as String, latitude: latitude , longitude: longitude)
                    } else {
                        self.getPosibleArtistApiCall(true, zipCode: "", latitude: latitude , longitude: longitude)
                    }
                } else {
                    self.getPosibleArtistApiCall(true, zipCode: "", latitude: latitude , longitude: longitude)
                }
            } else {
                print("*********************** response nil ***********************")
                self.getPosibleArtistApiCall(true, zipCode: "", latitude: latitude , longitude: longitude)
            }
        })
        locationManager.stopUpdatingLocation()
    }
    
    //  *** get posible artists API call ***
    
    func getPosibleArtistApiCall(locationFlag : Bool, zipCode : String, latitude : String, longitude : String) -> Void {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        if self.favoritesArray.count == 0 {
            
            // *** api call for get popular artists ***
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            var custId : String = ""
            if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
                
                custId = ("\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)")
            } else {
                custId = ""
            }
            
            // *** send parameters
            
            let getParam : NSDictionary
            
            getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "locationFlag" : (locationFlag),
                "zipCode" : zipCode,
                "latitude" : latitude,
                "longitude" : longitude,
                "customerId" : custId
            ]
            
            // **** call webservice
            WebServiceCall.sharedInstance.getPopularArtistSuperFan("GetPopularArtistForSuperFan.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                // * stop activityIndicator
                self.stopIndicator()
                
                // ** check for valid result
                
                if isTrue == true {
                    
                    // * get the response dictionary
                    let artistList : NSDictionary = responseDisctionary!
                    
                    // ** get the artist from response dictionary
                    if let artists : NSArray = artistList.objectForKey("artists") as? NSArray {
                        // *** get the artists names
                        var i : Int = 0
                        while i < artists.count {
                            
                            let artistDetail = artists.objectAtIndex(i) as! NSDictionary
                            self.favoritesArray .addObject(artistDetail.objectForKey("name")! as! String)
                            self.artistId .addObject(artistDetail.objectForKey("id")! as! NSInteger)
                            
                            if artistDetail.objectForKey("custSuperFanFlag")! as! Bool {
                                self.artistAdded = artistDetail.objectForKey("name")! as! String
                            }
                            i += 1
                        }
                    }
                    
                    // **** call method for loading artists names
                    if self.favoritesArray.count != 0 {
                        self.loadViewsInScrollView()
                    }
                    
                } else {
                    
                    // * if no response
                    if message == "" {
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
            
        } else {
            
            self.loadViewsInScrollView()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.resignKeyBoard()
    }
    
    override func viewDidAppear(animated: Bool) {
        self.resignKeyBoard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Text field action API call
    //MARK:-
    
    @IBAction func searchBarValueChanged(sender: AnyObject) {
        
        // *** create textField
        let textField = sender as! UITextField
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        if artistNameFlagValue
        {
            // *** api call for search artists ***
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            var latitude : String = ""
            var longitude : String = ""
            var zipCode : String = ""
            var locationFlag : Bool = false
            
            if (NSUserDefaults.standardUserDefaults().objectForKey("locationFlag")) != nil {
                locationFlag = (NSUserDefaults.standardUserDefaults().objectForKey("locationFlag") as! Bool)
            }
            
            if NSUserDefaults.standardUserDefaults().objectForKey("latitude") != nil {
                latitude = NSUserDefaults.standardUserDefaults().objectForKey("latitude") as! String
            }
            if NSUserDefaults.standardUserDefaults().objectForKey("longitude") != nil {
                longitude = NSUserDefaults.standardUserDefaults().objectForKey("longitude") as! String
            }
            if NSUserDefaults.standardUserDefaults().objectForKey("zipCode") != nil {
                zipCode = NSUserDefaults.standardUserDefaults().objectForKey("zipCode") as! String
            }
            
            var custId : String = ""
            if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
                
                custId = ("\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)")
            } else {
                custId = ""
            }
            
            // *** send parameters
            let getParam : NSDictionary
            
            getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "searchKey" : textField.text!,
                "locationFlag" : locationFlag,
                "zipCode" : zipCode,
                "latitude" : latitude,
                "longitude" : longitude,
                "customerId" : custId
            ]
            
            // *** call webservice
            WebServiceCall.sharedInstance.searchArtist("SearchArtist.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                self.stopIndicator()
                
                // * check for valid result
                if isTrue == true {
                    
                    // * get the response dictionary
                    let artistList : NSDictionary = responseDisctionary!
                    
                    // ** get the artist from response dictionary
                    if let artists : NSArray = artistList.objectForKey("artists") as? NSArray {
                        
                        // *** get the artists names
                        var i : Int = 0
                        self.artistNamesFromSearch .removeAllObjects()
                        while i < artists.count{
                            let artistDetail = artists.objectAtIndex(i) as! NSDictionary
                            self.artistNamesFromSearch .addObject(artistDetail.objectForKey("name")! as! String)
                            i += 1
                        }
                    }
                    
                    // **** unhide the tableView
                    if self.finalFlag {
                        if self.artistNamesFromSearch.count != 0
                        {
                            UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseInOut, animations: {
                                if self.artistNamesFromSearch.count != 0 {
                                    self.artistsTableView.hidden = false
                                }
                            }) { finished in
                            }
                            
                            // **** reload data in tableview
                            self.artistsTableView.reloadData()
                        }
                    }
                    
                } else {
                    
                    // * if no response
                    if message == "" {
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    //MARK:- Text field delegate
    //MARK:-
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if (string == " " || string == ""){
            artistNameFlagValue = false
        }
        else
        {
            artistNameFlagValue = true
        }
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        finalFlag = true
    }
    
    //MARK:- Table view delegate and datasource
    //MARK:-
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return artistNamesFromSearch.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL") as UITableViewCell!
        if (cell == nil) {
            cell = UITableViewCell(style:.Default, reuseIdentifier: "CELL")
        }
        
        if artistNamesFromSearch.count != 0 {
            cell.textLabel?.text = artistNamesFromSearch.objectAtIndex(indexPath.row) as? String
        }
        
        // *** font size for iPad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            cell.textLabel!.font = UIFont.systemFontOfSize(24)
        } else {
            cell.textLabel!.font = UIFont.systemFontOfSize(14)
        }
        
        cell.textLabel?.lineBreakMode = .ByCharWrapping
        cell.textLabel?.textColor = UIColor(colorLiteralRed: 86.0/255.0, green: 87.0/255.0, blue: 88.0/255.0, alpha: 1.0)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.resignKeyBoard()
        if artistNamesFromSearch.count != 0 {
            
            if self.favoritesArray.containsObject(artistNamesFromSearch.objectAtIndex(indexPath.row)) {
                self.favoritesArray.removeObject(artistNamesFromSearch.objectAtIndex(indexPath.row))
            }
            favoritesArray .insertObject(artistNamesFromSearch.objectAtIndex(indexPath.row), atIndex: 0)
            artistAdded = artistNamesFromSearch.objectAtIndex(indexPath.row) as! String
            finalFlag = false
            UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseInOut, animations: {
                if self.artistNamesFromSearch.count != 0 {
                    self.artistsTableView.hidden = true
                }
            }) { finished in
            }
            self.loadViewsInScrollView()
        }
    }
    
    //MARK:- code to resign keyboard
    //MARK:-
    
    // *** tap gesture action ***
    func resignKeyBoard() {
        
        self.searchBarTextField.resignFirstResponder()
        UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseInOut, animations: {
            if self.artistNamesFromSearch.count != 0 {
                self.artistsTableView.hidden = true
            }
        }) { finished in
        }
    }
    
    // *** touch events action ***
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.resignKeyBoard()
    }

    
    //MARK:- Loading Buttons in scroll view
    //MARK:-
    
    func loadViewsInScrollView() -> Void {
        
        var i : Int = 0
        var x : CGFloat = 0
        var y : CGFloat = 0
        
        var j: Int = 0
        let subviews = self.itemsScrollView.subviews as NSArray
        while j < subviews.count {
            let tempButton = subviews.objectAtIndex(j) as? UIButton
            tempButton?.removeFromSuperview()
            j += 1
        }
        
        self.itemsScrollView.contentSize.height = self.itemsScrollView.frame.height
        
        while i < self.favoritesArray.count {
            let customButton = UIButton()
            let tempView = UIView()
            customButton.frame = CGRectMake(x, y,  self.getWidth(self.favoritesArray.objectAtIndex(i) as! String).width+8, 30)
            customButton.backgroundColor = UIColor(colorLiteralRed: 242.0/255.0, green: 238.0/255.0, blue: 237.0/255.0, alpha: 1.0)
            customButton.setTitleColor(UIColor(colorLiteralRed: 176.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0), forState: .Normal)
            customButton.setTitle(self.favoritesArray.objectAtIndex(i) as? String, forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                customButton.titleLabel?.font = UIFont(name: "Helvetica", size: 20)
            } else {
                customButton.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
            }
            customButton.tag = i+1
            customButton.addTarget(self, action: #selector(ChooseFavoritesViewController.selectedArtist(_:)), forControlEvents: .TouchUpInside)
            tempView.frame = CGRectMake(x, y, customButton.frame.width, 30)
            
            if (tempView.frame.origin.x + tempView.frame.size.width  > UIScreen.mainScreen().bounds.width-40 ) {
                
                if i == 0 {
                    
                    customButton.frame = CGRectMake(x, y, UIScreen.mainScreen().bounds.width-40, 30)
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                        y = y + 42
                    } else {
                        y = y + 38
                    }
                    x = 0
                    
                } else {
                    
                    var tempDist : CGFloat = 0
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                        tempDist = (UIScreen.mainScreen().bounds.width-40) - (tempView.frame.origin.x - 12)
                    } else {
                        tempDist = (UIScreen.mainScreen().bounds.width-40) - (tempView.frame.origin.x - 8)
                    }
                    let lastAddedButton = self.itemsScrollView.subviews.last as! UIButton
                    let frame = CGRectMake(lastAddedButton.frame.origin.x, lastAddedButton.frame.origin.y, lastAddedButton.frame.width + tempDist , 30)
                    lastAddedButton.frame = frame
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                        y = y + 42
                    } else {
                        y = y + 38
                    }
                    x = 0
                    customButton.frame = CGRectMake(x, y, customButton.frame.width, 30)
                    x = x + customButton.frame.width + 8
                }
            }
            else
            {
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                    x = x + customButton.frame.size.width + 12
                } else {
                    x = x + customButton.frame.size.width + 8
                }
            }
            if y > self.itemsScrollView.frame.height {
                self.itemsScrollView.contentSize.height = y + 30
            }
            
            if artistAdded != "" {
                
                if i == 0 {
                    selectedValues.removeAllObjects()
                    customButton.backgroundColor = UIColor(colorLiteralRed: 39.0/255.0, green: 153.0/255.0, blue: 250.0/255.0, alpha: 1.0)
                    customButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    selectedValues.addObject(customButton.tag)
                }

            }
            
            self.itemsScrollView.addSubview(customButton)
            i += 1
        }
    }
    
    func getWidth(text: String) -> CGSize {
        let tempLabel = UILabel()
        tempLabel.text = text
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            tempLabel.font = UIFont(name: "Helvetica", size: 20)
        } else {
            tempLabel.font = UIFont(name: "Helvetica", size: 15)
        }
        tempLabel.numberOfLines = 0
        return tempLabel.intrinsicContentSize()
    }

    //MARK:- Button actions
    //MARK:-
    
    // *** finish button action ***
    @IBAction func finishButtonTapped(sender: AnyObject) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        self.resignKeyBoard()
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            
            if selectedValues.count != 0 {
                
                self.finalArtistId = self.artistId.objectAtIndex(selectedValues.objectAtIndex(0) as! Int) as! NSInteger
                
                // *** api call for get popular artists ***
                
                // * check for internet connection
                guard IJReachability.isConnectedToNetwork() else {
                    
                    alertVc.message = ErrorInternetConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                    return
                }
                
                // ** start activityIndicator
                setLoadingIndicator(self.view)
                
                // *** send parameters
                let getParam = [
                    
                    "configId" : configId,
                    "productType" : productType,
                    "artistActionType" : "SUPERFAN",
                    "customerId" : (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger),
                    "artistIds" : self.finalArtistId
                ]
                
                // **** call webservice
                WebServiceCall.sharedInstance.addFavoriteOrSuperArtist("AddFavoriteOrSuperArtist.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                    
                    // * stop activityIndicator
                    self.stopIndicator()
                    
                    // ** check for valid result
                    if isTrue == true {
//                        self.view.makeToast(message: message!)
                        
                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as! HomeViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        
                        // * if no response
                        if message == "" {
                            
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        } else {
                            
                            // * if error in the response
                            
                            alertVc.message = message!
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    }
                })
            } else {
                
                alertVc.message = "Please select artist"
                self.presentViewController(alertVc, animated: true, completion: nil)
            }
        } else {
            alertVc.message = "Please login to be a Super Fan"
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
    }
    
    // *** skip thi step action ***
    @IBAction func skipButtonTapped(sender: AnyObject) {
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "skipWithoutLogin")
        } else {
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "skipWithoutLogin")
        }
        
        self.resignKeyBoard()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as! HomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // *** selected artist action ***
    func selectedArtist(sender: AnyObject)
    {
        let button = sender as! UIButton
        let val = button.tag
        let flag : Bool = selectedValues.containsObject(val)
        
        if selectedValues.count == 1
        {
            let previousTag : Int = selectedValues.objectAtIndex(0) as! Int
            
            if let button1 = self.itemsScrollView.viewWithTag(previousTag) as? UIButton
            {
                button1.backgroundColor = UIColor(colorLiteralRed: 242.0/255.0, green: 238.0/255.0, blue: 237.0/255.0, alpha: 1.0)
                button1.setTitleColor(UIColor(colorLiteralRed: 176.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0), forState: .Normal)
                selectedValues.removeObject(previousTag)
            }
        }
        
        if (flag == true)
        {
            button.backgroundColor = UIColor(colorLiteralRed: 242.0/255.0, green: 238.0/255.0, blue: 237.0/255.0, alpha: 1.0)
            button.setTitleColor(UIColor(colorLiteralRed: 176.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0), forState: .Normal)
            selectedValues.removeObject(val)
        }
        else
        {
            button.backgroundColor = UIColor(colorLiteralRed: 39.0/255.0, green: 153.0/255.0, blue: 250.0/255.0, alpha: 1.0)
            button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            selectedValues.addObject(val)
        }
    }

}
