//
//  AllCollectionViewCell.swift
//  RewardTheFanFinal
//
//  Created by Anmol Rajdev on 29/04/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit

class AllCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var allLbl: UILabel!
    @IBOutlet weak var allEventBgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override var bounds : CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.makeItCircle()
    }
    
    func makeItCircle() {
        self.allEventBgView.layer.masksToBounds = true
        self.allEventBgView.layer.cornerRadius = self.allEventBgView.frame.height/2;
        self.allEventBgView.clipsToBounds = true
    }
    
}
