//
//  CustomSheetView.swift
//  FuturEd
//
//  Created by Anmol Rajdev on 30/03/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
@objc protocol customActionsheetDelegate {
    
    
    optional func shareOnFacebook(index:Int)
    optional func shareOnTwitter(index:Int)
    optional func shareViaEmail(index:Int)
    
    optional func priceFilterApplyButtonClicked(minimum : Int, maximum : Int)
    
    optional func sortDoneButtonClicked (type : String)
}

class CustomSheetView: UIView,UITextFieldDelegate {
    

    //MARK:- Properties
    //MARK:
    
    @IBOutlet var priceLowToHighButton: UIButton!
    
    @IBOutlet var priceHighToLowButton: UIButton!
    
    @IBOutlet var quantityLowToHighButton: UIButton!
    
    @IBOutlet var quantityHighToLowButton: UIButton!
    
    @IBOutlet weak var sharingBgView: UIView!
    
    @IBOutlet var botton_height: NSLayoutConstraint!
    
    @IBOutlet weak var maxminView: UIView!
    
    @IBOutlet weak var rightTicketPriceTf: UITextField!
    
    @IBOutlet weak var priceofTicketLeftTf: UITextField!
    
    @IBOutlet weak var customSheetBgView: UIView!
    
    @IBOutlet weak var maxBgView: UIView!
    
    @IBOutlet weak var minimunBgView: UIView!
    
    @IBOutlet weak var priceBgView: UIView!
    
    @IBOutlet weak var sortBgView: UIView!
    
    weak var delegate: customActionsheetDelegate?
    
    var keyBoardFlag = false
    
    var filterType : String = ""
    
    //MARK:
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomSheetView.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomSheetView.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
//        IQKeyboardManager.sharedManager().enable = false
//        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        keyBoardFlag = true
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()) != nil {
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseOut, animations: {
                }, completion: { finished in
            })
            
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        keyBoardFlag = false
        UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseOut, animations: {

            }, completion: { finished in
                
        })
        
    }
    
    func callOnEventDetailForKeyboard(){
        self.priceofTicketLeftTf.delegate = self
        self.rightTicketPriceTf.delegate = self
    }
    @IBOutlet weak var shareEmailBtn: UIButton!
    @IBAction func shareViaEmailTapped(sender: AnyObject) {
        self.delegate?.shareViaEmail!(shareEmailBtn.tag)
        
    }
    @IBOutlet weak var TwitterBtn: UIButton!
    @IBAction func shareOnTwitterTapped(sender: AnyObject) {
        self.delegate?.shareOnTwitter!(TwitterBtn.tag)
    }
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBAction func shareOnFbTapped(sender: AnyObject) {
        self.delegate?.shareOnFacebook!(shareBtn.tag)
    }
    
    
    @IBAction func priceLowToHighButtonTapped(sender: AnyObject) {
        
        self.filterType = "priceLowToHigh"
        self.priceLowToHighButton.backgroundColor = UIColor( red:39.0/255,   green:133.0/255, blue:250.0/255, alpha:1.0)
        self.priceHighToLowButton.backgroundColor = UIColor( red:229.0/255,   green:223.0/255, blue:223.0/255, alpha:1.0)
        self.quantityLowToHighButton.backgroundColor = UIColor.whiteColor()
        self.quantityHighToLowButton.backgroundColor = UIColor( red:229.0/255,   green:223.0/255, blue:223.0/255, alpha:1.0)
        
        self.priceLowToHighButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.priceHighToLowButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.quantityLowToHighButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.quantityHighToLowButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
    }
    
    @IBAction func priceHighToLowButtonTapped(sender: AnyObject) {
        self.filterType = "priceHighToLow"
        self.priceLowToHighButton.backgroundColor = UIColor.whiteColor()
        self.priceHighToLowButton.backgroundColor = UIColor( red:39.0/255,   green:133.0/255, blue:250.0/255, alpha:1.0)
        self.quantityLowToHighButton.backgroundColor = UIColor.whiteColor()
        self.quantityHighToLowButton.backgroundColor = UIColor( red:229.0/255,   green:223.0/255, blue:223.0/255, alpha:1.0)
        
        self.priceLowToHighButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.priceHighToLowButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.quantityLowToHighButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.quantityHighToLowButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
    }
    
    @IBAction func quantityLowToHighButtonTapped(sender: AnyObject) {
        self.filterType = "quantityLowToHigh"
        self.priceLowToHighButton.backgroundColor = UIColor.whiteColor()
        self.priceHighToLowButton.backgroundColor = UIColor( red:229.0/255,   green:223.0/255, blue:223.0/255, alpha:1.0)
        self.quantityLowToHighButton.backgroundColor = UIColor( red:39.0/255,   green:133.0/255, blue:250.0/255, alpha:1.0)
        self.quantityHighToLowButton.backgroundColor = UIColor( red:229.0/255,   green:223.0/255, blue:223.0/255, alpha:1.0)
        
        self.priceLowToHighButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.priceHighToLowButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.quantityLowToHighButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.quantityHighToLowButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
    }
    
    @IBAction func quantityHighToLowButtonTapped(sender: AnyObject) {
        self.filterType = "quantityHighToLow"
        self.priceLowToHighButton.backgroundColor = UIColor.whiteColor()
        self.priceHighToLowButton.backgroundColor = UIColor( red:229.0/255,   green:223.0/255, blue:223.0/255, alpha:1.0)
        self.quantityLowToHighButton.backgroundColor = UIColor.whiteColor()
        self.quantityHighToLowButton.backgroundColor = UIColor( red:39.0/255,   green:133.0/255, blue:250.0/255, alpha:1.0)
        
        self.priceLowToHighButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.priceHighToLowButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.quantityLowToHighButton.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        self.quantityHighToLowButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    }
    @IBAction func priceFilterApplyButtonTapped(sender: AnyObject) {
        
        let mini : String = priceofTicketLeftTf.text!.stringByReplacingOccurrencesOfString("$", withString: "")
        
        let maxi : String = rightTicketPriceTf.text!.stringByReplacingOccurrencesOfString("$", withString: "")
        
        let min:Int? = Int(mini)
        let max:Int? = Int(maxi)
        if min == nil || max == nil {
            self.makeToast(message: "Enter the valid price.")
//            self.removeFromSuperview()
        } else {
            if min > max {
                self.makeToast(message: "Enter the valid maximum price.")
            } else {
                self.delegate?.priceFilterApplyButtonClicked!(min!, maximum: max!)
            }
            
            
        }
        
        
    }
    
    @IBAction func doneButtonTapped(sender: AnyObject) {
        self.delegate?.sortDoneButtonClicked!(self.filterType)
    }
    
}


