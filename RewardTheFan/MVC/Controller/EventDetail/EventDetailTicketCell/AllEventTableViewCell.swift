//
//  AllEventTableViewCell.swift
//  RewardTheFanFinal
//
//  Created by Anmol Rajdev on 29/04/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit

class AllEventTableViewCell: UITableViewCell {

    //MARK:- Properties
    //MARK:
    
    @IBOutlet var sectionLabel: UILabel!
    
    @IBOutlet var rowLabel: UILabel!
    
    @IBOutlet var priceButton: UIButton!
    
    @IBOutlet var eventPriceLabel: UILabel!
    
    //MARK:
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
