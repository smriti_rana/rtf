 //
 //  EventDetailViewController.swift
 //  RewardTheFanFinal
 //
 //  Created by Anmol Rajdev on 29/04/16.
 //  Copyright © 2016 Anmol Rajdev. All rights reserved.
 //
 
 import UIKit
 import WebKit
 import FBSDKCoreKit
 import FBSDKShareKit
 import FBSDKLoginKit
 import Social
 import MessageUI
 
 class EventDetailViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, WKScriptMessageHandler, WKNavigationDelegate, customActionsheetDelegate,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var NoEventlabel: UILabel!
    var customerLoyalityRewardsPointArray : NSDictionary = [:]
    //MARK:- Properties
    //MARK:
    
    
    @IBOutlet var viewDetailView: UIView!
    
    @IBOutlet var webViewBackGround: UIView!
    
    @IBOutlet var eventDetailLabel: UILabel!
    
    @IBOutlet var eventNameLabel: UILabel!
    
    @IBOutlet weak var droparrBtn: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var eventBgView: UIView!
    
    @IBOutlet weak var dateBgView: UIView!
    
    @IBOutlet var eventMonthLabel: UILabel!
    
    @IBOutlet var eventDayLabel: UILabel!
    
    @IBOutlet var eventYearLabel: UILabel!
    
    @IBOutlet var eventWeekDatLabel: UILabel!
    
    @IBOutlet var mainEventLabel: UILabel!
    
    @IBOutlet var mainEventTimeAndVenueLabel: UILabel!
    
    @IBOutlet var favouriteButton: UIButton!
    
    @IBOutlet var shareButton: UIButton!
    
    @IBOutlet var quantityAlerMessageLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var webViewbackgroundViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var webViewHeightConstraint: NSLayoutConstraint!
    
    var svgMapWebView: WKWebView!
    
    var isLoaded = false
    
//    var timer = NSTimer()
    
    var tableviewDataArray : NSMutableArray = []
    
    var filteredValues : NSMutableArray = []
    
    var collectionViewData : NSMutableArray = []
    
    var eventDetail : NSMutableArray = []
    
    var mySubview:CustomSheetView!
    
    let greyborderColor = UIColor( red:120.0/255,   green:116.0/255, blue:117.0/255, alpha:0.6)
    
    let blueColor = UIColor( red:39.0/255,   green:133.0/255, blue:250.0/255, alpha:1.0)
    
    @IBOutlet var tableViewHeightConstraint: NSLayoutConstraint!
    
    //    var placeholderForText1 = "$189"
    //
    //    var placeholderForText2 = "$1890"
    
    var eventId : String = ""
    
    @IBOutlet weak var price_lbl: UILabel!
    var pointNow : CGPoint!
    
    var selectedItem : NSInteger = 0
    
    var isSuperFan : Bool = false
    
    var shareLinkUrl : String = ""
    
    var filterFlag : Bool = false
    
    var eventViewHeight : CGFloat = 0.0
    
    var scrollViewScroll : Bool = false
    
    var filterType : String = ""
    
    var min : Int = 0
    
    var max : Int = 0
    
    var storedValues : NSMutableArray = []
    
    var loaderFlag : Bool = true
    
    var svgKey : String = ""
    
    var index : Int = 0
    
    //MARK:
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "filterType")
        NSUserDefaults.standardUserDefaults().setInteger(0 , forKey: "min")
        NSUserDefaults.standardUserDefaults().setInteger(0 , forKey: "max")
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView!.registerNib(UINib(nibName: "AllCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "Cell")
        
        dateBgView.layer.cornerRadius = dateBgView.frame.size.height/2
        
        tableView.registerNib(UINib(nibName: "AllEventTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell1")
        mySubview = NSBundle.mainBundle().loadNibNamed("CustomSheetView", owner: self, options: nil).first as? CustomSheetView
        tableView.delegate = self
        tableView.dataSource = self
        timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(EventDetailViewController.update), userInfo: nil, repeats: false)
        
        tableView.estimatedRowHeight = 81
        tableView.rowHeight = UITableViewAutomaticDimension
        
        droparrBtn.hidden = true
        viewDetailView.hidden = true
        eventNameLabel.hidden = true
        droparrBtn.alpha = 0
        viewDetailView.alpha = 0
        eventNameLabel.alpha = 0
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            eventDetailLabel.font = UIFont(name: eventDetailLabel.font.fontName, size: 31)
        }
        
        mySubview.priceofTicketLeftTf.delegate = self
        mySubview.rightTicketPriceTf.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EventDetailViewController.tap1))
        mySubview.sortBgView.addGestureRecognizer(tap)
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EventDetailViewController.tap1))
        mySubview.priceBgView.addGestureRecognizer(tap1)
        
        // *** web view changes *** //
        
        let contentController = WKUserContentController();
        let userScript = WKUserScript(
            source: "var meta = document.createElement('meta');" +
                "meta.name = 'viewport';" +
                "meta.content = 'width=device-width, initial-scale=0.0, maximum-scale=4.0, user-scalable=yes';" +
                "var head = document.getElementsByTagName('head')[0];" +
            "head.appendChild(meta);",
            injectionTime: WKUserScriptInjectionTime.AtDocumentEnd,
            forMainFrameOnly: true
        )
        contentController.addUserScript(userScript)
        contentController.addScriptMessageHandler(
            self,
            name: "native"
        )
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        self.svgMapWebView = WKWebView (
            frame: self.webViewBackGround.frame,
            configuration: config
        )
        
        // *** web view delagte ***//
        
        svgMapWebView.navigationDelegate = self
        timer.invalidate()
        
    }
    
    func tap1() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") != nil {
            customerLoyalityRewardsPointArray = NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") as! NSDictionary
            print(customerLoyalityRewardsPointArray)
            if let activeDollars =  customerLoyalityRewardsPointArray.valueForKey("activeRewardDollers") as? Int {
                var activeDollarsIs = ""
                if activeDollars == 0{
                    activeDollarsIs = "$0"
                    
                }else{
                    activeDollarsIs = "$\(activeDollars)"
                }
                
                price_lbl.text = activeDollarsIs
            }
        }
    }
    
    func eventDetailApiCall() {
        
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        var customerId : String = ""
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = "\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)"
        }
        
        // *** api call for Event detail ***
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            "deviceId" : deviceId,
            "eventId" : eventId,
            "platForm" : platForm,
            "customerId" : customerId
        ]
        
        // **** call webservice
        
        WebServiceCall.sharedInstance.eventDetail("GetEventDetails.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            // * stop activityIndicator
            self.stopIndicator()
            
            // ** check for valid result
            if isTrue == true {
                
                if let event = responseDisctionary?.objectForKey("event") as? NSDictionary {
                    
                    let eventDetailBrief : NSMutableArray = []
                    
                    // *** get event id *** //
                    if let eventId = event.objectForKey("eventId") as? NSInteger {
                        eventDetailBrief.addObject(eventId)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save event name *** //
                    if let eventName = event.objectForKey("eventName") as? String {
                        eventDetailBrief.addObject(eventName)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save date *** //
                    if let eventDate = event.objectForKey("eventDateStr") as? String {
                        eventDetailBrief.addObject(eventDate)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save time *** //
                    if let eventTime = event.objectForKey("eventTimeStr") as? String {
                        eventDetailBrief.addObject(eventTime)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save venue name *** //
                    if let venueName = event.objectForKey("venueName") as? String {
                        eventDetailBrief.addObject(venueName)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save city *** //
                    if let city = event.objectForKey("city") as? String {
                        eventDetailBrief.addObject(city)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save state *** //
                    if let state = event.objectForKey("state") as? String {
                        eventDetailBrief.addObject(state)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save country *** //
                    if let country = event.objectForKey("country") as? String {
                        eventDetailBrief.addObject(country)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save ticket price *** //
                    if let ticketPriceTag = event.objectForKey("ticketPriceTag") as? String {
                        eventDetailBrief.addObject(ticketPriceTag)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save is favorite set *** //
                    if let isSuperFanEvent = event.objectForKey("isSuperFanEvent") as? Bool {
                        eventDetailBrief.addObject(isSuperFanEvent)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save is favorite set *** //
                    if let isFavoriteEvent = event.objectForKey("isFavoriteEvent") as? Bool {
                        eventDetailBrief.addObject(isFavoriteEvent)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save svgWebViewUrl *** //
                    if let svgWebViewUrl = event.objectForKey("svgWebViewUrl") as? String {
                        eventDetailBrief.addObject(svgWebViewUrl)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save sharelink *** //
                    if let shareLinkUrl = event.objectForKey("shareLinkUrl") as? String {
                        eventDetailBrief.addObject(shareLinkUrl)
                    } else {
                        eventDetailBrief.addObject("")
                    }
                    
                    // *** save event details *** //
                    self.eventDetail.addObject(eventDetailBrief)
                }
                
                let ticketGroupList : NSMutableArray = []
                
                if let ticketGroupQtyList = responseDisctionary?.objectForKey("ticketGroupQtyList") as? NSMutableArray {
                    
                    for i in 0..<ticketGroupQtyList.count {
                        
                        let perGroupDetails : NSMutableArray = []
                        
                        if let eachQtyGroup = ticketGroupQtyList.objectAtIndex(i) as? NSDictionary {
                            
                            // *** get quantity id *** //
                            if let qty = eachQtyGroup.objectForKey("qty") as? NSInteger {
                                perGroupDetails.addObject(qty)
                            } else {
                                perGroupDetails.addObject("")
                            }
                            
                            if let qtyAlterMsg = eachQtyGroup.objectForKey("qtyAlterMsg") as? String {
                                perGroupDetails.addObject(qtyAlterMsg)
                            } else {
                                perGroupDetails.addObject("")
                            }
                            
                            let categoryGroups : NSMutableArray = []
                            
                            if let categoryTicketGroups = eachQtyGroup.objectForKey("categoryTicketGroups") as? NSMutableArray {
                                
                                for i in 0..<categoryTicketGroups.count {
                                    
                                    let ticketDetailBerief : NSMutableArray = []
                                    if let detailList = categoryTicketGroups.objectAtIndex(i) as? NSDictionary {
                                        
                                        if let eventId = detailList.objectForKey("eventId") as? NSInteger {
                                            ticketDetailBerief.addObject(eventId)
                                        } else {
                                            ticketDetailBerief.addObject("")
                                        }
                                        
                                        if let section = detailList.objectForKey("section") as? String {
                                            ticketDetailBerief.addObject(section)
                                        } else {
                                            ticketDetailBerief.addObject("")
                                        }
                                        
                                        if let row = detailList.objectForKey("row") as? String {
                                            ticketDetailBerief.addObject(row)
                                        } else {
                                            ticketDetailBerief.addObject("")
                                        }
                                        
                                        if let quantity = detailList.objectForKey("quantity") as? NSInteger {
                                            ticketDetailBerief.addObject(quantity)
                                        } else {
                                            ticketDetailBerief.addObject("")
                                        }
                                        
                                        if let price = detailList.objectForKey("price") as? NSInteger {
                                            ticketDetailBerief.addObject(price)
                                        } else {
                                            ticketDetailBerief.addObject("")
                                        }
                                        
                                        if let svgKey = detailList.objectForKey("svgKey") as? String {
                                            ticketDetailBerief.addObject(svgKey)
                                        } else {
                                            ticketDetailBerief.addObject("")
                                        }
                                        
                                        if let id = detailList.objectForKey("id") as? NSInteger {
                                            ticketDetailBerief.addObject(id)
                                        } else {
                                            ticketDetailBerief.addObject("")
                                        }
                                        
                                        categoryGroups.addObject(ticketDetailBerief)
                                    } else {
                                        categoryGroups.addObject(ticketDetailBerief)
                                    }
                                }
                                perGroupDetails.addObject(categoryGroups)
                                
                            } else {
                                
                                perGroupDetails.addObject(categoryGroups)
                            }
                            
                            ticketGroupList.addObject(perGroupDetails)
                            
                        } else {
                            
                            ticketGroupList.addObject(perGroupDetails)
                        }
                    }
                    
                    self.eventDetail.addObject(ticketGroupList)
                    
                } else {
                    
                    self.eventDetail.addObject(ticketGroupList)
                }
                
                print(self.eventDetail)
                self.setValuesFromApi()
                self.filterFlag = false
                self.tableView.reloadData()
                self.collectionView.reloadData()
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
        
    }
    
    func setValuesFromApi() {
        
        if let eventDetail = self.eventDetail.objectAtIndex(0) as? NSMutableArray {
            
            // *** setting event name *** //
            
            var eventName : String = ""
            
            if let name = eventDetail.objectAtIndex(1) as? String {
                eventName = name
            }
            
            // *** set eventName *** //
            self.eventNameLabel.text = eventName
            
            // *** setting event month, date, year and weekday *** //
            
            // *** check for date *** //
            
            if eventDetail.objectAtIndex(2) as? String == "TBD" {
                
                // *** event month *** //
                self.eventMonthLabel.text = "TBD"
                
                // *** event day *** //
                self.eventDayLabel.text = "TBD"
                
                // *** event year *** //
                self.eventYearLabel.text = "TBD"
                
                // *** event week day *** //
                self.eventWeekDatLabel.text = "TBD"
                
            } else {
                // *** get date *** //
                
                let date = eventDetail.objectAtIndex(2) as? String
                
                // *** get date from string *** //
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let dateFromString = dateFormatter.dateFromString(date!)
                
                
                // *** get month *** //
                
                let dateFormatteerForMonth = NSDateFormatter()
                dateFormatteerForMonth.dateFormat = "MMM"
                let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                
                // *** event month *** //
                self.eventMonthLabel.text = monthFromDate
                
                
                // *** get day *** //
                
                let dateFormatteerForDay = NSDateFormatter()
                dateFormatteerForDay.dateFormat = "dd"
                let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                
                // *** event day *** //
                self.eventDayLabel.text = dayFromDate
                
                
                // *** get year *** //
                
                let dateFormatteerForYear = NSDateFormatter()
                dateFormatteerForYear.dateFormat = "YYYY"
                let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                
                // *** event year *** //
                self.eventYearLabel.text = yearFromDate
                
                // *** get week day *** //
                
                let dateFormatteerForWeekDay = NSDateFormatter()
                dateFormatteerForWeekDay.dateFormat = "EEE"
                let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                
                // *** event week day *** //
                self.eventWeekDatLabel.text = weekDayFromDate
            }
            
            // *** set main event name *** //
            self.mainEventLabel.text = self.eventNameLabel.text
            
            // *** set event date and venue *** //
            
            // *** get time *** //
            
            var time : String = ""
            
            if eventDetail.objectAtIndex(3) as? String == "TBD"{
                time = "TBD"
            } else {
                
                let timeSlot = (eventDetail.objectAtIndex(3) as? String)!
                
                let dateFormatterForTime = NSDateFormatter()
                dateFormatterForTime.dateFormat = "hh:mma"
                let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                
                let dateFormatteerForTimeFormat = NSDateFormatter()
                dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                time = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
            }
            
            // *** set address *** //
            
            self.mainEventTimeAndVenueLabel.text = "\(time) - \(eventDetail.objectAtIndex(4)), \(eventDetail.objectAtIndex(5)), \(eventDetail.objectAtIndex(6)), \(eventDetail.objectAtIndex(7))"
            
            self.view.layoutIfNeeded()
            
            self.eventViewHeight = self.eventBgView.frame.height;
            self.webViewbackgroundViewTopConstraint.constant = self.eventViewHeight
            
            // *** change the font for ipad ***
            if(UIDevice.currentDevice().userInterfaceIdiom != .Pad) {
                self.webViewHeightConstraint.constant = scrollView.frame.height - (self.eventViewHeight + 160)
                let frame = CGRectMake(0, 0, self.webViewBackGround.frame.width, self.webViewHeightConstraint.constant)
                
                self.svgMapWebView.frame = frame
                self.svgMapWebView.sizeToFit()
            }
            
            self.view.layoutIfNeeded()
            
            self.eventViewHeight = self.eventBgView.frame.height;
            self.webViewbackgroundViewTopConstraint.constant = self.eventViewHeight
            
            // *** set favorite *** //
            
            var favourite : Bool = false
            
            if let isFavourite = eventDetail.objectAtIndex(10) as? Bool {
                favourite = isFavourite
            }
            
            if favourite {
                self.favouriteButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
            } else {
                self.favouriteButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
            }
            
            
            // *** set web url *** //
            
            var url : String = ""
            
            if let webUrl = eventDetail.objectAtIndex(11) as? String {
                url = webUrl
            }
            
            if url != "" {
                svgMapWebView.loadRequest(NSURLRequest(URL: NSURL(string: url)!))
            }
            
            if let shareUrl = eventDetail.objectAtIndex(12) as? String {
                shareLinkUrl = shareUrl
            }
            
            if let superFan = eventDetail.objectAtIndex(9) as? Bool {
                isSuperFan = superFan
            }
            
        }
        
        // *** set values in collection view *** //
        
        if eventDetail.count != 0 {
            if let eventDetail = self.eventDetail.objectAtIndex(1) as? NSMutableArray {
                
                eventDetail.insertObject(eventDetail.objectAtIndex(eventDetail.count-1), atIndex: 0)
                eventDetail.removeLastObject()
                collectionViewData = eventDetail
                collectionView.reloadData()
            }
        }
        
        if eventDetail.count != 0 {
            
            if let eventDetail = self.eventDetail.objectAtIndex(1) as? NSMutableArray {
                
                if let perEventDetail = eventDetail.objectAtIndex(0) as? NSMutableArray {
                    
                    if let ticketDetails = perEventDetail.objectAtIndex(2) as? NSMutableArray {
                        
                        if ticketDetails.count > 0 {
                            
                            NoEventlabel.hidden =  true
                            storedValues.removeAllObjects()
                            storedValues = ticketDetails.mutableCopy() as! NSMutableArray
                            tableviewDataArray.removeAllObjects()
                            tableviewDataArray = ticketDetails.mutableCopy() as! NSMutableArray
                            
                        } else {
                            
                            NoEventlabel.hidden =  false
                            NoEventlabel.text = "No Tickets Available."
                        }
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    
    func update() {
        
        collectionView.reloadData() // Something cool
        dateBgView.layer.cornerRadius = dateBgView.frame.size.height/2
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        if loaderFlag {
            
            // ** start activityIndicator
            self.setLoadingIndicator(self.scrollView)
            eventDetailApiCall()
            self.loaderFlag = false
        }
        
        let frame = CGRectMake(0, 0, self.webViewBackGround.frame.width, self.webViewHeightConstraint.constant)
        
        self.svgMapWebView.frame = frame
        self.svgMapWebView.sizeToFit()
        self.webViewBackGround.addSubview(self.svgMapWebView)
        
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK:- UICollectionViewDataSource
    // MARK:
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if eventDetail.count != 0 {
            if let eventDetail = self.eventDetail.objectAtIndex(1) as? NSMutableArray {
                return (eventDetail.count)
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! AllCollectionViewCell
        //        cell.allEventBgView.backgroundColor = UIColor.whiteColor()
        if  indexPath.item == selectedItem {
            cell.allEventBgView.backgroundColor = blueColor
            cell.allLbl.textColor = UIColor.whiteColor()
        }
        else{
            cell.allEventBgView.backgroundColor = UIColor.whiteColor()
            cell.allLbl.textColor = UIColor.darkGrayColor()
            
        }
        cell.allLbl.text = ""
        if eventDetail.count != 0 {
            if let eventDetail = self.eventDetail.objectAtIndex(1) as? NSMutableArray {
                if let perEventDetail = eventDetail.objectAtIndex(indexPath.item) as? NSMutableArray {
                    
                    if let ticekts = perEventDetail.objectAtIndex(0) as? NSInteger {
                        if indexPath.item == 0 {
                            cell.allLbl.text = "All"
                            //                            self.quantityAlerMessageLabel.text = ""
                            //                            if let warningMessage = perEventDetail.objectAtIndex(1) as? String {
                            //                                self.quantityAlerMessageLabel.text = warningMessage
                            //                            }
                        } else {
                            cell.allLbl.text = "\((ticekts))"
                        }
                    }
                }
            }
        }
        cell.allEventBgView.layer.borderColor = UIColor.grayColor().CGColor
        cell.allEventBgView.layer.cornerRadius = cell.allEventBgView.frame.size.height/2
        cell.allEventBgView.layer.borderWidth = 0.5;
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! AllCollectionViewCell
        cell.allEventBgView.backgroundColor = blueColor
        cell.allLbl.textColor = UIColor.whiteColor()
        
        selectedItem = indexPath.item
        self.collectionView.reloadData()
        
        index = indexPath.item
        
        if eventDetail.count != 0 {
            
            if let eventDetail = self.eventDetail.objectAtIndex(1) as? NSMutableArray {
                
                if let perEventDetail = eventDetail.objectAtIndex(indexPath.item) as? NSMutableArray {
                    
                    if let ticketDetails = perEventDetail.objectAtIndex(2) as? NSMutableArray {
                        
                        self.storedValues.removeAllObjects()
                        self.storedValues = ticketDetails.mutableCopy() as! NSMutableArray
                        
                        tableviewDataArray.removeAllObjects()
                        tableviewDataArray = ticketDetails.mutableCopy() as! NSMutableArray
                        
                        if NSUserDefaults.standardUserDefaults().objectForKey("min") != nil {
                            
                            self.min = (NSUserDefaults.standardUserDefaults().objectForKey("min") as! Int)
                        }
                        
                        if NSUserDefaults.standardUserDefaults().objectForKey("max") != nil {
                            
                            self.max = (NSUserDefaults.standardUserDefaults().objectForKey("max") as! Int)
                        }
                        
                        if ( self.min != 0 ) || (self.max != 0 ) {
                            
                            self.priceFilterApplyButtonClicked(self.min , maximum : self.max)
                        }
                        
                        if NSUserDefaults.standardUserDefaults().objectForKey("filterType") != nil {
                            
                            self.filterType = (NSUserDefaults.standardUserDefaults().objectForKey("filterType") as? String)!
                            self.sortDoneButtonClicked(self.filterType)
                        }
                        
                        if svgKey != "" {
                            self.changeSvgKey(indexPath.row)
                        }
                        
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func changeSvgKey(index : Int) {
        if eventDetail.count != 0 {
            if let eventDetail = self.eventDetail.objectAtIndex(1) as? NSMutableArray {
                print(eventDetail)
                if let perEventDetail = eventDetail.objectAtIndex(index) as? NSMutableArray {
                    print(perEventDetail)
                    if let ticketDetails = perEventDetail.objectAtIndex(2) as? NSMutableArray {
                        
                        print(ticketDetails)
                        let tableData : NSMutableArray = []
                        
                        for i in 0..<ticketDetails.count {
                            if let perEvent = ticketDetails.objectAtIndex(i) as? NSMutableArray {
                                
                                if perEvent.objectAtIndex(5) as? String == svgKey {
                                    
                                    var detail : NSMutableArray = []
                                    detail = perEvent
                                    
                                    tableData.addObject(detail)
                                }
                            }
                        }
                        
                        tableviewDataArray.removeAllObjects()
                        tableviewDataArray = tableData.mutableCopy() as! NSMutableArray
                        //                            self.filterFlag = false
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return CGSizeMake(self.collectionView.frame.height-30, self.collectionView.frame.height-30)
        } else {
            return CGSizeMake(self.collectionView.frame.height-18, self.collectionView.frame.height-18)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return UIEdgeInsetsMake(5, 15, 5, 15); //top,left,bottom,right
        } else {
            return UIEdgeInsetsMake(8, 8, 8, 8); //top,left,bottom,right
        }
    }
    
    
    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableviewDataArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath)
            as! AllEventTableViewCell
        cell.selectionStyle = .None
        if (indexPath.row % 2 == 0) {
            
            cell.backgroundColor = UIColor.whiteColor()
            
        } else {
            cell.backgroundColor = UIColor( red:239.0/255,   green:239.0/255, blue:244.0/255, alpha:1.0)
        }
        
        cell.sectionLabel.text = ""
        cell.rowLabel.text = ""
        cell.priceButton.setTitle("", forState: .Normal)
        
        if let perEventDetail = tableviewDataArray.objectAtIndex(indexPath.row) as? NSMutableArray {
            
            if let section = perEventDetail.objectAtIndex(1) as? String {
                cell.sectionLabel.text = section
            }
            
            var rows : String = ""
            
            var tikcets : String = ""
            
            if let row = perEventDetail.objectAtIndex(2) as? String {
                rows = row
            }
            
            var qtyIs = 0
            
            if let qty = perEventDetail.objectAtIndex(3) as? NSInteger {
                qtyIs = qty
                tikcets = "\(qty)"
            }
            
            cell.rowLabel.text = "\(rows) | \(tikcets)"
            
            if rows == "" {
                var setRowIs = ""
                if qtyIs == 1{
                    setRowIs = "\(tikcets) ticket"
                }else{
                    setRowIs = "\(tikcets) tickets"
                }
                cell.rowLabel.text = setRowIs
            }
            
            if tikcets == "" {
                cell.rowLabel.text = rows
            }
            
            if let price = perEventDetail.objectAtIndex(4) as? NSInteger {
                cell.eventPriceLabel.text = "$\(price)"
            }
            
            //            let visibleCells = tableView.visibleCells as? NSArray
            
            //            for cell in tableView.visibleCells {
            
            
            //            let cells : [NSIndexPath] = tableView.indexPathsForVisibleRows!
            //            if indexPath == cells.first {
            //                if let svgKey = perEventDetail.objectAtIndex(5) as? String {
            //                    self.svgMapWebView!.evaluateJavaScript("highlightZone('\(svgKey)')", completionHandler: { (test, error) -> Void in
            //                    })
            //                }
            //            }
            
            //            if indexPath.row == tableView.visibleCells.startIndex {
            //                print(indexPath.row)
            //            }
            //                if indexPath.row == tableView.visibleCells.startIndex {
            //                    if let svgKey = perEventDetail.objectAtIndex(5) as? String {
            //                        self.svgMapWebView!.evaluateJavaScript("highlightZone('\(svgKey)')", completionHandler: { (test, error) -> Void in
            //                        })
            //                    }
            //                }
            //            }
            
            //            for (UITableViewCell *c in [tbl visibleCells])
            //            {
            //                //   UITableViewCell *cell2 = [tbl cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            //                UILabel *lbl = (UILabel*)[c.contentView viewWithTag:1];
            //                lbl.textColor = [UIColor redColor];
            //            }
            //
            //                        if cell == tableView.visibleCells.first {
            //            if let svgKey = perEventDetail.objectAtIndex(5) as? String {
            //                self.svgMapWebView!.evaluateJavaScript("highlightZone('\(svgKey)')", completionHandler: { (test, error) -> Void in
            //                })
            //            }
            //                        }
            
        }
        
        if isSuperFan {
            cell.priceButton.setBackgroundImage(UIImage (named: "ticketSuperFan"), forState: .Normal)
        } else {
            cell.priceButton.setBackgroundImage(UIImage (named: "eventsTicket"), forState: .Normal)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        //        if cell == tableView.visibleCells.last {
        //            if let perEventDetail = tableviewDataArray.objectAtIndex(indexPath.row) as? NSMutableArray {
        //                if let svgKey = perEventDetail.objectAtIndex(5) as? String {
        //                    self.svgMapWebView!.evaluateJavaScript("highlightZone('\(svgKey)')", completionHandler: { (test, error) -> Void in
        //                    })
        //                }
        //            }
        //        }
    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let perEventDetail = tableviewDataArray.objectAtIndex(indexPath.row) as? NSMutableArray {
            
            var eventIdToPass : String = ""
            var ticketIdToPass : String = ""
            
            if let eventId = perEventDetail.objectAtIndex(0) as? NSInteger {
                eventIdToPass = "\(eventId)"
            }
            
            if let id = perEventDetail.objectAtIndex(6) as? NSInteger {
                ticketIdToPass = "\(id)"
            }
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("OrderSummary") as! OrderSummaryViewController
            vc.eventId = eventIdToPass
            vc.ticketId = ticketIdToPass
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK:
    
    @IBOutlet weak var sortBtn: UIButton!
    @IBAction func sortTapped(sender: AnyObject) {
        
        commonCodeForCustomSheetPriceOrSort()
        mySubview.delegate = self
        mySubview.customSheetBgView.hidden = false
        mySubview.sortBgView.hidden = false
        mySubview.priceBgView.hidden = true
        mySubview.sharingBgView.hidden = true
        mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, mySubview.customSheetBgView.frame.size.height)
        self.view.addSubview(mySubview)
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 1.0
            self.mySubview.sortBgView.frame = CGRectMake(0, self.view.frame.size.height - 123, self.view.frame.size.width, self.mySubview.customSheetBgView.frame.size.height)
            }, completion: { finished in
        })
        
    }
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        removeCustomView()
    }
    func removeCustomView() {
        
        if mySubview == nil {
            return
        }
        
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            
            }, completion: { finished in
                if self.mySubview.keyBoardFlag == true{
                    self.mySubview.priceofTicketLeftTf.resignFirstResponder()
                    self.mySubview.rightTicketPriceTf.resignFirstResponder()
                }
                else{
                    self.mySubview.removeFromSuperview()
                }
        })
    }
    
    @IBOutlet weak var priceBtn: UIButton!
    @IBAction func priceTapped(sender: AnyObject) {
        
        commonCodeForCustomSheetPriceOrSort()
        mySubview.delegate = self
        //        self.mySubview.priceofTicketLeftTf.text = placeholderForText1
        //        self.mySubview.rightTicketPriceTf.text = placeholderForText2
        self.mySubview.minimunBgView.layer.borderWidth = 1
        self.mySubview.maxBgView.layer.borderWidth = 1
        self.mySubview.minimunBgView.layer.borderColor = greyborderColor.CGColor
        self.mySubview.maxBgView.layer.borderColor = greyborderColor.CGColor
        mySubview.customSheetBgView.hidden = false
        mySubview.sortBgView.hidden = true
        mySubview.priceBgView.hidden = false
        mySubview.sharingBgView.hidden = true
        mySubview.priceBgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, mySubview.customSheetBgView.frame.size.height)
        self.view.addSubview(mySubview)
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 1.0
            self.mySubview.priceBgView.frame = CGRectMake(0, self.view.frame.size.height - 123, self.view.frame.size.width, self.mySubview.customSheetBgView.frame.size.height)
            }, completion: { finished in
        })
        
    }
    func commonCodeForCustomSheetPriceOrSort(){
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.6)
        mySubview.delegate = self
        mySubview.customSheetBgView.backgroundColor = greyColor
        self.mySubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        let tap = UITapGestureRecognizer(target: self, action: #selector(EventDetailViewController.handleTap(_:)))
        mySubview.customSheetBgView.addGestureRecognizer(tap)
    }
    
    //MARK:ScrollViewDelegate
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        pointNow = scrollView.contentOffset
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y > pointNow.y + 30 {
            if scrollView == self.scrollView {
                
                
                UIView.animateWithDuration(0.5, delay:0.0, options: .CurveEaseIn, animations: {
                    self.eventBgView.frame = CGRectMake(self.eventBgView.frame.origin.x, self.eventBgView.frame.origin.y, self.eventBgView.frame.width, 0)
                    self.webViewbackgroundViewTopConstraint.constant = 0
                    //                    self.scrollView.setContentOffset(CGPointZero, animated: true)
                    self.droparrBtn.alpha = 1
                    self.viewDetailView.alpha = 1
                    self.eventDetailLabel.alpha = 0
                    self.eventNameLabel.alpha = 1
                    self.droparrBtn.hidden = false
                    self.viewDetailView.hidden = false
                    self.eventBgView.hidden = true
                    self.eventNameLabel.hidden = false
                    self.eventDetailLabel.hidden = true
                    self.view.layoutIfNeeded()
                    }, completion: { finished in
                        //                            self.scrollView.setContentOffset(CGPointZero, animated: true)
                        //                            self.scrollViewScroll = true
                })
                
                
            }
        }
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    
    @IBAction func eventInfoButtonTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EventInfo") as! EventInfoViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func drop_arraow(sender: AnyObject) {
        
        UIView.animateWithDuration(0.5, delay:0.0, options: .CurveEaseOut, animations: {
            
            self.eventBgView.frame = CGRectMake(self.eventBgView.frame.origin.x, self.eventBgView.frame.origin.y, self.eventBgView.frame.width, self.eventViewHeight)
            self.webViewbackgroundViewTopConstraint.constant = self.eventViewHeight
            //            self.scrollView.setContentOffset(CGPointZero, animated: true)
            self.droparrBtn.alpha = 0
            self.viewDetailView.alpha = 0
            self.eventDetailLabel.alpha = 1.0
            self.eventNameLabel.alpha = 0
            self.eventBgView.hidden = false
            self.droparrBtn.hidden = true
            self.viewDetailView.hidden = true
            self.eventNameLabel.hidden = true
            self.eventDetailLabel.hidden = false
            self.view.layoutIfNeeded()
            }, completion: { finished in
                //                self.scrollView.scrollToTop()
                //                self.scrollView.setContentOffset(CGPointZero, animated: true)
                //                self.scrollViewScroll = false
        })
        
    }
    
    // Mark:TextfeildDelegates
    func textFieldDidBeginEditing(textField: UITextField) {
        
        //        if textField.text == placeholderForText1 || textField.text == placeholderForText2 {
        textField.text = "$"
        textField.textColor = UIColor.blackColor()
        //        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if range.length>0  && range.location == 0 {
            
            let changedText = NSString(string: textField.text!).substringWithRange(range)
            if changedText.containsString("$") {
                return false
            }
        }
        return true
    }
    
    
    //MARK:- Webview delgates
    //MARK:
    
    func message(message:String)
    {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        //                self.svgMapWebView!.evaluateJavaScript("highlightZone('h2')", completionHandler: { (test, error) -> Void in
        //                    print(error);
        //                    print(test);
        //                })
    }
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        //            print(message.name)
        if(message.name == "native") {
            //                    print("JavaScript is sending a message \(message.body)")
            let svgKey = message.body.objectForKey("catId") as! String
            
            let removeFilter = message.body.objectForKey("removeFilter") as! Bool
            
            if !removeFilter {
                
                self.svgKey = svgKey
                
                if eventDetail.count != 0 {
                    if let eventDetail = self.eventDetail.objectAtIndex(1) as? NSMutableArray {
                        if let perEventDetail = eventDetail.objectAtIndex(index) as? NSMutableArray {
                            if let ticketDetails = perEventDetail.objectAtIndex(2) as? NSMutableArray {
                                
                                let tableData : NSMutableArray = []
                                
                                for i in 0..<ticketDetails.count {
                                    if let perEvent = ticketDetails.objectAtIndex(i) as? NSMutableArray {
                                        
                                        if perEvent.objectAtIndex(5) as? String == svgKey {
                                            
                                            var detail : NSMutableArray = []
                                            detail = perEvent
                                            
                                            tableData.addObject(detail)
                                        }
                                    }
                                }
                                
                                tableviewDataArray.removeAllObjects()
                                tableviewDataArray = tableData.mutableCopy() as! NSMutableArray
                                //                            self.filterFlag = false
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            } else {
                self.svgKey = ""
                if eventDetail.count != 0 {
                    
                    if let eventDetail = self.eventDetail.objectAtIndex(1) as? NSMutableArray {
                        
                        if let perEventDetail = eventDetail.objectAtIndex(index) as? NSMutableArray {
                            
                            if let ticketDetails = perEventDetail.objectAtIndex(2) as? NSMutableArray {
                                
                                self.storedValues.removeAllObjects()
                                self.storedValues = ticketDetails.mutableCopy() as! NSMutableArray
                                
                                tableviewDataArray.removeAllObjects()
                                tableviewDataArray = ticketDetails.mutableCopy() as! NSMutableArray
                                
                                if NSUserDefaults.standardUserDefaults().objectForKey("min") != nil {
                                    
                                    self.min = (NSUserDefaults.standardUserDefaults().objectForKey("min") as! Int)
                                }
                                
                                if NSUserDefaults.standardUserDefaults().objectForKey("max") != nil {
                                    
                                    self.max = (NSUserDefaults.standardUserDefaults().objectForKey("max") as! Int)
                                }
                                
                                if ( self.min != 0 ) || (self.max != 0 ) {
                                    
                                    self.priceFilterApplyButtonClicked(self.min , maximum : self.max)
                                }
                                
                                if NSUserDefaults.standardUserDefaults().objectForKey("filterType") != nil {
                                    
                                    self.filterType = (NSUserDefaults.standardUserDefaults().objectForKey("filterType") as? String)!
                                    self.sortDoneButtonClicked(self.filterType)
                                }
                                
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
                
            }
        }
    }
    
    
    //MARK:- Button actions
    //MARK:
    
    // *** favourite button action *** //
    @IBAction func favouriteButtonTapped(sender: AnyObject) {
        
        if let detail = self.eventDetail.objectAtIndex(0) as? NSMutableArray {
            if let eventId = detail.objectAtIndex(0) as? NSInteger {
                
                var customerId : String = ""
                
                if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
                    customerId = "\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)"
                }
                
                if customerId != "" {
                    
                    // *** alert controller *** //
                    
                    let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
                    alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
                    
                    // *** api call for Home cards ***
                    
                    // * check for internet connection
                    guard IJReachability.isConnectedToNetwork() else {
                        
                        alertVc.message = ErrorInternetConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                        return
                    }
                    
                    // *** send parameters
                    let getParam = [
                        
                        "configId" : configId,
                        "productType" : productType,
                        "customerId" : customerId,
                        "eventId" : (eventId)
                    ]
                    
                    // **** call webservice
                    
                    WebServiceCall.sharedInstance.favouriteEvent("FavouriteEvents.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                        
                        // ** check for valid result
                        if isTrue == true {
                            
                            self.view.makeToast(message: message!)
                            
                            var fav : Bool = false
                            
                            if let isfavorite = detail.objectAtIndex(10) as? Bool {
                                
                                fav = isfavorite
                                
                                if fav == true {
                                    self.favouriteButton.setImage((UIImage(named: "heartWhite")), forState: .Normal)
                                    
                                    self.eventDetail.objectAtIndex(0).replaceObjectAtIndex(10, withObject: false)
                                    
                                } else {
                                    self.favouriteButton.setImage((UIImage(named: "heartRed")), forState: .Normal)
                                    self.eventDetail.objectAtIndex(0).replaceObjectAtIndex(10, withObject: true)
                                }
                            }
                            
                        } else {
                            
                            if message == "" {
                                // * if no response
                                
                                alertVc.message = ErrorServerConnection
                                self.presentViewController(alertVc, animated: true, completion: nil)
                            } else {
                                // * if error in the response
                                
                                alertVc.message = message!
                                self.presentViewController(alertVc, animated: true, completion: nil)
                            }
                        }
                    })
                } else {
                    
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
        
        
    }
    
    // *** share Button action *** //
    @IBAction func shareButtonTapped(sender: AnyObject) {
        
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.3)
        
        mySubview.customSheetBgView.backgroundColor = greyColor
        
        self.mySubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(EventDetailViewController.handleTap(_:)))
        mySubview.customSheetBgView.addGestureRecognizer(tap)
        
        mySubview.customSheetBgView.hidden = false
        mySubview.sortBgView.hidden = true
        mySubview.priceBgView.hidden = true
        mySubview.sharingBgView.hidden = false
        mySubview.sharingBgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, mySubview.customSheetBgView.frame.size.height)
        self.view.addSubview(mySubview)
        mySubview.delegate = self
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 1.0
            self.mySubview.sharingBgView.frame = CGRectMake(0, self.view.frame.size.height - 123, self.view.frame.size.width, self.mySubview.customSheetBgView.frame.size.height)
            }, completion: { finished in
        })
    }
    
    
    //MARK:- ShareDialogMethods
    
    func shareOnFacebook(index: Int){
        
        
        let facebookPost = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        facebookPost.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                //Code to deal with it being cancelled
                //print("cancel clicked")
                self.removeCustomView()
                break
                
            case SLComposeViewControllerResult.Done:
                //Code here to deal with it being completed
                self.removeCustomView()
                break
            }
        }
        
        facebookPost.setInitialText(shareLinkUrl) //The default text in the tweet
        self.presentViewController(facebookPost, animated: false, completion: {
            //Optional completion statement
        })
        
    }
    
    //MARK:- TwitterCicked
    func shareOnTwitter(index: Int){
        
        let twitterSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        twitterSheet.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                //print("cancel clicked")
                
                break
                
            case SLComposeViewControllerResult.Done:
                self.removeCustomView()
                //Code here to deal with it being completed
                break
            }
        }
        twitterSheet.setInitialText(shareLinkUrl)
        self.presentViewController(twitterSheet, animated: false, completion: {
            
        })
        
    }
    
    //MARK:- Emailsharing Tapped
    
    func shareViaEmail(index: Int) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            //            mail.setToRecipients(["preeti@42works.net"])
            mail.setMessageBody(shareLinkUrl, isHTML: true)
            
            presentViewController(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        removeCustomView()
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func priceFilterApplyButtonClicked(minimum: Int, maximum: Int) {
        
        NSUserDefaults.standardUserDefaults().setInteger(minimum, forKey: "min")
        NSUserDefaults.standardUserDefaults().setInteger(maximum, forKey: "max")
        
        let tempValues : NSMutableArray = self.storedValues.mutableCopy() as! NSMutableArray
        
        tableviewDataArray.removeAllObjects()
        
        for i in 0..<tempValues.count {
            
            if let tempData = tempValues.objectAtIndex(i) as? NSMutableArray {
                if let priceValues = tempData.objectAtIndex(4) as? NSInteger {
                    
                    if ((priceValues >= minimum) && (priceValues <= maximum)) {
                        tableviewDataArray.addObject(tempData)
                    }
                }
            }
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("filterType") != nil {
            self.filterType = (NSUserDefaults.standardUserDefaults().objectForKey("filterType") as? String)!
            self.sortDoneButtonClicked(self.filterType)
        }
        
        self.tableView.reloadData()
        
        removeCustomView()
    }
    
    func sortDoneButtonClicked (type : String) {
        
        NSUserDefaults.standardUserDefaults().setObject(type, forKey: "filterType")
        
        if type == "priceLowToHigh" {
            
            let ascending = tableviewDataArray.sort { $0[4].intValue < $1[4].intValue }
            
            tableviewDataArray.removeAllObjects()
            
            for i in 0..<ascending.count {
                tableviewDataArray.addObject(ascending[i])
            }
            
        } else if type == "priceHighToLow" {
            
            let ascending = tableviewDataArray.sort { $0[4].intValue > $1[4].intValue }
            
            tableviewDataArray.removeAllObjects()
            
            for i in 0..<ascending.count {
                tableviewDataArray.addObject(ascending[i])
            }
            
        } else if type == "quantityLowToHigh" {
            
            let ascending = tableviewDataArray.sort { $0[3].intValue < $1[3].intValue }
            
            tableviewDataArray.removeAllObjects()
            
            for i in 0..<ascending.count {
                tableviewDataArray.addObject(ascending[i])
            }
            
        } else if type == "quantityHighToLow" {
            
            let ascending = tableviewDataArray.sort {$0[3].intValue > $1[3].intValue  }
            
            tableviewDataArray.removeAllObjects()
            
            for i in 0..<ascending.count {
                tableviewDataArray.addObject(ascending[i])
            }
        }
        self.tableView.setContentOffset(CGPointZero, animated:true)
        self.tableView.scrollsToTop = true
        self.tableView.reloadData()
        removeCustomView()
    }
    
 }
