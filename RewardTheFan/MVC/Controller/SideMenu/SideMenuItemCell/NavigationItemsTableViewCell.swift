//
//  NavigationItemsTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 25/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

@objc protocol navigationCellDelegate
{
    optional func itemSelected(button: UIButton, index : NSInteger)
}

class NavigationItemsTableViewCell: UITableViewCell {

    //MARK:- Properties
    //MARK:-
    
    weak var delegate: navigationCellDelegate?

    @IBOutlet var backGroundView: UIView!
    
    @IBOutlet var iconImageView: UIImageView!
    
    @IBOutlet var selectionView: UIView!
    
    @IBOutlet var itemNameLabel: UILabel!
    
    @IBOutlet var cellButton: UIButton!
    
    @IBOutlet var iconButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func cellButtonTapped(sender: AnyObject) {
        self.delegate?.itemSelected!(sender as! UIButton, index: sender.tag)
    }
}
