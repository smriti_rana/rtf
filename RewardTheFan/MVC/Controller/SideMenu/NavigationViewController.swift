//
//  NavigationViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 20/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class NavigationViewController: UIViewController, navigationCellDelegate {

    @IBOutlet var baseTableView: UITableView!
    
    var tableHeaderView = UIView()
    
    let userImageImageView = UIImageView()
    
    let userNameLabel = UILabel()
    
    let creditLabel = UILabel()
    
    let userCreditsLabel = UILabel()
    
    let creditsOuterView = UIView()
    
    let seperatorView = UIView()
    
    var listItemNames : NSMutableArray = []
    
    var listItemImages : [UIImage] = []
    
    var selectedIndex : NSInteger = 0
    
    @IBOutlet var baseView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // *** register table view cell ***
        baseTableView.registerNib(UINib(nibName: "NavigationItemsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")

        
        
        listItemNames = ["Home", "My Account", "Favorites", "Get Free Tickets", "Points Earned", "Super Fan Rewards", "Refer a Friend", "Reward Tickets","Settings"]
        
        listItemImages = [UIImage(named: "home")!, UIImage(named: "myAccount")!, UIImage(named: "favorites")!, UIImage(named: "freeTickets")!, UIImage(named: "pointsEarned")!, UIImage(named: "superFanRewards")!, UIImage(named: "referFriend")!, UIImage(named: "ticket")!,UIImage(named: "settings")!]
        //set userLoggedIn name
       
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            userNameLabel.font =  UIFont.systemFontOfSize(27)
            creditLabel.font =  UIFont.systemFontOfSize(27)
            userCreditsLabel.font =  UIFont.systemFontOfSize(27)
        } else {
            userNameLabel.font = UIFont.systemFontOfSize(17)
            creditLabel.font = UIFont.systemFontOfSize(17)
            userCreditsLabel.font = UIFont.systemFontOfSize(17)
        }
    

    }

    override func viewDidAppear(animated: Bool) {
        
        self.baseTableView .reloadData()
        print(NSUserDefaults.standardUserDefaults().valueForKey("customerName"))
        if NSUserDefaults.standardUserDefaults().valueForKey("customerName") != nil {
            let  getUserName = NSUserDefaults.standardUserDefaults().valueForKey("customerName") as! String
            userImageImageView.image = UIImage(named: "nav")
            userNameLabel.text = getUserName
            userNameLabel.textColor = UIColor.whiteColor()
        }
        
        //Set Dollar Saved In user Defaults
        if NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") != nil {
            let customerLoyalityRewardsPointArray = NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") as! NSDictionary
            print(customerLoyalityRewardsPointArray)
            if let activeDollars =  customerLoyalityRewardsPointArray.valueForKey("activeRewardDollers") as? Int {
                var activeDollarsIs = ""
                if activeDollars == 0{
                    activeDollarsIs = "$0"
                    
                }else{
                    activeDollarsIs = "$\(activeDollars)"
                }
                
                creditLabel.text = "Credits :"
                creditLabel.textColor = UIColor.whiteColor()
                userCreditsLabel.text = activeDollarsIs
                userCreditsLabel.textColor = UIColor.whiteColor()
            }
        }
        print(NSUserDefaults.standardUserDefaults().objectForKey("customerId"))
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                
                tableHeaderView.frame = CGRectMake(0, 0, self.baseTableView.frame.width, 300)
                userImageImageView.frame = CGRectMake(0, 8, 180, 180)
                self.userImageImageView.layer.cornerRadius = self.userImageImageView.frame.height/2;
                self.userImageImageView.clipsToBounds = true
                userNameLabel.frame = CGRectMake(0, userImageImageView.frame.size.height + userImageImageView.frame.origin.y + 16, getWidth(userNameLabel.text!).width, getWidth(userNameLabel.text!).height)
                creditLabel.frame = CGRectMake(0, 0, getWidth(creditLabel.text!).width, getWidth(creditLabel.text!).height)
                userCreditsLabel.frame = CGRectMake(creditLabel.frame.width + 5, 0, getWidth(userCreditsLabel.text!).width, getWidth(userCreditsLabel.text!).height)
                creditsOuterView.frame = CGRectMake(0, userNameLabel.frame.size.height + userNameLabel.frame.origin.y + 16, creditLabel.frame.width + userCreditsLabel.frame.width + 8, userNameLabel.frame.height)
                seperatorView.frame = CGRectMake(0, self.tableHeaderView.frame.height - 2, self.tableHeaderView.frame.width, 2)
                seperatorView.backgroundColor = UIColor.grayColor()
                
                tableHeaderView.addSubview(userImageImageView)
                tableHeaderView.addSubview(userNameLabel)
                tableHeaderView.addSubview(creditsOuterView)
                tableHeaderView.addSubview(seperatorView)
                creditsOuterView.addSubview(creditLabel)
                creditsOuterView.addSubview(userCreditsLabel)
                
                self.userImageImageView.center.x = self.tableHeaderView.center.x
                self.userNameLabel.center.x = self.userImageImageView.center.x
                self.creditsOuterView.center.x = self.userNameLabel.center.x
                
                baseTableView.tableHeaderView = tableHeaderView
                
            } else {
                
                tableHeaderView.frame = CGRectMake(0, 0, self.baseTableView.frame.width, 200)
                userImageImageView.frame = CGRectMake(0, 8, 120, 120)
                self.userImageImageView.layer.cornerRadius = self.userImageImageView.frame.height/2;
                self.userImageImageView.clipsToBounds = true
                userNameLabel.frame = CGRectMake(0, userImageImageView.frame.size.height + userImageImageView.frame.origin.y + 8, getWidth(userNameLabel.text!).width, getWidth(userNameLabel.text!).height)
                creditLabel.frame = CGRectMake(0, 0, getWidth(creditLabel.text!).width, getWidth(creditLabel.text!).height)
                userCreditsLabel.frame = CGRectMake(creditLabel.frame.width + 5, 0, getWidth(userCreditsLabel.text!).width, getWidth(userCreditsLabel.text!).height)
                creditsOuterView.frame = CGRectMake(0, userNameLabel.frame.size.height + userNameLabel.frame.origin.y + 8, creditLabel.frame.width + userCreditsLabel.frame.width + 8, userNameLabel.frame.height)
                seperatorView.frame = CGRectMake(0, self.tableHeaderView.frame.height - 2, self.tableHeaderView.frame.width, 2)
                seperatorView.backgroundColor = UIColor.grayColor()
                
                tableHeaderView.addSubview(userImageImageView)
                tableHeaderView.addSubview(userNameLabel)
                tableHeaderView.addSubview(creditsOuterView)
                tableHeaderView.addSubview(seperatorView)
                creditsOuterView.addSubview(creditLabel)
                creditsOuterView.addSubview(userCreditsLabel)
                
                self.userImageImageView.center.x = self.tableHeaderView.center.x
                self.userNameLabel.center.x = self.userImageImageView.center.x
                self.creditsOuterView.center.x = self.userNameLabel.center.x
                
                baseTableView.tableHeaderView = tableHeaderView
            }
        } else {
            baseTableView.tableHeaderView = nil
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    func getWidth(text: String) -> CGSize {
        let tempLabel = UILabel()
        tempLabel.text = text
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            tempLabel.font =  UIFont.systemFontOfSize(27)
        } else {
            tempLabel.font = UIFont.systemFontOfSize(17)
        }
        tempLabel.numberOfLines = 0
        return tempLabel.intrinsicContentSize()
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Table view delegate and datasource methods
    //MARK:-
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItemNames.count
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        // *** changes for iPad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 84
        } else {
            return 56
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
            as! NavigationItemsTableViewCell
        cell.itemNameLabel.text = self.listItemNames.objectAtIndex(indexPath.row) as? String
        cell.iconButton.setImage(self.listItemImages[indexPath.row], forState: .Normal)
        if indexPath.row == selectedIndex {
            cell.backGroundView.backgroundColor = UIColor(red:16.0/255, green:120.0/255, blue:248.0/255, alpha:1.0)
            cell.selectionView.backgroundColor = UIColor(red:240.0/255, green:104.0/255, blue:18.0/255, alpha:1.0)
        } else {
            cell.backGroundView.backgroundColor = UIColor(red:39.0/255, green:133.0/255, blue:250.0/255, alpha:1.0)
            cell.selectionView.backgroundColor = UIColor.clearColor()
        }
        cell.cellButton.tag = indexPath.row
        cell.delegate = self
        return cell
    }
    
    func itemSelected(button: UIButton, index: NSInteger) {
        self.view.removeFromSuperview()
        let cell = button.superview?.superview?.superview as! NavigationItemsTableViewCell
        
        cell.backGroundView.backgroundColor = UIColor(red:16.0/255, green:120.0/255, blue:248.0/255, alpha:1.0)
        cell.selectionView.backgroundColor = UIColor(red:240.0/255, green:104.0/255, blue:18.0/255, alpha:1.0)
        
        if index != 0 {
            if index == 1 {
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("myaccountView") as! MyAccountViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
//            else if index == 3{
//                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("MyTickets") as! MyTicketsViewController
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
            else if index == 6{
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("referFriend") as! ReferfriendViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if index == 8{
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Settings") as! SettingsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
            var sg = ""
            
            sg = listItemNames.objectAtIndex(index) as! String
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("NavigationItems") as! NavigationItemsViewController
            vc.message = sg
            self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        selectedIndex = index
    }
    
}
