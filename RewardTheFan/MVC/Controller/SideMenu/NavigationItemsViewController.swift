//
//  NavigationItemsViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 26/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class NavigationItemsViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    
    var message : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        titleLabel.text = message
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backButtonTapped(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
}
