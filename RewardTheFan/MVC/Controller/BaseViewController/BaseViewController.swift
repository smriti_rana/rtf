//
//  BaseViewController.swift
//  RewardOfFans
//
//  Created by Smriti Thakur on 22/02/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
//import CoreLocation
@objc protocol ordersummaryDelegate
{
    optional func setTimerDelegate(minutes:Int,seconds:Int,totalSec:Int)
     optional func setTimerCheckoutDelegate(minutes:Int,seconds:Int,totalSec:Int)
}
class BaseViewController: UIViewController, UINavigationControllerDelegate {

    weak var delegate: ordersummaryDelegate?
    var popToViewFlag = false
    let blueColorComment = UIColor(red: 39/255, green: 133/255, blue: 255/255, alpha: 1.0)
    let baseColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1.0)
    
	let navigationBar = UINavigationBar()
    
    var configId : String = ""
    
    var productType : String = ""
    
    var platForm : String = ""
    
    var deviceId : String = ""
    
     var customerId : Int = 0
    
    var AppName = "Rewardthefan"
    var timer:NSTimer = NSTimer()
    var minutes1 : Int = 0
    var totalseconds  = 60
    var seconds1 : Int = 0
    var emptyTableSearchMsg = "Try another search or check back soon"
    //var locationManager: CLLocationManager!

    override func viewDidLoad() {
        super.viewDidLoad()
		self.navigationController?.navigationBar.hidden = false
        
        // *** get values from defaults ***
        
        if NSUserDefaults.standardUserDefaults().objectForKey("configId") != nil {
            configId = NSUserDefaults.standardUserDefaults().objectForKey("configId") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("productType") != nil {
            productType = NSUserDefaults.standardUserDefaults().objectForKey("productType") as! String
        }

        if NSUserDefaults.standardUserDefaults().objectForKey("platForm") != nil {
            platForm = NSUserDefaults.standardUserDefaults().objectForKey("platForm") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("deviceId") != nil {
            deviceId = NSUserDefaults.standardUserDefaults().objectForKey("deviceId") as! String
        }
       
    }
    
	func scrollViewTapped() {
		self.view.endEditing(true)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	//Mark// Loading Indicator
	var obj:LoadingIndicatorView!
	
    func setLoadingIndicator(view :UIView) {
		if obj != nil {
			obj.removeFromSuperview()
		}
		
		obj = NSBundle.mainBundle().loadNibNamed("LoadingIndicator", owner: self, options: nil).first as! LoadingIndicatorView
		obj.frame = view.frame
		view.addSubview(obj)
		obj.StartLoad()
	}
	
	func checkInternetConnection() {
        
        guard IJReachability.isConnectedToNetwork() else {
			let alertView = UIAlertView(title: "", message: ErrorInternetConnection, delegate: nil, cancelButtonTitle: "OK")
			alertView.show()
			return
		}
	}
	
	func stopIndicator() {
		delay(0.1) { () -> () in
			if self.obj != nil {
				self.obj.StopLoad()
				self.obj.removeFromSuperview()
			}
		}
	}
	
	func delay(delay:Double, closure:()->()) {
		dispatch_after(
			dispatch_time(
				DISPATCH_TIME_NOW,
				Int64(delay * Double(NSEC_PER_SEC))
			),
			dispatch_get_main_queue(), closure)
	}
	

    func setLoadingIndicatorForSearch(navbar_height:CGRect) {
        
        if obj != nil {
            obj.removeFromSuperview()
        }
        
        obj = NSBundle.mainBundle().loadNibNamed("LoadingIndicator", owner: self, options: nil).first as! LoadingIndicatorView
        obj.frame =  CGRectMake(0, navbar_height.size.height, self.view.frame.size.width, self.view.frame.size.height)
        self.view.addSubview(obj)
        obj.StartLoad()
    }
    //MARK:- validations
    //MARK:-
    
    // *** for email
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
   
    //MARK:-HandleTime methods
     func handleTimerMethod(){
        minutes1 = (totalseconds / 60)-1
        seconds1 =  60
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(BaseViewController.subtractTime), userInfo: nil, repeats: true)
    }
    
    //MARK:-handleTimer Methods
    var checkoutFlag = ""
    func subtractTime() {
        if(seconds1 == 0){
            minutes1 -= 1
            seconds1 = 60
        }
        seconds1 -= 1
        totalseconds -= 1
         print("minutes:",minutes1)
        print("seconds:",seconds1)
        
        
            self.delegate!.setTimerDelegate!(minutes1, seconds: seconds1, totalSec: totalseconds)
       
        print("total seconds left:",totalseconds)
        if totalseconds == 0 {
            print("Timer expires")
            stopTimer()
        }
      
        
    }
    func stopTimer(){
        print("remove timer ")
        timer.invalidate()
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        alertVc.message = timerExpireAlertmsg
        self.presentViewController(alertVc, animated: true, completion: nil)
     
        
    }
    //   
//    //MARK:-Alert Ok
//    func AlertController(){
//        poptoEventDetail()
//    }
//    
//    func poptoEventDetail(){
//    }
}
