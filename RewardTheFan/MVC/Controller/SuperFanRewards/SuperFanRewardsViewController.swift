//
//  SuperFanRewardsViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 01/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class SuperFanRewardsViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var topViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var teamName: UILabel!
    
    @IBOutlet var ticketsCountLabel: UILabel!
    
    @IBOutlet var baseTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        baseTableView.registerNib(UINib(nibName: "SuperFanRewardsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")
        
        baseTableView.estimatedRowHeight = 44
        baseTableView.rowHeight = UITableViewAutomaticDimension
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
            as! SuperFanRewardTableViewCell
        cell.selectionStyle = .None
        if (indexPath.row % 2 == 0) {
            cell.backgroundColor = UIColor.whiteColor()
        } else {
            cell.backgroundColor = UIColor( red:229.0/255,   green:223.0/255, blue:223.0/255, alpha:1.0)
        }
        
        return cell
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func submitButtonTapped(sender: AnyObject) {
    }
}
