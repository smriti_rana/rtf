//
//  EventInfoViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 03/05/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class EventInfoViewController: UIViewController {

    @IBOutlet var eventInfoLable: UILabel!
    
    @IBOutlet var baseTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        baseTableView.registerNib(UINib(nibName: "EventInfoArtistDescriptionTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")
        baseTableView.registerNib(UINib(nibName: "EventInfoNewsFeedTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell1")
        baseTableView.registerNib(UINib(nibName: "HomeEventsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell2")
        self.baseTableView.estimatedRowHeight = 901
        self.baseTableView.rowHeight = UITableViewAutomaticDimension
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            eventInfoLable.font = UIFont(name: eventInfoLable.font.fontName, size: 32)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Table view delegate and datasource methods
    //MARK:-
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
                as! EventInfoArtistDescriptionTableViewCell
            return cell
        } else if indexPath.row == 1 || indexPath.row == 2 {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath)
                as! EventInfoNewsFeedTableViewCell
            if indexPath.row == 2 {
                cell.seperatorHeightConstraint.constant = 0
            } else {
                cell.seperatorHeightConstraint.constant = 2
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell2", forIndexPath: indexPath)
                as! HomeEventsTableViewCell
            cell.dateView.layer.cornerRadius = cell.dateView.frame.height/2;
            //cell.dateView.clipsToBounds = true
//            cell.dateViewLeading.constant = 32
            
            cell.cardNameHeightConstraint.constant = 0
                if indexPath.row % 2 == 0 {
                cell.colorView.backgroundColor = UIColor(colorLiteralRed: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 0.2)
            } else {
                cell.colorView.backgroundColor = UIColor.whiteColor()
            }
            return cell
        }
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
