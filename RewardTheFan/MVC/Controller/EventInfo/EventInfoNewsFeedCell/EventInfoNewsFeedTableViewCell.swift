//
//  EventInfoNewsFeedTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 03/05/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class EventInfoNewsFeedTableViewCell: UITableViewCell {
    @IBOutlet var readMoreButton: UIButton!

    @IBOutlet var seperatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet var newsFeedDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        newsFeedDescriptionLabel.text = "Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text"
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func readMoreButtonTapped(sender: AnyObject) {
    }
    
}
