//
//  EventInfoArtistDescriptionTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 03/05/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class EventInfoArtistDescriptionTableViewCell: UITableViewCell {

    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        descriptionLabel.text = "Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text"
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
