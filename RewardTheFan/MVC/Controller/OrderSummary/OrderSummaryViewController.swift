//
//  OrderSummaryViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 02/05/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import WebKit
@objc protocol checkTimerDelegate
{
    optional func getTimerCheckDelegate(minutes:Int,seconds:Int,totalSec:Int)
}
class OrderSummaryViewController: BaseViewController, WKNavigationDelegate , WKScriptMessageHandler,ordersummaryDelegate {
    weak var delegate1: checkTimerDelegate?
//     var timer = NSTimer()
    var getminutes1 : Int = 0
    var gettotalseconds1 : Int = 0
    var getseconds1 : Int = 0
    @IBOutlet var webViewBackground: UIView!
    
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var TutleLbl: UILabel!
    //var svgMapWebView: WKWebView!
    var eventId = ""
    var ticketId = ""
    
    var price : Int = 0
    var qty : Int = 0
    
    @IBOutlet weak var zoneDescrLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var sectionLbl: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var deliveryMethodLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBAction func editTapped(sender: AnyObject) {
    }
    @IBOutlet weak var delieveryMethods_lbl: UILabel!
    @IBOutlet var webViewBackGround: UIView!
    @IBOutlet weak var venueAndAddress: UILabel!
    @IBOutlet weak var EnameLbl: UILabel!
    var responseDictFromEvent = [:]
    @IBOutlet weak var dayLbl: UILabel!
    
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var dateView: UIView!
    
    @IBOutlet var ticketWebView: WKWebView!
    let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
       checkoutFlag = ""
        // Do any additional setup after loading the view.
        
        descriptionLabel.text = "Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text Some dummy Text"
        
        self.ticketWebView.layer.borderWidth = 1.0
        self.ticketWebView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        self.dateView.layer.cornerRadius = self.dateView.frame.height/2;
        self.dateView.clipsToBounds = true
        
        NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(OrderSummaryViewController.updateData), userInfo: nil, repeats: false)
        
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            TutleLbl.font = UIFont(name: TutleLbl.font.fontName, size: 31)
            navbar_height.constant = 74
        }
        
        //Api Call
        NSUserDefaults.standardUserDefaults().setValue(eventId, forKey: "eventId")
        NSUserDefaults.standardUserDefaults().setValue(ticketId, forKey: "id")
        setLoadingIndicator(self.view)
        setDataFromEventDetailApi()
        
        // *** web view changes *** //
        
        let contentController = WKUserContentController();
        let userScript = WKUserScript(
            source: "var meta = document.createElement('meta');" +
                "meta.name = 'viewport';" +
                "meta.content = 'width=device-width, initial-scale=0.0, maximum-scale=4.0, user-scalable=yes';" +
                "var head = document.getElementsByTagName('head')[0];" +
            "head.appendChild(meta);",
            injectionTime: WKUserScriptInjectionTime.AtDocumentEnd,
            forMainFrameOnly: true
        )
        contentController.addUserScript(userScript)
        contentController.addScriptMessageHandler(
            self,
            name: "native"
        )
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = false
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        //config.preferences = preferences
        
        self.ticketWebView = WKWebView (
            frame: self.webViewBackground.frame,
            configuration: config
        )
        
        // *** web view delegate ***//
        ticketWebView.navigationDelegate = self
        ticketWebView.scrollView.userInteractionEnabled = true
        handleTimerMethod()
        self.delegate = self
      
       
       
        
    }
    //MARK:-Alert Ok
      //MARK:-PoptoSpecificView
    func poptoEventDetail(){
        for controller in self.navigationController!.viewControllers as Array {
            print("controllers in Stack :",controller)
            if controller.isKindOfClass(EventDetailViewController) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }

   //MARK:SetTimeOn Label
      func setTimerDelegate(minutes:Int,seconds:Int,totalSec:Int){
        print(totalSec)
        let seconds1Str = String(seconds)
        if seconds1Str.characters.count == 1{
            timeLabel.text = "\(0)\(minutes)\(":")\(0)\(seconds)"
        }
        else{
            timeLabel.text = "\(0)\(minutes)\(":")\(seconds)"
        }
        
        if checkoutFlag == "Checkout"{
            self.vc.getTimerCheckDelegate(minutes, seconds1: seconds, totalSec1: totalSec)

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateData() {
        self.dateView.layer.cornerRadius = self.dateView.frame.height/2;
        self.dateView.clipsToBounds = true
    }
    
    override func viewDidAppear(animated: Bool) {
        let frame = CGRectMake(0, 0, self.webViewBackground.frame.width, self.webViewBackground.frame.height)
        
        self.ticketWebView.frame = frame
        self.ticketWebView.sizeToFit()
        self.webViewBackground.addSubview(self.ticketWebView)
    }
    
    @IBAction func backButtonTaped(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    var vc = CheckoutTypeViewController()
    @IBAction func proceedToCheckOutButtonTapped(sender: AnyObject) {
        // *** alert controller *** //
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** get customer ID
        var customerId : String = ""
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = ("\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)")
        }
        
        if customerId == "" {
            
            // *** show alert message *** //
            alertVc.message = loginAlertMessage
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else {
            
            vc = self.storyboard?.instantiateViewControllerWithIdentifier("CheckoutType") as! CheckoutTypeViewController
            vc.total = qty * price
            checkoutFlag = "Checkout"
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    //MARK:- Webview delgates
    //MARK:
    
    func message(message:String)
    {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        //        self.svgMapWebView!.evaluateJavaScript("highlightZone('h2')", completionHandler: { (test, error) -> Void in
        //            print(error);
        //            print(test);
        //        })
    }
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        //        print(message.name)
        //        if(message.name == "native") {
        //            //            print("JavaScript is sending a message \(message.body)")
        //        }
    }
    
    //MARK:-SetDatafromEventDetail As
    func setDataFromEventDetailApi() {
        // *** api call for forgot password ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** get customer ID
        var customerId : String = ""
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = ("\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)")
        }
        
        // *** send parameters
        let getParam = [
            
            "productType" : productType,
            "sessionIdOrDeviceId" : deviceId,
            "categoryTicketGroupId" : ticketId,
            "eventId" : eventId,
            "customerId" : customerId,
            "platForm" : platForm,
            "configId" : configId,
            ]
        
        // **** call webservice
        WebServiceCall.sharedInstance.postOrderSummary("AutoCatsLockTicketGroup.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            self.stopIndicator()
            
            if isTrue == true {
                
                print(responseDisctionary!)
                
                if let eventArray = responseDisctionary!.valueForKey("event") as? NSDictionary  {
                    print(eventArray)
                    
                    // *** save event name *** //
                    if let eventName = eventArray.valueForKey("eventName") as? String {
                        self.EnameLbl.text = eventName
                    }
                    
                    //venue Address
                    var eTime = ""
                    var eVenue = ""
                    var eCity = ""
                    var eState = ""
                    var eCountry = ""
                    var svgWebUrl = ""
                    
                    // *** save time *** //
                    if let eventTime = eventArray.valueForKey("eventTimeStr") as? String {
                        
                        if eventTime == "TBD" {
                            eTime = "TBD"
                        } else {
                            
                            //24hr format
                            let timeSlot = eventTime
                            let dateFormatterForTime = NSDateFormatter()
                            dateFormatterForTime.dateFormat = "hh:mma"
                            let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                            let dateFormatteerForTimeFormat = NSDateFormatter()
                            dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                            eTime = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                        }
                    }
                    
                    // *** save venue name *** //
                    if let venueName = eventArray.valueForKey("venueName") as? String {
                        eVenue = venueName
                    }
                    // *** save city *** //
                    if let city = eventArray.valueForKey("city") as? String {
                        eCity = city
                    }
                    // *** save state *** //
                    if let state = eventArray.valueForKey("state") as? String {
                        eState =  state
                    }
                    
                    // *** save country *** //
                    if let country = eventArray.valueForKey("country") as? String {
                        eCountry = country
                    }
                    
                    let resultStr = "\(eTime)-\(eVenue),\(eCity),\(eState),\(eCountry)"
                    
                    self.venueAndAddress.text = resultStr
                    
                    // *** save date *** //
                    if let eventDate = eventArray.valueForKey("eventDateStr") as? String {
                        if eventDate != "TBD" {
                            
                            // *** get datey from string *** //
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "MM/dd/yyyy"
                            let dateFromString = dateFormatter.dateFromString(eventDate)
                            
                            // *** get day *** //
                            
                            let dateFormatteerForDay = NSDateFormatter()
                            dateFormatteerForDay.dateFormat = "dd"
                            let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                            self.dateLbl.text = dayFromDate
                            
                            // *** get year *** //
                            
                            let dateFormatteerForYear = NSDateFormatter()
                            dateFormatteerForYear.dateFormat = "YYYY"
                            let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                            self.yearLbl.text = yearFromDate
                            
                            // *** get month *** //
                            
                            let dateFormatteerForMonth = NSDateFormatter()
                            dateFormatteerForMonth.dateFormat = "MMM"
                            let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                            self.monthLbl.text = monthFromDate
                            
                            // *** get week day *** //
                            
                            let dateFormatteerForWeekDay = NSDateFormatter()
                            dateFormatteerForWeekDay.dateFormat = "EEE"
                            let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                            self.dayLbl.text = weekDayFromDate
                            
                        }
                    }
                    // *** save svgWebViewUrl *** //
                    if let svgWebViewUrl = eventArray.objectForKey("svgWebViewUrl") as? String {
                        svgWebUrl = svgWebViewUrl
                        // Your webView code goes here
                        let url = NSURL (string: svgWebUrl)
                        let requestObj = NSURLRequest(URL: url!)
                        self.ticketWebView.loadRequest(requestObj)
                        
                        self.ticketWebView.scrollView.userInteractionEnabled = true
                        self.ticketWebView.allowsBackForwardNavigationGestures = false
                        //                        self.ticketWebView.scrollView.minimumZoomScale = 0.0
                        //                        self.ticketWebView.scrollView.maximumZoomScale = 4.0
                    }
                }
                
                if let catGroupArry = responseDisctionary!.valueForKey("categoryTicketGroup") as? NSDictionary  {
                    
                    var newSectionAS : String = ""
                    var newSectionOrrowAS : String = ""
                    
                    // *** save ticket Section *** //
                    if let ticketSection = catGroupArry.valueForKey("section") as? String {
                        newSectionAS = ticketSection
                    }
                    
                    if let ticketRow = catGroupArry.valueForKey("row") as? String {
                        if ticketRow == ""{
                            newSectionOrrowAS = newSectionAS
                        }else{
                            newSectionOrrowAS = newSectionAS +   "\(" | ")" + "\("Row ")"  + ticketRow
                        }
                        self.sectionLbl.text = newSectionOrrowAS
                    }
                    
                    if let ticketPrice = catGroupArry.valueForKey("price") as? Int {
                        
                        self.price = ticketPrice
                        self.priceLbl.text = "US $\(ticketPrice)"
                    }
                    
                    if let sectDescr = catGroupArry.valueForKey("sectionDescription") as? String {
                        var descriptionIS = ""
                        if sectDescr == ""{
                            descriptionIS = "No description available"
                        }else{
                            descriptionIS = sectDescr
                        }
                        self.descriptionLabel.text = descriptionIS
                    }
                    
                    if let qty = catGroupArry.valueForKey("quantity") as? NSInteger {
                        self.qty = qty
                        var qtyStr = ""
                        if qty == 1{
                            qtyStr = "\(qty) ticket"
                        }
                        else{
                            qtyStr = "\(qty) tickets seated together"
                        }
                        self.qtyLbl.text = qtyStr
                    }
                    
                    if let delevieryMethod = catGroupArry.objectForKey("shippingMethod") as? String {
                        self.delieveryMethods_lbl.text = delevieryMethod
                    }
                }
            } else {
                self.stopIndicator()
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    override func viewWillAppear(animated: Bool) {
        checkoutFlag = ""
      
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderSummaryViewController.poptoEventDetail), name:"TimerExpires", object: nil)
            
        }
    override func viewWillDisappear(animated: Bool) {
         NSNotificationCenter.defaultCenter().removeObserver(self, name: "TimerExpires", object: nil)
    }
}
