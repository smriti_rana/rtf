//
//  LocationTabViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 06/05/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import Alamofire
import MapKit
import CoreLocation

class LocationTabViewController: BaseViewController , CLLocationManagerDelegate, MKMapViewDelegate , locationNamesTableViewCellDelegate {
    
    //MARk:- Properties
    //MARK:
    
    @IBOutlet var mapView: MKMapView!
    
    @IBOutlet var milesLabel: UILabel!
    
    @IBOutlet var locationNameTextField: UITextField!
    
    @IBOutlet var allLocationsLabel: UILabel!
    
    @IBOutlet var allLocationsSelectionImageButton: UIButton!
    
    @IBOutlet var currentlocationLabel: UILabel!
    
    @IBOutlet var currentLocationSelectionImageButton: UIButton!
    
    @IBOutlet var searchBarTextField: UITextField!
    
    @IBOutlet var locationTableView: UITableView!
    
    @IBOutlet var baseTableView: UITableView!
    
    @IBOutlet var baseTaableViewHeightConstraint: NSLayoutConstraint!
    
    var suggestionsArray : NSMutableArray = []
    
    var locationNames : NSMutableArray = []
    
    var locationLatLongValues : NSMutableArray = []
    
    var savedLocationParamters : NSMutableArray = []
    
    var locationManager: CLLocationManager!
    
    var indexPath: NSIndexPath!
    
    var locationSetFlag : Bool = true
    
    var index : Int = 0
    
    var locationsFlag : Bool = true
    
    //MARK:
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // *** register table view cell ***
        baseTableView.registerNib(UINib(nibName: "LocationNamesTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")
        
        // *** table view changes *** //
        self.baseTableView.estimatedRowHeight = 50
        self.baseTableView.rowHeight = UITableViewAutomaticDimension
        
        // *** locations table view changes *** //
        self.locationTableView.hidden = true
        self.locationTableView.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.locationTableView.layer.borderWidth = 1.0
        self.locationTableView.layer.cornerRadius = 2.0
        
        // *** location lat long values stored *** //
        
        if NSUserDefaults.standardUserDefaults().objectForKey("locationLatLongValues") != nil {
            
            if let storedArray = NSUserDefaults.standardUserDefaults().objectForKey("locationLatLongValues") as? NSMutableArray {
                self.locationLatLongValues = storedArray.mutableCopy() as! NSMutableArray
            }
        }
        
        // *** set location names in the below array *** //
        
        if NSUserDefaults.standardUserDefaults().objectForKey("locationNames") != nil {
            
            if let storedArray = NSUserDefaults.standardUserDefaults().objectForKey("locationNames") as? NSMutableArray {
                self.locationNames = storedArray.mutableCopy() as! NSMutableArray
            }
        }
        
        // *** when app enters background and comes to fore ground *** //
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LocationTabViewController.appWillEnterForeground(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
        if self.savedLocationParamters.count != 0 {
            
            if self.savedLocationParamters.objectAtIndex(0) as? String == "" ||  self.savedLocationParamters.objectAtIndex(1) as? String == ""{
                self.locationsFlag = false
                self.baseTableView.reloadData()
                self.currentlocationLabel.textColor = UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
                self.allLocationsLabel.textColor = UIColor(colorLiteralRed: 240.0/255.0, green: 104.0/255.0, blue: 18.0/255.0, alpha: 1.0)
                self.currentLocationSelectionImageButton.setImage(nil, forState: .Normal)
                self.allLocationsSelectionImageButton.setImage(UIImage(named: "locationArrow"), forState: .Normal)
            } else {
                let lat : Double = (savedLocationParamters.objectAtIndex(1) as? Double)!
                let long : Double = (savedLocationParamters.objectAtIndex(2) as? Double)!
                self.searchBarTextField.text = (savedLocationParamters.objectAtIndex(4) as? String)!
                
                locationSetValues(lat , long: long)
            }
        } else {
            
            let center = CLLocationCoordinate2D(latitude: mapView.centerCoordinate.latitude , longitude: mapView.centerCoordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.0, longitudeDelta: 0.0))
            self.mapView.setRegion(region, animated: true)
            self.locationsFlag = false
            self.baseTableView.reloadData()
            self.currentlocationLabel.textColor = UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
            self.allLocationsLabel.textColor = UIColor(colorLiteralRed: 240.0/255.0, green: 104.0/255.0, blue: 18.0/255.0, alpha: 1.0)
            self.currentLocationSelectionImageButton.setImage(nil, forState: .Normal)
            self.allLocationsSelectionImageButton.setImage(UIImage(named: "locationArrow"), forState: .Normal)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        baseTaableViewHeightConstraint.constant = self.baseTableView.contentSize.height
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func appWillEnterForeground(notification: NSNotification) {
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
        } else {
            
            let alert:UIAlertController=UIAlertController(title: "Turn on your location services", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
                self.locationSetValues(0.0, long: 0.0)
            }
            let setttingsAction = UIAlertAction(title: "Setttings", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
            }
            // Add the actions
            alert.addAction(setttingsAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        //
        //        locationManager = CLLocationManager()
        //        locationManager.requestAlwaysAuthorization()
        //        locationManager.delegate = self
        //        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //        locationManager.distanceFilter = 10
    }
    
    //MARK:- Location manager delegates
    //MARK:
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
            
        case .Authorized, .AuthorizedWhenInUse:
            locationManager.startUpdatingLocation()
        case .NotDetermined, .Denied:
            
            let alert:UIAlertController=UIAlertController(title: "Please allow RewardTheFan to access location service", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
                self.locationSetValues(0.0, long: 0.0)
            }
            let setttingsAction = UIAlertAction(title: "Setttings", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
            }
            // Add the actions
            alert.addAction(setttingsAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        //            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
        default: break
            
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        self.locationsFlag = false
        self.baseTableView.reloadData()
        self.currentlocationLabel.textColor = UIColor(colorLiteralRed: 240.0/255.0, green: 104.0/255.0, blue: 18.0/255.0, alpha: 1.0)
        self.allLocationsLabel.textColor = UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        self.currentLocationSelectionImageButton.setImage(UIImage(named: "locationArrow"), forState: .Normal)
        self.allLocationsSelectionImageButton.setImage(nil, forState: .Normal)
        
        
        let location = locations.last! as CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        let miles: Double!
        miles = 100.0
        let scalingFactor: Double = abs((cos(2 * M_PI * center.latitude / 360.0)))
        
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: miles / 69.0, longitudeDelta: miles / (scalingFactor * 69.0)))
        
        let objectAnnotation = MKPointAnnotation()
        objectAnnotation.coordinate = center
        
        mapView.addAnnotation(objectAnnotation)
        mapView.setRegion(region, animated: true)
        
        // *** Get location name and address *** //
        
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            // Address dictionary
            
            if !(placeMark == nil) {
                
                if !(placeMark.addressDictionary == nil) {
                    
                    var cityName : String = ""
                    var stateName : String = ""
                    var countryCode : String = ""
                    
                    // City
                    if let city = placeMark.addressDictionary!["City"] as? NSString {
                        cityName = city as String
                    }
                    
                    // state name
                    if let state = placeMark.addressDictionary!["State"] as? NSString {
                        stateName = state as String
                    }
                    
                    // Country
                    if let country = placeMark.addressDictionary!["CountryCode"] as? NSString {
                        countryCode = country as String
                    }
                    self.searchBarTextField.text = "\(cityName), \(stateName), \(countryCode)"
                }
            } else {
                print("*********************** response nil **********************************")
            }
        })
        locationManager.stopUpdatingLocation()
    }
    
    //MARK:- Search bar values changed
    
    @IBAction func searchBarValueChanged(sender: AnyObject) {
        
        let textField = sender as! UITextField
        
        let baseUrl: String = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(textField.text!)&types=(cities)&key=AIzaSyBa-LT2cQTQwg-xo5OFl7RQ5z0OxOo38as"
        
        let urlString :String = baseUrl.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        request(Method.GET, urlString, parameters: [:], encoding: ParameterEncoding.URL, headers: nil)
            .responseJSON { (response) in
                
                //print(response.result.value)
                if response.result.value != nil {
                    
                    if response.result.isSuccess {
                        
                        if let jsonResponseGroups = response.result.value as? NSDictionary {
                            
                            self.suggestionsArray = []
                            print("response",jsonResponseGroups)
                            
                            if let res = jsonResponseGroups.objectForKey("predictions") as? NSArray {
                                for i in 0..<res.count {
                                    if let place = res[i].objectForKey("description") as? String {
                                        self.locationTableView.hidden = false
                                        self.suggestionsArray.addObject(place)
                                    }
                                }
                                self.locationTableView.reloadData()
                            }
                            
                            var frame = self.locationTableView.frame
                            frame.size.height = self.locationTableView.contentSize.height
                            self.locationTableView.frame = frame
                        }
                    }
                }
        }
    }
    
    //MARK:- Table view delegate and datasource methods
    //MARK:-
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == locationTableView {
            return suggestionsArray.count
        } else {
            return locationNames.count
        }
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        if tableView == locationTableView {
            return true
        } else {
            return false
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView == locationTableView {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("CELL") as UITableViewCell!
            if (cell == nil) {
                cell = UITableViewCell(style:.Default, reuseIdentifier: "CELL")
            }
            
            cell.textLabel?.text = suggestionsArray.objectAtIndex(indexPath.row) as? String
            
            // *** font size for iPad ***
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                cell.textLabel!.font = UIFont.systemFontOfSize(24)
            } else {
                cell.textLabel!.font = UIFont.systemFontOfSize(14)
            }
            cell.textLabel?.lineBreakMode = .ByCharWrapping
            cell.textLabel?.textColor = UIColor(colorLiteralRed: 86.0/255.0, green: 87.0/255.0, blue: 88.0/255.0, alpha: 1.0)
            
            return cell
            
        } else {
            
            
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
                as! LocationNamesTableViewCell
            cell.locationNameLabel.text = locationNames.objectAtIndex(indexPath.row) as? String
            
            if locationsFlag {
                if indexPath.row == index {
                    cell.locationNameLabel.textColor = UIColor(colorLiteralRed: 240.0/255.0, green: 104.0/255.0, blue: 18.0/255.0, alpha: 1.0)
                    cell.selectionButton.setImage(UIImage(named: "locationArrow"), forState: .Normal)
                } else {
                    cell.locationNameLabel.textColor = UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
                    cell.selectionButton.setImage(nil, forState: .Normal)
                }
            } else {
                cell.locationNameLabel.textColor = UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
                cell.selectionButton.setImage(nil, forState: .Normal)
            }
            
            cell.cellButton.tag = indexPath.row
            cell.delegate = self
            baseTaableViewHeightConstraint.constant = self.baseTableView.contentSize.height
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // *** resign keyboard *** //
        self.searchBarTextField.resignFirstResponder()
        
        // *** select tableview *** //
        if tableView == locationTableView {
            
            // *** url to get address from place name *** //
            let baseUrl: String = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(self.suggestionsArray.objectAtIndex(indexPath.row))"
            
            // *** creating url *** //
            let urlString :String = baseUrl.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            // *** set indicator *** //
            self.setLoadingIndicator(self.view)
            
            request(Method.GET, urlString, parameters: [:], encoding: ParameterEncoding.URL, headers: nil)
                .responseJSON { (response) in
                    
                    // *** stop indicator *** //
                    self.stopIndicator()
                    
                    if response.result.value != nil {
                        
                        if response.result.isSuccess {
                            
                            if let jsonResponseGroups = response.result.value as? NSDictionary {
                                
                                // *** clear suggestion sarray *** //
                                self.suggestionsArray.removeAllObjects()
                                
                                if let results = jsonResponseGroups.objectForKey("results") as? NSArray {
                                    
                                    if let geometry = results[0].objectForKey("geometry") as? NSDictionary {
                                        
                                        if let formattedAddress = results[0].objectForKey("formatted_address") as? String {
                                            self.searchBarTextField.text = formattedAddress
                                        }
                                        
                                        if let location = geometry.objectForKey("location") as? NSDictionary {
                                            
                                            var lat : Double = 0.0
                                            var long : Double = 0.0
                                            
                                            if let latitude = location.objectForKey("lat") as? Double {
                                                lat = latitude
                                            }
                                            if let longitude = location.objectForKey("lng") as? Double {
                                                long = longitude
                                            }
                                            self.locationSetValues(lat, long: long)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // *** hide locations textfield *** //
                    self.locationTableView.hidden = true
            }
        }
    }
    
    
    //MARK:- Set map to search location
    //MARk:
    
    /**
     Another complicated function.
     
     - Parameter lat: The latitude of location.
     - Parameter long: The longitude of location.
     - Returns: nill.
     
     //     - Remark:
     //     There's a counterpart function that concatenates the first and last name into a full name.
     //
     //     - SeeAlso:  `createFullName(_:lastname:)`
     
     - Precondition: `cell` should not be nil.
     - Requires: both paramters *in double*.
     
     //     - Todo: Support middle name in the next version.
     
     - Warning: A wonderful **crash** will be the result of a `nil` argument.
     
     - Version: 1.0
     
     - Author: lakshmi kanth
     //
     //     - Note: Too much documentation for such a small function.
     */
    
    func locationSetValues(lat : Double, long : Double) {
        
        let center = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let miles: Double!
        miles = 100.0
        let scalingFactor: Double = abs((cos(2 * M_PI * center.latitude / 360.0)))
        
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: miles / 69.0, longitudeDelta: miles / (scalingFactor * 69.0)
            
            ))
        
        if mapView.annotations.count != 0 {
            let annotations : [MKAnnotation] = mapView.annotations
            mapView.removeAnnotations(annotations)
        }
        
        let objectAnnotation = MKPointAnnotation()
        objectAnnotation.coordinate = center
        mapView.addAnnotation(objectAnnotation)
        mapView.setRegion(region, animated: false)
    }
    
    //MARK:- Map View delagetes
    //MARK:
    
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        if locationSetFlag {
            
            // get center coordinate
            let centerCoor = mapView.centerCoordinate
            let centerLocation = CLLocation(latitude: centerCoor.latitude, longitude: centerCoor.longitude)
            
            // get top left coordinate
            let topLeftCoor = mapView.convertPoint(CGPointMake(0,0), toCoordinateFromView: mapView)
            let topLeftLocation =  CLLocation(latitude: topLeftCoor.latitude, longitude: topLeftCoor.longitude)
            
            // get top right coordinate
            let topRightCoor = mapView.convertPoint(CGPointMake(mapView.frame.size.width,0), toCoordinateFromView: mapView)
            let topRightLocation = CLLocation(latitude: topRightCoor.latitude, longitude: topRightCoor.longitude)
            
            // the distance from center to top left
            let hypotenuse:CLLocationDistance = centerLocation.distanceFromLocation(topLeftLocation)
            
            // half of the distance from top left to top right
            let x:CLLocationDistance = topLeftLocation.distanceFromLocation(topRightLocation)/2.0
            
            // what we want is this
            let y:CLLocationDistance = sqrt(pow(hypotenuse, 2.0) - pow(x, 2.0)) //meter
            
            var miles : Double = 0.0
            
            miles = y/1609.34
            let num = miles
            let formatter = NSNumberFormatter()
            formatter.maximumFractionDigits = 0
            formatter.minimumFractionDigits = 0
            let str = formatter.stringFromNumber(num)
            
            // *** set number of miles *** //
            milesLabel.text = str
            
            if let radius : Double = y/1609.34{
                if (radius > 50.00)
                {
                    let miles: Double!
                    miles = 100.0
                    let scalingFactor: Double = abs((cos(2 * M_PI * mapView.centerCoordinate.latitude / 360.0)))
                    var region : MKCoordinateRegion =  mapView.region
                    region.center = mapView.centerCoordinate
                    region.span.latitudeDelta = miles / 69.0
                    region.span.longitudeDelta = miles / (scalingFactor * 69.0)
                    mapView.setRegion(region, animated: true)
                }
            }
        } else {
            let center = CLLocationCoordinate2D(latitude: mapView.centerCoordinate.latitude , longitude: mapView.centerCoordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.0, longitudeDelta: 0.0))
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    // *** add custom annotation to the map *** //
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        var pinView: MKAnnotationView? = nil
        let defaultPinID: String = "com.invasivecode.pin"
        pinView = (mapView.dequeueReusableAnnotationViewWithIdentifier(defaultPinID))
        if pinView == nil {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: defaultPinID)
        }
        pinView!.canShowCallout = true
        pinView!.image = UIImage(named: "locationPin")
        
        return pinView!
    }
    
    
    //MARK:- Button action
    //MARK:
    
    func cellButtonTapped(button: UIButton, index: NSInteger) {
        
        self.locationsFlag = true
        self.currentlocationLabel.textColor = UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        self.allLocationsLabel.textColor = UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        self.currentLocationSelectionImageButton.setImage(nil, forState: .Normal)
        self.allLocationsSelectionImageButton.setImage(nil, forState: .Normal)
        
        var latlongValues : NSMutableArray = []
        latlongValues = self.locationLatLongValues.objectAtIndex(index) as! NSMutableArray
        let lat : Double = (latlongValues.objectAtIndex(0) as? Double)!
        let long : Double = (latlongValues.objectAtIndex(1) as? Double)!
        locationSetValues(lat, long: long)
        self.searchBarTextField.text = self.locationNames.objectAtIndex(index) as? String
        self.index = index
        self.baseTableView.reloadData()
        self.baseTaableViewHeightConstraint.constant = self.baseTableView.contentSize.height
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // *** done bubutton clicked *** //
    @IBAction func doneButtonTapped(sender: AnyObject) {
        
        if self.searchBarTextField.text == "" {
            
            self.locationsFlag = false
            self.baseTableView.reloadData()
            self.currentlocationLabel.textColor = UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
            self.allLocationsLabel.textColor = UIColor(colorLiteralRed: 240.0/255.0, green: 104.0/255.0, blue: 18.0/255.0, alpha: 1.0)
            self.currentLocationSelectionImageButton.setImage(nil, forState: .Normal)
            self.allLocationsSelectionImageButton.setImage(UIImage(named: "locationArrow"), forState: .Normal)
            
            locationSetValues(0.0, long: 0.0)
            NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(LocationTabViewController.pushToNext), userInfo: nil, repeats: false)
            let locationParamters : NSMutableArray = ["", "", "", "", "Location"]
            NSNotificationCenter.defaultCenter().postNotificationName("locationParamters", object: locationParamters)
        } else {
            
            // ** start activityIndicator
            self.setLoadingIndicator(self.view)
            
            var zipCode : String = ""
            var miles : Double = 0.0
            var latitude : Double = 0.0
            var longitude : Double = 0.0
            
            // *** get miles *** //
            
            // get center coordinate
            let centerCoor = mapView.centerCoordinate
            let centerLocation = CLLocation(latitude: centerCoor.latitude, longitude: centerCoor.longitude)
            
            // get top left coordinate
            let topLeftCoor = mapView.convertPoint(CGPointMake(0,0), toCoordinateFromView: mapView)
            let topLeftLocation =  CLLocation(latitude: topLeftCoor.latitude, longitude: topLeftCoor.longitude)
            
            // get top right coordinate
            let topRightCoor = mapView.convertPoint(CGPointMake(mapView.frame.size.width,0), toCoordinateFromView: mapView)
            let topRightLocation = CLLocation(latitude: topRightCoor.latitude, longitude: topRightCoor.longitude)
            
            // the distance from center to top left
            let hypotenuse:CLLocationDistance = centerLocation.distanceFromLocation(topLeftLocation)
            
            // half of the distance from top left to top right
            let x:CLLocationDistance = topLeftLocation.distanceFromLocation(topRightLocation)/2.0
            
            // what we want is this
            let y:CLLocationDistance = sqrt(pow(hypotenuse, 2.0) - pow(x, 2.0)) //meter
            
            miles = y/1609.34
            
            // *** get lat long names *** //
            
            let touchCoordinate = mapView.convertPoint(mapView.center, toCoordinateFromView: mapView)
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: touchCoordinate.latitude, longitude: touchCoordinate.longitude)
            latitude = touchCoordinate.latitude
            longitude = touchCoordinate.longitude
            geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
                
                // * stop activityIndicator
                self.stopIndicator()
                
                // Place details
                var placeMark: CLPlacemark!
                placeMark = placemarks?[0]
                
                // Address dictionary
                if !(placeMark == nil) {
                    
                    // ** start activityIndicator
                    self.setLoadingIndicator(self.view)
                    
                    if !(placeMark.addressDictionary == nil) {
                        
                        // Zip code
                        if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                            
                            // * stop activityIndicator
                            self.stopIndicator()
                            
                            var cityName : String = ""
                            var stateName : String = ""
                            var countryCode : String = ""
                            
                            // City
                            if let city = placeMark.addressDictionary!["City"] as? NSString {
                                cityName = city as String
                            }
                            
                            // state name
                            if let state = placeMark.addressDictionary!["State"] as? NSString {
                                stateName = state as String
                            }
                            
                            // Country
                            if let country = placeMark.addressDictionary!["CountryCode"] as? NSString {
                                countryCode = country as String
                            }
                            
                            self.searchBarTextField.text = "\(cityName) \(stateName) \(countryCode)"
                            
                            zipCode = zip as String
                            latitude = self.mapView.centerCoordinate.latitude
                            longitude = self.mapView.centerCoordinate.longitude
                            let locationParamters : NSMutableArray = [zipCode, latitude, longitude, miles, self.searchBarTextField.text!]
                            NSNotificationCenter.defaultCenter().postNotificationName("locationParamters", object: locationParamters)
                            
                            NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(LocationTabViewController.pushToNext), userInfo: nil, repeats: false)
                            
                        } else {
                            
                            // * stop activityIndicator
                            self.stopIndicator()
                            
                            let text = self.searchBarTextField.text!
                            let locationParamters : NSMutableArray = [zipCode, latitude, longitude, miles, text]
                            NSNotificationCenter.defaultCenter().postNotificationName("locationParamters", object: locationParamters)
                            NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(LocationTabViewController.pushToNext), userInfo: nil, repeats: false)
                        }
                    } else {
                        
                        // * stop activityIndicator
                        self.stopIndicator()
                        let text = self.searchBarTextField.text!
                        let locationParamters : NSMutableArray = [zipCode, latitude, longitude, miles, text]
                        NSNotificationCenter.defaultCenter().postNotificationName("locationParamters", object: locationParamters)
                        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(LocationTabViewController.pushToNext), userInfo: nil, repeats: false)
                    }
                } else {
                    
                    // * stop activityIndicator
                    self.stopIndicator()
                    let text = self.searchBarTextField.text!
                    let locationParamters : NSMutableArray = [zipCode, latitude, longitude, miles, text]
                    NSNotificationCenter.defaultCenter().postNotificationName("locationParamters", object: locationParamters)
                    NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(LocationTabViewController.pushToNext), userInfo: nil, repeats: false)
                }
                
                self.locationNames.insertObject(self.searchBarTextField.text! , atIndex: 0)
                
                self.baseTableView.reloadData()
                self.baseTaableViewHeightConstraint.constant = self.baseTableView.contentSize.height
                
                NSUserDefaults.standardUserDefaults().setObject(self.locationNames, forKey: "locationNames")
                
                let latlongValues : NSMutableArray = [latitude, longitude]
                
                self.locationLatLongValues.insertObject(latlongValues, atIndex: 0)
                
                NSUserDefaults.standardUserDefaults().setObject(self.locationLatLongValues, forKey: "locationLatLongValues")
            })
        }
    }
    
    func pushToNext() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func allLocationsButtonTapped(sender: AnyObject) {
        
        self.locationsFlag = false
        self.baseTableView.reloadData()
        self.currentlocationLabel.textColor = UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        self.allLocationsLabel.textColor = UIColor(colorLiteralRed: 240.0/255.0, green: 104.0/255.0, blue: 18.0/255.0, alpha: 1.0)
        self.currentLocationSelectionImageButton.setImage(nil, forState: .Normal)
        self.allLocationsSelectionImageButton.setImage(UIImage(named: "locationArrow"), forState: .Normal)
        
        locationSetValues(0.0, long: 0.0)
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(LocationTabViewController.pushToNext), userInfo: nil, repeats: false)
        let locationParamters : NSMutableArray = ["", "", "", "", "Location"]
        NSNotificationCenter.defaultCenter().postNotificationName("locationParamters", object: locationParamters)
    }
    
    @IBAction func currentLocationButtonTapped(sender: AnyObject) {
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
        } else {
            
            let alert:UIAlertController=UIAlertController(title: "Turn on your location services", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
                self.locationSetValues(0.0, long: 0.0)
            }
            let setttingsAction = UIAlertAction(title: "Setttings", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
            }
            // Add the actions
            alert.addAction(setttingsAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}
