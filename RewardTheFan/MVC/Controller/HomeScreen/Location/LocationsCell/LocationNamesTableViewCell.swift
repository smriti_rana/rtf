//
//  LocationNamesTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 18/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

//MARK:- Custom delagete
//MARK:

@objc protocol locationNamesTableViewCellDelegate {
    
    optional func cellButtonTapped(button: UIButton, index : NSInteger)
}

class LocationNamesTableViewCell: UITableViewCell {

    weak var delegate: locationNamesTableViewCellDelegate?
    
    @IBOutlet var locationNameLabel: UILabel!
    @IBOutlet var selectionButton: UIButton!
    @IBOutlet var cellButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- ButtonActions
    //MARK:
    
    // *** cell Button clicked *** //
    @IBAction func cellButtonClicked(sender: AnyObject) {
        self.delegate?.cellButtonTapped!(sender as! UIButton, index: sender.tag)
    }
    
    
}
