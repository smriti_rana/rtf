//
//  LocationTableHeaderTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 28/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

//MARK:- Custom delagete 
//MARK:

@objc protocol locationTableHeaderTableViewCellDelegate {
    
    optional func allLocationButtonTapped(button: UIButton, index : NSInteger)
    
    optional func currentLocationButtonTapped(button: UIButton, index : NSInteger)
}

//MARK:

class LocationTableHeaderTableViewCell: UITableViewCell, CLLocationManagerDelegate, MKMapViewDelegate {

    //MARK:- Properties
    //MARK:
    
    weak var delegate: locationTableHeaderTableViewCellDelegate?
    
    @IBOutlet var locationNameTextField: UITextField!
    
    @IBOutlet var milesLabel: UILabel!
    
    @IBOutlet var mapView: MKMapView!
    
    @IBOutlet var textFieldBorder: UIView!
    
    var latitude : Double = 0.0
    
    var longitude : Double = 0.0
    
    var locationManager: CLLocationManager!
    
    @IBOutlet var allLocationButton: UIButton!
    
    @IBOutlet var currentLocationButton: UIButton!
    
    //MARK:
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        
        // *** giving border to the text *** //
        textFieldBorder.layer.borderWidth = 1.0
        textFieldBorder.layer.borderColor = UIColor.lightGrayColor().CGColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
 
    //MARK:- ButtonActions
    //MARK:

    // *** All locations clicked *** //
    @IBAction func allLocationsButtonTapped(sender: AnyObject) {
        self.delegate?.allLocationButtonTapped!(sender as! UIButton, index: sender.tag)
    }
    
    // *** current location tapped *** //
    @IBAction func currentlocationButtonTapped(sender: AnyObject) {
        self.delegate?.currentLocationButtonTapped!(sender as! UIButton, index: sender.tag)
    }
    
}
