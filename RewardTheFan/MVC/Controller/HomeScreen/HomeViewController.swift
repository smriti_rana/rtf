//
//
//  HomeViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 20/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import Kingfisher
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import Social
import MessageUI

class HomeViewController: BaseViewController, HomeEventsTableViewCellDelegate, HomeEventsCardTableViewCellDelegate, staticCardDelegate, customActionsheetDelegate,MFMailComposeViewControllerDelegate {
    
    
    //MARK:- Properties
    //MARK:
    
    @IBOutlet var grandChildCategoryCollectionView: UICollectionView!
    
    @IBOutlet var baseTableView: UITableView!
    
    @IBOutlet var searchBarView: UIView!
    
    @IBOutlet var searchBarTextField: UITextField!
    
    @IBOutlet var priceLabel: UILabel!
    
    @IBOutlet var locationLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    var leftSwipeFlag : Bool = false
    
    var rightSwipeFlag : Bool = true
    
    var cards : NSMutableArray = []
    
    var cardsWithEventsDetail : NSMutableArray = []
    
    var mySubview:CustomSheetView!
    
    let navigationVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Navigation") as! NavigationViewController
    
    var latitude : String = ""
    
    var longitude : String = ""
    
    var searchMile : String = ""
    
    var orginZipCode : String = ""
    
    var startDate : String = ""
    
    var endDate : String = ""
    
    var customerLoyalityRewardsPointArray : NSDictionary = [:]
    
    var shareLinkUrl : String = ""
    
    var grandChildCategory : NSMutableArray = []
    var grandChildIdArray : NSMutableArray = []
    
    
    var colorsArray : NSMutableArray = []
    
    var saveLocationParamters : NSMutableArray = []
    
    var pageCount : Int = 1
    
    @IBOutlet var baseView: UIView!
    
    @IBOutlet var dataView: UIView!
    
    var refreshControl: UIRefreshControl!
    
    
    var countryArray :NSMutableArray = []
    var stateArray :NSMutableArray = []
    
    //MARK:
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // *** swipe gestures *** //
        
        UIView.hr_setToastThemeColor(color: ThemeColor)
        UIView.hr_setToastFontColor(color: fontColor)
        
        
        let swipeLeft: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(HomeViewController.handleSwipe(_:)))
        let swipeRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(HomeViewController.handleSwipe(_:)))
        
        // Setting the swipe direction.
        swipeLeft.direction = .Left
        swipeRight.direction = .Right
        
        if NSUserDefaults.standardUserDefaults().objectForKey("datesLabel") != nil {
            dateLabel.text = NSUserDefaults.standardUserDefaults().objectForKey("datesLabel") as? String
        }
        
        // Adding the swipe gesture on view
        self.view.addGestureRecognizer(swipeLeft)
        self.view.addGestureRecognizer(swipeRight)
        
        // *** registering collection view cell *** //
        
        self.grandChildCategoryCollectionView!.registerNib(UINib(nibName: "HomeBannersCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "cell")
        
        // *** register table view cell ***
        
        baseTableView.registerNib(UINib(nibName: "HomeEventsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell")
        baseTableView.registerNib(UINib(nibName: "FirstTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell1")
        baseTableView.registerNib(UINib(nibName: "HomeEventsCardTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "Cell2")
        
        baseTableView.estimatedRowHeight = 200
        baseTableView.rowHeight = UITableViewAutomaticDimension
        
        mySubview = NSBundle.mainBundle().loadNibNamed("CustomSheetView", owner: self, options: nil).first as? CustomSheetView
        mySubview.delegate = self
        
        
        if NSUserDefaults.standardUserDefaults().objectForKey("latitude") != nil {
            latitude = NSUserDefaults.standardUserDefaults().objectForKey("latitude") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("longitude") != nil {
            longitude = NSUserDefaults.standardUserDefaults().objectForKey("longitude") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("searchMile") != nil {
            searchMile = NSUserDefaults.standardUserDefaults().objectForKey("latitude") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("orginZipCode") != nil {
            orginZipCode = NSUserDefaults.standardUserDefaults().objectForKey("orginZipCode") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("startDate") != nil {
            startDate = NSUserDefaults.standardUserDefaults().objectForKey("startDate") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("endDate") != nil {
            endDate = NSUserDefaults.standardUserDefaults().objectForKey("endDate") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("locationLabel") != nil {
            locationLabel.text = NSUserDefaults.standardUserDefaults().objectForKey("locationLabel") as? String
        } else {
            locationLabel.text = "Location"
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") != nil {
            customerLoyalityRewardsPointArray = NSUserDefaults.standardUserDefaults().objectForKey("customerLoyalty") as! NSDictionary
            print(customerLoyalityRewardsPointArray)
            if let activeDollars =  customerLoyalityRewardsPointArray.valueForKey("activeRewardDollers") as? Int {
                var activeDollarsIs = ""
                if activeDollars == 0{
                    activeDollarsIs = "$0"
                    
                }else{
                    activeDollarsIs = "$\(activeDollars)"
                }
                
                priceLabel.text = activeDollarsIs
            }
        }
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.setLocationParamters(_:)), name:"locationParamters", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.dateSearchParamaters(_:)), name:"dateSearch", object: nil)
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(HomeViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        baseTableView.addSubview(refreshControl)
        
        setLoadingIndicator(self.view)
        pageCount = 1
        homeCardsApiCall(pageCount)
        grandChildCategoryApiCall()
        
    }
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.refreshControl.beginRefreshing()
        self.baseTableView.userInteractionEnabled = false
        pageCount = 1
        homeCardsApiCall(pageCount)
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching results")
    }
    
    
    // *** grand child category API call *** //
    
    func grandChildCategoryApiCall() {
        
        let colors : NSMutableArray = [UIColor(colorLiteralRed: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0), UIColor(colorLiteralRed: 240.0/255.0, green: 104.0/255.0, blue: 18.0/255.0, alpha: 1.0), UIColor(colorLiteralRed: 255.0/255.0, green: 20.0/255.0, blue: 147.0/255.0, alpha: 1.0), UIColor(colorLiteralRed: 0.0/255.0, green: 191.0/255.0, blue: 255.0/255.0, alpha: 1.0)]
        
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** api call for grand child categories cards ***
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            // * stop activityIndicator
            self.stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        //        self.setLoadingIndicator(self.view)
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            ]
        
        WebServiceCall.sharedInstance.LogIn("GetGrandChildCategory.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            //            // * stop activityIndicator
            //            self.stopIndicator()
            
            if isTrue == true {
                
                //                if self.cards.count == 0 {
                //                    self.setLoadingIndicator(self.view)
                //                }
                
                //                print("Response",responseDisctionary!)
                
                if let childCategories =  responseDisctionary!.valueForKey("childCategories") as? NSMutableArray {
                    print(childCategories)
                    self.colorsArray.removeAllObjects()
                    
                    for i in 0..<childCategories.count {
                        self.grandChildCategory.addObject(childCategories[i].objectForKey("name") as! String)
                        self.grandChildIdArray.addObject(childCategories[i].objectForKey("id") as! Int)
                    }
                    
                    for _ in 0..<self.grandChildCategory.count {
                        self.colorsArray.addObjectsFromArray(colors as [AnyObject])
                    }
                    
                    self.grandChildCategoryCollectionView.reloadData()
                }
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
                
            }
            
        })
    }
    
    
    
    
    //Mark:-PaginationTableView
    var pagingSpinner = UIActivityIndicatorView()
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if scrollView == baseTableView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
                pagingSpinner.startAnimating()
                pagingSpinner.color = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
                pagingSpinner.hidesWhenStopped = true
                
                baseTableView.tableFooterView = pagingSpinner
                pageCount += 1
                homeCardsApiCall(pageCount)
                
            }
        }
    }
    
    
    
    // *** home cards API call *** //
    
    func homeCardsApiCall( pageCount : Int ) {
        
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        var customerId : String = ""
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = "\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)"
        }
        
        // *** api call for Home cards ***
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            // * stop activityIndicator
            self.stopIndicator()
            self.pagingSpinner.stopAnimating()
            refreshControl.endRefreshing()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        
        //        if !(refreshControl.refreshing) {
        //            self.setLoadingIndicator(self.view)
        //        }
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            "cardType" : "HOME_CARD",
            "deviceId" : deviceId,
            "latitude" : latitude,
            "longitude" : longitude,
            "searchMile" : searchMile,
            "orginZipCode" : orginZipCode,
            "startDate" : startDate,
            "endDate" : endDate,
            "customerId" : customerId,
            "pageNumber" : (pageCount)
        ]
        
        // * start date
        NSUserDefaults.standardUserDefaults().setObject(startDate, forKey: "startDate")
        
        // * end date
        NSUserDefaults.standardUserDefaults().setObject(endDate, forKey: "endDate")
        
        // * origin zip code
        NSUserDefaults.standardUserDefaults().setObject(orginZipCode, forKey: "orginZipCode")
        
        // * search mile
        NSUserDefaults.standardUserDefaults().setObject(searchMile, forKey: "searchMile")
        
        // * latitude
        NSUserDefaults.standardUserDefaults().setObject(latitude, forKey: "latitude")
        
        // * longitude
        NSUserDefaults.standardUserDefaults().setObject(longitude, forKey: "longitude")
        
        // **** call webservice
        
        WebServiceCall.sharedInstance.getHomeCards("GetHomeCards.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            // * stop activityIndicator
            self.stopIndicator()
            self.refreshControl.endRefreshing()
            self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.baseTableView.userInteractionEnabled = true
            self.pagingSpinner.stopAnimating()
            //            print(responseDisctionary)
            
            // ** check for valid result
            if isTrue == true {
                
                // * get all cards
                if let cards = responseDisctionary?.objectForKey("cards") as? NSMutableArray {
                    
                    let tempData : NSMutableArray = cards.mutableCopy() as! NSMutableArray
                    
                    if pageCount == 1 {
                        
                        self.cards.removeAllObjects()
                        // ** save cards
                        self.cards = cards.mutableCopy() as! NSMutableArray
                        // *** remove previus values *** //
                        self.cardsWithEventsDetail.removeAllObjects()
                        
                    } else {
                        // *** remove previus values *** //
                        self.cardsWithEventsDetail.removeAllObjects()
                        self.cards.addObjectsFromArray(tempData.mutableCopy() as! [AnyObject])
                    }
                    
                    // *** differentiate based on card types
                    
                    for i in (0..<self.cards.count) {
                        
                        // * get card type
                        if let cardType = self.cards[i].objectForKey("cardType") as? String {
                            
                            if cardType == "TEAM_CARD" {
                                
                                // ************ HOME CARD ************ //
                                
                                // *** save card deatils call //
                                self.getEventDetails(i)
                            } else if cardType == "POPULAR_CARD" {
                                
                                // ************ POPULAR CARD ************ //
                                
                                // *** save card deatils call //
                                self.getEventDetails(i)
                            } else if cardType == "REGULAR_CARD" {
                                
                                // ************ REGULAR CARD ************ //
                                
                                // *** save card deatils call //
                                self.getEventDetails(i)
                            } else if cardType == "STATIC_CARD" {
                                
                                // ************ STATIC CARD ************ //
                                
                                // *** save card deatils call //
                                let perCardDetails : NSMutableArray = []
                                
                                if let imageUrl = self.cards[i].objectForKey("cardImage") as? String {
                                    perCardDetails.addObject(imageUrl)
                                } else {
                                    perCardDetails.addObject("")
                                }
                                
                                self.cardsWithEventsDetail.addObject(perCardDetails)
                                
                            } else {
                                self.cardsWithEventsDetail.addObject([])
                            }
                        } else {
                            self.cardsWithEventsDetail.addObject([])
                        }
                    }
                    
                    //                    print(self.cardsWithEventsDetail)
                    
                    self.baseTableView.reloadData()
                    
                } else {
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
    }
    
    func dateSearchParamaters(notification : NSNotification) {
        //        print(notification)
        if let info = notification.valueForKey("object") as? NSArray {
            
            var firstDate : String = ""
            
            var lastDate : String = ""
            
            if info.count != 0 {
                if info.count == 1 {
                    
                    let initialDate = info.objectAtIndex(0) as! NSDate
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yyyy"
                    let date = dateFormatter.stringFromDate(initialDate)
                    
                    let dateFormatteerForDay = NSDateFormatter()
                    dateFormatteerForDay.dateFormat = "dd"
                    let dayFromDate = dateFormatteerForDay.stringFromDate(initialDate)
                    
                    let dateFormatteerForDay1 = NSDateFormatter()
                    dateFormatteerForDay1.dateFormat = "MMM"
                    let dayFromDate1 = dateFormatteerForDay1.stringFromDate(initialDate)
                    
                    firstDate = "\(dayFromDate1) \(dayFromDate)"
                    startDate = date
                    
                    dateLabel.text = "\(firstDate)"
                    pageCount = 1
                    self.setLoadingIndicator(self.view)
                    homeCardsApiCall(pageCount)
                } else if info.count == 2 {
                    let initialDate = info.objectAtIndex(0) as! NSDate
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yyyy"
                    let date = dateFormatter.stringFromDate(initialDate)
                    
                    let dateFormatteerForDay = NSDateFormatter()
                    dateFormatteerForDay.dateFormat = "dd"
                    let dayFromDate = dateFormatteerForDay.stringFromDate(initialDate)
                    
                    let dateFormatteerForDay1 = NSDateFormatter()
                    dateFormatteerForDay1.dateFormat = "MMM"
                    let dayFromDate1 = dateFormatteerForDay1.stringFromDate(initialDate)
                    
                    firstDate = "\(dayFromDate1) \(dayFromDate)"
                    
                    startDate = date
                    
                    
                    let finalDate = info.objectAtIndex(1) as! NSDate
                    let dateFormatter1 = NSDateFormatter()
                    dateFormatter1.dateFormat = "MM/dd/yyyy"
                    let date2 = dateFormatter.stringFromDate(finalDate)
                    
                    let dateFormatteerForDay2 = NSDateFormatter()
                    dateFormatteerForDay2.dateFormat = "dd"
                    let dayFromDate2 = dateFormatteerForDay2.stringFromDate(finalDate)
                    
                    
                    let dateFormatteerForDay3 = NSDateFormatter()
                    dateFormatteerForDay3.dateFormat = "MMM"
                    let dayFromDate3 = dateFormatteerForDay3.stringFromDate(finalDate)
                    
                    lastDate = "\(dayFromDate3) \(dayFromDate2)"
                    
                    endDate = date2
                    
                    dateLabel.text = "\(firstDate) - \(lastDate)"
                    pageCount = 1
                    self.setLoadingIndicator(self.view)
                    homeCardsApiCall(pageCount)
                }
                NSUserDefaults.standardUserDefaults().setObject(dateLabel.text!, forKey: "dateLabel")
            } else {
                startDate = ""
                endDate = ""
                dateLabel.text = "Date"
                
                
                NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "startingRow")
                NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "endingRow")
                NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "startingSection")
                NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "endingSection")
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "selectionFlag")
                NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "selectedDates")
                NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "datesLabel")
                
                pageCount = 1
                self.setLoadingIndicator(self.view)
                homeCardsApiCall(pageCount)
            }
        }
    }
    
    func setLocationParamters(notification : NSNotification) {
        //        print(notification)
        if let info = notification.valueForKey("object") as? NSMutableArray {
            saveLocationParamters = info
            orginZipCode = "\(info.objectAtIndex(0))"
            latitude = "\(info.objectAtIndex(1))"
            longitude = "\(info.objectAtIndex(2))"
            searchMile = "\(info.objectAtIndex(3))"
            locationLabel.text = "\(info.objectAtIndex(4))"
            NSUserDefaults.standardUserDefaults().setObject(locationLabel.text, forKey: "locationLabel")
            pageCount = 1
            self.setLoadingIndicator(self.view)
            homeCardsApiCall(pageCount)
        }
    }
    
    // *** save card details method *** //
    
    func getEventDetails(index : Int) -> Void {
        
        
        let perCardDetails : NSMutableArray = []
        
        // *** save image url *** //
        if let imageUrl = self.cards[index].objectForKey("cardImage") as? String {
            perCardDetails.addObject(imageUrl)
        } else {
            perCardDetails.addObject("")
        }
        
        let perCardEventsDetails : NSMutableArray = []
        
        // *** get event details *** //
        
        if let events = self.cards[index].objectForKey("events") as? NSMutableArray {
            
            for i in (0..<events.count) {
                
                let perEventDetailBrief : NSMutableArray = []
                
                // *** get event id *** //
                if let eventId = events[i].objectForKey("eventId") as? NSInteger {
                    perEventDetailBrief.addObject(eventId)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save event name *** //
                if let eventName = events[i].objectForKey("eventName") as? String {
                    perEventDetailBrief.addObject(eventName)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save date *** //
                if let eventDate = events[i].objectForKey("eventDateStr") as? String {
                    perEventDetailBrief.addObject(eventDate)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save time *** //
                if let eventTime = events[i].objectForKey("eventTimeStr") as? String {
                    perEventDetailBrief.addObject(eventTime)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save venue name *** //
                if let venueName = events[i].objectForKey("venueName") as? String {
                    perEventDetailBrief.addObject(venueName)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save state *** //
                if let state = events[i].objectForKey("state") as? String {
                    perEventDetailBrief.addObject(state)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save country *** //
                if let country = events[i].objectForKey("country") as? String {
                    perEventDetailBrief.addObject(country)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save ticket price *** //
                if let ticketPriceTag = events[i].objectForKey("ticketPriceTag") as? String {
                    perEventDetailBrief.addObject(ticketPriceTag)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save is favorite set *** //
                if let isFavoriteSet = events[i].objectForKey("isFavoriteEvent") as? Bool {
                    perEventDetailBrief.addObject(isFavoriteSet)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save sharelink *** //
                if let shareLinkUrl = events[i].objectForKey("shareLinkUrl") as? String {
                    perEventDetailBrief.addObject(shareLinkUrl)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                // *** save city *** //
                if let city = events[i].objectForKey("city") as? String {
                    perEventDetailBrief.addObject(city)
                } else {
                    perEventDetailBrief.addObject("")
                }
                
                
                // *** save per event details *** //
                perCardEventsDetails.addObject(perEventDetailBrief)
            }
            
            // *** save per card details *** //
            perCardDetails.addObject(perCardEventsDetails)
        } else {
            
            // *** save per card details *** //
            perCardDetails.addObject(perCardEventsDetails)
        }
        
        if let cardType = self.cards[index].objectForKey("cardType") as? String {
            if cardType == "REGULAR_CARD" {
                
                // *** save card name *** //
                if let cardName = self.cards[index].objectForKey("cardName") as? String {
                    perCardDetails.addObject(cardName)
                } else {
                    perCardDetails.addObject("")
                }
            } else if cardType == "TEAM_CARD" {
                
                // *** save artist ID *** //
                if let artistId = self.cards[index].objectForKey("artistId") as? NSInteger {
                    perCardDetails.addObject(artistId)
                } else {
                    perCardDetails.addObject("")
                }
                
                // *** save is Favorite Artist *** //
                if let isFavoriteArtist = self.cards[index].objectForKey("isFavoriteArtist") as? Bool {
                    perCardDetails.addObject(isFavoriteArtist)
                } else {
                    perCardDetails.addObject("")
                }
                
            }
        }
        
        // ** save per card event detail
        self.cardsWithEventsDetail.addObject(perCardDetails)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        // *** hide side menu *** //
        
        hideNavigation()
        rightSwipeFlag = true
        leftSwipeFlag = false
        
        //Call country/state Api
        getstateOrcountryApi()
        
        //Call rewardsPoint
        getRewardsPointApi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Table view delegate and datasource methods
    //MARK:-
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.cardsWithEventsDetail.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let events = self.cardsWithEventsDetail.objectAtIndex(section) as? NSMutableArray {
            
            if events.count > 1 {
                if let data = events.objectAtIndex(1) as? NSMutableArray {
                    return data.count
                }
            }
        }
        return 1
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return 25
        } else {
            return 15
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // *** get all the cards *** //
        if let cards = self.cards.objectAtIndex(indexPath.section) as? NSDictionary {
            
            // *** get the card type *** //
            if let  cardType = cards.objectForKey("cardType") as? String {
                
                // *** get the events in card based on card type *** //
                let events = self.cardsWithEventsDetail.objectAtIndex(indexPath.section) as! NSMutableArray
                
                // ************************** TEAM CARD ************************** //
                
                if cardType == "TEAM_CARD" {
                    
                    // *** get the event details saved from API *** //
                    let eventDetails = events.objectAtIndex(1) as! NSMutableArray
                    
                    // *** get each event details saved from API *** //
                    let eventBrief = eventDetails.objectAtIndex(indexPath.row) as! NSMutableArray
                    
                    // *** check for the first cell to load *** //
                    
                    if indexPath.row == 0 {
                        
                        let cell = tableView.dequeueReusableCellWithIdentifier("Cell2", forIndexPath: indexPath)
                            as! HomeEventsCardTableViewCell
                        
                        // *** cell selection style *** //
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        
                        // *** cell border changes *** //
                        if eventDetails.count == 1 {
                            cell.topViewHeightConstraint.constant = 2
                            cell.bottomViewHeightConstraint.constant = 2
                            cell.layoutIfNeeded()
                        } else {
                            
                            cell.topViewHeightConstraint.constant = 2
                            cell.bottomViewHeightConstraint.constant = 0
                            cell.layoutIfNeeded()
                        }
                        
                        // *** team card Button changes *** //
                        cell.teamCardInfoButton.hidden = false
                        cell.teamCardFavouriteButton.hidden = false
                        
                        // *** set image *** //
                        cell.urlImageImageView.kf_setImageWithURL(NSURL(string: events.objectAtIndex(0) as! String)!)
                        
                        // *** set event name *** //
                        cell.eventNameLabel.text = eventBrief.objectAtIndex(1) as? String
                        
                        // *** check for date *** //
                        if eventBrief.objectAtIndex(2) as? String == "TBD" {
                            cell.dayLabel.text = "TBD"
                            cell.yearLabel.text = "TBD"
                            cell.monthLabel.text = "TBD"
                            cell.weekDayLabel.text = "TBD"
                        } else {
                            // *** get date *** //
                            
                            let date = eventBrief.objectAtIndex(2) as? String
                            
                            // *** get date from string *** //
                            
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "MM/dd/yyyy"
                            let dateFromString = dateFormatter.dateFromString(date!)
                            
                            // *** get day *** //
                            
                            let dateFormatteerForDay = NSDateFormatter()
                            dateFormatteerForDay.dateFormat = "dd"
                            let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                            
                            cell.dayLabel.text = dayFromDate
                            
                            // *** get year *** //
                            
                            let dateFormatteerForYear = NSDateFormatter()
                            dateFormatteerForYear.dateFormat = "YYYY"
                            let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                            
                            cell.yearLabel.text = yearFromDate
                            
                            // *** get month *** //
                            
                            let dateFormatteerForMonth = NSDateFormatter()
                            dateFormatteerForMonth.dateFormat = "MMM"
                            let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                            
                            cell.monthLabel.text = monthFromDate
                            
                            // *** get week day *** //
                            
                            let dateFormatteerForWeekDay = NSDateFormatter()
                            dateFormatteerForWeekDay.dateFormat = "EEE"
                            let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                            
                            cell.weekDayLabel.text = weekDayFromDate
                        }
                        
                        // *** get time *** //
                        
                        var time : String = ""
                        
                        if eventBrief.objectAtIndex(3) as? String == "TBD"{
                            time = "TBD"
                        } else {
                            
                            let timeSlot = (eventBrief.objectAtIndex(3) as? String)!
                            
                            let dateFormatterForTime = NSDateFormatter()
                            dateFormatterForTime.dateFormat = "hh:mma"
                            let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                            
                            let dateFormatteerForTimeFormat = NSDateFormatter()
                            dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                            time = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                        }
                        
                        // *** set address *** //
                        
                        cell.eventTimeAndAddressLabel.text = "\(time) - \(eventBrief.objectAtIndex(4)), \(eventBrief.objectAtIndex(10)), \(eventBrief.objectAtIndex(5)), \(eventBrief.objectAtIndex(6))"
                        
                        // *** set price *** //
                        cell.priceLabel.text = eventBrief.objectAtIndex(7) as? String
                        
                        
                        // *** set favorites *** //
                        
                        if let isFavorite = eventBrief.objectAtIndex(8) as? Bool {
                            if isFavorite {
                                cell.favouritesButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
                            } else {
                                cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                            }
                        } else {
                            cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                        }
                        
                        
                        // *** set team as favorites *** //
                        
                        if let teamCardFavourite = events.objectAtIndex(3) as? Bool {
                            if teamCardFavourite {
                                cell.teamCardFavouriteButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
                            } else {
                                cell.teamCardFavouriteButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                            }
                        } else {
                            cell.teamCardFavouriteButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                        }
                        
                        
                        // *** cell button actions *** //
                        cell.favouritesButton.tag = indexPath.row
                        cell.shareButton.tag = indexPath.row
                        cell.teamCardFavouriteButton.tag = indexPath.row
                        cell.teamCardInfoButton.tag = indexPath.row
                        cell.delegate = self
                        
                        return cell
                        
                    } else {
                        
                        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
                            as! HomeEventsTableViewCell
                        
                        // *** change color of cell *** //
                        
                        if indexPath.row % 2 != 0 {
                            cell.colorView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
                        } else {
                            cell.colorView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
                        }
                        
                        cell.cardNameHeightConstraint.constant = 0
                        cell.cardNameViewHeightConstraint.constant = 0
                        cell.layoutIfNeeded()
                        
                        
                        // *** cell border changes *** //
                        if indexPath.row == eventDetails.count - 1 {
                            
                            cell.topViewHeightConstraint.constant = 0
                            cell.bottomViewHeightConstraint.constant = 2
                            cell.layoutIfNeeded()
                        } else {
                            
                            cell.topViewHeightConstraint.constant = 0
                            cell.bottomViewHeightConstraint.constant = 0
                            cell.layoutIfNeeded()
                        }
                        
                        // *** hide the seperator line *** //
                        cell.borderBgView_height.constant = 0
                        
                        // *** hide the card name *** //
                        cell.cardNameHeightConstraint.constant = 0
                        cell.cardNameView.hidden = true
                        cell.cardNameView.layoutIfNeeded()
                        
                        // *** cell selection style *** //
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        
                        // *** set event name *** //
                        
                        cell.eventNameLabel.text = eventBrief.objectAtIndex(1) as? String
                        
                        // *** check for date *** //
                        
                        if eventBrief.objectAtIndex(2) as? String == "TBD" {
                            cell.dayLabel.text = "TBD"
                            cell.yearLabel.text = "TBD"
                            cell.monthLabel.text = "TBD"
                            cell.weekDayLabel.text = "TBD"
                        } else {
                            // *** get date *** //
                            
                            let date = eventBrief.objectAtIndex(2) as? String
                            
                            // *** get date from string *** //
                            
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "MM/dd/yyyy"
                            let dateFromString = dateFormatter.dateFromString(date!)
                            
                            // *** get day *** //
                            
                            let dateFormatteerForDay = NSDateFormatter()
                            dateFormatteerForDay.dateFormat = "dd"
                            let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                            
                            cell.dayLabel.text = dayFromDate
                            
                            // *** get year *** //
                            
                            let dateFormatteerForYear = NSDateFormatter()
                            dateFormatteerForYear.dateFormat = "YYYY"
                            let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                            
                            cell.yearLabel.text = yearFromDate
                            
                            // *** get month *** //
                            
                            let dateFormatteerForMonth = NSDateFormatter()
                            dateFormatteerForMonth.dateFormat = "MMM"
                            let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                            
                            cell.monthLabel.text = monthFromDate
                            
                            // *** get week day *** //
                            
                            let dateFormatteerForWeekDay = NSDateFormatter()
                            dateFormatteerForWeekDay.dateFormat = "EEE"
                            let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                            
                            cell.weekDayLabel.text = weekDayFromDate
                        }
                        
                        // *** get time *** //
                        
                        var time : String = ""
                        
                        if eventBrief.objectAtIndex(3) as? String == "TBD"{
                            time = "TBD"
                        } else {
                            
                            let timeSlot = (eventBrief.objectAtIndex(3) as? String)!
                            
                            let dateFormatterForTime = NSDateFormatter()
                            dateFormatterForTime.dateFormat = "hh:mma"
                            let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                            
                            let dateFormatteerForTimeFormat = NSDateFormatter()
                            dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                            time = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                        }
                        
                        // *** set address *** //
                        
                        cell.eventTimeAndAddressLabel.text = "\(time) - \(eventBrief.objectAtIndex(4)), \(eventBrief.objectAtIndex(10)), \(eventBrief.objectAtIndex(5)), \(eventBrief.objectAtIndex(6))"
                        
                        // *** set price *** //
                        cell.priceLabel.text = eventBrief.objectAtIndex(7) as? String
                        
                        // *** set favorites *** //
                        
                        if let isFavorite = eventBrief.objectAtIndex(8) as? Bool {
                            if isFavorite {
                                cell.favouritesButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
                            } else {
                                cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                            }
                        } else {
                            cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                        }
                        
                        // *** cell button actions *** //
                        cell.favouritesButton.tag = indexPath.row
                        cell.shareButton.tag = indexPath.row
                        cell.delegate = self
                        
                        return cell
                    }
                    
                } else if cardType == "REGULAR_CARD" {
                    
                    // ************************** REGULAR CARD ************************** //
                    
                    // *** get the event details saved from API *** //
                    let eventDetails = events.objectAtIndex(1) as! NSMutableArray
                    
                    // *** get each event details saved from API *** //
                    let eventBrief = eventDetails.objectAtIndex(indexPath.row) as! NSMutableArray
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
                        as! HomeEventsTableViewCell
                    
                    // *** change color of cell *** //
                    
                    if indexPath.row % 2 != 0 {
                        cell.colorView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
                    } else {
                        cell.colorView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
                    }
                    
                    cell.cardNameViewHeightConstraint.constant = 0
                    cell.cardNameHeightConstraint.constant = 0
                    cell.layoutIfNeeded()
                    
                    // *** hide the card name *** //
                    
                    if indexPath.row == 0 {
                        
                        cell.cardNameLabel.text = events.objectAtIndex(2) as? String
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
                            cell.cardNameHeightConstraint.constant = 45
                        } else {
                            cell.cardNameHeightConstraint.constant = 30
                        }
                        cell.cardNameViewHeightConstraint.constant = 1
                        cell.layoutIfNeeded()
                        cell.cardNameView.hidden = false
                    } else {
                        cell.cardNameHeightConstraint.constant = 0
                        cell.cardNameViewHeightConstraint.constant = 0
                        cell.layoutIfNeeded()
                        cell.cardNameView.hidden = true
                    }
                    
                    // *** cell selection style *** //
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    
                    // *** cell border changes *** //
                    if indexPath.row == 0 {
                        
                        cell.topViewHeightConstraint.constant = 2
                        cell.bottomViewHeightConstraint.constant = 0
                        cell.layoutIfNeeded()
                    } else if indexPath.row == eventDetails.count - 1 {
                        
                        cell.topViewHeightConstraint.constant = 0
                        cell.bottomViewHeightConstraint.constant = 2
                        cell.layoutIfNeeded()
                    } else {
                        
                        cell.topViewHeightConstraint.constant = 0
                        cell.bottomViewHeightConstraint.constant = 0
                        cell.layoutIfNeeded()
                    }
                    
                    if eventDetails.count == 1 {
                        
                        cell.topViewHeightConstraint.constant = 2
                        cell.bottomViewHeightConstraint.constant = 2
                        cell.layoutIfNeeded()
                    }
                    
                    // *** hide the seperator line *** //
                    cell.borderBgView_height.constant = 0
                    
                    // *** set card name *** //
                    
                    cell.cardNameLabel.text = events.objectAtIndex(2) as? String
                    
                    // *** set event name *** //
                    
                    cell.eventNameLabel.text = eventBrief.objectAtIndex(1) as? String
                    
                    // *** check for date *** //
                    
                    if eventBrief.objectAtIndex(2) as? String == "TBD" {
                        cell.dayLabel.text = "TBD"
                        cell.yearLabel.text = "TBD"
                        cell.monthLabel.text = "TBD"
                        cell.weekDayLabel.text = "TBD"
                    } else {
                        // *** get date *** //
                        
                        let date = eventBrief.objectAtIndex(2) as? String
                        
                        // *** get date from string *** //
                        
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "MM/dd/yyyy"
                        let dateFromString = dateFormatter.dateFromString(date!)
                        
                        // *** get day *** //
                        
                        let dateFormatteerForDay = NSDateFormatter()
                        dateFormatteerForDay.dateFormat = "dd"
                        let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                        
                        cell.dayLabel.text = dayFromDate
                        
                        // *** get year *** //
                        
                        let dateFormatteerForYear = NSDateFormatter()
                        dateFormatteerForYear.dateFormat = "YYYY"
                        let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                        
                        cell.yearLabel.text = yearFromDate
                        
                        // *** get month *** //
                        
                        let dateFormatteerForMonth = NSDateFormatter()
                        dateFormatteerForMonth.dateFormat = "MMM"
                        let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                        
                        cell.monthLabel.text = monthFromDate
                        
                        // *** get week day *** //
                        
                        let dateFormatteerForWeekDay = NSDateFormatter()
                        dateFormatteerForWeekDay.dateFormat = "EEE"
                        let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                        
                        cell.weekDayLabel.text = weekDayFromDate
                    }
                    
                    // *** get time *** //
                    
                    var time : String = ""
                    
                    if eventBrief.objectAtIndex(3) as? String == "TBD"{
                        time = "TBD"
                    } else {
                        
                        let timeSlot = (eventBrief.objectAtIndex(3) as? String)!
                        
                        let dateFormatterForTime = NSDateFormatter()
                        dateFormatterForTime.dateFormat = "hh:mma"
                        let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                        
                        let dateFormatteerForTimeFormat = NSDateFormatter()
                        dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                        time = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                    }
                    
                    // *** set address *** //
                    
                    cell.eventTimeAndAddressLabel.text = "\(time) - \(eventBrief.objectAtIndex(4)), \(eventBrief.objectAtIndex(10)), \(eventBrief.objectAtIndex(5)), \(eventBrief.objectAtIndex(6))"
                    
                    // *** set price *** //
                    cell.priceLabel.text = eventBrief.objectAtIndex(7) as? String
                    
                    // *** set favorites *** //
                    
                    if let isFavorite = eventBrief.objectAtIndex(8) as? Bool {
                        if isFavorite {
                            cell.favouritesButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
                        } else {
                            cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                        }
                    } else {
                        cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                    }
                    
                    // *** cell button actions *** //
                    cell.favouritesButton.tag = indexPath.row
                    cell.shareButton.tag = indexPath.row
                    cell.delegate = self
                    
                    return cell
                    
                } else if cardType == "POPULAR_CARD" {
                    
                    // ************************** POPULAR CARD ************************** //
                    
                    // *** get the event details saved from API *** //
                    let eventDetails = events.objectAtIndex(1) as! NSMutableArray
                    
                    // *** get each event details saved from API *** //
                    let eventBrief = eventDetails.objectAtIndex(indexPath.row) as! NSMutableArray
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("Cell2", forIndexPath: indexPath)
                        as! HomeEventsCardTableViewCell
                    
                    // *** cell selection style *** //
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    
                    
                    // *** cell border changes *** //
                    cell.topViewHeightConstraint.constant = 2
                    cell.bottomViewHeightConstraint.constant = 2
                    cell.layoutIfNeeded()
                    
                    // *** team card Button changes *** //
                    cell.teamCardInfoButton.hidden = true
                    cell.teamCardFavouriteButton.hidden = true
                    
                    // *** set image *** //
                    cell.urlImageImageView.kf_setImageWithURL(NSURL(string: events.objectAtIndex(0) as! String)!)
                    
                    // *** set event name *** //
                    
                    cell.eventNameLabel.text = eventBrief.objectAtIndex(1) as? String
                    
                    // *** check for date *** //
                    
                    if eventBrief.objectAtIndex(2) as? String == "TBD" {
                        cell.dayLabel.text = "TBD"
                        cell.yearLabel.text = "TBD"
                        cell.monthLabel.text = "TBD"
                        cell.weekDayLabel.text = "TBD"
                    } else {
                        // *** get date *** //
                        
                        let date = eventBrief.objectAtIndex(2) as? String
                        
                        // *** get date from string *** //
                        
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "MM/dd/yyyy"
                        let dateFromString = dateFormatter.dateFromString(date!)
                        
                        // *** get day *** //
                        
                        let dateFormatteerForDay = NSDateFormatter()
                        dateFormatteerForDay.dateFormat = "dd"
                        let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                        
                        cell.dayLabel.text = dayFromDate
                        
                        // *** get year *** //
                        
                        let dateFormatteerForYear = NSDateFormatter()
                        dateFormatteerForYear.dateFormat = "YYYY"
                        let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                        
                        cell.yearLabel.text = yearFromDate
                        
                        // *** get month *** //
                        
                        let dateFormatteerForMonth = NSDateFormatter()
                        dateFormatteerForMonth.dateFormat = "MMM"
                        let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                        
                        cell.monthLabel.text = monthFromDate
                        
                        // *** get week day *** //
                        
                        let dateFormatteerForWeekDay = NSDateFormatter()
                        dateFormatteerForWeekDay.dateFormat = "EEE"
                        let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                        
                        cell.weekDayLabel.text = weekDayFromDate
                    }
                    
                    // *** get time *** //
                    
                    var time : String = ""
                    
                    if eventBrief.objectAtIndex(3) as? String == "TBD"{
                        time = "TBD"
                    } else {
                        
                        let timeSlot = (eventBrief.objectAtIndex(3) as? String)!
                        
                        let dateFormatterForTime = NSDateFormatter()
                        dateFormatterForTime.dateFormat = "hh:mma"
                        let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
                        
                        let dateFormatteerForTimeFormat = NSDateFormatter()
                        dateFormatteerForTimeFormat.dateFormat = "HH:mm"
                        time = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                    }
                    
                    // *** set address *** //
                    
                    cell.eventTimeAndAddressLabel.text = "\(time) - \(eventBrief.objectAtIndex(4)), \(eventBrief.objectAtIndex(10)), \(eventBrief.objectAtIndex(5)), \(eventBrief.objectAtIndex(6))"
                    
                    // *** set price *** //
                    cell.priceLabel.text = eventBrief.objectAtIndex(7) as? String
                    
                    // *** set favorites *** //
                    
                    if let isFavorite = eventBrief.objectAtIndex(8) as? Bool {
                        if isFavorite {
                            cell.favouritesButton.setImage(UIImage(named: "heartRed"), forState: .Normal)
                        } else {
                            cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                        }
                    } else {
                        cell.favouritesButton.setImage(UIImage(named: "heartWhite"), forState: .Normal)
                    }
                    
                    // *** cell button actions *** //
                    cell.favouritesButton.tag = indexPath.row
                    cell.shareButton.tag = indexPath.row
                    cell.delegate = self
                    
                    return cell
                    
                } else if cardType == "STATIC_CARD" {
                    
                    // ************************** STATIC CARD ************************** //
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath)
                        as! FirstTableViewCell
                    
                    // *** cell selection style *** //
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    
                    // *** set image *** //
                    cell.urlImageImageView.kf_setImageWithURL(NSURL(string: events.objectAtIndex(0) as! String)!)
                    
                    //                    // *** cell button actions *** //
                    //                    cell.favouritesButton.tag = indexPath.row
                    //                    cell.delegate = self
                    
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath)
                        as! FirstTableViewCell
                    
                    return cell
                }
            } else {
                let cell = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath)
                    as! FirstTableViewCell
                
                return cell
            }
        } else {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath)
                as! FirstTableViewCell
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.searchBarTextField.resignFirstResponder()
        
        
        if let cardDetail =  self.cardsWithEventsDetail.objectAtIndex(indexPath.section) as? NSMutableArray {
            
            if cardDetail.count >= 2 {
                
                if let eventDetail = cardDetail.objectAtIndex(1) as? NSMutableArray {
                    
                    var eventId : String = ""
                    
                    if let id = eventDetail.objectAtIndex(indexPath.row).objectAtIndex(0) as? NSInteger {
                        eventId = "\(id)"
                        
                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EventDetail") as! EventDetailViewController
                        vc.eventId = eventId
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            }
            
        }
        
        
    }
    
    //MARK:- Cell button action
    //MARK:-
    
    // *** regular card Favourites tapped *** //
    
    func homeEventsFavouritesButtonClicked(button: UIButton, index: NSInteger) {
        
        self.searchBarTextField.resignFirstResponder()
        let cell = button.superview?.superview?.superview?.superview?.superview?.superview as! HomeEventsTableViewCell
        let indexPath : NSIndexPath = self.baseTableView.indexPathForCell(cell)!
        
        let cardDetail =  self.cardsWithEventsDetail.objectAtIndex(indexPath.section) as! NSMutableArray
        
        let eventDetail = cardDetail.objectAtIndex(1) as! NSMutableArray
        
        var eventId : String = ""
        
        if let id = eventDetail.objectAtIndex(indexPath.row).objectAtIndex(0) as? NSInteger {
            eventId = "\(id)"
        }
        
        
        if let favorite = eventDetail.objectAtIndex(indexPath.row).objectAtIndex(8) as? Bool {
            
            var isFavorite : Bool = false
            
            isFavorite = favorite
            
            if isFavorite == true {
                isFavorite = false
            } else {
                isFavorite = true
            }
            
            eventDetail.objectAtIndex(indexPath.row).replaceObjectAtIndex(8, withObject: isFavorite)
        } else {
            
        }
        
        self.baseTableView.reloadData()
        
        var customerId : String = ""
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = "\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)"
        }
        
        if customerId != "" {
            
            // *** alert controller *** //
            
            let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
            alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
            
            // *** api call for Home cards ***
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            // *** send parameters
            let getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "customerId" : customerId,
                "eventId" : eventId
            ]
            
            // **** call webservice
            
            WebServiceCall.sharedInstance.favouriteEvent("FavouriteEvents.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                // ** check for valid result
                if isTrue == true {
                    
                    self.view.makeToast(message: message!)
                    
                } else {
                    
                    if message == "" {
                        // * if no response
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        } else {
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    // *** regular card Favourites tapped *** //
    
    func homeEventsShareButtonClicked(button: UIButton, index: NSInteger) {
        
        self.searchBarTextField.resignFirstResponder()
        let cell = button.superview?.superview?.superview?.superview?.superview?.superview as! HomeEventsTableViewCell
        let indexPath : NSIndexPath = self.baseTableView.indexPathForCell(cell)!
        
        let cardDetail =  self.cardsWithEventsDetail.objectAtIndex(indexPath.section) as! NSMutableArray
        
        let eventDetail = cardDetail.objectAtIndex(1) as! NSMutableArray
        
        if let shareUrl = eventDetail.objectAtIndex(indexPath.row).objectAtIndex(9) as? String {
            shareLinkUrl = shareUrl
        }
        
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.3)
        mySubview.delegate = self
        mySubview.customSheetBgView.backgroundColor = greyColor
        
        self.mySubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.handleTap(_:)))
        mySubview.customSheetBgView.addGestureRecognizer(tap)
        
        mySubview.customSheetBgView.hidden = false
        mySubview.sortBgView.hidden = true
        mySubview.priceBgView.hidden = true
        mySubview.sharingBgView.hidden = false
        mySubview.sharingBgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, mySubview.customSheetBgView.frame.size.height)
        self.view.addSubview(mySubview)
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 1.0
            self.mySubview.sharingBgView.frame = CGRectMake(0, self.view.frame.size.height - 123, self.view.frame.size.width, self.mySubview.customSheetBgView.frame.size.height)
            }, completion: { finished in
        })
        
    }
    
    // *** Popular card Favourites tapped *** //
    
    func homeEventsCardFavouritesButtonClicked(button: UIButton, index: NSInteger) {
        
        self.searchBarTextField.resignFirstResponder()
        let cell = button.superview?.superview?.superview?.superview?.superview as! HomeEventsCardTableViewCell
        let indexPath : NSIndexPath = self.baseTableView.indexPathForCell(cell)!
        
        
        let cardDetail =  self.cardsWithEventsDetail.objectAtIndex(indexPath.section) as! NSMutableArray
        
        let eventDetail = cardDetail.objectAtIndex(1) as! NSMutableArray
        
        var eventId : String = ""
        
        if let id = eventDetail.objectAtIndex(indexPath.row).objectAtIndex(0) as? NSInteger {
            eventId = "\(id)"
        }
        
        
        if let favorite = eventDetail.objectAtIndex(indexPath.row).objectAtIndex(8) as? Bool {
            
            var isFavorite : Bool = false
            
            isFavorite = favorite
            
            if isFavorite == true {
                isFavorite = false
            } else {
                isFavorite = true
            }
            
            eventDetail.objectAtIndex(indexPath.row).replaceObjectAtIndex(8, withObject: isFavorite)
        } else {
            
        }
        
        
        self.baseTableView.reloadData()
        
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        var customerId : String = ""
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = "\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)"
        }
        
        if customerId != "" {
            
            // *** api call for Home cards ***
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            // *** send parameters
            let getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "customerId" : customerId,
                "eventId" : eventId
            ]
            
            // **** call webservice
            
            WebServiceCall.sharedInstance.favouriteEvent("FavouriteEvents.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                // ** check for valid result
                if isTrue == true {
                    
                    self.view.makeToast(message: message!)
                    
                } else {
                    
                    if message == "" {
                        // * if no response
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        } else {
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    // *** Popular card Share tapped *** //
    
    func homeEventsCardShareButtonClicked(button: UIButton, index: NSInteger) {
        
        
        self.searchBarTextField.resignFirstResponder()
        let cell = button.superview?.superview?.superview?.superview?.superview as! HomeEventsCardTableViewCell
        let indexPath : NSIndexPath = self.baseTableView.indexPathForCell(cell)!
        
        let cardDetail =  self.cardsWithEventsDetail.objectAtIndex(indexPath.section) as! NSMutableArray
        
        let eventDetail = cardDetail.objectAtIndex(1) as! NSMutableArray
        
        if let shareUrl = eventDetail.objectAtIndex(indexPath.row).objectAtIndex(9) as? String {
            shareLinkUrl = shareUrl
        }
        
        let greyColor = UIColor( red:0.0/255,   green:0.0/255, blue:0.0/255, alpha:0.3)
        mySubview.delegate = self
        mySubview.customSheetBgView.backgroundColor = greyColor
        
        self.mySubview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.handleTap(_:)))
        mySubview.customSheetBgView.addGestureRecognizer(tap)
        
        mySubview.customSheetBgView.hidden = false
        mySubview.sortBgView.hidden = true
        mySubview.priceBgView.hidden = true
        mySubview.sharingBgView.hidden = false
        mySubview.sharingBgView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, mySubview.customSheetBgView.frame.size.height)
        self.view.addSubview(mySubview)
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 1.0
            self.mySubview.sharingBgView.frame = CGRectMake(0, self.view.frame.size.height - 123, self.view.frame.size.width, self.mySubview.customSheetBgView.frame.size.height)
            }, completion: { finished in
        })
    }
    
    func teamCardFavoriteButtonClikced(button: UIButton, index: NSInteger) {
        
        self.searchBarTextField.resignFirstResponder()
        let cell = button.superview?.superview?.superview?.superview?.superview as! HomeEventsCardTableViewCell
        let indexPath : NSIndexPath = self.baseTableView.indexPathForCell(cell)!
        
        let cardDetail =  self.cardsWithEventsDetail.objectAtIndex(indexPath.section) as! NSMutableArray
        
        var artistId : String = ""
        
        if let id = cardDetail.objectAtIndex(2) as? NSInteger {
            artistId = "\(id)"
        }
        
        if let teamCardFavourite = cardDetail.objectAtIndex(3) as? Bool {
            
            var isFavorite : Bool = false
            
            isFavorite = teamCardFavourite
            
            if isFavorite == true {
                isFavorite = false
            } else {
                isFavorite = true
            }
            
            cardDetail.replaceObjectAtIndex(3, withObject: isFavorite)
        } else {
            
        }
        
        self.baseTableView.reloadData()
        
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        var customerId : String = ""
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = "\(NSUserDefaults.standardUserDefaults().objectForKey("customerId") as! NSInteger)"
        }
        
        if customerId != "" {
            
            // *** api call for get popular artists ***
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            // *** send parameters
            let getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "artistActionType" : "FAVORITE",
                "customerId" : customerId,
                "artistIds" : artistId
            ]
            
            // **** call webservice
            WebServiceCall.sharedInstance.addFavoriteOrSuperArtist("AddFavoriteOrSuperArtist.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                // ** check for valid result
                if isTrue == true {
                    
                    self.view.makeToast(message: message!)
                    
                } else {
                    
                    // * if no response
                    if message == "" {
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        } else {
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func teamCardInfoButtonClikced(button: UIButton, index: NSInteger) {
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EventInfo") as! EventInfoViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- UICollectionViewDataSource
    //MARK:-
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return grandChildCategory.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = self.grandChildCategoryCollectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! HomeBannersCollectionViewCell
        
        cell.backGroundView.backgroundColor = colorsArray.objectAtIndex(indexPath.row) as? UIColor
        cell.titleLabel.text = grandChildCategory.objectAtIndex(indexPath.row) as? String
        
        return cell
        
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ResultSearchArtist") as! ResultSearchEventViewController
        vc.searchedtxtIS = (grandChildCategory.objectAtIndex(indexPath.row) as? String)!
        vc.grandChildIdtoPassApi = (grandChildIdArray.objectAtIndex(indexPath.row) as? Int)!
        print(vc.grandChildIdtoPassApi)
        vc.viewTypeIs = "HomeView"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return CGSizeMake(self.grandChildCategoryCollectionView.frame.height-10, self.grandChildCategoryCollectionView.frame.height-10);
        } else {
            return CGSizeMake(self.grandChildCategoryCollectionView.frame.height-5, self.grandChildCategoryCollectionView.frame.height-5);
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return UIEdgeInsetsMake(15, 15, 15, 15); //top,left,bottom,right
        } else {
            return UIEdgeInsetsMake(8, 8, 8, 8); //top,left,bottom,right
        }
    }
    
    //MARK:- Custom View delegate methods
    //MARK:
    
    //MARK: -OnTap Remove
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        removeCustomView()
    }
    func removeCustomView() {
        
        if mySubview == nil {
            return
        }
        
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            self.mySubview.alpha = 0.0
            }, completion: { finished in
                self.mySubview.removeFromSuperview()
        })
    }
    
    
    //MARK:- ShareDialogMethods
    
    func shareOnFacebook(index: Int){
        
        
        let facebookPost = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        facebookPost.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                //Code to deal with it being cancelled
                //print("cancel clicked")
                self.removeCustomView()
                break
                
            case SLComposeViewControllerResult.Done:
                //Code here to deal with it being completed
                self.removeCustomView()
                break
            }
        }
        
        facebookPost.setInitialText(shareLinkUrl) //The default text in the tweet
        self.presentViewController(facebookPost, animated: false, completion: {
            //Optional completion statement
        })
        
    }
    
    //MARK:- TwitterCicked
    func shareOnTwitter(index: Int){
        
        let twitterSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        twitterSheet.completionHandler = {
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                self.removeCustomView()
                //print("cancel clicked")
                
                break
                
            case SLComposeViewControllerResult.Done:
                self.removeCustomView()
                //Code here to deal with it being completed
                break
            }
        }
        twitterSheet.setInitialText(shareLinkUrl)
        self.presentViewController(twitterSheet, animated: false, completion: {
            
        })
        
    }
    
    //MARK:- Emailsharing Tapped
    
    func shareViaEmail(index: Int) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            //            mail.setToRecipients(["preeti@42works.net"])
            mail.setMessageBody(shareLinkUrl, isHTML: true)
            
            presentViewController(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        removeCustomView()
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:
    
    // *** swipe gesture actions *** //
    
    func handleSwipe(swipe: UISwipeGestureRecognizer) {
        
        // *** left swip *** //
        if swipe.direction == .Left {
            
            if leftSwipeFlag {
                
                hideNavigation()
                rightSwipeFlag = true
                leftSwipeFlag = false
            }
        }
        
        // *** right swipe *** //
        if swipe.direction == .Right {
            if rightSwipeFlag {
                
                self.searchBarTextField.resignFirstResponder()
                showNavigation()
                leftSwipeFlag = true
                rightSwipeFlag = false
            }
        }
    }
    
    
    // *** touch events *** //
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
        self.searchBarTextField.resignFirstResponder()
        hideNavigation()
        rightSwipeFlag = true
        leftSwipeFlag = false
    }
    
    //MARK:- Button actions
    //MARK:
    
    // *** menu clicked *** //
    
    @IBAction func menuButtonTapped(sender: AnyObject) {
        
        self.searchBarTextField.resignFirstResponder()
        showNavigation()
        leftSwipeFlag = true
        rightSwipeFlag = false
    }
    
    // *** location clicked *** //
    
    @IBAction func locationButtonTapped(sender: AnyObject) {
        
        self.searchBarTextField.resignFirstResponder()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LocationTab") as! LocationTabViewController
        vc.savedLocationParamters = self.saveLocationParamters
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // *** date clicked *** //
    
    @IBAction func dateButtonTapped(sender: AnyObject) {
        
        self.searchBarTextField.resignFirstResponder()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("DateTab") as! DateTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // *** explore clicked *** //
    
    @IBAction func exploreButtonTapped(sender: AnyObject) {
        
        self.searchBarTextField.resignFirstResponder()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Explore") as! ExploreViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // *** tickets clicked *** //
    
    @IBAction func myTicketsButtonTapped(sender: AnyObject) {
        
        self.searchBarTextField.resignFirstResponder()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("OrderMyTicket") as! OrderViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // *** settings clicked *** //
    
    @IBAction func settingsButtonTapped(sender: AnyObject) {
        
        self.searchBarTextField.resignFirstResponder()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Settings") as! SettingsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Side menu actions
    //MARK:
    
    // *** hide side menu *** //
    
    func hideNavigation() -> Void {
        
        self.searchBarTextField.resignFirstResponder()
        
        self.navigationVc.view.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height)
        UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseInOut, animations: {
            self.navigationVc.baseView.frame = CGRectMake(-(UIScreen.mainScreen().bounds.width*(2/3)), 0, UIScreen.mainScreen().bounds.width*(2/3), UIScreen.mainScreen().bounds.height)
            self.navigationVc.view.backgroundColor = UIColor.clearColor()
            
        }) { finished in
            self.navigationVc.baseView.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width*(2/3), UIScreen.mainScreen().bounds.height)
            self.navigationVc.view.removeFromSuperview()
        }
    }
    
    // *** show side menu *** //
    
    func showNavigation() -> Void {
        
        self.searchBarTextField.resignFirstResponder()
        self.addChildViewController(self.navigationVc)
        self.view .addSubview(self.navigationVc.view)
        self.navigationVc.view.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height)
        self.navigationVc.baseView.frame = CGRectMake(-(UIScreen.mainScreen().bounds.width*(2/3)), 0, UIScreen.mainScreen().bounds.width*(2/3), UIScreen.mainScreen().bounds.height)
        UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseInOut, animations: {
            self.navigationVc.baseView.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width*(2/3), UIScreen.mainScreen().bounds.height)
            self.navigationVc.view.backgroundColor = UIColor(red:0.0/255, green:0.0/255, blue:0.0/255, alpha:0.5)
        }) { finished in
        }
    }
    
    //MARK:-PushtoSearchEvent
    @IBOutlet weak var PEventBtn: UIButton!
    @IBAction func PushToEventTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("searchEventArtist") as! SearchEventArtistViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    //MARK:-Counrty/state APi call
    func getstateOrcountryApi(){
        // *** api call for forgot password ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        // *** send parameters
        let getParam = [
            "productType" : productType,
            "platForm":platForm,
            "configId":configId,
            
            ]
        print(getParam)
        
        // **** call webservice
        WebServiceCall.sharedInstance.postStateorCounryApiCall("GetStateAndCountry.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            if isTrue == true {
                self.stopIndicator()
                if let countryArray = responseDisctionary!.valueForKey("countryList") as? NSArray{
                    print("country Array Is:",countryArray)
                    if countryArray.count > 0{
                        self.countryArray.removeAllObjects()
                        for i in 0 ..< countryArray.count {
                            self.countryArray .addObject(countryArray[i])
                        }
                        
                    }
                    if let stateArayIs = countryArray.valueForKey("stateList") as? NSArray{
                        if stateArayIs.count > 0{
                            self.stateArray.removeAllObjects()
                            for i in 0 ..< stateArayIs.count {
                                self.stateArray .addObject(stateArayIs[i])
                            }
                            
                        }
                        
                    }
                }
                //Save CountryArray to access PickerData
                // * get customer name
                NSUserDefaults.standardUserDefaults().setValue(self.countryArray, forKey: "countryList")
                
            } else {
                self.stopIndicator()
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
            
        })
    }
    
    func homeEventsCardSeeMoreClicked(button: UIButton, index : NSInteger){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ResultSearchArtist") as! ResultSearchEventViewController
        // *** get event id *** //
        if let artistId = cards[index].objectForKey("artistId") as? NSInteger {
            if let cardName = cards[index].objectForKey("cardName") as? String {
                vc.searchedtxtIS = cardName
                vc.getartistIdtoPass = "\(artistId)"
                vc.viewTypeIs = "HomeViewWithSeeMore"
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    //MARK:-Delegate SeeMore
    func homeEventsSeeMoreClicked(button: UIButton, index : NSInteger){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ResultSearchArtist") as! ResultSearchEventViewController
        // *** get event id *** //
        if let artistId = cards[index].objectForKey("artistId") as? NSInteger {
            if let cardName = cards[index].objectForKey("cardName") as? String {
                vc.searchedtxtIS = cardName
                vc.getartistIdtoPass = "\(artistId)"
                vc.viewTypeIs = "HomeViewWithSeeMore"
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    //MARK:-GetRewardsPoint Api
    func getRewardsPointApi(){
        
        // *** api call for forgot password ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        // *** send parameters
        let getParam = [
            "productType" : productType,
            "platForm":platForm,
            "configId":configId,
            "customerId":customerId
            
        ]
        print(getParam)
        
        // **** call webservice
        WebServiceCall.sharedInstance.postStateorCounryApiCall("GetCustomerLoyaltyRewards.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            if isTrue == true {
                self.stopIndicator()
                print(responseDisctionary)
                if let customerLoyalityDict = responseDisctionary!.valueForKey("customerLoyaltyDetails") as? NSDictionary{
                    print(customerLoyalityDict)
                    if let customerLoyality = customerLoyalityDict.objectForKey("customerLoyalty") as? NSMutableDictionary {
                        print(customerLoyality)
                        NSUserDefaults.standardUserDefaults().setObject(customerLoyality, forKey: "customerLoyalty")
                    }
                }
                
            } else {
                self.stopIndicator()
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
            
        })
    }
    
    @IBAction func futuresButtonTapped(sender: AnyObject) {
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Futures") as? FuturesViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}
