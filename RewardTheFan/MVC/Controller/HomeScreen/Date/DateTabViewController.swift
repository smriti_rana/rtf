//
//  DateTabViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 06/05/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class DateTabViewController: UIViewController, EPCalendarPickerDelegate {
    
    @IBOutlet var selectedDateslabel: UILabel!
    
    @IBOutlet var datesLabel: UILabel!
    
    @IBOutlet var setButton: UIButton!
    
    @IBOutlet var allDatesButton: UIButton!
    
    @IBOutlet var datesView: UIView!
    
    var calendarPicker : EPCalendarPicker!
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        allDatesButton.layer.cornerRadius = 3.0
        allDatesButton.layer.borderWidth = 1.0
        allDatesButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        setButton.layer.cornerRadius = 3.0
        
//        selectedDateslabel.hidden = true
        
        if NSUserDefaults.standardUserDefaults().objectForKey("datesLabel") != nil {
            selectedDateslabel.hidden = false
            datesLabel.text = NSUserDefaults.standardUserDefaults().objectForKey("datesLabel") as? String
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DateTabViewController.setDates(_:)), name:"dates", object: nil)
        
    }
    
    func setDates(notification : NSNotification) {
        print(notification)
        if let info = notification.valueForKey("object") as? NSArray {
            var firstDate : String = ""
            var lastDate : String = ""
            
            if info.count != 0 {
                if info.count == 1 {
                    
                    let initialDate = info.objectAtIndex(0) as! NSDate
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yyyy"
//                    let date = dateFormatter.stringFromDate(initialDate)
                    
                    let dateFormatteerForDay = NSDateFormatter()
                    dateFormatteerForDay.dateFormat = "dd"
                    let dayFromDate = dateFormatteerForDay.stringFromDate(initialDate)
                    
                    let dateFormatteerForDay1 = NSDateFormatter()
                    dateFormatteerForDay1.dateFormat = "MMM"
                    let dayFromDate1 = dateFormatteerForDay1.stringFromDate(initialDate)
                    
                    firstDate = "\(dayFromDate1) \(dayFromDate)"
                    
                    selectedDateslabel.hidden = false
                    datesLabel.text = firstDate
                    NSUserDefaults.standardUserDefaults().setObject(datesLabel.text!, forKey: "datesLabel")
                    
                } else if info.count == 2 {
                    let initialDate = info.objectAtIndex(0) as! NSDate
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yyyy"
//                    let date = dateFormatter.stringFromDate(initialDate)
                    
                    let dateFormatteerForDay = NSDateFormatter()
                    dateFormatteerForDay.dateFormat = "dd"
                    let dayFromDate = dateFormatteerForDay.stringFromDate(initialDate)
                    
                    let dateFormatteerForDay1 = NSDateFormatter()
                    dateFormatteerForDay1.dateFormat = "MMM"
                    let dayFromDate1 = dateFormatteerForDay1.stringFromDate(initialDate)
                    
                    firstDate = "\(dayFromDate1) \(dayFromDate)"
                    
                    let finalDate = info.objectAtIndex(1) as! NSDate
                    let dateFormatter1 = NSDateFormatter()
                    dateFormatter1.dateFormat = "MM/dd/yyyy"
//                    let date2 = dateFormatter.stringFromDate(finalDate)
                    
                    let dateFormatteerForDay2 = NSDateFormatter()
                    dateFormatteerForDay2.dateFormat = "dd"
                    let dayFromDate2 = dateFormatteerForDay2.stringFromDate(finalDate)
                    
                    
                    let dateFormatteerForDay3 = NSDateFormatter()
                    dateFormatteerForDay3.dateFormat = "MMM"
                    let dayFromDate3 = dateFormatteerForDay3.stringFromDate(finalDate)
                    
                    lastDate = "\(dayFromDate3) \(dayFromDate2)"
                    
                    selectedDateslabel.hidden = false
                    datesLabel.text = "\(firstDate) - \(lastDate)"
                    NSUserDefaults.standardUserDefaults().setObject(datesLabel.text!, forKey: "datesLabel")
                    
                }
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        calendarPicker = EPCalendarPicker(startYear: 2016, endYear: 2017, multiSelection: true, selectedDates: [])
        calendarPicker.calendarDelegate = self
        calendarPicker.startDate = NSDate()
        calendarPicker.hightlightsToday = true
        calendarPicker.showsTodaysButton = true
        calendarPicker.hideDaysFromOtherMonth = true
        calendarPicker.backgroundColor = UIColor(red:170.0/255, green:170.0/255, blue:170.0/255, alpha:0.1)
        
        let navigationController = UINavigationController(rootViewController: calendarPicker)
        navigationController.navigationBar.hidden = true
        navigationController.view.frame = CGRectMake(0, 0, self.view.frame.width, self.datesView.frame.height)
        self.addChildViewController(navigationController)
        navigationController.view.userInteractionEnabled = true
        self.datesView.addSubview(navigationController.view)
        navigationController.didMoveToParentViewController(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func allDatesButtonTapped(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("dateSearch", object: [])
        self.navigationController?.popViewControllerAnimated(true)
//        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func setButtonTapped(sender: AnyObject) {
        
        let value : NSArray = calendarPicker.doneButton()
        
        var dates = [NSDate]()
        
        for i in 0..<value.count {
            let date = value[i] as! NSDate
            dates.append(date)
        }
        print(dates)
        let array : [NSDate] = dates
        NSNotificationCenter.defaultCenter().postNotificationName("dateSearch", object: array)
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
}
