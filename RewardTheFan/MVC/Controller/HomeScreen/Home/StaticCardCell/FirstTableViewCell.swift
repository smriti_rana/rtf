//
//  FirstTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 27/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
@objc protocol staticCardDelegate
{
    optional func staticCardShareButtonClicked(button: UIButton, index : NSInteger)
}

class FirstTableViewCell: UITableViewCell {

    weak var delegate: staticCardDelegate?
    
    @IBOutlet var favouritesButton: UIButton!
    
    @IBOutlet var shareButton: UIButton!
    
    @IBOutlet var topViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var bottomViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet var urlImageImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func shareButtonTapped(sender: AnyObject) {
        self.delegate?.staticCardShareButtonClicked!(sender as! UIButton, index: sender.tag)
    }
    

}
