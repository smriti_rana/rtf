//
//  HomeBannersCollectionViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 27/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class HomeBannersCollectionViewCell: UICollectionViewCell {

    @IBOutlet var titleLabel: UILabel!

    @IBOutlet var backGroundView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
//        self.backGroundView.layoutIfNeeded()
        
    }
    
    override var bounds : CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.makeItCircle()
    }
    
    func makeItCircle() {
        self.backGroundView.layer.masksToBounds = true
        self.backGroundView.layer.cornerRadius = self.backGroundView.frame.width/2;
        self.backGroundView.clipsToBounds = true
    }
    
}
