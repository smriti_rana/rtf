//
//  ExploreCollectionViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 02/05/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class ExploreCollectionViewCell: UICollectionViewCell {

    @IBOutlet var cardsImageView: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
