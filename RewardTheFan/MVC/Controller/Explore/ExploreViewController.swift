//
//  ExploreViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 26/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import Kingfisher

class ExploreViewController: BaseViewController {
    
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    @IBOutlet weak var topbar: UIView!
    @IBOutlet var baseCollectionView: UICollectionView!
    var cardDetails : NSMutableArray = []
    var collect_lblIs = ""
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        self.baseCollectionView!.registerNib(UINib(nibName: "ExploreCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "cell")
        
        //CallApi For ExploreCards
        ApiCallForExploreCardS()
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ExploreViewController.pushViewController), name:"alertMessageforExplore", object: nil)
        
        
    }
    //Mark:-
    func pushViewController() {
        self.delay(0.2) { () -> () in
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoginScreen") as! LoginScreenViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardDetails.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = self.baseCollectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! ExploreCollectionViewCell
        
        let perCardDetails : NSMutableArray = cardDetails.objectAtIndex(indexPath.row) as! NSMutableArray
        cell.nameLabel.text = perCardDetails.objectAtIndex(0) as? String
        collect_lblIs = cell.nameLabel.text!
        cell.cardsImageView.kf_setImageWithURL(NSURL(string: perCardDetails.objectAtIndex(1) as! String)!)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if customerId == 0 {
            let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
            alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
            alertVc.message = loggedInAlertMsg
            alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
            self.presentViewController(alertVc, animated: true, completion: nil)

            
        }
        else{
        let perCardDetails : NSMutableArray = cardDetails.objectAtIndex(indexPath.row) as! NSMutableArray
        let exploreTypeIS = perCardDetails.objectAtIndex(2) as? String
        print(exploreTypeIS)
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ResultSearchArtist") as! ResultSearchEventViewController
        print("exploreTypeIS",(perCardDetails.objectAtIndex(0) as? String)!)
        vc.exploreTypeToSearch = exploreTypeIS!
        vc.viewTypeIs = "Explore"
        vc.SearcbarTextfromexploreCell =  (perCardDetails.objectAtIndex(0) as? String)!
        
        self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSizeMake((UIScreen.mainScreen().bounds.width - 23) / 2 , (UIScreen.mainScreen().bounds.width - 23) / 2);
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(10, 10, 10, 10); //top,left,bottom,right
    }
    //MARK:-PushtoSearchEvent
    @IBOutlet weak var searchEventBtn: UIButton!
    @IBAction func pushToSearchEvents(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("searchEventArtist") as! SearchEventArtistViewController
        vc.viewTypeIs = "ExploreSearchAll"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    //MARK:-APICall For Explore
    func ApiCallForExploreCardS(){
        // *** alert controller *** //
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** api call for grand child categories cards ***
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        self.setLoadingIndicator(self.view)
        
        // *** send parameters
        let getParam = [
            
            "configId" : configId,
            "productType" : productType,
            ]
        print(getParam)
        WebServiceCall.sharedInstance.exploreCards("GetExploreCards.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
            
            // * stop activityIndicator
            self.stopIndicator()
            
            if isTrue == true {
                
                print("Response",responseDisctionary!)
                
                if let exploreCards =  responseDisctionary!.valueForKey("exploreCards") as? NSMutableArray {
                    
                    for i in 0..<exploreCards.count {
                        
                        let cardDetails : NSMutableArray = []
                        
                        if let cardName = exploreCards[i].objectForKey("cardName") as? String {
                            cardDetails.addObject(cardName)
                        } else {
                            cardDetails.addObject("")
                        }
                        
                        if let imageUrl = exploreCards[i].objectForKey("imageUrl") as? String {
                            cardDetails.addObject(imageUrl)
                        } else {
                            cardDetails.addObject("")
                        }
                        if let expType = exploreCards[i].objectForKey("exploreType") as? String {
                            cardDetails.addObject(expType)
                        } else {
                            cardDetails.addObject("")
                        }
                        
                        self.cardDetails.addObject(cardDetails)
                    }
                    self.baseCollectionView.reloadData()
                }
            } else {
                
                if message == "" {
                    // * if no response
                    
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
                
            }
            
        })
    }
}
