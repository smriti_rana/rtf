//
//  CardDetailViewController.swift
//  RewardTheFanFinal
//
//  Created by Anmol Rajdev on 05/05/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit

class CardDetailViewController: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource,CardIOPaymentViewControllerDelegate {
    
    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var visaCardImageView: UIImageView!
    
    @IBOutlet weak var navbar_heigth: NSLayoutConstraint!
    @IBOutlet var masterCardImageView: UIImageView!
    
    @IBOutlet var americanExpressCardImageView: UIImageView!
    
    @IBOutlet var discoverCardImageView: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var selectTypeBgView: UIView!
    
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var CvvTf: UITextField!
    
    @IBOutlet weak var fullNameTf: UITextField!
    
    @IBOutlet weak var yearTf: UITextField!
    
    @IBOutlet weak var monthTf: UITextField!
    
    @IBOutlet weak var cardNumbetTf: UITextField!
    
    @IBOutlet weak var cardDetailLbl: UILabel!
    
    @IBOutlet weak var YearBtn: UIButton!
    
    @IBOutlet weak var monthBtn: UIButton!
    
    var monthArray : NSMutableArray = []
    
    var yearArray : NSMutableArray = []
    
    //    var countryArray :NSMutableArray = []
    //
    //    var stateArray :NSMutableArray = []
    
    var selectMonthType  = ""
    
    var selectYearType  = ""
    
    var pickerTypeIs = false
    
    var eventId : String = ""
    
    var categoryTicketGroupId : String = ""
    
    var quantity : String = ""
    
    var ticketPrice : String = ""
    
    var billingAddressId : Int = 0
    
    var shippingAddressId : Int = 0
    
    //MARK:-
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            cardDetailLbl.font = UIFont(name: cardDetailLbl.font.fontName, size: 32)
            navbar_heigth.constant = 74
        }
        
        monthArray = ["01","02","03","04","05","06","07","08","09","10","11","12"]
        yearArray = ["2016","2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027"]
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CardDetailViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        //getstateOrcountryApi()
    }
    
    //MARK:-
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        
        monthTf.resignFirstResponder()
        yearTf.resignFirstResponder()
        selectTypeBgView.hidden=true
        cardNumbetTf.resignFirstResponder()
        fullNameTf.resignFirstResponder()
        CvvTf.resignFirstResponder()
    }
    
    
    //MARK:-Remove Keyboard
    func removekeyBoard() {
        
        cardNumbetTf.resignFirstResponder()
        fullNameTf.resignFirstResponder()
        CvvTf.resignFirstResponder()
    }
    
    //MARK:- YearTapped and MonthTapped
    //MARK:-
    
    @IBAction func YearTapped(sender: AnyObject) {
        
        removekeyBoard()
        pickerTypeIs = true
        if YearBtn.selected == true {
            
            selectTypeBgView.hidden=true
            picker.hidden=true
        } else {
            
            self.YearBtn.resignFirstResponder()
            selectTypeBgView.hidden = false
            picker.hidden = false
        }
        picker.reloadAllComponents()
    }
    
    @IBAction func monthTapped(sender: AnyObject) {
        
        removekeyBoard()
        pickerTypeIs = false
        if monthBtn.selected == true {
            
            selectTypeBgView.hidden=true
            picker.hidden=true
        } else {
            
            self.monthTf.resignFirstResponder()
            selectTypeBgView.hidden = false
            picker.hidden = false
        }
        picker.reloadAllComponents()
    }
    
    //MARK:- Picker Delegates
    //MARK:-
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerTypeIs == false {
            return monthArray.count
        } else {
            return yearArray.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerTypeIs == false {
            return monthArray[row] as? String
        } else {
            return yearArray[row] as? String
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerTypeIs == false {
            selectMonthType = (monthArray[pickerView.selectedRowInComponent(0)] as? String)!
        } else {
            selectYearType = (yearArray[pickerView.selectedRowInComponent(0)] as? String)!
        }
    }
    
    //MARK:- Picker Done or Cancel
    //MARK:-
    
    @IBAction func cancelTapped(sender: AnyObject) {
        
        monthTf.resignFirstResponder()
        yearTf.resignFirstResponder()
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: 0)
        selectTypeBgView.hidden=true
    }
    
    @IBAction func doneTapped(sender: AnyObject) {
        
        if pickerTypeIs == false {
            
            monthTf.resignFirstResponder()
            scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: 0)
            selectTypeBgView.hidden=true
            monthTf.text = (monthArray[picker.selectedRowInComponent(0)] as? String)!
            
        } else {
            
            yearTf.resignFirstResponder()
            scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: 0)
            selectTypeBgView.hidden=true
            yearTf.text = (yearArray[picker.selectedRowInComponent(0)] as? String)!
        }
    }
    
    //MARK:- Textfeild Delegates
    //MARK:-
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        monthTf.resignFirstResponder()
        yearTf.resignFirstResponder()
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: 0)
        selectTypeBgView.hidden=true
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == cardNumbetTf {
            getCreditCardType(textField.text!.stringByReplacingOccurrencesOfString("-", withString: ""))
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string == "\n" {
            if textField.text != "" {
                textField.resignFirstResponder()
            }
        }
        
        if textField == fullNameTf {
            
            if isValidName(string) {
                return false
            }
        }
        
        if textField == CvvTf {
            
        }
        
        if textField == cardNumbetTf {
            
            if textField.text?.characters.count == 4 || textField.text?.characters.count == 9 || textField.text?.characters.count == 14 {
                textField.text = "\(textField.text!)-"
            }
        }
        
        return true
    }
    
    //MARK:- Check for valid name
    //MARK:-
    
    func isValidName(testStr:String) -> Bool {
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/1234567890.]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluateWithObject(testStr)
        
        return specialresult
    }
    
    /*
     func getstateOrcountryApi() {
     
     // *** alert controller ***
     let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
     alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
     
     // * check for internet connection
     guard IJReachability.isConnectedToNetwork() else {
     
     // *** stop indicator *** //
     stopIndicator()
     alertVc.message = ErrorInternetConnection
     self.presentViewController(alertVc, animated: true, completion: nil)
     return
     }
     
     // *** send parameters
     let getParam = [
     
     "productType" : productType,
     "platForm":platForm,
     "configId":configId,
     ]
     
     // **** call webservice
     WebServiceCall.sharedInstance.postStateorCounryApiCall("GetStateAndCountry.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
     
     if isTrue == true {
     
     // ** stop indicator ** //
     self.stopIndicator()
     
     print("response for state",responseDisctionary!)
     if let countryArray = responseDisctionary!.valueForKey("countryList") as? NSArray {
     
     print("country Array Is:",countryArray)
     if countryArray.count > 0 {
     
     self.countryArray.removeAllObjects()
     for i in 0 ..< countryArray.count {
     self.countryArray .addObject(countryArray[i])
     }
     }
     
     if let stateArayIs = countryArray.valueForKey("stateList") as? NSArray {
     
     print("state is",stateArayIs)
     if stateArayIs.count > 0 {
     
     self.stateArray.removeAllObjects()
     for i in 0 ..< stateArayIs.count {
     self.stateArray .addObject(stateArayIs[i])
     }
     }
     }
     }
     } else {
     
     // ** stop inidcator ** //
     self.stopIndicator()
     
     if message == "" {
     // * if no response
     alertVc.message = ErrorServerConnection
     self.presentViewController(alertVc, animated: true, completion: nil)
     } else {
     // * if error in the response
     
     alertVc.message = message!
     self.presentViewController(alertVc, animated: true, completion: nil)
     }
     }
     })
     }
     */
    
    
    //MARK:- CardIO Delaget methods
    //MARK:-
    
    func userDidCancelPaymentViewController(paymentViewController: CardIOPaymentViewController!) {
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func userDidProvideCreditCardInfo(cardInfo: CardIOCreditCardInfo!, inPaymentViewController paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            //            let str = NSString(format: "Received card info.\n Number: %@\n expiry: %02lu/%lu\n cvv: %@.", info.cardNumber, info.expiryMonth, info.expiryYear, info.cvv)
            //            print(str)
            
            self.cardNumbetTf.text = info.cardNumber
            self.monthTf.text = "\(info.expiryMonth)"
            self.yearTf.text = "\(info.expiryYear)"
            self.CvvTf.text = info.cvv
            self.fullNameTf.text = info.cardholderName
            
            let _ : String = getCreditCardType(info.cardNumber.stringByReplacingOccurrencesOfString("-", withString: ""))
        }
        
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:-
    
    func getCreditCardType(cardNumber : String) -> String {
        
        let visa  = "^4[0-9]{6,}$"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", visa)
        let visaResult = texttest1.evaluateWithObject(cardNumber)
        
        let masterCard  = "^5[1-5][0-9]{5,}$"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", masterCard)
        let masterCardResult = texttest2.evaluateWithObject(cardNumber)
        
        let americanExpress  = "^3[47][0-9]{5,}$"
        let texttest = NSPredicate(format:"SELF MATCHES %@", americanExpress)
        let americanExpressResult = texttest.evaluateWithObject(cardNumber)
        
        let deninersClub  = "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        let texttest3 = NSPredicate(format:"SELF MATCHES %@", deninersClub)
        _ = texttest3.evaluateWithObject(cardNumber)
        
        let discovers  = "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        let texttest4 = NSPredicate(format:"SELF MATCHES %@", discovers)
        let dicoversResult = texttest4.evaluateWithObject(cardNumber)
        
        let jcb = "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        let texttest5 = NSPredicate(format:"SELF MATCHES %@", jcb)
        _ = texttest5.evaluateWithObject(cardNumber)
        
        if visaResult {
            
            visaCardImageView.image = UIImage(named: "visa")
            masterCardImageView.image = UIImage(named: "masterCardGrey")
            americanExpressCardImageView.image = UIImage(named: "americanExpressGrey")
            discoverCardImageView.image = UIImage(named: "discoverGrey")
            return "visa"
        } else if masterCardResult {
            
            visaCardImageView.image = UIImage(named: "visaGrey")
            masterCardImageView.image = UIImage(named: "masterCard")
            americanExpressCardImageView.image = UIImage(named: "americanExpressGrey")
            discoverCardImageView.image = UIImage(named: "discoverGrey")
            return "master card"
        } else if americanExpressResult {
            
            visaCardImageView.image = UIImage(named: "visaGrey")
            masterCardImageView.image = UIImage(named: "masterCardGrey")
            americanExpressCardImageView.image = UIImage(named: "americanExpress")
            discoverCardImageView.image = UIImage(named: "discoverGrey")
            return "american express"
        }  else if dicoversResult {
            
            visaCardImageView.image = UIImage(named: "visaGrey")
            masterCardImageView.image = UIImage(named: "masterCardGrey")
            americanExpressCardImageView.image = UIImage(named: "americanExpressGrey")
            discoverCardImageView.image = UIImage(named: "discover")
            return "discovers network"
        }  else {
            
            visaCardImageView.image = UIImage(named: "visaGrey")
            masterCardImageView.image = UIImage(named: "masterCardGrey")
            americanExpressCardImageView.image = UIImage(named: "americanExpressGrey")
            discoverCardImageView.image = UIImage(named: "discoverGrey")
            return ""
        }
    }
    
    //MARK:- Button actions
    //MARK:-
    
    
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func scanCardTapped(sender: AnyObject) {
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC.modalPresentationStyle = .FormSheet
        presentViewController(cardIOVC, animated: true, completion: nil)
    }
    
    
    // *** submit button clicked ***
    @IBAction func submitButtonTapped(sender: AnyObject) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        if cardNumbetTf.text?.characters.count == 0 && monthTf.text?.characters.count == 0 && yearTf.text?.characters.count == 0 && fullNameTf.text?.characters.count == 0 && CvvTf.text?.characters.count == 0 {
            
            alertVc.message = allfeildsAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if cardNumbetTf.text?.characters.count == 0 {
            alertVc.message = cardNoAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if monthTf.text?.characters.count == 0 {
            alertVc.message = cardExpiryMonthAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if yearTf.text?.characters.count == 0 {
            alertVc.message = cardExpiryYearAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if fullNameTf.text?.characters.count == 0 {
            alertVc.message = fullNameAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if CvvTf.text?.characters.count == 0 {
            alertVc.message = cvvAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if cardNumbetTf.text?.characters.count < 17 || cardNumbetTf.text?.characters.count > 22 {
            alertVc.message = validCardAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if CvvTf.text?.characters.count != 3 {
            alertVc.message = rangecvvAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if (getCreditCardType(self.cardNumbetTf.text!.stringByReplacingOccurrencesOfString("-", withString: "")) == "") {
            alertVc.message = validCardAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else {
            
            self.setLoadingIndicator(self.view)
            createOrderApiCall()
        }
    }
    
    func createOrderApiCall() {
        
        // *** alert message view controller *** //
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        guard IJReachability.isConnectedToNetwork() else {
            
            // *** stop indicator *** //
            stopIndicator()
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** get custoemr ID *** //
        var customerId : Int = 0
        
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        
        // *** get card type *** //
        var cardType : String = ""
        cardType = getCreditCardType(self.cardNumbetTf.text!.stringByReplacingOccurrencesOfString("-", withString: ""))
        
        // *** get the paramters *** //
        let getParam = [
            
            "productType" : productType,
            "platForm" : platForm,
            "configId" : configId,
            "sessionId" : deviceId,
            "customerId" : (customerId),
            "shippingMethod" : "ETICKET",
            "paymentMethod" : "CREDITCARD",
            "isPartialLoyaltyPointUsed" : "",
            "eventId" : eventId,
            "categoryTicketGroupId" : categoryTicketGroupId,
            "loyaltySpent" : "",
            "quantity" : quantity,
            "ticketPrice" : ticketPrice,
            "shippingSameAsBilling" : "",
            "billingAddressId" : (billingAddressId),
            "shippingAddressId" : (shippingAddressId),
            "expiryMonth" : monthTf.text!,
            "expiryYear" : yearTf.text!,
            "cardNo" : self.cardNumbetTf.text!.stringByReplacingOccurrencesOfString("-", withString: ""),
            "cardType" : cardType,
            "nameOnCard" : fullNameTf.text!,
            "securityCode" : CvvTf.text!
            
        ]
        
        // **** create order webservice method call *** //
        WebServiceCall.sharedInstance.createOrder("CreateOrder.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            // *** stop indicator *** //
            self.stopIndicator()
            
            if isTrue == true {
                
                // *** get order id *** //
                if let orderId = responseDisctionary?.objectForKey("orderId") as? Int {
                    
                    if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetails") as? OrderDetailsViewController {
                        vc.orderId = orderId
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            } else {
                
                // *** stop indicator *** //
                self.stopIndicator()
                
                if message == "" {
                    
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    
                    // * if error in the response
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
        
    }
    
}
