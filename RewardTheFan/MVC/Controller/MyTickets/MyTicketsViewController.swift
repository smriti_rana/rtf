//
//  MyTicketsViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 26/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class MyTicketsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var NotLoggedInBgView: UIView!
    @IBOutlet weak var myTicketLbl: UILabel!
    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    @IBOutlet weak var loginorSignUpBtn: UIButton!
    @IBAction func LoginOrsignUpTapped(sender: AnyObject) {
    }
    var responseCustomerOrderArray :NSArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        //SetView by LoginCheck
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
            tableView.hidden = false
            self.setLoadingIndicator(self.view)
            getTicketsDetailApi()
        }
        else{
            NotLoggedInBgView.hidden = true
        }
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            myTicketLbl.font = UIFont(name: myTicketLbl.font.fontName, size: 31)
//            navbar_height.constant = 74
        }

        //Regestering cell
        tableView.registerNib(UINib(nibName: "HomeEventsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 72.0
//        tableView.rowHeight = UITableViewAutomaticDimension
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 5
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
        return 100.0;//Choose your custom row height
        }else{
          return  UITableViewAutomaticDimension
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! HomeEventsTableViewCell
//            cell.delegate = self
        cell.shareBgView.hidden = true
        cell.TicketBgView.hidden = false
        
        if responseCustomerOrderArray.count > 0 {
            //to set  name
            if let eventName = responseCustomerOrderArray[indexPath.row].valueForKey("eventName") as? String{
                cell.eventNameLabel.text = eventName
                
            }
               var eTime = ""
            //to set date
            if let Eventdate =  responseCustomerOrderArray[indexPath.row].valueForKey("eventDate") as? String{
                if Eventdate != "TBD" {
                    
                    // *** get datey from string *** //
                    let dateFormatter = NSDateFormatter()
                    
                    dateFormatter.dateFormat = "MM/dd/yyyy"
                    
                    let dateFromString = dateFormatter.dateFromString(Eventdate)
                    
                    // *** get day *** //
                    
                    let dateFormatteerForDay = NSDateFormatter()
                    
                    dateFormatteerForDay.dateFormat = "dd"
                    
                    let dayFromDate = dateFormatteerForDay.stringFromDate(dateFromString!)
                    
                    cell.dayLabel.text = dayFromDate
                    
                    // *** get year *** //
                    
                    let dateFormatteerForYear = NSDateFormatter()
                    
                    dateFormatteerForYear.dateFormat = "YYYY"
                    
                    let yearFromDate = dateFormatteerForYear.stringFromDate(dateFromString!)
                    
                    cell.yearLabel.text = yearFromDate
                    
                    
                    
                    // *** get month *** //
                    
                    
                    let dateFormatteerForMonth = NSDateFormatter()
                    
                    dateFormatteerForMonth.dateFormat = "MMM"
                    
                    let monthFromDate = dateFormatteerForMonth.stringFromDate(dateFromString!)
                    
                    
                    
                    cell.monthLabel.text = monthFromDate
                    
                    // *** get week day *** //
                    
                    
                    let dateFormatteerForWeekDay = NSDateFormatter()
                    
                    dateFormatteerForWeekDay.dateFormat = "EEE"
                    
                    let weekDayFromDate = dateFormatteerForWeekDay.stringFromDate(dateFromString!)
                    
                    cell.weekDayLabel.text = weekDayFromDate
                    
                    //Time
                    if let Eventtime = responseCustomerOrderArray[indexPath.row].valueForKey("eventTime") as? String{
                        if Eventtime == "TBD"{
                            eTime = "TBD"
                        }else{
                            //24hr format
                            let timeSlot = Eventtime
                            let dateFormatterForTime = NSDateFormatter()
                            eTime = timeSlot
//                            dateFormatterForTime.dateFormat = "hh:mma"
//                            let dateFromString = dateFormatterForTime.dateFromString(timeSlot)
//                            
//                            
//                            
//                            let dateFormatteerForTimeFormat = NSDateFormatter()
//                            
//                            dateFormatteerForTimeFormat.dateFormat = "HH:mm"
//                            
//                            eTime = dateFormatteerForTimeFormat.stringFromDate(dateFromString!)
                        }
                    }
                    else{
                        eTime = "12:00AM"
                    }
                    
                }
                
            }

            //venue Address
         
            var eVenue = ""
            var eCity = ""
            var eState = ""
         
            if let Eventvenue = responseCustomerOrderArray[indexPath.row].valueForKey("venueName") as? String{
                eVenue = Eventvenue
            }
            if let Eventcity = responseCustomerOrderArray[indexPath.row].valueForKey("state") as? String{
                eCity = Eventcity
            }
            if let EventState = responseCustomerOrderArray[indexPath.row].valueForKey("country") as? String{
                eState = EventState
            }
            let string1 = eTime
            let resultStr = "\(string1)"+"\("-")" + " \(eVenue)" + "\(" ")" + "\(eCity)" + "\(" ")" + "\(eState)"
            
            cell.eventTimeAndAddressLabel.text = resultStr
            
            if let ticketQtyStr = responseCustomerOrderArray[indexPath.row].valueForKey("qty") as? Int{
                cell.priceLabel.text = "\("Qty:")" + "\(ticketQtyStr)" + "\(" View")"
            }
           
        }
        
        
        // *** change color of cell *** //
        if indexPath.row % 2 != 0 {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
        } else {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        }
        return cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PreOrderConfirmation") as! PreOrderConfirmationViewController
//         self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:-GetTicketsApi
    func getTicketsDetailApi(){
        
        // *** api call for AllArtist ***
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for validations
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // ** start activityIndicator
        //        self.setLoadingIndicator()
        
        // *** send parameters
        let getParam = [
            "productType" : productType,
            "customerId":customerId,
            "configId":configId
        ]
        print(getParam)
        // **** call webservice
        WebServiceCall.sharedInstance.GetActiveEventsCustomerOrders("GetActiveEventsCustomerOrders.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            //  * stop activityIndicator
            self.stopIndicator()
            
            // ** check for valid result
            if isTrue == true {
                self.tableView.hidden = false
                print("dict IS:",responseDisctionary)
                if let resCustomerorderArray = responseDisctionary!.valueForKey("customerOrders") as? NSArray  {
                  print(resCustomerorderArray)
                self.responseCustomerOrderArray = resCustomerorderArray
                    
                }
                self.tableView.reloadData()
                
            } else {
                
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
        
    }
}
