//
//  SignUpViewController.swift
//  RewardtheFan
//
//  Created by Anmol Rajdev on 30/03/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import CoreLocation

class SignUpViewController: BaseViewController {

    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var nameTextField: UITextField!
    
    @IBOutlet var emailTextField: UITextField!
    
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var skipThisStepLabel: UILabel!
    
    @IBOutlet var checkBoxImageView: UIImageView!
    
    @IBOutlet var termsAndConditionsTextView: UITextView!
    
    @IBOutlet var checkBoxButton: UIButton!
    
    @IBOutlet var passwordButton: UIButton!
    
    @IBOutlet var backButton: UIButton!
    
    //MARK:-
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // *** Underline Skip this step ***
        let text1 = NSMutableAttributedString.init(attributedString: skipThisStepLabel.attributedText!)
        text1.addAttribute(NSUnderlineStyleAttributeName, value: Int(1), range: NSMakeRange(0, (skipThisStepLabel.text?.characters.count)!))
        skipThisStepLabel.attributedText = text1
        
        // *** textview changes ***
        let text = NSMutableAttributedString.init(attributedString: termsAndConditionsTextView.attributedText!)
        
        // *** changes for iPad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            
            skipThisStepLabel.font = UIFont(name: skipThisStepLabel.font.fontName, size: 25)

            if #available(iOS 8.2, *) {
                text.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(25, weight: 0), range: NSMakeRange(0, 31))
                text.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(25, weight: 0.3), range: NSMakeRange(31, 15))
                text.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(25, weight: 0), range: NSMakeRange(46, 5))
                text.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(25, weight: 0.3), range: NSMakeRange(51, 14))
            } else {
                // Fallback on earlier versions
            }
            termsAndConditionsTextView.attributedText = text
            backButton.imageEdgeInsets = UIEdgeInsetsMake(18, 18, 18, 18);
        } else {
            backButton.imageEdgeInsets = UIEdgeInsetsMake(13, 13, 13, 13);
            if #available(iOS 8.2, *) {
                text.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(15, weight: 0.3), range: NSMakeRange(31, 15))
                text.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(15, weight: 0.3), range: NSMakeRange(51, 14))
            } else {
                // Fallback on earlier versions
            }
            termsAndConditionsTextView.attributedText = text
        }
        
        // *** tap gesture ***
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.resignKeyBoard))
        self.view.addGestureRecognizer(tapRecognizer)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.resignKeyBoard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- code to resign keyboard
    //MARK:-
    
    // *** tap gesture action ***
    func resignKeyBoard() {
        
        self.nameTextField.resignFirstResponder()
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    // *** touch events action ***
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.nameTextField.resignFirstResponder()
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }

    //MARK:- Button actions
    //MARK:-
    
    // *** cross button action ***
    @IBAction func crossButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    // *** show password button action ***
    @IBAction func passwordButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        if passwordButton.selected {
            
            passwordButton.selected = false
            passwordButton.setTitle("SHOW", forState: .Normal)
            passwordTextField.secureTextEntry = true
        } else {
            
            passwordButton.selected = true
            passwordButton.setTitle("HIDE", forState: .Normal)
            passwordButton.setTitleColor(UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0), forState: .Selected)
            passwordTextField.secureTextEntry = false
        }
    }
    
    // *** check box action ***
    @IBAction func checkBoxButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        if self.checkBoxButton.selected {
            
            self.checkBoxButton.selected = false
            self.checkBoxImageView.image = UIImage(named: "uncheck")
        } else {
            
            self.checkBoxButton.selected = true
            self.checkBoxImageView.image = UIImage(named: "checked")
        }
    }
    
    // *** signUp button action ***
    @IBAction func signUpButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        if checkBoxButton.selected == false {
            
            let alert: UIAlertController = UIAlertController(title:"Reward The Fan", message:"You must agree to terms to proceed further", preferredStyle: .Alert)
            
            //Create and add first option action
            let postAction: UIAlertAction = UIAlertAction(title: "Ok", style: .Default) { action -> Void in
                //Code for launching the camera goes here
            }
            alert.addAction(postAction)
            //Present the AlertController
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            
            // * check for validations
            if emailTextField.text == "" && passwordTextField.text == "" && nameTextField.text == ""{
                
                alertVc.message = allfeildsAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            }  else  if nameTextField.text == "" {
                
                alertVc.message = nameAlertmsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else  if emailTextField.text == "" {
                
                alertVc.message = emailAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else  if passwordTextField.text == "" {
                
                alertVc.message = passwordAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else if isValidEmail(emailTextField.text!) == false {
                
                alertVc.message = validEmailAlertmsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else if passwordTextField.text!.characters.count < 8 || passwordTextField.text!.characters.count > 20 {
                
                alertVc.message = rangePwAlertmsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else if isValidPassword(passwordTextField.text!) == false {
                
                alertVc.message = validPwAlertMsg
                self.presentViewController(alertVc, animated: true, completion: nil)
            } else {
                
                // *** api call for sign Up ***
                
                // * check for internet connection
                guard IJReachability.isConnectedToNetwork() else {
                    
                    alertVc.message = ErrorInternetConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                    return
                }
                
                // ** start activityIndicator
                self.setLoadingIndicator(self.view)
                
                // *** send parameters
                
                let getParam = [
                    
                    "configId" : configId,
                    "productType" : productType,
                    "name" : nameTextField.text!,
                    "email" : emailTextField.text!,
                    "password" : passwordTextField.text!,
                    "signUpType" : "REWARDTHEFAN",
                    "platForm" : platForm,
                    "deviceId" : deviceId,
                    "socialAccountId" : "",
                    "fbAccessToken" : ""
                ]
                
                // **** call webservice
                WebServiceCall.sharedInstance.signUp("CustomerRegistration.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                    
                    // * stop activityIndicator
                    self.stopIndicator()
                    
                    // ** check for valid result
                    if isTrue == true {
                        
                        if let customerDetail = responseDisctionary?.objectForKey("customerDetails") as? NSMutableDictionary {
                            if let customer = customerDetail.objectForKey("customer") as? NSMutableDictionary {
                                
                                // * get customer id
                                if let customerId = customer.objectForKey("id") as? NSInteger {
                                    NSUserDefaults.standardUserDefaults().setInteger(customerId, forKey: "customerId")
                                }
                                // * get customer name
                                if let customerName = customer.objectForKey("customerName") as? String {
                                    NSUserDefaults.standardUserDefaults().setValue(customerName, forKey: "customerName")
                                }

                                // * is first time login
                                if let isFirstTimeLogin = customer.objectForKey("firstTimeLogin") as? String {
                                    NSUserDefaults.standardUserDefaults().setObject(isFirstTimeLogin, forKey: "firstTimeLogin")
                                    
                                    // * navigate to next screens according to values
                                    if isFirstTimeLogin == "Yes" {
                                        if (CLLocationManager.locationServicesEnabled()) {
                                            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        } else {
                                            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SuperFanPageLocation") as! SuperFanPageLocationViewController
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    } else {
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as! HomeViewController
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                    // to save reward points
                                    if let customerLoyality = customerDetail.objectForKey("customerLoyalty") as? NSMutableDictionary {
                                        print(customerLoyality)
                                        NSUserDefaults.standardUserDefaults().setObject(customerLoyality, forKey: "customerLoyalty")
                                    }
                                } else {
                                    alertVc.message = ErrorServerConnection
                                    self.presentViewController(alertVc, animated: true, completion: nil)
                                }
                            } else {
                                
                                alertVc.message = ErrorServerConnection
                                self.presentViewController(alertVc, animated: true, completion: nil)
                            }
                        } else {
                            
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    } else {
                        
                        if message == "" {
                            // * if no response
                            
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        } else {
                            // * if error in the response
                            
                            alertVc.message = message!
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    }
                })
            }
        }
    }
    
    // *** signIn button action ***
    @IBAction func signInButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SignIn") as! SignInViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // *** skip this step button action ***
    @IBAction func skipThisStepButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Textfield delegate method
    //MARK:-
    
    func textField(textField: UITextField!, shouldChangeCharactersInRange
        range: NSRange, replacementString string: String!) -> Bool {
        
        if textField == passwordTextField {
            
            if string == " " {
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    //MARK:- validations
    //MARK:-
    
    // *** for valid email ***
    override func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    // *** validation for password *** 
    func isValidPassword(testStr:String) -> Bool {
        
        let numberRegEx  = ".*[A-Za-z]+[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluateWithObject(testStr)
        
        let specialCharacterRegEx  = ".*[A-Za-z]+[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluateWithObject(testStr)
        
        return numberresult || specialresult
    }

}
