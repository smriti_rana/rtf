//
//  ForgotPasswordViewController.swift
//  RewardtheFan
//
//  Created by Anmol Rajdev on 29/03/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {

    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var emailTextField: MKTextField!
    
    @IBOutlet var backButton: UIButton!
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // *** changes for iPad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            emailTextField.font = UIFont(name: emailTextField.font!.fontName, size: 25)
            backButton.imageEdgeInsets = UIEdgeInsetsMake(18, 18, 18, 18);
        } else {
            backButton.imageEdgeInsets = UIEdgeInsetsMake(13, 13, 13, 13);
        }
        
        // *** tap gesture ***
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForgotPasswordViewController.resignKeyBoard))
        self.view.addGestureRecognizer(tapRecognizer)
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.resignKeyBoard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- code to resign keyboard
    //MARK:-
    
    // *** tap gesture action ***
    func resignKeyBoard() {
        self.emailTextField.resignFirstResponder()
    }
    
    // *** touch events action ***
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.emailTextField.resignFirstResponder()
    }
    
    //MARK:- Button actions
    //MARK:-
    
    // *** cross button action ***
    @IBAction func crossButtonTapped(sender: AnyObject) {
        self.resignKeyBoard()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // *** send password button action ***
    @IBAction func sendPasswordButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for validations
        if emailTextField.text == "" {
            
            alertVc.message = emailAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if isValidEmail(emailTextField.text!) == false {
            
            alertVc.message = validEmailAlertmsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        else
        {
            // *** api call for forgot password ***
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            // ** start activityIndicator
            self.setLoadingIndicator(self.view)
            
            // *** send parameters
            let getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "email" : emailTextField.text!,
                
            ]
            
            // **** call webservice
            WebServiceCall.sharedInstance.resetPassword("ResetPasswordRequest.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                // * stop activityIndicator
                self.stopIndicator()
                
                // ** check for valid result
                if isTrue == true {
                    
                    alertVc.message = "Email sent to reset password"
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    
                    if message == "" {
                        // * if no response
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    // *** signUp button action ***
    @IBAction func signUpButtonTapped(sender: AnyObject) {
        
        resignKeyBoard()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SignUp") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // *** signIn button action ***
    @IBAction func signInButtonTapped(sender: AnyObject) {
        
        resignKeyBoard()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
 
}
