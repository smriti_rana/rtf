//
//  LoginScreenViewController.swift
//  RewardtheFan
//
//  Created by Anmol Rajdev on 29/03/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import AVFoundation
import Social
import FBSDKShareKit
import AddressBookUI
import CoreLocation

// *** theme color for toast
//let ThemeColor   = UIColor(colorLiteralRed: 226.0/255.0, green: 226.0/255.0, blue: 226.0/255.0, alpha: 1.0)
let ThemeColor   = UIColor.blackColor()
let fontColor = UIColor.whiteColor()

class LoginScreenViewController: BaseViewController, FacebookDelegate, GIDSignInUIDelegate, GIDSignInDelegate {

    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var skipThisStepLabel: UILabel!
    
    var presentWindow : UIWindow?

    var isSocialLogin = false
    
    var likesFromFacebook : NSMutableArray = []
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // *** For Toast message ***
        title = "Swift Toast"
        edgesForExtendedLayout = .None
        UIView.hr_setToastThemeColor(color: ThemeColor)
        UIView.hr_setToastFontColor(color: fontColor)
        presentWindow = UIApplication.sharedApplication().keyWindow
        
        // *** Underline Skip this step ***
        let text = NSMutableAttributedString.init(attributedString: skipThisStepLabel.attributedText!)
        text.addAttribute(NSUnderlineStyleAttributeName, value: Int(1), range: NSMakeRange(0, (skipThisStepLabel.text?.characters.count)!))
        skipThisStepLabel.attributedText = text
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            skipThisStepLabel.font = UIFont(name: "Lato-Light", size: 25)
        } else {
            skipThisStepLabel.font = UIFont(name: "Lato-Light", size: 17)
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Button actions
    //MARK:-
    
    // *** sign in button action ***
    @IBAction func signInButtonTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SignIn") as! SignInViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // *** sign up button action ***
    @IBAction func signUpButtonTapped(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SignUp") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // *** Facebook login button action ***
    @IBAction func signInWithFacebookButtonTapped(sender: AnyObject) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** check internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        setLoadingIndicator(self.view)
        facebook()
    }
    
    // *** google login button action ***
    @IBAction func signInWithGoogleButtonTapped(sender: AnyObject) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** check internet connection
        guard IJReachability.isConnectedToNetwork() else {
            
            alertVc.message = ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        setLoadingIndicator(self.view)
        googleLogin()
    }
    
    // *** Skip this step button action ***
    @IBAction func skipThisStepButtonTapped(sender: AnyObject) {
        if (CLLocationManager.locationServicesEnabled()) {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SuperFanPageLocation") as! SuperFanPageLocationViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- Toast messages settings
    //MARK:-
    
    // handle events
    func handleSingleToastClicked(sender: UIButton) {
        presentWindow!.makeToast(message: sender.titleForState(.Normal)!)
    }
    
    func handleTitleToastClicked(sender: UIButton) {
        view.makeToast(message: sender.titleForState(.Normal)!, duration: 2, position: HRToastPositionTop, title: "<Title>")
    }
    
    func handleImageToastClicked(sender: UIButton) {
        let image = UIImage(named: "swift-logo.png")
        presentWindow!.makeToast(message: sender.titleForState(.Normal)!, duration: 2, position: "center", title: "Image!", image: image!)
    }
    
    func showActivity() {
        presentWindow!.makeToastActivity()
    }
    
    func showActivityWithMessage() {
        presentWindow!.makeToastActivity(message: "Loading...")
    }
    
    func hideActivity() {
        presentWindow!.hideToastActivity()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        presentWindow!.hideToastActivity()
    }
    
    // ui helper
    func quickAddButtonWithTitle(title: String, target: AnyObject!, action: Selector) -> UIButton {
        let ret = UIButton(type: .Custom)
        ret.setTitle(title, forState: .Normal)
        ret.setTitleColor(ThemeColor, forState: .Normal)
        ret.addTarget(target, action: action, forControlEvents: .TouchUpInside)
        return ret
    }
    
    //MARK:- Facebook
    //MARK:-
    
    // *** facebook method call ***
    func facebook() {
        isSocialLogin = true
        Facebook.sharedInstance.delegate = self
        Facebook.sharedInstance.facebookSignUp(self)
    }
    
    // *** delegate method call ***
    func removeAfterGetInfoFromFacebook(email:String, firstName:String, lastName:String, token:String, tokenSecret:String , verifier:String, visitorId:String, image:String, name:String) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // *** API call to get user likes and events ***
        let request: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "likes,events"], HTTPMethod: "GET")
        request.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
            if(error == nil) {
                
                //print("Result \(result)")
                
                // * get likes from the result
                if let likesResult = result.objectForKey("likes")?.objectForKey("data") as? NSMutableArray {
                    
                    // ** get the artists names from likes
                    let artistNames : NSMutableArray = []
                    var i : Int = 0
                    while i < likesResult.count {
                        
                        artistNames.addObject((likesResult.objectAtIndex(i) as! NSDictionary).objectForKey("name")! as! String)
                        i += 1
                    }
                    
                    // call API
                    if artistNames.count != 0 {
                        
                        // * create arstist names as comma seperated string
                        let artistFromFacebook : String = artistNames.componentsJoinedByString(",")
                        
                        // *** api call for get posible artist by social info ***
                        
                        // * check internet connection
                        guard IJReachability.isConnectedToNetwork() else {
                            
                            alertVc.message = ErrorInternetConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                            return
                        }
                        
                        // ** start activityIndicator
                        self.setLoadingIndicator(self.view)
                        
                        // *** send parameters
                        let getParam = [
                            
                            "configId" : self.configId,
                            "productType" : self.productType,
                            "artistKeys" : artistFromFacebook,
                            "customerId" : ""
                        ]
                        
                        // **** call webservice
                        WebServiceCall.sharedInstance.getPossibleArtistsBySocialInfo("GetPossibleArtistBySocialInfo.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                            
                            // * stop activityIndicator
                            self.stopIndicator()
                            // ** check for valid result
                            if isTrue == true {
                                
                                // * get the response dictionary
                                let artistList : NSDictionary = responseDisctionary!
                                
                                // ** get the artist from response dictionary
                                if let artists : NSArray = artistList.objectForKey("artists") as? NSArray {
                                    // *** get the artists names
                                    var i : Int = 0
                                    while i < artists.count {
                                        let artistDetail = artists.objectAtIndex(i) as! NSDictionary
                                        self.likesFromFacebook .addObject(artistDetail.objectForKey("name")! as! String)
                                        i += 1
                                    }
                                    // **** send paramters to login with facebook
                                    self.facebookLogin("", name: name, deviceToken: token, id : visitorId)
                                } else {
                                    // **** send paramters to login with facebook
                                    self.facebookLogin("", name: name, deviceToken: token, id : visitorId)
                                }
                            } else {
                                
                                // * if no response
                                if message == "" {
                                    
                                    alertVc.message = ErrorServerConnection
                                    self.presentViewController(alertVc, animated: true, completion: nil)
                                } else {
                                    // * if error in the response
                                    
                                    alertVc.message = message!
                                    self.presentViewController(alertVc, animated: true, completion: nil)
                                }
                                
                                // ** send paramters to login with facebook
                                self.facebookLogin(email, name: name, deviceToken: token, id : visitorId)
                            }
                        })
                    } else {
                        self.facebookLogin(email, name: name, deviceToken: token, id: visitorId)
                    }
                } else {
                    self.facebookLogin(email, name: name, deviceToken: token, id: visitorId)
                }
                
            } else {
                
                print("get likes error is ***** : \(error)")
                self.stopIndicator()
                
                // * send paramters to login with facebook
                self.facebookLogin(email, name: name, deviceToken: token, id : visitorId)
            }
        })
    }

    // *** delegate method call ***
    func removeAfterGetInfo(isSiguUp:Bool) {
        if isSiguUp == true {
            
        } else {
            
            let alertView = UIAlertView(title: "Error!", message: ErrorServerConnection, delegate: nil, cancelButtonTitle: "OK")
            alertView.show()
            stopIndicator()
        }
    }
    
    // *** login with facebook method ***
    func facebookLogin(emailId : String, name : String, deviceToken : String, id : String) -> Void {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        var email : String = emailId
        
        if email == "" {
            
            // * request paramters
            let getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "signUpType" : "FACEBOOK",
                "email" : email,
                "platForm" : platForm,
                "deviceId" : deviceId,
                "socialAccountId" : id,
                "fbAccessToken" : deviceToken
                
            ]
            
            // ** webservice call
            WebServiceCall.sharedInstance.facebookLoginValidator("SocialLoginValidator.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                self.stopIndicator()
                
                if isTrue == true {
                    
                    if let customerDetail = responseDisctionary?.objectForKey("customerDetails") as? NSMutableDictionary {
                        if let customer = customerDetail.objectForKey("customer") as? NSMutableDictionary {
                            print(customer)
                            // * get customer id
                            if let customerId = customer.objectForKey("id") as? NSInteger {
                                NSUserDefaults.standardUserDefaults().setInteger(customerId, forKey: "customerId")
                            }
                            // * get customer name
                            if let customerName = customer.objectForKey("customerName") as? String {
                                NSUserDefaults.standardUserDefaults().setValue(customerName, forKey: "customerName")
                                print(customerName)
                            }
                            
                            // * is first time login
                            if let isFirstTimeLogin = customer.objectForKey("firstTimeLogin") as? String {
                                NSUserDefaults.standardUserDefaults().setObject(isFirstTimeLogin, forKey: "firstTimeLogin")
                                
                                // * navigate to next screens according to values
                                if isFirstTimeLogin == "Yes" {
                                    if (CLLocationManager.locationServicesEnabled()) {
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    } else {
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SuperFanPageLocation") as! SuperFanPageLocationViewController
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                } else {
                                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as! HomeViewController
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                // to save reward points
                                if let customerLoyality = customerDetail.objectForKey("customerLoyalty") as? NSMutableDictionary {
//                                    print(customerLoyality)
                                    NSUserDefaults.standardUserDefaults().setObject(customerLoyality, forKey: "customerLoyalty")
                                }
                            } else {
                                
                                alertVc.message = ErrorServerConnection
                                self.presentViewController(alertVc, animated: true, completion: nil)
                            }
                        } else {
                            
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    } else {
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                } else {
                    
                    if message == "" {
                        // * if no response
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        
                        let alert: UIAlertController = UIAlertController(title:nil, message:fbloginAlertMsg, preferredStyle: .Alert)
                        
                        alert.addTextFieldWithConfigurationHandler({ (textfield : UITextField) in
                            textfield.placeholder = "email"
                        })
                        
                        let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .Default) { action -> Void in
                            //Code for launching the camera goes here
                            let test = alert.textFields! as NSArray
                            let emailTextField = test.objectAtIndex(0) as! UITextField
                            email = emailTextField.text!
                            self.facebookLogin(email , name: name, deviceToken: deviceToken, id: id)
                        }
                        alert.addAction(okAction)
                        //Present the AlertController
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        } else {
            
            // * request paramters
            let getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "name" : name,
                "email" : email,
                "password" : "",
                "signUpType" : "FACEBOOK",
                "platForm" : platForm,
                "deviceId" : deviceId,
                "socialAccountId" : id,
                "fbAccessToken" : deviceToken
            ]
            
            // ** webservice call
            WebServiceCall.sharedInstance.signUp("CustomerRegistration.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                self.stopIndicator()
                
                if isTrue == true {
                    
                    if let customerDetail = responseDisctionary?.objectForKey("customerDetails") as? NSMutableDictionary {
                        if let customer = customerDetail.objectForKey("customer") as? NSMutableDictionary {
                            
                            // * get customer id
                            if let customerId = customer.objectForKey("id") as? NSInteger {
                                NSUserDefaults.standardUserDefaults().setInteger(customerId, forKey: "customerId")
                            }
                            
                            // * is first time login
                            if let isFirstTimeLogin = customer.objectForKey("firstTimeLogin") as? String {
                                NSUserDefaults.standardUserDefaults().setObject(isFirstTimeLogin, forKey: "firstTimeLogin")
                                
                                // * navigate to next screens according to values
                                if isFirstTimeLogin == "Yes" {
                                    if (CLLocationManager.locationServicesEnabled()) {
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    } else {
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SuperFanPageLocation") as! SuperFanPageLocationViewController
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                } else {
                                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as! HomeViewController
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                
                                // to save reward points
                                if let customerLoyality = customerDetail.objectForKey("customerLoyalty") as? NSMutableDictionary {
                                    print(customerLoyality)
                                    NSUserDefaults.standardUserDefaults().setObject(customerLoyality, forKey: "customerLoyalty")
                                }
                            } else {
                                
                                alertVc.message = ErrorServerConnection
                                self.presentViewController(alertVc, animated: true, completion: nil)
                            }
                        } else {
                            
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    } else {
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                } else {
                    
                    if message == "" {
                        // * if no response
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    //MARK:- Google Signup
    
    // *** google login method call ***
    func googleLogin() {
        
        self.isSocialLogin = true
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }

    // *** google login delegate method call **
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
        withError error: NSError!) {
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
            if (error == nil) {
                // Perform any operations on signed in user here.
                let userId = user.userID                  // For client-side use only!
                let idToken = user.authentication.idToken // Safe to send to the server
                let name = user.profile.name
                let email = user.profile.email
                let image = user.profile.imageURLWithDimension(400*400)
                print(idToken, userId)
                print(email, name, image)
                
                // * pass paramters
                let getParam = [
                    
                    "configId" : configId,
                    "productType" : productType,
                    "name" : name,
                    "email" : email,
                    "password" : "",
                    "signUpType" : "GOOGLE",
                    "platForm" : platForm,
                    "deviceId" : deviceId,
                    "socialAccountId" : userId,
                    "fbAccessToken" : ""
                ]
                
                // ** API Call
                WebServiceCall.sharedInstance.signUp("CustomerRegistration.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                    
                    self.stopIndicator()
                    
                    if isTrue == true {
                        
                        if let customerDetail = responseDisctionary?.objectForKey("customerDetails") as? NSMutableDictionary {
                            if let customer = customerDetail.objectForKey("customer") as? NSMutableDictionary {
                                
                                // * get customer id
                                if let customerId = customer.objectForKey("id") as? NSInteger {
                                    NSUserDefaults.standardUserDefaults().setInteger(customerId, forKey: "customerId")
                                }
                                // * get customer name
                                if let customerName = customer.objectForKey("customerName") as? String {
                                    NSUserDefaults.standardUserDefaults().setValue(customerName, forKey: "customerName")
                                }

                                // * is first time login
                                if let isFirstTimeLogin = customer.objectForKey("firstTimeLogin") as? String {
                                    NSUserDefaults.standardUserDefaults().setObject(isFirstTimeLogin, forKey: "firstTimeLogin")
                                    
                                    // * navigate to next screens according to values
                                    if isFirstTimeLogin == "Yes" {
                                        if (CLLocationManager.locationServicesEnabled()) {
                                            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        } else {
                                            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SuperFanPageLocation") as! SuperFanPageLocationViewController
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    } else {
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as! HomeViewController
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                    // to save reward points
                                    if let customerLoyality = customerDetail.objectForKey("customerLoyalty") as? NSMutableDictionary {
                                        print(customerLoyality)
                                        NSUserDefaults.standardUserDefaults().setObject(customerLoyality, forKey: "customerLoyalty")
                                    }
                                } else {
                                    
                                    alertVc.message = ErrorServerConnection
                                    self.presentViewController(alertVc, animated: true, completion: nil)
                                }
                            } else {
                                
                                alertVc.message = ErrorServerConnection
                                self.presentViewController(alertVc, animated: true, completion: nil)
                            }
                        } else {
                            
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    } else {
                        
                        if message == "" {
                            // * if no response
                            
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        } else {
                            // * if error in the response
                            
                            alertVc.message = message!
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                    }
                })
            } else {
                
                // * if error
                self.stopIndicator()
                print("\(error.localizedDescription)")
            }
    }

    // *** google delgate method ***
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
        withError error: NSError!) {
            stopIndicator()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
        presentViewController viewController: UIViewController!) {
            self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
        dismissViewController viewController: UIViewController!) {
            self.dismissViewControllerAnimated(true, completion: nil)
    }

}
