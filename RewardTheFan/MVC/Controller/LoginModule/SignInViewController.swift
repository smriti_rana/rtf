//
//  SignInViewController.swift
//  RewardtheFan
//
//  Created by Anmol Rajdev on 30/03/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit
import CoreLocation

class SignInViewController: BaseViewController {

    //MARK:- Properties
    //MARK:-
    
    @IBOutlet var emailTextField: UITextField!
    
    @IBOutlet var checkBoxButton: UIButton!
    
    @IBOutlet var checkBoxImageView: UIImageView!
    
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var skipThisStepLabel: UILabel!
    
    @IBOutlet var passwordButton: UIButton!
    
    @IBOutlet var backButton: UIButton!
    
    //MARK:-
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // *** Underline Skip this step ***
        let text = NSMutableAttributedString.init(attributedString: skipThisStepLabel.attributedText!)
        text.addAttribute(NSUnderlineStyleAttributeName, value: Int(1), range: NSMakeRange(0, (skipThisStepLabel.text?.characters.count)!))
        skipThisStepLabel.attributedText = text
        
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            skipThisStepLabel.font = UIFont(name: skipThisStepLabel.font.fontName, size: 25)
            backButton.imageEdgeInsets = UIEdgeInsetsMake(18, 18, 18, 18);
        } else {
            backButton.imageEdgeInsets = UIEdgeInsetsMake(13, 13, 13, 13);
        }
        
        // *** tap gesture ***
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignInViewController.resignKeyBoard))
        self.view.addGestureRecognizer(tapRecognizer)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.resignKeyBoard()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- code to resign keyboard
    //MARK:-
    
    // *** tap gesture action ***
    func resignKeyBoard() {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    // *** touch events action ***
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    //MARK:- Button actions
    //MARK:-
    
    // *** cross button action ***
    @IBAction func crossButtonTapped(sender: AnyObject) {
        self.resignKeyBoard()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    // *** show password button action ***
    @IBAction func passwordButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        
        if passwordButton.selected {
            
            passwordButton.selected = false
            passwordButton.setTitle("SHOW", forState: .Normal)
            passwordTextField.secureTextEntry = true
        } else {
            
            passwordButton.selected = true
            passwordButton.setTitle("HIDE", forState: .Normal)
            passwordButton.setTitleColor(UIColor(colorLiteralRed: 39.0/255.0, green: 133.0/255.0, blue: 250.0/255.0, alpha: 1.0), forState: .Selected)
            passwordTextField.secureTextEntry = false
        }
    }
    
    // *** keep me logged in button action ***
    @IBAction func keepMeLoggedInButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        if self.checkBoxButton.selected {
            
            self.checkBoxButton.selected = false
            self.checkBoxImageView.image = UIImage(named: "uncheck")
        } else {
            
            self.checkBoxButton.selected = true
            self.checkBoxImageView.image = UIImage(named: "checked")
        }
    }
    
    // *** forgot password button action ***
    @IBAction func forgotPasswordButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ForgotPassword") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // *** signIn button action ***
    @IBAction func signInButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        // * check for validations
        if emailTextField.text == "" && passwordTextField.text == ""{
            
            alertVc.message = allfeildsAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else  if emailTextField.text == "" {
            
            alertVc.message = emailAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else  if passwordTextField.text == "" {
            
            alertVc.message = passwordAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else if isValidEmail(emailTextField.text!) == false {
            
            alertVc.message = validEmailAlertmsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else {
            // *** api call for sign in ***
            
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
            
            // ** start activityIndicator
            self.setLoadingIndicator(self.view)
            
            // *** send parameters
            let getParam = [
                
                "configId" : configId,
                "productType" : productType,
                "userName" : emailTextField.text!,
                "password" : passwordTextField.text!,
                "platForm" : platForm,
                "deviceId" : deviceId
                
            ]
            
            // **** call webservice
            
            WebServiceCall.sharedInstance.signIn("CustomerLogin.json", params: getParam, oncompletion: { (isTrue, message, responseDisctionary) -> Void in
                
                // * stop activityIndicator
                self.stopIndicator()
                
                // ** check for valid result
                if isTrue == true {
                    print(responseDisctionary)
                    if let customerDetail = responseDisctionary?.objectForKey("customerDetails") as? NSMutableDictionary {
                        if let customer = customerDetail.objectForKey("customer") as? NSMutableDictionary {
                            print(customer)
                            // * get customer name
                            if let customerName = customer.objectForKey("customerName") as? String {
                                NSUserDefaults.standardUserDefaults().setValue(customerName, forKey: "customerName")
                            }
                            // * get customer id
                            if let customerId = customer.objectForKey("id") as? NSInteger {
                                NSUserDefaults.standardUserDefaults().setInteger(customerId, forKey: "customerId")
                            }
                            
                            // * is first time login
                            if let isFirstTimeLogin = customer.objectForKey("firstTimeLogin") as? String {
                                NSUserDefaults.standardUserDefaults().setObject(isFirstTimeLogin, forKey: "firstTimeLogin")
                                
                                // * navigate to next screens according to values
                                if isFirstTimeLogin == "Yes" {
                                    if (CLLocationManager.locationServicesEnabled()) {
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    } else {
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SuperFanPageLocation") as! SuperFanPageLocationViewController
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                } else {
                                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as! HomeViewController
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                // to save reward points
                                if let customerLoyality = customerDetail.objectForKey("customerLoyalty") as? NSMutableDictionary {
                                    print(customerLoyality)
                                     NSUserDefaults.standardUserDefaults().setObject(customerLoyality, forKey: "customerLoyalty")
                                }
                            } else {
                                alertVc.message = ErrorServerConnection
                                self.presentViewController(alertVc, animated: true, completion: nil)
                            }
                        } else {
                            
                            alertVc.message = ErrorServerConnection
                            self.presentViewController(alertVc, animated: true, completion: nil)
                        }
                      
                    } else {
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                } else {
                    
                    if message == "" {
                        // * if no response
                        
                        alertVc.message = ErrorServerConnection
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    } else {
                        // * if error in the response
                        
                        alertVc.message = message!
                        self.presentViewController(alertVc, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    // *** signUp button action ***
    @IBAction func signUpButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SignUp") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // *** skip this step button action ***
    @IBAction func skipThisStepButtonTapped(sender: AnyObject) {
        
        self.resignKeyBoard()
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChooseFavorites") as! ChooseFavoritesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Textfield delegate method
    //MARK:-
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == passwordTextField) {
            if string == " " {
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    //MARK:- validations
    //MARK:-
    
    // *** for valid email ***
    override func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
}
