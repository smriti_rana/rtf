// based on : http://stackoverflow.com/questions/24752627/accessing-ios-address-book-with-swift-array-count-of-zero

// http://stackoverflow.com/questions/24752627/accessing-ios-address-book-with-swift-array-count-of-zero

import APAddressBook

func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef? {
    if let ab = abRef {
        return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
    }
    return nil
}

func testAddressBook() {
    if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.NotDetermined) {
        print("requesting access...")
        var errorRef: Unmanaged<CFError>? = nil
        let addressBook = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        ABAddressBookRequestAccessWithCompletion(addressBook, { success, error in
            if success {
                // prrane: add a call back some time later
                //                    ABAddressBookRegisterExternalChangeCallback(addressBook, callback: (addressBook:ABAddressBookRef, info:CFDictionaryRef) -> , context: UnsafeMutablePointer<()>)
                getContactNames()
            }
            else {
                print("error")
            }
        })
    }
    else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Denied || ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Restricted) {
        print("access denied")
    }
    else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Authorized) {
        print("access granted")
        getContactNames()
    }
}

func getContactNames() {
    
    var errorRef: Unmanaged<CFError>?
    let addressBook = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
    let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
    print("records in the array \(contactList.count)")
    
    for record:ABRecordRef in contactList {
        //if record != nil {
        let contactPerson: ABRecordRef = record
        print(record)
        
        if  let emailProperty:ABMultiValueRef = ABRecordCopyValue(record, kABPersonEmailProperty).takeRetainedValue() as ABMultiValueRef {
            if ABMultiValueGetCount(emailProperty) > 0 {
                
                if let allEmailIDs:NSArray  = ABMultiValueCopyArrayOfAllValues(emailProperty).takeUnretainedValue() as NSArray {
                    for email in allEmailIDs {
                        let emailID = email as! String
                        print ("contactEmail : \(emailID) :=>")
                    }
                }
                
                if let contactFirstName = ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty).takeRetainedValue() as? NSString {
                    print ("\t\t contactFirstName : \(contactFirstName)")
                }
                if let contactLastName = ABRecordCopyValue(contactPerson, kABPersonLastNameProperty).takeRetainedValue() as? NSString {
                    print ("\t\t contactLastName : \(contactLastName)")
                }
            }
        }
    }
}