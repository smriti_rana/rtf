//
//  BillingInfoViewController.swift
//  RewardTheFanFinal
//
//  Created by Anmol Rajdev on 13/05/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit

class BillingInfoViewController: BaseViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    @IBOutlet weak var bilingTitleLbl: UILabel!
 
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var navbarHeight: NSLayoutConstraint!
    var viewTypeForLblTitle :String = ""
     var billingAddressId :Int = 0
    var shippingArrayFromView:NSDictionary = [:]
    var selectstateType  = ""
    var selectcountryType  = ""
    var pickerTypeIs = false
    var stateArray = []
    var countryArray = []
    var countryNameArray = []
    var stateNameArray  = []
    var stateIdIs : Int = 0
     var countryIdIs : Int = 0
    var countryTapped = false
    var currentIndex :Int = 0
    var billingAddArray:NSMutableArray = []
    var alreadyBillingAdd =  false
    var addressTypeIs = ""
   
    @IBOutlet weak var picker: UIPickerView!
  
    @IBOutlet weak var phoneNumberTf: UITextField!
    @IBOutlet weak var countryPickerBgView: UIView!
   
    @IBOutlet weak var zipcodeTf: UITextField!
   
    @IBOutlet weak var countryTf: UITextField!
    @IBOutlet weak var stateTf: UITextField!
    @IBOutlet weak var cityTf: UITextField!
    @IBOutlet weak var AddressTf: UITextField!
    @IBOutlet weak var LastTf: UITextField!
    @IBOutlet weak var FirstTf: UITextField!
    //MARK:-didLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            bilingTitleLbl.font = UIFont(name: bilingTitleLbl.font.fontName, size: 32)
            navbarHeight.constant = 84
        }
        
        //getCustomerId
        if NSUserDefaults.standardUserDefaults().objectForKey("customerId") != nil {
            customerId = (NSUserDefaults.standardUserDefaults().objectForKey("customerId") as? Int)!
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BillingInfoViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //Set title Heading
        if viewTypeForLblTitle == "Shipping"
        {
            bilingTitleLbl.text = "Shipping Information"
            addressTypeIs = "SHIPPING_ADDRESS"
            setDataFromShippingToUpdate(shippingArrayFromView)
            
        }
        else{
            bilingTitleLbl.text = "Billing Information"
            addressTypeIs = "BILLING_ADDRESS"
            //GetBillingInfo Api
            setLoadingIndicator(self.view)
            GetCustomerApi()
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("countryList") != nil {
            countryArray = (NSUserDefaults.standardUserDefaults().objectForKey("countryList") as? NSArray)!
           
            
        }
        countryNameArray = (countryArray.valueForKey("countryName") as? NSArray)!
        print(countryNameArray)
        
       
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BillingInfoViewController.pushViewController), name:"alertMessageBilling", object: nil)
       
    }
    //MARK:- AlertOk clicked
    func pushViewController() {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    //MARK:-
    //Calls this function when the tap is recognized.
        func dismissKeyboard() {
            [FirstTf .resignFirstResponder()]
            [LastTf .resignFirstResponder()]
            [AddressTf .resignFirstResponder()]
            [cityTf .resignFirstResponder()]
            [stateTf .resignFirstResponder()]
            [countryTf .resignFirstResponder()]
            [zipcodeTf .resignFirstResponder()]
            countryPickerBgView.hidden=true
          
            
        }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-CountryClicked
    @IBOutlet weak var countryBtn: UIButton!
    @IBAction func countryTapped(sender: AnyObject) {
        pickerTypeIs = true
        picker.reloadAllComponents()
        countryTapped = true
        removekeyBoard()
        if countryBtn.selected == true {
            countryPickerBgView.hidden=true
            picker.hidden=true
            
        }else{
            [self.countryTf .resignFirstResponder()]
            countryPickerBgView.hidden = false
            picker.hidden = false
            
        }
        picker.reloadAllComponents()
        
    }
    @IBOutlet weak var stateBtn: UIButton!
    @IBAction func stateTapped(sender: AnyObject) {
        picker.reloadAllComponents()
        pickerTypeIs = false
        removekeyBoard()
        if stateBtn.selected == true {
            countryPickerBgView.hidden=true
            picker.hidden=true
            
        }else{
            [self.stateTf .resignFirstResponder()]
            if countryTapped == true {
                self.countryPickerBgView.hidden = false
                self.picker.hidden = false
                picker.reloadAllComponents()
                
            }
            else{
                if  stateTf.text == "" {
                    self.countryPickerBgView.hidden = true
                    self.picker.hidden = true
                }
                else{
                    self.countryPickerBgView.hidden = false
                    self.picker.hidden = false
                    stateArray = (countryArray[currentIndex].valueForKey("stateList") as? NSArray)!
                    stateNameArray = (stateArray.valueForKey("name") as? NSArray)!
                    picker.reloadAllComponents()
                }
                
            }
            
        }
        
    }
    //MARK:-Picker Delegates
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if pickerTypeIs == false{
            return stateNameArray.count
        }else{
            return countryNameArray.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        currentIndex = row
        if pickerTypeIs == false{
            return stateNameArray[row] as? String
            
        }else{
            return countryNameArray[row] as? String
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if pickerTypeIs == false{
            selectstateType = (stateNameArray[pickerView.selectedRowInComponent(0)] as? String)!
        }else{
            
            stateArray = (countryArray[row].valueForKey("stateList") as? NSArray)!
            stateIdIs = (stateArray[currentIndex].valueForKey("id") as? Int)!
            stateNameArray = (stateArray.valueForKey("name") as? NSArray)!
            stateTf.text = (stateNameArray[0]) as? String
            
            selectcountryType = (countryNameArray[pickerView.selectedRowInComponent(0)] as? String)!
            countryTf.text = selectcountryType
            countryIdIs = countryArray.objectAtIndex(row).valueForKey("id") as! Int
            picker.reloadAllComponents()
        }
        
    }
    //MARK:-PickerDone or Cancel
    @IBOutlet weak var cnclPickerBtn: UIButton!
    @IBAction func pickerCnclTapped(sender: AnyObject) {
        [stateTf .resignFirstResponder()]
        [countryTf .resignFirstResponder()]
        countryPickerBgView.hidden = true
        
        
    }
    @IBOutlet weak var pickerDoneBtn: UIButton!
    @IBAction func pickerDoneTapped(sender: AnyObject) {
        if pickerTypeIs == false{
            [stateTf .resignFirstResponder()]
            stateTf.text = (stateNameArray[picker.selectedRowInComponent(0)] as? String)!
            stateIdIs = (stateArray[currentIndex].valueForKey("id") as? Int)!
            countryPickerBgView.hidden = true
        }
        else{
            print(currentIndex)
            [countryTf .resignFirstResponder()]
            countryPickerBgView.hidden=true
            countryTf.text = (countryNameArray[picker.selectedRowInComponent(0)] as? String)!
            stateArray = (countryArray[currentIndex].valueForKey("stateList") as? NSArray)!
            stateNameArray = (stateArray.valueForKey("name") as? NSArray)!
            print(stateNameArray)
            stateTf.text = (stateNameArray[0]) as? String
            countryIdIs = countryArray.objectAtIndex(currentIndex).valueForKey("id") as! Int
            print(countryIdIs)
            picker.reloadAllComponents()
            
            
        }
        
        
    }
    //MARK:- Textfeild Delegates
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        [stateTf .resignFirstResponder()]
        [countryTf .resignFirstResponder()]
        countryPickerBgView.hidden=true
        
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        //print(textField.text)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        //print(textField.text)
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string == "\n" {
            if textField.text != "" {
                textField.resignFirstResponder()
                
            }
            
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    //MARK:-Remove Keyboard
    func removekeyBoard(){
        FirstTf.resignFirstResponder()
        LastTf.resignFirstResponder()
        AddressTf.resignFirstResponder()
        cityTf.resignFirstResponder()
        zipcodeTf.resignFirstResponder()
    }
    //MARK:-BackTapped
    @IBOutlet weak var backBtn: UIButton!
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    //MARK:-Billing/Shipiing Add CustomerInfo
    func GetCustomerApi(){
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        
        // * check for internet connection
        guard IJReachability.isConnectedToNetwork() else {
            stopIndicator()
            alertVc.message = AlertMsgs.ErrorInternetConnection
            self.presentViewController(alertVc, animated: true, completion: nil)
            return
        }
        
        // *** send parameters
        let getParam = [
            "productType" : productType,
            "customerId":customerId,
            "platForm":platForm,
            "configId":configId,
            
            ]
        print(getParam)
        
        // **** call webservice
        WebServiceCall.sharedInstance.postCustomerInfoApiCall("GetCustomerInfo.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            if isTrue == true {
                self.stopIndicator()
                print("response for customer",responseDisctionary!)
                if let customerArray = responseDisctionary!.valueForKey("customer") as? NSDictionary{
                    if self.bilingTitleLbl.text == "Billing Information" {
                    if let billingArryIs = customerArray.valueForKey("billingAddress") as? NSArray{
                        
                        self.billingAddArray.removeAllObjects()
                        if billingArryIs.count > 0{
                            self.alreadyBillingAdd = true
                            for i in 0 ..< billingArryIs.count {
                                self.billingAddArray .addObject(billingArryIs[i])
                            }
                            print("billing address Array",self.billingAddArray)
                            //firstname
                            if let billingfirstnameStr = self.billingAddArray.objectAtIndex(0).valueForKey("firstName") as? String{
                                self.FirstTf.text = "\(billingfirstnameStr)"
                                if let billinglastnameStr = self.billingAddArray.objectAtIndex(0).valueForKey("lastName") as? String{
                                self.LastTf.text = billinglastnameStr

                                    
                                }
                                
                            }
                            //set Address
                            if let billingaddStreetStr = self.billingAddArray.objectAtIndex(0).valueForKey("addressLine1") as? String{
                                    self.AddressTf.text = billingaddStreetStr
                                
                            }
                            //set PhoneNumber
                            if let billingPhone = self.billingAddArray.objectAtIndex(0).valueForKey("phone1") as? String{
                                self.phoneNumberTf.text = billingPhone
                                
                            }
                            //set city/State
                            if let billingcityStr = self.billingAddArray.objectAtIndex(0).valueForKey("city") as? String{
                                self.cityTf.text = billingcityStr
                                if let billingstateStr = self.billingAddArray.objectAtIndex(0).valueForKey("stateName") as? String{
                                    self.stateTf.text =  "\(billingstateStr)"
                                    
                                }
                                if let billingStateArray = self.billingAddArray.objectAtIndex(0).valueForKey("state") as? NSDictionary{
                                    if let billingStateId = billingStateArray.valueForKey("id") as? Int{
                                         self.stateIdIs = Int(billingStateId)
                                    }
                                   
                                    
                                }

                                
                            }
                            // country/zipcode
                            if let billingcountryStr = self.billingAddArray.objectAtIndex(0).valueForKey("countryName") as? String{
                                self.countryTf.text = billingcountryStr
                                if let billingZipcodeStr = self.billingAddArray.objectAtIndex(0).valueForKey("zipCode") as? String{
                                    self.zipcodeTf.text = billingZipcodeStr
                                    
                                }
                                if let billingCountryArray = self.billingAddArray.objectAtIndex(0).valueForKey("country") as? NSDictionary{
                                    if let billingCountryId = billingCountryArray.valueForKey("id") as? Int{
                                        self.countryIdIs = Int(billingCountryId)
                                    }
                                    
                                    
                                }
                                
                            }
                            //set AddressId
                            if let billingId = self.billingAddArray.objectAtIndex(0).valueForKey("id") as? Int{
                                self.billingAddressId = billingId
                            }
                            
                            
                        }
                       
                        print("Billing address",self.billingAddArray)
                        
                    }
                }
                }
            } else {
                self.stopIndicator()
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
            
        })
    }
    //MARK:AddCustomer DetailApi
    func AddCustomerDetailApi(actionType:String){
         stopIndicator()
        let alertVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Alert") as! AlertViewController
        alertVc.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        // * check for validations
        if FirstTf.text == "" && LastTf.text == ""  && AddressTf.text == "" && cityTf.text == "" && stateTf.text == ""
            && countryTf.text == "" && zipcodeTf.text == ""{
            
            alertVc.message = allfeildsAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        } else  if FirstTf.text == "" {
            
            alertVc.message = firstnameAlertmsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        else  if LastTf.text == "" {
            
            alertVc.message = lastnameAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        else  if phoneNumberTf.text == "" {
            
            alertVc.message = phoneNoAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        else  if AddressTf.text == "" {
            
            alertVc.message = addressAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        else  if cityTf.text == "" {
            
            alertVc.message = cityAlertmsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        else  if stateTf.text == "" {
            
            alertVc.message = stateAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        else  if countryTf.text == "" {
            
            alertVc.message = countryAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
        else  if zipcodeTf.text == "" {
            
            alertVc.message = zipcodeAlertMsg
            self.presentViewController(alertVc, animated: true, completion: nil)
        }
            
        else{
            // * check for internet connection
            guard IJReachability.isConnectedToNetwork() else {
                stopIndicator()
                alertVc.message = ErrorInternetConnection
                self.presentViewController(alertVc, animated: true, completion: nil)
                return
            }
        
        let getParam = [
            
            "productType" : productType,
            "customerId" : customerId,
            "platForm" : platForm,
            "phone1":phoneNumberTf.text!,
            "configId" : configId,
            "addressType" : addressTypeIs,
            "firstName" : FirstTf.text!,
            "lastName" : LastTf.text!,
            "addressLine1" : AddressTf.text!,
            "city" : cityTf.text!,
            "state": stateIdIs,
            "country": countryIdIs,
            "postalCode": zipcodeTf.text!,
            "action": actionType,
            "addressId" : billingAddressId
            
        ]
        print(getParam)
        
        // **** call webservice
        WebServiceCall.sharedInstance.postAddCustomerInfoApiCall("AddCustomerAddress.json", params: getParam, oncompletion:{ (isTrue, message, responseDisctionary) -> Void in
            
            if isTrue == true {
                self.stopIndicator()
               
                if let customerArray = responseDisctionary!.valueForKey("customer") as? NSDictionary{
                    if let billingArryIs = customerArray.valueForKey("billingAddress") as? NSArray{
                        print(billingArryIs)
                        if self.bilingTitleLbl.text == "Billing Information" {
                        if self.alreadyBillingAdd == true{
                        alertVc.message = updateBillingAddAlertMsg
                             }else{
                               alertVc.message = addBillingAddAlertMsg
                            }
                        self.GetCustomerApi()
                        }
                       
                    
                    }
                    if let shippingArryIs = customerArray.valueForKey("shippingAddress") as? NSArray{
                        print(shippingArryIs)
                        if  self.bilingTitleLbl.text == "Shipping Information" {
                        if self.alreadyBillingAdd == true{
                        alertVc.message = updateShippingAlertMsg
                        }
                        else {
                            alertVc.message = addShippingAddAlertMsg
                        }
                        self.GetCustomerApi()
                        }
                        
                    }
                     self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
            else {
                self.stopIndicator()
                if message == "" {
                    // * if no response
                    alertVc.message = ErrorServerConnection
                    self.presentViewController(alertVc, animated: true, completion: nil)
                } else {
                    // * if error in the response
                    
                    alertVc.message = message!
                    self.presentViewController(alertVc, animated: true, completion: nil)
                }
            }
        })
        
        
    }
  
}
    //MARK:-SubmitTapped
    @IBOutlet var submitBtn: UIButton!
    @IBAction func submitTapped(sender: AnyObject) {
         setLoadingIndicator(self.baseView)
        if  alreadyBillingAdd == true {
            AddCustomerDetailApi("Update")
            self.alreadyBillingAdd = true
        }
        else{
            AddCustomerDetailApi("Save")
            self.alreadyBillingAdd = false
        }
    }
    //MARK:-Setdata From Shipping array from ShippingView To Update
    func setDataFromShippingToUpdate(arrayToSetData:NSDictionary){
        if arrayToSetData.count > 0{
        self.alreadyBillingAdd = true
        print("billing address Array",arrayToSetData)
        //firstname
        var billingnameStrIs = ""
        var billingaddphone = ""
        var billingcitySateIs  = ""
        var billingcountryZipcodeIs = ""
        
        if let billingfirstnameStr = arrayToSetData.valueForKey("firstName") as? String{
            billingnameStrIs = billingfirstnameStr
            if let billinglastnameStr = arrayToSetData.valueForKey("lastName") as? String{
                FirstTf.text = "\(billingnameStrIs)"
                LastTf.text = billinglastnameStr
                
            }
            
        }
        //set Address
        if let billingaddStreetStr = arrayToSetData.valueForKey("addressLine1") as? String{
            AddressTf.text = billingaddStreetStr
            
        }
        //set Phone
            if let billingaddPhone = arrayToSetData.valueForKey("phone1") as? String{
            billingaddphone = billingaddPhone
            phoneNumberTf.text = billingaddPhone
            
        }
        //set city/State
        if let billingcityStr = arrayToSetData.valueForKey("city") as? String{
            billingcitySateIs = billingcityStr
            if let billingstateStr = arrayToSetData.valueForKey("stateName") as? String{
                cityTf.text = "\(billingcityStr)"
                stateTf.text = "\(billingstateStr)"
                
            }
            
        }
        // country/zipcode
        if let billingcountryStr = arrayToSetData.valueForKey("countryName") as? String{
            billingcountryZipcodeIs = billingcountryStr
            if let billingZipcodeStr = arrayToSetData.valueForKey("zipCode") as? String{
                countryTf.text = "\(billingcountryZipcodeIs)"
                zipcodeTf.text =  "\(billingZipcodeStr)"
                
            }
            
        }
        //set AddressId
        if let billingId = arrayToSetData.valueForKey("id") as? Int{
            self.billingAddressId = billingId
        }
        ///## get Country/StateId ## to pass
        if let stateArrayIs = arrayToSetData.valueForKey("state") as? NSDictionary{
            if let countryId = stateArrayIs.valueForKey("countryId") as? Int{
                countryIdIs = countryId
                
            }
            if let stateId = stateArrayIs.valueForKey("id") as? Int{
                stateIdIs = stateId
                
            }
        }

        
        }
        
    }
}
