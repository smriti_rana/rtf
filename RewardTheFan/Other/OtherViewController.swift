//
//  OtherViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 14/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class OtherViewController: UIViewController {
 @IBOutlet weak var navbar_heigth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            navbar_heigth.constant = 74
        }  // Do any additional setup after loading the view.
    }
       @IBOutlet weak var backBtn: UIButton!
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
