
//
//  CustomBillingSheet.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 19/05/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
@objc protocol BillingActionsheetDelegate {
    optional func countryDelegateM(addId:String,index:Int,countryIdtoPass:Int,stateId : Int,getViewType:String,stateIdToPass:String,getArray:NSDictionary)
    optional func ShippingNewAddDelegate(getArray:NSArray, getIndex :Int)
    optional func getShippingInfoOnGridTapDelegate(getArray:NSDictionary)
    optional func morePushToPreToUpdateShippingAddress(getIndex:Int)
    
    
}
class CustomBillingSheet: UIView,UIPickerViewDelegate,UIPickerViewDataSource,UICollectionViewDataSource, UICollectionViewDelegate,billingSheetCollectionViewCellDelegate {
    
    @IBOutlet weak var shippingTitleLbl: UILabel!
    var ViewTypeIs = ""
    var addressIdToPass = ""
    var selectedItemIndex = 0
    var countryTapped = false
    @IBOutlet weak var shippingAddView: UIView!
    
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var addorUpdateAddLbl: UILabel!
    @IBOutlet weak var TitleLbl: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var stateTf: UITextField!
    var countryId : Int = 0
    var stateId : Int = 0
    var dictToPassApi  = [:]
    var countryNameArray  = []
    var stateNameArray  = []
    var stateArray = []
    var countryArray  = []
    
    
    var currentIndex :Int = 0
    var cellCount = 1
    @IBOutlet weak var phoneNumberTf: UITextField!
    var selectstateType  = ""
    var selectcountryType  = ""
    
    @IBOutlet weak var picker: UIPickerView!
    var pickerTypeIs = false
    @IBOutlet weak var countryPickerBgView: UIView!
    weak var delegate: BillingActionsheetDelegate?
    @IBOutlet weak var countryTf: UITextField!
    var predicate = NSPredicate()
    
    
    @IBOutlet weak var zipcodeTf: UITextField!
    @IBOutlet weak var stateBtn: UIButton!
    
    var keyBoardFlag = false
    var shippingArrayFromPre = []
    
    @IBOutlet weak var cityTf: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var lastNameTf: UITextField!
    @IBOutlet weak var firstNameTf: UITextField!
    @IBOutlet weak var AddAddView: UIView!
    @IBOutlet weak var BillingBgView: UIView!
    var countryDictis:NSMutableArray = []
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomBillingSheet.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomBillingSheet.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        
        if NSUserDefaults.standardUserDefaults().objectForKey("countryList") != nil {
            countryArray = (NSUserDefaults.standardUserDefaults().objectForKey("countryList") as? NSArray)!
            print(countryArray)
            
        }
        
        countryNameArray = (countryArray.valueForKey("countryName") as? NSArray)!
        //to remove Shipping/Billing View
        removeShippingCustomView()
        
    }
    
    //MARK:-Registering Cell
    func registerCustomCellForCollectionView(shippingArrayTosetData:NSArray){
        print(shippingArrayTosetData)
        shippingArrayFromPre = shippingArrayTosetData
        print("shipping Array Is",shippingArrayFromPre)
        collectionView.registerNib(UINib(nibName: "CustomBillingSheetCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.delegate = self
        collectionView.reloadData()
        
    }
    
    //MARK:-Country Tapped
    @IBOutlet weak var countryBtn: UIButton!
    @IBAction func countryTapped(sender: AnyObject) {
        picker.reloadAllComponents()
        pickerTypeIs = true
        countryTapped = true
        removekeyBoard()
        if countryBtn.selected == true {
            countryPickerBgView.hidden=true
            picker.hidden=true
            
        }else{
            [self.countryTf .resignFirstResponder()]
            countryPickerBgView.hidden = false
            picker.hidden = false
            
        }
        picker.reloadAllComponents()
        
    }
    @IBAction func stateTapped(sender: AnyObject) {
        picker.reloadAllComponents()
        pickerTypeIs = false
        removekeyBoard()
        if stateBtn.selected == true {
            countryPickerBgView.hidden=true
            picker.hidden=true
            
        }else{
            [self.stateTf .resignFirstResponder()]
            if countryTapped == true {
                self.countryPickerBgView.hidden = false
                self.picker.hidden = false
                picker.reloadAllComponents()
                
            }
            else{
                self.countryPickerBgView.hidden = true
                self.picker.hidden = true
            }
            
        }
        
    }
    //MARK:-Remove Keyboard
    func removekeyBoard(){
        firstNameTf.resignFirstResponder()
        lastNameTf.resignFirstResponder()
        phoneNumberTf.resignFirstResponder()
        addressTF.resignFirstResponder()
        cityTf.resignFirstResponder()
        stateTf.resignFirstResponder()
        zipcodeTf.resignFirstResponder()
        
    }
    //MARK:-Picker Delegates
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if pickerTypeIs == false{
            return stateNameArray.count
        }else{
            return countryNameArray.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        currentIndex = row
        if pickerTypeIs == false{
            return stateNameArray[row] as? String
            
        }else{
            return countryNameArray[row] as? String
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if pickerTypeIs == false{
            selectstateType = (stateNameArray[pickerView.selectedRowInComponent(0)] as? String)!
        }else{
            
            stateArray = (countryArray[row].valueForKey("stateList") as? NSArray)!
            stateId = (stateArray[currentIndex].valueForKey("id") as? Int)!
            stateNameArray = (stateArray.valueForKey("name") as? NSArray)!
            stateTf.text = (stateNameArray[0]) as? String
            
            selectcountryType = (countryNameArray[pickerView.selectedRowInComponent(0)] as? String)!
            countryTf.text = selectcountryType
            countryId = countryArray.objectAtIndex(row).valueForKey("id") as! Int
            picker.reloadAllComponents()
        }
        
    }
    //MARK:-PickerDone or Cancel
    @IBOutlet weak var cnclPickerBtn: UIButton!
    @IBAction func pickerCnclTapped(sender: AnyObject) {
        [stateTf .resignFirstResponder()]
        [countryTf .resignFirstResponder()]
        countryPickerBgView.hidden = true
        
        
    }
    @IBOutlet weak var pickerDoneBtn: UIButton!
    @IBAction func pickerDoneTapped(sender: AnyObject) {
        if pickerTypeIs == false{
            [stateTf .resignFirstResponder()]
            countryPickerBgView.hidden=true
            stateTf.text = (stateNameArray[picker.selectedRowInComponent(0)] as? String)!
            stateId = (stateArray[currentIndex].valueForKey("id") as? Int)!
        }
        else{
            print(currentIndex)
            [countryTf .resignFirstResponder()]
            countryPickerBgView.hidden=true
            countryTf.text = (countryNameArray[picker.selectedRowInComponent(0)] as? String)!
            stateArray = (countryArray[currentIndex].valueForKey("stateList") as? NSArray)!
            stateNameArray = (stateArray.valueForKey("name") as? NSArray)!
            print(stateNameArray)
            countryId = countryArray.objectAtIndex(currentIndex).valueForKey("id") as! Int
            print(countryId)
            stateId = (stateArray[currentIndex].valueForKey("id") as? Int)!
            picker.reloadAllComponents()
            
        }
        
    }
    //MARK:- Textfeild Delegates
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        [stateTf .resignFirstResponder()]
        [countryTf .resignFirstResponder()]
        countryPickerBgView.hidden=true
        
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        //print(textField.text)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        //print(textField.text)
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string == "\n" {
            if textField.text != "" {
                textField.resignFirstResponder()
                
            }
            
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.endEditing(true)
        return false
    }
    func keyboardWillShow(notification: NSNotification) {
        keyBoardFlag = true
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()) != nil {
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseOut, animations: {
                
                }, completion: { finished in
            })
            
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        keyBoardFlag = false
        UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseOut, animations: {
            
            }, completion: { finished in
                
        })
        
    }
    @IBOutlet weak var submitBtn: UIButton!
    @IBAction func submitTapped(sender: AnyObject) {
        var setViewTypeIs:String = ""
        print(addressIdToPass)
        var dictToPassApi:NSDictionary = [:]
        if ViewTypeIs == "moreTapped" {
            setViewTypeIs = "moreTapped"
            
        }
        else{
            setViewTypeIs = ViewTypeIs
        }
        if shippingArrayFromPre.count > 0 {
            if let dictToPass = shippingArrayFromPre[selectedItemIndex] as? NSDictionary{
                dictToPassApi = dictToPass
            }
            else{
                dictToPassApi = [:]
            }
        }
        
        
        self.delegate?.countryDelegateM!(addressIdToPass,index: selectedItemIndex,countryIdtoPass: countryId,stateId: stateId,getViewType: ViewTypeIs,stateIdToPass: addressIdToPass,getArray:dictToPassApi )
    }
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shippingArrayFromPre.count
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomBillingSheetCollectionViewCell
        cell.delegate = self
        cell.moreBtn.tag = indexPath.item
        cell.addInfoBtn.tag = indexPath.item
        selectedItemIndex = indexPath.item
        cell.backgroundColor = UIColor(colorLiteralRed: 236.0/255.0, green: 236.0/255.0, blue:236.0/255.0, alpha: 0.5)
        print("shipping Array To display",indexPath.item)
        if shippingArrayFromPre.count  > 0 {
            
            //firstname
            var shippingnameStrIs = ""
            var shippingaddStreetIs:String = ""
            var shippingcitySateIs:String = ""
            var shippingcountryZipcodeIs:String = ""
            
            if let billingfirstnameStr = shippingArrayFromPre.objectAtIndex(indexPath.item).valueForKey("firstName") as? String{
                shippingnameStrIs = billingfirstnameStr
                if let shippinglastnameStr = shippingArrayFromPre.objectAtIndex(indexPath.item).valueForKey("lastName") as? String{
                    cell.firstNameLbl.text = "\(shippingnameStrIs)" + "\(" ")" + "\(shippinglastnameStr)"
                    
                }
                
            }
            //set Address
            if let shippingaddStreetStr = shippingArrayFromPre.objectAtIndex(indexPath.item).valueForKey("addressLine1") as? String{
                shippingaddStreetIs = shippingaddStreetStr
                cell.Add_StreetLbl.text = shippingaddStreetStr
                
            }
            //set city/State
            if let shippingcityStr = shippingArrayFromPre.objectAtIndex(indexPath.item).valueForKey("city") as? String{
                shippingcitySateIs = shippingcityStr
                if let shippingstateStr = shippingArrayFromPre.objectAtIndex(indexPath.row).valueForKey("stateName") as? String{
                    cell.city_stateLbl.text = "\(shippingcityStr)" + "\(" ")" + "\(shippingstateStr)"
                    
                }
                
            }
            // country/zipcode
            if let shippingcountryStr = shippingArrayFromPre.objectAtIndex(indexPath.item).valueForKey("countryName") as? String{
                shippingcountryZipcodeIs = shippingcountryStr
                if let billingZipcodeStr = shippingArrayFromPre.objectAtIndex(indexPath.item).valueForKey("zipCode") as? String{
                    cell.country_zipcodelbl.text = "\(shippingcountryStr)" + "\(" ")" + "\(billingZipcodeStr)"
                    
                }
                
                
            }
            
        }
        return cell
    }
    // MARK: - UICollectionViewDelegate protocol
    
    //    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    //        // handle tap events
    //        print("You selected cell #\(indexPath.item)!")
    //        selectedItemIndex = indexPath.item
    //        print("You selected cell for array #\(shippingArrayFromPre[indexPath.item])!")
    //
    //
    //
    //    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            if shippingArrayFromPre.count > 0 {
                if shippingArrayFromPre.count == 1{
                    return CGSizeMake(self.collectionView.frame.width, self.collectionView.frame.height)
                }
            }
            return CGSizeMake(self.collectionView.frame.width/2, self.collectionView.frame.height)
            
        } else {
            return CGSizeMake(self.collectionView.frame.width, self.collectionView.frame.height)
        }
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            return UIEdgeInsetsMake(5, 15, 5, 15); //top,left,bottom,right
        } else {
            return UIEdgeInsetsMake(8, 8, 8, 8); //top,left,bottom,right
        }
    }
    
    //MARK:-Shipping NewAddress
    @IBOutlet weak var addnewBtn: UIButton!
    @IBAction func shippingAddNewAddressTapped(sender: AnyObject) {
        
        self.delegate!.ShippingNewAddDelegate!(shippingArrayFromPre,getIndex: selectedItemIndex)
        
        
        
    }
    //MARK:-from pre-Order Afteradding Shipping address
    func reloadcollectionView(getArray: NSArray){
        print(getArray.count)
        shippingArrayFromPre = getArray
        print("Added Customer is",shippingArrayFromPre)
        collectionView.reloadData()
        self.delegate!.getShippingInfoOnGridTapDelegate!(shippingArrayFromPre[selectedItemIndex] as! NSDictionary)
        
    }
    //MARK:-Remove AddView
    func removeShippingCustomView() {
        
        if AddAddView == nil || shippingAddView == nil{
            return
        }
        
        UIView.animateWithDuration(0.3, delay:0.0, options: .CurveEaseOut, animations: {
            
            }, completion: { finished in
                
                if self.AddAddView.hidden == false || self.shippingAddView.hidden == false {
                    self.keyBoardFlag = true
                }
                else{
                    self.keyBoardFlag = false
                    
                }
                
        })
    }
    //MARK:- Moretapped
    func moreUpdateShippingAddress(getIndex:Int) {
        selectedItemIndex = getIndex
        ViewTypeIs = "moreTapped"
        AddAddView.hidden = false
        shippingAddView.hidden = true
        selectedItemIndex = getIndex
        //firstname
        var shippingcitySateIs = ""
        var shippingSateIs = ""
        var shippingcountryZipcodeIs = ""
        var shippingZipcodeIs = ""
        
        if let billingfirstnameStr = shippingArrayFromPre.objectAtIndex(getIndex).valueForKey("firstName") as? String{
            firstNameTf.text = billingfirstnameStr
            if let shippinglastnameStr = shippingArrayFromPre.objectAtIndex(getIndex).valueForKey("lastName") as? String{
                lastNameTf.text = shippinglastnameStr
                
            }
            
        }
        //set Address
        if let shippingaddStreetStr = shippingArrayFromPre.objectAtIndex(getIndex).valueForKey("addressLine1") as? String{
            addressTF.text = shippingaddStreetStr
            
            
        }
        //set city/State
        if let shippingcityStr = shippingArrayFromPre.objectAtIndex(selectedItemIndex).valueForKey("city") as? String{
            shippingcitySateIs = shippingcityStr
            if let shippingstateStr = shippingArrayFromPre.objectAtIndex(getIndex).valueForKey("stateName") as? String{
                shippingSateIs = shippingstateStr
                cityTf.text = shippingcityStr
                stateTf.text = shippingSateIs
                
            }
            
        }
        // country/zipcode
        if let shippingcountryStr = shippingArrayFromPre.objectAtIndex(selectedItemIndex).valueForKey("countryName") as? String{
            shippingcountryZipcodeIs = shippingcountryStr
            if let billingZipcodeStr = shippingArrayFromPre.objectAtIndex(getIndex).valueForKey("zipCode") as? String{
                shippingZipcodeIs = billingZipcodeStr
                countryTf.text = shippingcountryZipcodeIs
                zipcodeTf.text = shippingZipcodeIs
                
            }
            
        }
        // country/zipcode
        if let shippingid = shippingArrayFromPre.objectAtIndex(getIndex).valueForKey("id") as? Int{
            addressIdToPass = "\(shippingid)"
            
        }
        
        
    }
    //MARK:-ChangeAddress
    func addShippingAddress(getIndex:Int){
        print(getIndex)
        selectedItemIndex = getIndex
        ViewTypeIs = "UpdateAllInfo"
        self.delegate!.getShippingInfoOnGridTapDelegate!(shippingArrayFromPre[getIndex] as! NSDictionary)
        
    }
}