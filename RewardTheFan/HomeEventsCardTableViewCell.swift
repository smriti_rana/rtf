//
//  HomeEventsCardTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 05/05/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

@objc protocol HomeEventsCardTableViewCellDelegate
{
    optional func homeEventsCardFavouritesButtonClicked(button: UIButton, index : NSInteger)
    
    optional func homeEventsCardShareButtonClicked(button: UIButton, index : NSInteger)
    
    optional func teamCardFavoriteButtonClikced(button: UIButton, index : NSInteger)
    
    optional func teamCardInfoButtonClikced(button: UIButton, index : NSInteger)
    
    optional func homeEventsCardSeeMoreClicked(button: UIButton, index : NSInteger)
}

class HomeEventsCardTableViewCell: UITableViewCell {

    weak var delegate: HomeEventsCardTableViewCellDelegate?

    @IBOutlet var favouritesButton: UIButton!
    
    @IBOutlet var shareButton: UIButton!
    
    @IBOutlet var colorView: UIView!
    
    @IBOutlet var dateView: UIView!
    
    @IBOutlet var eventNameLabel: UILabel!
    
    @IBOutlet var eventTimeAndAddressLabel: UILabel!
    
    @IBOutlet var urlImageImageView: UIImageView!
    
    @IBOutlet var monthLabel: UILabel!
    
    @IBOutlet var dayLabel: UILabel!
    
    @IBOutlet var yearLabel: UILabel!
    
    @IBOutlet var weekDayLabel: UILabel!
    
    @IBOutlet var priceLabel: UILabel!
    
    @IBOutlet var teamCardFavouriteButton: UIButton!
    
    @IBOutlet var teamCardInfoButton: UIButton!
    
    @IBOutlet var topViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var bottomViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        eventNameLabel.text = "Katty perry"
        eventTimeAndAddressLabel.text = "New York"
    }
    
    override var bounds : CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.makeItCircle()
    }
    
    func makeItCircle() {
        self.dateView.layer.masksToBounds = true
        self.dateView.layer.cornerRadius = self.dateView.frame.height/2;
        self.dateView.clipsToBounds = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func shareButtonTapped(sender: AnyObject) {
        self.delegate?.homeEventsCardShareButtonClicked!(sender as! UIButton, index: sender.tag)
    }
    
    @IBAction func favouritesButtonTapped(sender: AnyObject) {
        self.delegate?.homeEventsCardFavouritesButtonClicked!(sender as! UIButton, index: sender.tag)
    }
    
    @IBAction func teamCardFavoriteButtonTapped(sender: AnyObject) {
        self.delegate?.teamCardFavoriteButtonClikced!(sender as! UIButton, index: sender.tag)
    }
    
    @IBAction func teamCardInfoButtonTapped(sender: AnyObject) {
        self.delegate?.teamCardInfoButtonClikced!(sender as! UIButton, index: sender.tag)
    }
    
    //MARK:-SeeMore Clicked
    @IBOutlet weak var seeMoreBtn: UIButton!
    @IBAction func seeMoreTapped(sender: AnyObject) {
        self.delegate?.homeEventsCardSeeMoreClicked!(sender as! UIButton, index: sender.tag)
    }
    
}
