//
//  ContactFriendListViewController.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 01/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class ContactFriendListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,contactListTableViewCellDelegate {

    @IBOutlet weak var navbar_height: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    var phoneArrayAs:NSMutableArray = []
    var nameArrayAs:NSMutableArray = []
    var contactListArray:NSMutableArray = []
    var contactNameListArray:NSMutableArray = []
    
   
    @IBOutlet weak var emptyLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerNib(UINib(nibName: "ContactListFriendTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
        if phoneArrayAs.count > 0 {
            contactListArray = phoneArrayAs
            
        }
        if nameArrayAs.count == 0{
            tableView.hidden =  true
            emptyLbl.hidden = false
            emptyLbl.text = "No contact found."
        }
        // *** change the font for ipad ***
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            TitleLbl.font = UIFont(name: TitleLbl.font.fontName, size: 32)

        }
        print(contactListArray)
         print(nameArrayAs)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Tableview Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
       
        return nameArrayAs.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! ContactListFriendTableViewCell
      
        // *** change color of cell *** //
        if indexPath.row % 2 != 0 {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
        } else {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        }
        cell.selectionStyle = .None
        cell.delegate = self
        cell.sendInviteBtn.tag = indexPath.row
        cell.nameLbl_height.constant = 55
        cell.mailLbl_height.constant = 0
        
        cell.usernameLbl.text = nameArrayAs.objectAtIndex(indexPath.row) as? String
//        cell.usernameLbl.text = phonedictAs[0].objectAtIndex(indexPath.row) as? String
        // *** change color of cell *** //
        if indexPath.row % 2 != 0 {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
        } else {
            cell.contentView.backgroundColor = UIColor(colorLiteralRed: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        }
        return cell
        
    }
    //MARK:-SendInviteDelegate
     func sendInviteButtonClicked(index : NSInteger,cell:ContactListFriendTableViewCell) {
        if cell.sendInviteBtn.selected {
            
            cell.sendInviteBtn.selected = false
            cell.sendInviteBtn.setImage(UIImage(named: "invite"), forState: .Normal)

        } else {
            
            cell.sendInviteBtn.selected = true
            cell.sendInviteBtn.setImage(UIImage(named: "sendinvite"), forState: .Normal)

        }
        
    }
    @IBAction func backTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
