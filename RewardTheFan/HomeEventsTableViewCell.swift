//
//  HomeEventsTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 27/04/16.
//  Copyright © 2016 Smriti. All rights reserved.
//

import UIKit

@objc protocol HomeEventsTableViewCellDelegate
{
    optional func homeEventsFavouritesButtonClicked(button: UIButton, index : NSInteger)
    
    optional func homeEventsShareButtonClicked(button: UIButton, index : NSInteger)
    optional func homeEventsSeeMoreClicked(button: UIButton, index : NSInteger)
}
class HomeEventsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var TicketBgView: UIView!
    @IBOutlet weak var ticketbgView_height: NSLayoutConstraint!
    @IBOutlet weak var shareBgView_height: NSLayoutConstraint!
    @IBOutlet var cardNameViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var shareBgView: UIView!
    
    @IBOutlet var seeMoreButtonSeperatorConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var trailingBgView: NSLayoutConstraint!
    @IBOutlet weak var leadingBgView: NSLayoutConstraint!
    @IBOutlet weak var borderBgView_height: NSLayoutConstraint!
    @IBOutlet var cardNameView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet var topViewHeightConstraint: NSLayoutConstraint!
    
   
    @IBOutlet var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var seeMoreBgView_height: NSLayoutConstraint!
    @IBOutlet var favouritesButton: UIButton!
    
    @IBOutlet var shareButton: UIButton!
    
    @IBOutlet var cardNameLabel: UILabel!
    
    @IBOutlet var cardNameHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var monthLabel: UILabel!

    @IBOutlet var dayLabel: UILabel!
    
    @IBOutlet var yearLabel: UILabel!
    
    @IBOutlet var weekDayLabel: UILabel!
    
    @IBOutlet var priceLabel: UILabel!
    
    @IBOutlet var dateViewLeading: NSLayoutConstraint!
    
   
    weak var delegate: HomeEventsTableViewCellDelegate?
    
    @IBOutlet var colorView: UIView!

    @IBOutlet var dateView: UIView!
    
    @IBOutlet var eventNameLabel: UILabel!

    @IBOutlet var eventTimeAndAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        eventNameLabel.text = "Katty perry"
        eventTimeAndAddressLabel.text = "New York"
    }
    
    override var bounds : CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.makeItCircle()
    }
    
    func makeItCircle() {
        
        self.dateView.layer.masksToBounds = true
        self.dateView.layer.cornerRadius = self.dateView.frame.height/2;
        self.dateView.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func shareButtonTapped(sender: AnyObject) {
        self.delegate?.homeEventsShareButtonClicked!(sender as! UIButton, index: sender.tag)
    }
    
    @IBAction func favouritesButtonTapped(sender: AnyObject) {
        self.delegate?.homeEventsFavouritesButtonClicked!(sender as! UIButton, index: sender.tag)
    }
    //MARK:-SeeMore Clicked
    @IBOutlet weak var seeMoreBtn: UIButton!
    @IBAction func seeMoreTapped(sender: AnyObject) {
        self.delegate?.homeEventsSeeMoreClicked!(sender as! UIButton, index: sender.tag)
    }
}
