//
//  ContactListFriendTableViewCell.swift
//  RewardTheFan
//
//  Created by Anmol Rajdev on 01/06/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit
@objc protocol contactListTableViewCellDelegate
{
    optional func sendInviteButtonClicked(index : NSInteger,cell:ContactListFriendTableViewCell)
}

class ContactListFriendTableViewCell: UITableViewCell {
 weak var delegate: contactListTableViewCellDelegate?
    @IBOutlet weak var mailLbl_height: NSLayoutConstraint!
    
   
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var nameLbl_height: NSLayoutConstraint!
    @IBOutlet weak var userImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        mailLbl_height.constant = 0
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK:-SendInviteTapped
    @IBOutlet weak var sendInviteBtn: UIButton!
    @IBAction func sendInviteTapped(sender: AnyObject) {
        self.delegate?.sendInviteButtonClicked!(sendInviteBtn.tag,cell: self)
    }
    
}
